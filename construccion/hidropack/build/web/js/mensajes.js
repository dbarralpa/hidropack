
function OffMsg(txtMsg, tipoMsg){   
    var titulo;
    var icono;
    if(tipoMsg==0){
        titulo = "Informaci&oacute;n";
        icono  = "<img src=\"../img/mensajeria/info.png\" border=\"0\">";
    }else if(tipoMsg==1){
        titulo = "Error";
        icono  = "<img src=\"../img/mensajeria/error.png\" border=\"0\">";
    }else if(tipoMsg==2){
        titulo = "Advertencia";
        icono  = "<img src=\"../img/mensajeria/Warning.png\" border=\"0\">";
    }else if(tipoMsg==3){
        titulo = "Ayuda";
        icono  = "<img src=\"../img/mensajeria/help.png\" border=\"0\">";
    }else{
        titulo = "";
        icono  = "";
    }
    
    if(txtMsg==null){
        txtMsg = "Notificaci&oacute;n no especificada.";
    }   
    
    MsgBody  = "<table border=0 cellpadding='0' cellspacing='0' style='color: white' width='100%'>";
    MsgBody += "<tr>";
    MsgBody += "<td width='170' align='left'>" + icono + "</td>";
    MsgBody += "<td width='170' align='center' valign='bottom'><h3>" + titulo + "</h3></td>";    
    MsgBody += "<td width='170'>" + "&nbsp;" + "</td>";
    MsgBody += "</tr>";
    MsgBody += "</table><br>";        
    MsgBody += "<table border=0 cellpadding='0' cellspacing='0' style='color: white' width='100%'>";
    MsgBody += "<tr><td style='padding: 0px; padding-bottom: 0px;'>" + txtMsg + "</td></tr>";
    MsgBody += "</table><br>";        
    
    return divAlert("Planificación de recursos",MsgBody,'Cerrar este mensaje','closeAlert()',null,null);
}
function divAlert(titleStr,msgStr,button1Text,button1Action,button2Text,button2Action){
        var alertStr = "";
        var backgroundclass = "noTitle";        
        if (typeof(titleStr) !== "string") titleStr = "";
        var hasButton1 = typeof(button1Text) == "string" && typeof(button1Action) == "string";
        var hasButton2 = typeof(button2Text) == "string" && typeof(button2Action) == "string";
        
        alertStr += "<table border='0' class='Title'>";
        alertStr += "<tr>";
        alertStr += "<td class='alertTitle'><b>" +titleStr+ "</b></td>";
        alertStr += "<td class='x'><a href='javascript:closeAlert()'><img src='../img/mensajeria/x-notif.jpg' border='0'></a></td>";
        alertStr += "</tr>";        
        alertStr += "</table>";
        
        alertStr += "<table border='0' class='"+backgroundclass+"'>";
        if (titleStr == ""){
            alertStr += "<tr><td></td></tr><tr><td colspan='2' class='alertText'>" +msgStr + "</td></tr>";
        }else{
            alertStr += "<tr><td class='alertSpace'></td></tr><tr><td colspan='2' class='alertText'>" +msgStr+ "</td></tr>";
        } 

        if (hasButton1){
           alertStr += "<tr><td colspan='2' class='alertButton'>";
           if (hasButton1)  
              alertStr += "<input type='button' value='" +button1Text+ "' onClick='" +button1Action+ "' />";
           if (hasButton2) 
              alertStr += "&nbsp;<input type='button' value='" +button2Text+ "' onClick='" +button2Action+ "' />";
           alertStr += "</td></tr>";
        }
        alertStr += "<tr><td class='alertSpace'></td></tr></table>";        


        if (!document.getElementById("divAlertID"))
        {          
           newDiv = document.createElement("div");
           newDiv.id = "divAlertID";
           document.body.appendChild(newDiv);
        }
       
       document.getElementById("divAlertID").innerHTML = alertStr;       
       cWidth = document.body.clientWidth; 
       cHeight = document.body.clientHeight;       
       //document.getElementById("divAlertID").style.top = document.body.scrollTop + 100;
       document.getElementById("divAlertID").style.top  = (cHeight / 2) - 200;
       document.getElementById("divAlertID").style.left = (cWidth / 2)  - 260;
       document.getElementById("divAlertID").style.display = "block";
       
       return Dimmer();
}

function closeAlert(){
    if (document.getElementById("divAlertID"))
        document.getElementById("divAlertID").style.display = "none";
    if (document.getElementById("dimmerDiv"))
        document.getElementById("dimmerDiv").style.display = "none";
        
    var selCount=document.all.tags("select")
    for (i=0; i<selCount.length; i++)
        selCount[i].style.visibility="visible";     
}
function Dimmer(){
    if (!document.getElementById("dimmerDiv"))
    {
        newDiv2 = document.createElement("div");
        newDiv2.id = "dimmerDiv";
        document.body.appendChild(newDiv2);
    }
    if (document.body.clientHeight < document.body.scrollHeight) 
    {
        document.getElementById("dimmerDiv").style.height = document.body.scrollHeight + 'px';
    }
    document.getElementById("dimmerDiv").style.width = document.body.scrollWidth + 'px';
    // if (is_ie5 || is_ie6) {hideSelects()};
    document.getElementById("dimmerDiv").style.display = "block";
    var selCount = document.all.tags("select");
    for (i=0; i<selCount.length; i++)
        selCount[i].style.visibility = "hidden";  
}

