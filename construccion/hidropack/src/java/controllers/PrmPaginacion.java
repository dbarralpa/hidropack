package controllers;

import java.util.ArrayList;
import model.ModEquipo;
import model.ModPaginacion;
import model.ModPedidoMaterial;

public class PrmPaginacion {

    public ModPaginacion mPedido = new ModPaginacion();
    public ModPaginacion mEquipo = new ModPaginacion();
    public ModPaginacion mSIN = new ModPaginacion();
    public int RPP = 10;
    public int TOPE_SET = 15;
    public int LARGODATOS = 3;

    public void indicePedidos(ArrayList aux) throws Exception {
        mPedido = new ModPaginacion();
        mPedido.datos = new String[LARGODATOS][aux.size()];

        for (int i = 0; i < aux.size(); i++) {
            ModPedidoMaterial articulo = (ModPedidoMaterial) aux.get(i);
            mPedido.datos[1][i] = "<br><i>Num pedido: </i>" + articulo.getNumeroDoc();
            mPedido.datos[2][i] = String.valueOf(i);
            articulo = null;
        }
    }

    public void indiceEquipos(ArrayList aux) throws Exception {
        mEquipo = new ModPaginacion();
        mEquipo.datos = new String[LARGODATOS][aux.size()];

        for (int i = 0; i < aux.size(); i++) {
            ModEquipo equipo = (ModEquipo) aux.get(i);
            mEquipo.datos[1][i] = "<br><i>Np: </i>" + equipo.getNp() + "&nbsp;<br><i>Item: </i>" + equipo.getItem();
            mEquipo.datos[2][i] = String.valueOf(i);
            equipo = null;
        }
    }

    public void indiceSinEntregar(ArrayList aux) throws Exception {
        mSIN = new ModPaginacion();
        mSIN.datos = new String[LARGODATOS][aux.size()];

        for (int i = 0; i < aux.size(); i++) {
            ModPedidoMaterial articulo = (ModPedidoMaterial) aux.get(i);
            mSIN.datos[1][i] = "<br><i>Num pedido: </i>" + articulo.getNumeroDoc();
            mSIN.datos[2][i] = String.valueOf(i);
            articulo = null;
        }
    }

    public ModPaginacion getDetalle(String id) throws Exception {
        if (id.equals("HALL")) {
            return mPedido;
        } else if (id.equals("EQUIPO")) {
            return mEquipo;
        } else if (id.equals("SIN")) {
            return mSIN;
        } else {
            return new ModPaginacion();
        }
    }

    public void setDetalle(String id) throws Exception {
        if (id.equals("HALL")) {
            mPedido = new ModPaginacion();
        } else if (id.equals("EQUIPO")) {
            mEquipo = new ModPaginacion();
        } else if (id.equals("SIN")) {
            mSIN = new ModPaginacion();
        }
    }

    public String verPaginador(String id, boolean conResumen, boolean ordenAsc, int RPP, int TOPE_SET) throws Exception {
        return verPaginador(id, conResumen, ordenAsc, false, RPP, TOPE_SET);
    }

    public String verPaginador(String id, boolean conResumen, boolean ordenAsc, boolean conAjax, int RPP, int TOPE_SET) throws Exception {
        String html = "";
        String htm2 = "";
        String tip = "";
        ModPaginacion ver = null;
        ModPaginacion verAux = null;
        int i = 0;
        int tpag = 0;
        int pi = 0;
        int pt = 0;
        int ri = 0;
        int rt = 0;
        verAux = getDetalle(id);
        if (ordenAsc) {
            ver = verAux;
        } else {
            ver = new ModPaginacion(LARGODATOS, verAux.getTotal());
            for (int j = 0; j < verAux.getTotal(); j++) {
                ver.datos[0][j] = verAux.datos[0][verAux.getTotal() - (j + 1)];
                ver.datos[1][j] = verAux.datos[1][verAux.getTotal() - (j + 1)];
                ver.datos[2][j] = verAux.datos[2][verAux.getTotal() - (j + 1)];
            }
            ver.pag = verAux.pag;
        }

        if (ver.getTotal() > 0) {
            tpag = (ver.getTotal() + (RPP - 1)) / RPP;
            if (tpag > TOPE_SET) {
                int y1 = (ver.pag / TOPE_SET);
                y1 = y1 * TOPE_SET;
                if (ver.pag == y1) {
                    pi = ver.pag - (TOPE_SET - 1);
                } else {
                    pi = y1 + 1;
                }

                pt = pi + (TOPE_SET - 1);
                if (pt > tpag) {
                    pt = tpag;
                }
            } else {
                pi = 1;
                pt = tpag;
            }
            ri = (RPP * (ver.pag - 1)) + 1;
            rt = (RPP * ver.pag);
            if (rt > ver.getTotal()) {
                rt = ver.getTotal();
            }

            html = "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
            html = html + "<tr class='Paginador'>\n";
            html = html + "<td width=\"10\">" + "[" + ri + "/" + rt + "]" + "</td>\n";
            html = html + "<td width=\"10\">" + "[" + ver.getTotal() + "]" + "</td>\n";
            if (pi > 1) {
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + ver.codigoDesde_Pag(1, RPP) + "<br>" + "<b>hasta: </b>" + ver.codigoDesde_Pag(pi, RPP));
                } else {
                    tip = "";
                }
                html = html + "<td width=\"17\">";
                if (conAjax) {
                    html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada' " + tip + " onClick=\"nd();cambiarPaginaGrilla('" + (pi - 1) + "');\" >";

                } else {
                    html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada' " + tip + " onClick=\"this.value='" + (pi - 1) + "'\" >";
                }
                html = html + "</td>\n";
                htm2 = htm2 + "<td>" + "</td>\n";
            }
            for (i = pi; i <= pt; i++) {
                tip = "";
                html = html + "<td width=\"17\">";
                if (conAjax) {
                    html = html + "<input name='pag" + id + "' type='submit' value='" + i + "' onClick=\"nd();cambiarPaginaGrilla('" + i + "');\" ";
                } else {
                    html = html + "<input name='pag" + id + "' type='submit' value='" + i + "' ";
                }
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + ver.codigoDesde_Pag(i, RPP) + "<br>" + "<b>hasta: </b>" + ver.codigoHasta_Pag(i, RPP));
                } else {
                    tip = "";
                }
                if (ver.pag == i) {
                    html = html + "class='boxNumeradaActiva' disabled " + tip + ">";
                } else {
                    html = html + "class='boxNumerada' " + tip + ">";
                }
                html = html + "</td>\n";
            }
            if (tpag > pt) {
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + ver.codigoHasta_Pag(pt, RPP) + "<br>" + "<b>hasta: </b>" + ver.codigoHasta_Pag(tpag, RPP));
                } else {
                    tip = "";
                }
                html = html + "<td width=\"17\">";
                if (conAjax) {
                    html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada' " + tip + " onClick=\"nd();cambiarPaginaGrilla('" + (pt + 1) + "');\" >";

                } else {
                    html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada' " + tip + " onClick=\"this.value='" + (pt + 1) + "'\" >";
                }
                html = html + "</td>\n";
                htm2 = htm2 + "<td>" + "</td>\n";
            }
            html = html + "<td width=\"600\" align=\"right\">" + "reg. por p�gina:" + "</td>\n";
            html = html + "<td width=\"50\" align='right'>";
            html = html + RPP;
            html = html + "</td>\n";
            html = html + "</tr>\n";
            html = html + "</table>\n";
        } else {
            html = "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
            html = html + "<tr class='Paginador'>\n";
            html = html + "<td width=\"10\">" + "[" + 0 + "/" + 0 + "]" + "</td>\n";
            html = html + "<td width=\"10\">" + "[" + 0 + "]" + "</td>\n";
            html = html + "<td width=\"17\">";
            html = html + "<input name='pag1' type='submit' value='1' class='boxNumeradaActiva' disabled>";
            html = html + "</td>\n";
            html = html + "<td width=\"600\" align=\"right\">" + "</td>\n";
            html = html + "</tr>\n";
            html = html + "</table>\n";
        }
        return html;
    }
}
