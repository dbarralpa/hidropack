package model.hidropack;

import model.comercial.ModCliente;

public class ModObra {

    private int id;
    private int idObra;
    private int numPresupuesto;
    private String nombreObra;
    private String fecha;
    private String fechaEntregaMaterial;
    private String descripcion;
    private double ventaEstimada;
    private double costoServicio;
    
    private ModCliente cliente;

    public ModObra() {
        nombreObra = "";
        fecha = "";
        fechaEntregaMaterial = "";
        descripcion = "";
        cliente = new ModCliente();
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNumPresupuesto() {
        return numPresupuesto;
    }

    public void setNumPresupuesto(int numPresupuesto) {
        this.numPresupuesto = numPresupuesto;
    }

    public double getCostoServicio() {
        return costoServicio;
    }

    public void setCostoServicio(double costoServicio) {
        this.costoServicio = costoServicio;
    }

    public ModCliente getCliente() {
        return cliente;
    }

    public void setCliente(ModCliente cliente) {
        this.cliente = cliente;
    }

    public double getVentaEstimada() {
        return ventaEstimada;
    }

    public void setVentaEstimada(double ventaEstimada) {
        this.ventaEstimada = ventaEstimada;
    }

    public String getFechaEntregaMaterial() {
        return fechaEntregaMaterial;
    }

    public void setFechaEntregaMaterial(String fechaEntregaMaterial) {
        this.fechaEntregaMaterial = fechaEntregaMaterial;
    }

    public String getNombreObra() {
        return nombreObra;
    }

    public void setNombreObra(String nombreObra) {
        this.nombreObra = nombreObra;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdObra() {
        return idObra;
    }

    public void setIdObra(int idObra) {
        this.idObra = idObra;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
