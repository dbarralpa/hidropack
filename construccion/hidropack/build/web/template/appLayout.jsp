<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion"    scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"        scope="page"    class="controllers.PrmApp" />

<%--@ taglib uri="http://struts.apache.org/tags-bean"     prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles"    prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-html"     prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic"    prefix="logic" --%>
<%
    //response.setDateHeader("Expires", 0);
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0,max-age=0,max-stale = 0'");
    //response.setHeader("Pragma", "no-cache");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <!-- TITULO -->
        <%--title><bean:message key="title.main"/></title--%>
       
        <!-- HOJAS DE ESTILO -->
        <link rel="stylesheet" href="../css/body.css" type="text/css"/>
        <link rel="stylesheet" href="../css/table.css" type="text/css"/>
        <link rel="stylesheet" href="../css/search.css" type="text/css" />  
        <link rel="stylesheet" type="text/css" href="../css/loading.css" />
        <link rel="stylesheet" type="text/css" href="../css/notification.css" />

        <!-- JAVASCRIPT -->
        <script type="text/javascript" src="../js/overlib.js"></script>
        <script type="text/javascript" src="../js/javascript.js"></script>
        <script type="text/javascript" src="../js/jquery-1.6.4.js"></script>
        <script type="text/javascript" src="../js/functions.js"></script>
        <script type="text/javascript" src="../js/ajax_search.js"></script>  
        <script type="text/javascript" src="../js/loading.js"></script> 
        <script type="text/javascript" src="../js/mensajes.js"></script>


    </head>
    <body onload="asignaVariables();">
        <%request.setAttribute("LOAD_ONSUBMIT", "onsubmit=\"javascript:loading();\"");
        request.setAttribute("LOAD_ONCLICK", "onclick=\"javascript:loading();\"");%>
        <div id="overLoading" style="display:none;"><div id="mensajeLoading"><table width="200"></table></div></div>
        <div id="page">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0">        
                            <tr>
                                <td>
                                    <%try {%>
                                    <%--tiles:insert attribute="header"/--%>
                                    <jsp:include page="../jsp/header.jsp" >
                                        <jsp:param name="" value=""/>
                                    </jsp:include>
                                    <%} catch (Exception ex) {
                                        vw.addError(ex, true);%>ERROR SECCION<%}
                                    %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0">                
                            <tr>
                                <td>
                                    <%try {%>
                                    <%--tiles:insert attribute="body"/--%>
                                    <%try {%>
                                    <%if (Sesion.isEnSesion()) {%>
                                    <jsp:include page="<%=Sesion.getUrl()%>" >
                                        <jsp:param name="" value=""/>
                                    </jsp:include>
                                    <%} else {%>
                                    <jsp:include page="../jsp/lnk_login.jsp" >
                                        <jsp:param name="" value=""/>
                                    </jsp:include>
                                    <%}%>
                                    <%} catch (Exception ex) {
                                        vw.addError(ex, true);%>ERROR SECCION<%}
                                    %>
                                    <%} catch (Exception ex) {
                                        vw.addError(ex, true);%>ERROR SECCION<%}
                                    %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>        
            </table>          
        </div>
    </body>
</html>
<%if (Sesion.isConTip()) {%>
<script type="text/javascript">
    OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
</script>
<%Sesion.cleanTip();%>
<%}%>