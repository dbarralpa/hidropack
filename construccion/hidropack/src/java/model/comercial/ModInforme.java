package model.comercial;

public class ModInforme {

    private int numero;
    private int presupuesto;
    private double costoPresupuesto;
    private double costoDevolucion;
    private double costoGuia;
    private double cantidad;
    private int idTrabajo;
    private String nombre;
    private String nombreTrabajo;
    private String fecha;
    private ModCliente cliente;
    private ModMaterial material;

    public ModInforme() {
        nombre = "";
        nombreTrabajo = "";
        fecha = "";
        cliente = new ModCliente();
        material = new ModMaterial();
    }

    public ModMaterial getMaterial() {
        return material;
    }

    public void setMaterial(ModMaterial material) {
        this.material = material;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getCostoDevolucion() {
        return costoDevolucion;
    }

    public void setCostoDevolucion(double costoDevolucion) {
        this.costoDevolucion = costoDevolucion;
    }

    public double getCostoGuia() {
        return costoGuia;
    }

    public void setCostoGuia(double costoGuia) {
        this.costoGuia = costoGuia;
    }

    public double getCostoPresupuesto() {
        return costoPresupuesto;
    }

    public void setCostoPresupuesto(double costoPresupuesto) {
        this.costoPresupuesto = costoPresupuesto;
    }

    public ModCliente getCliente() {
        return cliente;
    }

    public void setCliente(ModCliente cliente) {
        this.cliente = cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(int presupuesto) {
        this.presupuesto = presupuesto;
    }

    public int getIdTrabajo() {
        return idTrabajo;
    }

    public void setIdTrabajo(int idTrabajo) {
        this.idTrabajo = idTrabajo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreTrabajo() {
        return nombreTrabajo;
    }

    public void setNombreTrabajo(String nombreTrabajo) {
        this.nombreTrabajo = nombreTrabajo;
    }
}
