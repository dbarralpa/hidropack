/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Timestamp;

/**
 *
 * @author dbarra
 */
public class HelperGeneral {

    public static boolean diferenciaFecha(Timestamp fecha2, Timestamp fecha1, double minuto) {
        boolean val = false;
        if (fecha1 != null) {
            double valor = fecha2.getTime() - fecha1.getTime();
            double result = valor / 60000;
            if (result >= minuto) {
                val = true;
            }
        }
        return val;
    }   
}
