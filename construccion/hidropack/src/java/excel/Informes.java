package excel;

import controllers.PrmApp;
import controllers.PrmComercial;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModSesion;
import model.comercial.ModInforme;
import util.HelperComercial;

public class Informes extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ModSesion Sesion = null;
        DecimalFormat number = new DecimalFormat("###,###.##");
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/informes.xls");
            int indice = 12;

            if (request.getParameter("idNumeroInforme") != null) {
                ModInforme man = (ModInforme) Sesion.getAuxiliar().get(Integer.parseInt(request.getParameter("idNumeroInforme")));
                excel.setValor(0, 0, 1, man.getNumero(), "DET_TNUMERO", 0);
                excel.setValor(0, 1, 1, man.getNombre(), "DET_TTEXTO", 1);
                excel.setValor(0, 2, 1, man.getFecha(), "DET_TTEXTO", 2);
                excel.setValor(0, 3, 1, man.getCliente().getNombre(), "DET_TTEXTO", 3);
                excel.setValor(0, 4, 1, man.getNombreTrabajo(), "DET_TTEXTO", 4);
                excel.setValor(0, 5, 1, man.getCostoPresupuesto(), "DET_TNUMERICO", 5);
                excel.setValor(0, 6, 1, man.getCostoGuia(), "DET_TNUMERICO", 6);
                excel.setValor(0, 7, 1, man.getCostoDevolucion(), "DET_TNUMERICO", 7);

                ArrayList presupuesto = PrmComercial.informePresupuesto(man.getNumero(), man.getIdTrabajo());
                ArrayList guias = PrmComercial.informeGuia(man.getNumero(), man.getIdTrabajo());
                ArrayList devoluciones = PrmComercial.informeDevolucion(man.getNumero());
                HelperComercial.codigosSinRepetir(Sesion.getMateriales(), presupuesto);
                HelperComercial.codigosSinRepetir(Sesion.getMateriales(), guias);
                HelperComercial.codigosSinRepetir(Sesion.getMateriales(), devoluciones);

                for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                    ModInforme inf = (ModInforme) Sesion.getMateriales().get(i);
                    excel.setValor(0, indice, 0, inf.getMaterial().getCodigo(), "DET_TTEXTO", indice);
                    excel.setValor(0, indice, 1, inf.getMaterial().getDescripcion(), "DET_TTEXTO", indice);
                    excel.setValor(0, indice, 2, inf.getMaterial().getUm(), "DET_TTEXTO", indice);
                    double pres = HelperComercial.cantidad(inf.getMaterial().getCodigo(), presupuesto);
                    if (pres == 0) {
                        excel.setValor(0, indice, 3, "---", "DET_TNUMERO", indice);
                    } else {
                        excel.setValor(0, indice, 3, pres, "DET_TNUMERO", indice);
                    }
                    double gui = HelperComercial.cantidad(inf.getMaterial().getCodigo(), guias);
                    if (gui == 0) {
                        excel.setValor(0, indice, 4, "---", "DET_TNUMERO", indice);
                    } else {
                        excel.setValor(0, indice, 4, gui, "DET_TNUMERO", indice);
                    }
                    double dev = HelperComercial.cantidad(inf.getMaterial().getCodigo(), devoluciones);
                    if (dev == 0) {
                        excel.setValor(0, indice, 5, "---", "DET_TNUMERO", indice);
                    } else {
                        excel.setValor(0, indice, 5, dev, "DET_TNUMERO", indice);
                    }


                    if ((gui - dev) < 0) {
                        excel.setValor(0, indice, 6, (gui - dev), "DET_STNUMERO", indice);
                    } else {
                        excel.setValor(0, indice, 6, (gui - dev), "DET_TNUMERO", indice);
                    }

                    if ((pres - gui) < 0) {
                        excel.setValor(0, indice, 7, (pres - gui), "DET_STNUMERO", indice);
                    } else {
                        excel.setValor(0, indice, 7, (pres - gui), "DET_TNUMERO", indice);
                    }

                    indice++;
                }

            }
            Sesion.setMateriales(new ArrayList());
            excel.borrarHojaEstilo();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"Informe gerencia.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
