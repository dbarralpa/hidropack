<%@page import="controllers.PrmHidropack,util.HelperHidropack"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1" session="true"%>
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"            scope="page"    class="view.VwHidropack" />
<%
    String html = "";
    if (request.getParameter("pag").equals("1")) {
        Sesion.setMateriales(PrmHidropack.ordenesCompra(request.getParameter("codigo")));
        Sesion.setPopUp(HelperHidropack.htmlOrdenesCompra(Sesion));
    }
    if (request.getParameter("pag").equals("2")) {
        Sesion.setMateriales(PrmHidropack.necesidadMaterial(request.getParameter("codigo"))); 
        Sesion.getMaterial().setMateriaPrima(request.getParameter("codigo"));        
        Sesion.setPopUp(HelperHidropack.htmlNecesidadMaterial(Sesion));
    }
    html += vw.popup(request);
    html += "";
    out.println(html);

%>