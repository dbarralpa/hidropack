package model.comercial;


public class ModPlantilla {
   private String codigo; 
   private String descripcion; 
   private String um; 
   private double precioUnitario; 
   private double precioCosto; 
   private float cantidad; 
   private byte est; 
   
   public ModPlantilla(){
       codigo = "";
       descripcion = "";
       um = "";
   }

    public byte getEst() {
        return est;
    }

    public void setEst(byte est) {
        this.est = est;
    }

    public double getPrecioCosto() {
        return precioCosto;
    }

    public void setPrecioCosto(double precioCosto) {
        this.precioCosto = precioCosto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
}
