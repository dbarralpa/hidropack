package excel;

import controllers.PrmApp;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModSesion;
import model.comercial.ModPresupuesto;


public class PresupuestoObra extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModSesion Sesion = null;
        DecimalFormat number = new DecimalFormat("###,###.##");
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/presupuestoObra.xls");
            int indice = 8;
            excel.setValor(0, 0, 1, Sesion.getPresupuesto().getPlantilla(), "DET_TTEXTO", 0);
            excel.setValor(0, 1, 1, Sesion.getPresupuesto().getNumero(), "DET_TNUMERO", 1);
            excel.setValor(0, 2, 1, Sesion.getPresupuesto().getTrabajo().getDescripcion(), "DET_TTEXTO", 2);
            excel.setValor(0, 3, 1, Sesion.getPresupuesto().getCliente().getNombre(), "DET_TTEXTO", 3);
            excel.setValor(0, 4, 1, Sesion.getPresupuesto().getFecha(), "DET_TTEXTO", 4);
            ArrayList presupuestoObra = Sesion.getMateriales();
            double unitario = 0d;
            double total = 0d;
            for (int i = 0; i < presupuestoObra.size(); i++) {
                ModPresupuesto pre = (ModPresupuesto) presupuestoObra.get(i);
                excel.setValor(0, indice, 0, pre.getCodigo(), "DET_TTEXTO", indice);
                excel.setValor(0, indice, 1, pre.getDescripcion(), "DET_TTEXTO", indice);
                excel.setValor(0, indice, 2, pre.getUm(), "DET_TNUMERO", indice);
                excel.setValor(0, indice, 3, pre.getCantidad(), "DET_TNUMERO", indice);
                excel.setValor(0, indice, 4, pre.getPrecioUnitario(), "DET_TTEXTO", indice);
                excel.setValor(0, indice, 5, pre.getPrecioTotal(), "DET_TTEXTO", indice);
                unitario += pre.getPrecioUnitario();
                total += pre.getPrecioTotal();
                indice++;
            }
            excel.setValor(0, indice, 3, "Total", "DET_TTEXTO", indice);
            excel.setValor(0, indice, 4, number.format(unitario), "DET_TNUMERICO", indice);
            excel.setValor(0, indice, 5, number.format(total), "DET_TNUMERICO", indice);
            excel.borrarHojaEstilo();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"presupuestoObra.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
