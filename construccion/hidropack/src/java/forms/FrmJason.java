package forms;

import controllers.PrmApp;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModMaterial;
import model.ModSesion;
import org.json.JSONArray;
import org.json.JSONObject;
import util.HelperHidropack;

public class FrmJason extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ModSesion Sesion = null;
        try {
            if (req.getSession().getAttribute("Sesion") != null) {
                Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
                JSONObject result = new JSONObject();
                JSONArray array = new JSONArray();
                //int amount;
                int start = 0;
                int echo = 0;
                int col = 0;

                String dir = "asc";
                String sStart = req.getParameter("iDisplayStart");
                String sAmount = req.getParameter("iDisplayLength");
                String sEcho = req.getParameter("sEcho");
                String sCol = req.getParameter("iSortCol_0");
                String sdir = req.getParameter("sSortDir_0");

                String buscar = req.getParameter("sSearch");

                if (sStart != null) {
                    start = Integer.parseInt(sStart);
                    if (start < 0) {
                        start = 0;
                    }
                }
                /*
                 Sesion.setiDisplayLenght(Integer.parseInt(sAmount));     
                 int amount = Integer.parseInt(sAmount);
                 if (sAmount != null) {                  
                                   
                 if (amount == -1) {
                 amount = Sesion.getAuxiliar().size();
                 Sesion.setiDisplayLenght(Sesion.getAuxiliar().size());
                 }
                 }
                 */
                int amount = Sesion.getAuxiliar().size();
                if (sEcho != null) {
                    echo = Integer.parseInt(sEcho);
                }
                if (sCol != null) {
                    col = Integer.parseInt(sCol);
                    if (col < 0 || col > 13) {
                        col = 0;
                    }
                }
                if (sdir != null) {
                    if (!sdir.equals("asc")) {
                        dir = "desc";
                    }
                }
                int total = Sesion.getAuxiliar().size();
                int totalAfterFilter = total;
                result.put("sEcho", echo);
                ArrayList coincidencias = new ArrayList();

                HelperHidropack.ordenarArreglo(Sesion.getAuxiliar(), col, dir);
                if (!buscar.equals("")) {
                    totalAfterFilter = 0;
                    for (int i = 0; i < total; i++) {
                        ModMaterial mat = (ModMaterial) Sesion.getAuxiliar().get(i);
                        if (mat.getMateriaPrima() != null && !buscar.equals("")) {
                            if (HelperHidropack.buscaCoincidencia(mat, buscar)) {
                                totalAfterFilter++;
                                mat.getMateriaPrima();
                                mat.getDescripcion();
                                mat.getUm();
                                mat.getCantRequerida();
                                mat.getCantidadR();
                                mat.getStockMin();
                                mat.getStockMax();
                                mat.getSaldo();
                                mat.getLeadTime();
                                mat.getOc();
                                coincidencias.add(mat);
                            }
                        }
                    }
                    coincidencias = HelperHidropack.ordenarArreglo(coincidencias, col, dir);
                    for (int a = start; a < start + amount; a++) {
                        if (a < coincidencias.size()) {
                            ModMaterial mat = (ModMaterial) coincidencias.get(a);
                            JSONArray ja = new JSONArray();
                            ja.put(mat.getMateriaPrima());
                            ja.put(mat.getDescripcion());
                            ja.put(mat.getUm());
                            ja.put("<a href='../FrmHidropack?necesidadCompra=" + a + "' onclick='asignarCodigoNecMat(" + mat.getMateriaPrima() + ")' data-reveal-id=\"myModal\" style='text-decoration:none;'>" + mat.getCantRequerida() + "</a>");
                            ja.put(String.format("%.2f", mat.getCantidadR()));
                            ja.put(mat.getStockMin());
                            ja.put(mat.getStockMax());
                            ja.put(mat.getSaldo());
                            ja.put(mat.getLeadTime());
                            ja.put("<a href='../FrmHidropack?ordenCompra=" + a + "' onclick='asignarCodigoOrden(" + mat.getMateriaPrima() + ")' data-reveal-id=\"myModal\" style='text-decoration:none;'>" + mat.getOc() + "</a>");
                            array.put(ja);
                        }
                    }
                } else {
                    for (int a = start; a < start + amount; a++) {
                        if (a < Sesion.getAuxiliar().size()) {
                            ModMaterial act = (ModMaterial) Sesion.getAuxiliar().get(a);
                            JSONArray ja = new JSONArray();
                            ja.put(act.getMateriaPrima());
                            ja.put(act.getDescripcion());
                            ja.put(act.getUm());
                            ja.put("<a href='../FrmHidropack?necesidadCompra=" + a + "' onclick='asignarCodigoNecMat(" + act.getMateriaPrima() + ")' data-reveal-id=\"myModal\" style='text-decoration:none;'>" + act.getCantRequerida() + "</a>");
                            ja.put(String.format("%.2f", act.getCantidadR()));
                            ja.put(act.getStockMin());
                            ja.put(act.getStockMax());
                            ja.put(act.getSaldo());
                            ja.put(act.getLeadTime());
                            ja.put("<a href='../FrmHidropack?ordenCompra=" + a + "' onclick='asignarCodigoOrden(" + act.getMateriaPrima() + ")' data-reveal-id=\"myModal\" style='text-decoration:none;'>" + act.getOc() + "</a>");
                            array.put(ja);
                        }
                    }
                }

                result.put(
                        "iTotalRecords", total);
                result.put(
                        "iTotalDisplayRecords", totalAfterFilter);
                result.put(
                        "aaData", array);
                response.setContentType(
                        "application/json");
                response.setHeader(
                        "Cache-Control", "no-store");
                out.print(result);
            }
        } catch (Exception e) {
            PrmApp.addError(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
