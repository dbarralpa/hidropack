package model.comercial;

public class ModPrueba1 {

    private String comprador;
    private int ocVencidaNecesidad;
    private int ocFaltanteNecesidad;
    private int ocVencidaStockM;
    private int ocFaltanteStockM;
    private int sobrepasado;

    public ModPrueba1() {
        comprador = "";
    }

    public int getSobrepasado() {
        return sobrepasado;
    }

    public void setSobrepasado(int sobrepasado) {
        this.sobrepasado = sobrepasado;
    }

    public int getOcVencidaStockM() {
        return ocVencidaStockM;
    }

    public void setOcVencidaStockM(int ocVencidaStockM) {
        this.ocVencidaStockM = ocVencidaStockM;
    }

    public int getOcFaltanteStockM() {
        return ocFaltanteStockM;
    }

    public void setOcFaltanteStockM(int ocFaltanteStockM) {
        this.ocFaltanteStockM = ocFaltanteStockM;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public int getOcVencidaNecesidad() {
        return ocVencidaNecesidad;
    }

    public void setOcVencidaNecesidad(int ocVencidaNecesidad) {
        this.ocVencidaNecesidad = ocVencidaNecesidad;
    }

    public int getOcFaltanteNecesidad() {
        return ocFaltanteNecesidad;
    }

    public void setOcFaltanteNecesidad(int ocFaltanteNecesidad) {
        this.ocFaltanteNecesidad = ocFaltanteNecesidad;
    }
}
