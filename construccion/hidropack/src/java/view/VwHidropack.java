package view;

import controllers.PrmApp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import model.ModEquipo;
import model.ModMaterial;
import model.ModPaginacion;
import model.ModPedidoMaterial;
import model.ModSesion;
import model.comercial.ModTrabajo;
import util.HelperHidropack;

public class VwHidropack extends PrmApp {

    NumberFormat number = new DecimalFormat("#########.####");

    public String grillaSolicitudMaterialesObra(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td>";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>C&oacute;digo</td>";
            html += "<td class='Estilo1' width='300'>Descripci&oacute;n</td>";
            html += "<td class='Estilo1' width='30'>Um</td>";
            html += "<td class='Estilo1' width='60'>Requerido</td>";
            html += "<td class='Estilo1' width='30'>Eli</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                if (equipo.getMaterial().getCantidad() > 0) {
                    html += "<tr>";
                } else {
                    html += "<tr>";
                }
                html += "<td class='Estilo1' width='100'>" + equipo.getMaterial().getMateriaPrima() + "</td>";
                html += "<td class='Estilo1' width='300'>" + equipo.getMaterial().getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='30'>" + equipo.getMaterial().getUm() + "</td>";
                if (equipo.getMaterial().getTipoProd() == 3) {
                    html += "<td width='60px'><input size='3' type='text' value='" + equipo.getMaterial().getCantRequerida() + "' name='cantRequerida" + i + "' id='cantRequerida" + i + "' /></td>";
                    html += "<td class='Estilo1' width='30px'><input type='checkbox' onclick='confirmar(this)' name='eliPedidoHidropack" + i + "' id='eliPedidoHidropack" + i + "'></td>";
                } else {
                    html += "<td class='Estilo1' width='60px'>" + equipo.getMaterial().getCantRequerida() + "</td>";
                    html += "<td class='Estilo1' width='30px'><input type='checkbox' disabled onclick='confirmar(this)' name='eliPedidoHidropack" + i + "' id='eliPedidoHidropack" + i + "'></td>";
                }
                html += "</tr>";
            }
            for (int a = 0; a < (10 - Sesion.getPedidosEspeciales().size()); a++) {
                html += "<tr>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "<td>";
            html += "<table cellpadding='0' cellspacing='0'>";
            html += "<tr>";
            html += "<td><input type='submit' class='Estilo1' name='generarPedidoMaterial' value='Requerir'></td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";

            html += "</tr>";
            // }
            html += "<tr><td>&nbsp;</td></tr>";

            html += "<tr>";
            html += "<td>";
            html += "<table cellpadding='0' cellspacing='0'>";
            html += "<tr>";
            html += "<td>";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#EDEFE4'>";
            html += "<td class='Estilo1' width='100px'>C&oacute;digo</td>";
            html += "<td class='Estilo1' width='300px'>Descripci&oacute;n</td>";
            html += "<td class='Estilo1' width='30px'>Um</td>";
            html += "<td class='Estilo1' width='60px'>Stock</td>";
            html += "</tr>";
            if (!Sesion.getEquipo().getMaterial().getMateriaPrima().equals("")) {
                html += "<tr>";
                html += "<td class='Estilo1' width='100px'>" + Sesion.getEquipo().getMaterial().getMateriaPrima() + "</td>";
                html += "<td class='Estilo1' width='300px'>" + Sesion.getEquipo().getMaterial().getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='30px'>" + Sesion.getEquipo().getMaterial().getUm() + "</td>";
                html += "<td class='Estilo1' width='60px'>" + number.format(HelperHidropack.stock(Sesion.getSaldoMateriales(), Sesion.getEquipo().getMaterial())) + "</td>";
                html += "</tr>";
            } else {
                html += "<tr>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "<td>";
            html += "&nbsp;<input type='submit' class='Estilo1' name='agregarApedidoMaterial1' value='Agregar'>";
            html += "&nbsp;<input type='submit' class='Estilo1' name='limpiarMaterialAAgregar' value='Limpiar'>";
            html += "</td>";
            html += "</tr>";


            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaSolicitudCompraMaterialesObra(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td>";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>C&oacute;digo</td>";
            html += "<td class='Estilo1' width='300'>Descripci&oacute;n</td>";
            html += "<td class='Estilo1' width='30'>Um</td>";
            html += "<td class='Estilo1' width='60'>Requerido</td>";
            html += "<td class='Estilo1' width='30'>Eli</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                if (equipo.getMaterial().getCantidad() > 0) {
                    html += "<tr>";
                } else {
                    html += "<tr>";
                }
                html += "<td class='Estilo1' width='100'>" + equipo.getMaterial().getMateriaPrima() + "</td>";
                html += "<td class='Estilo1' width='300'>" + equipo.getMaterial().getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='30'>" + equipo.getMaterial().getUm() + "</td>";
                html += "<td width='60px'><input size='3' type='text' value='" + equipo.getMaterial().getCantRequerida() + "' name='cantRequerida" + i + "' id='cantRequerida" + i + "' /></td>";
                html += "<td class='Estilo1' width='30px'><input type='checkbox' onclick='confirmar(this)' name='eliPedidoHidropack" + i + "' id='eliPedidoHidropack" + i + "'></td>";
                html += "</tr>";
            }
            for (int a = 0; a < (10 - Sesion.getPedidosEspeciales().size()); a++) {
                html += "<tr>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "<td>";
            html += "<table cellpadding='0' cellspacing='0'>";
            html += "<tr>";
            html += "<td><input type='submit' class='Estilo1' name='generarSolicitudCompraMaterial' onclick='return validarFechaEntrega()' value='Requerir'></td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td><input type='submit' class='Estilo1' name='limpiarPedidoMaterial' value='Eliminar'></td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td><input type='file' class='Estilo1' name='subir' id='subir' /> </td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td><input type='submit' class='Estilo1' name='cargarArchivo' id='cargarArchivo' value='Cargar archivo desde Excel'></td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";
            // }
            html += "<tr><td>&nbsp;</td></tr>";

            html += "<tr>";
            html += "<td colspan='2'>";
            html += "<table cellpadding='0' cellspacing='0'>";
            html += "<tr>";
            html += "<td>";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#EDEFE4'>";
            html += "<td class='Estilo1' width='100px'>C&oacute;digo</td>";
            html += "<td class='Estilo1' width='300px'>Descripci&oacute;n</td>";
            html += "<td class='Estilo1' width='30px'>Um</td>";
            html += "<td class='Estilo1' width='60px'>Stock</td>";
            html += "</tr>";
            if (!Sesion.getEquipo().getMaterial().getMateriaPrima().equals("")) {
                html += "<tr>";
                html += "<td class='Estilo1' width='100px'>" + Sesion.getEquipo().getMaterial().getMateriaPrima() + "</td>";
                html += "<td class='Estilo1' width='300px'>" + Sesion.getEquipo().getMaterial().getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='30px'>" + Sesion.getEquipo().getMaterial().getUm() + "</td>";
                html += "<td class='Estilo1' width='60px'>" + number.format(HelperHidropack.stock(Sesion.getSaldoMateriales(), Sesion.getEquipo().getMaterial())) + "</td>";
                html += "</tr>";
            } else {
                html += "<tr>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "<td class='Estilo1'>&nbsp</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "<td>";
            html += "&nbsp;<input type='submit' class='Estilo1' name='agregarApedidoMaterial' value='Agregar'>";
            html += "&nbsp;<input type='submit' class='Estilo1' name='limpiarMaterialAAgregar' value='Limpiar'>";
            html += "</td>";
            html += "</tr>";


            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaPedidosABodega(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr><td>&nbsp</td></tr>" + "\n";
            ModPaginacion ver = Sesion.getPset().getDetalle("SIN");
            int ri = 0;
            int rt = 0;

            if (ver.getTotal() == 0) {
                html += "<tr class=\"Estilo1\">\n";
                html += "<td colspan=\"13\" align =\"center\" width=\"900px\"><center>NO HAY DATOS</center></td>\n";
                html += "</tr>\n";
            } else {
                ri = Sesion.getPset().RPP * (ver.pag - 1);
                rt = Sesion.getPset().RPP * ver.pag;
                if (rt > ver.getTotal()) {
                    rt = ver.getTotal();
                }
                for (int y = ri; y < rt; y++) {
                    ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(y);
                    html += "<tr>\n";
                    html += "<td>\n";
                    html += "<table cellpadding='2' cellspacing='2' class='Estilo1'>" + "\n";
                    html += "<tr>\n";
                    html += "<td width='100' class=\"ConBorder1\">Requerimiento </td><td class=\"ConBorder1\">" + pedidoMaterial.getNumeroDoc() + "</td></td><td>&nbsp</td>\n";
                    html += "<td class=\"ConBorder1\">Estado solicitud&nbsp; </td>" + HelperHidropack.estadoPedidoMateriales(pedidoMaterial.getEstado()) + "<td>&nbsp<a href='../FrmHidropack?imprimirUsuario=" + y + "' target='_blank'><font size='+1' color='#FF0000'>Imprimir</font></a></td>\n";
                    html += "</tr>\n";
                    html += "<tr>\n";
                    html += "<td class=\"ConBorder\">Fecha emisi�n</td><td class=\"ConBorder\">" + pedidoMaterial.getFechaCreacion() + "</td><td>&nbsp</td>\n";
                    html += "<td class=\"ConBorder\">Usuario</td><td class=\"ConBorder\">" + pedidoMaterial.getUsuario() + "</td><td>&nbsp</td>\n";
                    html += "</tr>\n";
                    html += "</table>\n";
                    html += "</td>\n";
                    html += "</tr>\n";
                    html += "<tr>\n";
                    html += "<td style='padding-top:10px'>\n";
                    html += "<table cellpadding='2' cellspacing='2' border='1' class='Estilo1'>" + "\n";
                    html += "<tr>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Item</td>\n";
                    html += "<td width=\"300px\" bgcolor='#FCE9DA'>Descripci�n</td>\n";
                    html += "<td width=\"100px\" bgcolor='#FCE9DA'>C�digo</td>\n";
                    html += "<td width=\"30px\" bgcolor='#FCE9DA'>Um</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Obra</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Solicitado</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Traspasado</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>N� Gu�a</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Cant Gu�a</td>\n";
                    //html += "<td width=\"60px\" bgcolor='#FCE9DA'>Fecha gu�a</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>N� OC</td>\n";
                    //html += "<td width=\"60px\" bgcolor='#FCE9DA'>Entrega OC</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Cant OC</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Stock Bod 1</td>\n";
                    html += "<td width=\"30px\" bgcolor='#FCE9DA'>Eli</td>\n";
                    html += "</tr>\n";
                    html += "</table>\n";
                    html += "</td>\n";
                    html += "</tr>\n";

                    for (int i = 0; i < Sesion.getDetallePedidosProcesarBodega().size(); i++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(i);
                        if (pedidoMaterial.getNumeroDoc() == equipo.getMaterial().getNumero()) {
                            html += "<tr>\n";
                            html += "<td>\n";
                            html += "<table cellpadding='2' cellspacing='2' border='1'>" + "\n";
                            if (equipo.getMaterial().getEstado().equals("0")) {
                                html += "<tr>\n";
                            } else if (equipo.getMaterial().getEstado().equals("1")) {
                                html += "<tr bgcolor='#FF0000'>";
                            } else if (equipo.getMaterial().getEstado().equals("2")) {
                                html += "<tr bgcolor='#00FF00'>";
                            }
                            html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getLinea() + "</td>";
                            html += "<td class='Estilo1' width=\"300px\" " + verOverLib("Producci�n", "<b> C�digo: </b><br/>" + equipo.getMaterial().getDescripcion()) + ">" + verHtmlNull(equipo.getMaterial().getDescripcion(), 40) + "</td>";
                            if (equipo.getMaterial().getEstado().equals("1")) {
                                html += "<td class='Estilo1' width=\"100px\"><a href='../FrmHidropack?changeCodigo=" + equipo.getId() + "'>" + equipo.getMaterial().getMateriaPrima() + "</a></td>";
                            } else {
                                html += "<td class='Estilo1' width=\"100px\">" + equipo.getMaterial().getMateriaPrima() + "</td>";
                            }
                            html += "<td class='Estilo1' width=\"30px\">" + equipo.getMaterial().getUm() + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + equipo.getNp() + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + number.format(equipo.getMaterial().getCantRequerida()) + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + number.format(equipo.getMaterial().getCantidadAsignada()) + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getNumGuia() + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + number.format(equipo.getMaterial().getCantGuia()) + "</td>";
                            //html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getFechaGuia().trim() + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getNumOc() + "</td>";
                            //html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getFechaOc().trim() + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getCantOc() + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getStock() + "</td>";
                            if (equipo.getMaterial().getCantidadAsignada() == 0 && equipo.getMaterial().getNumGuia() == 0) {
                                html += "<td class='Estilo1' width=\"30px\"><a href='../FrmHidropack?eliminarMaterialRequerimiento=" + equipo.getId() + "'>Eli</a></td>";
                            } else {
                                html += "<td class='Estilo1' width=\"30px\">&nbsp;</td>";
                            }
                            html += "</tr>\n";
                            html += "</table>\n";
                            html += "</td>\n";
                            html += "</tr>\n";
                        }
                    }
                    html += "<tr><td>&nbsp</td></tr>" + "\n";
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaBodegaProcesar(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr><td>&nbsp</td></tr>" + "\n";
            ModPaginacion ver = Sesion.getPset().getDetalle("SIN");
            int ri = 0;
            int rt = 0;

            if (ver.getTotal() == 0) {
                html += "<tr class=\"Estilo1\">\n";
                html += "<td colspan=\"13\" align =\"center\" width=\"900px\"><center>NO HAY DATOS</center></td>\n";
                html += "</tr>\n";
            } else {
                ri = Sesion.getPset().RPP * (ver.pag - 1);
                rt = Sesion.getPset().RPP * ver.pag;
                if (rt > ver.getTotal()) {
                    rt = ver.getTotal();
                }
                for (int y = ri; y < rt; y++) {
                    ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(y);
                    html += "<tr>\n";
                    html += "<td colspan='2'>\n";
                    html += "<table cellpadding='2' cellspacing='2' class='Estilo1'>" + "\n";
                    html += "<tr>\n";
                    html += "<td width='100' class=\"ConBorder1\">Requerimiento </td><td class=\"ConBorder1\">" + pedidoMaterial.getNumeroDoc() + "</td><td>&nbsp</td>\n";
                    html += "<td colspan='2'>&nbsp<a href='../FrmHidropack?descartar=" + y + "' class='Estilo1' style='text-decoration:underline;' onclick='return confirmacion();javascript:loading();'><font size='+1' color='#FF0000'>Descartar</font></a></td><td>&nbsp<a href='../FrmHidropack?imprimirBodega=" + y + "' target='_blank'><font size='+1' color='#FF0000'>Imprimir</font></a></td>\n";
                    html += "<td style='padding-left: 20px' rowspan='2'>";
                    html += "<table cellpadding='0' cellspacing='0'>";
                    html += "<tr>";
                    html += "<td class='Estilo10'>N� obra: &nbsp;" + Sesion.getObra().getIdObra() + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class='Estilo10'>Nombre obra: &nbsp;" + Sesion.getObra().getNombreObra() + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class='Estilo10'>Rut Cliente: &nbsp;" + Sesion.getObra().getCliente().getRut() + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class='Estilo10'>Nombre Cliente: &nbsp;" + Sesion.getObra().getCliente().getNombre() + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    html += "</tr>\n";
                    html += "<tr>\n";
                    html += "<td class=\"ConBorder\">Fecha emisi�n</td><td class=\"ConBorder\">" + pedidoMaterial.getFechaCreacion() + "</td><td>&nbsp</td>\n";
                    html += "<td class=\"ConBorder\">Usuario</td><td class=\"ConBorder\">" + pedidoMaterial.getUsuario() + "</td><td>&nbsp</td>\n";
                    html += "</tr>\n";
                    html += "</table>\n";
                    html += "</td>\n";
                    html += "</tr>\n";


                    html += "<tr>\n";
                    html += "<td style='padding-top:10px' colspan='2'>\n";
                    html += "<table cellpadding='2' cellspacing='2' border='1' class='Estilo1'>" + "\n";
                    html += "<tr>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Item</td>\n";
                    html += "<td width=\"300px\" bgcolor='#FCE9DA'>Descripci�n</td>\n";
                    html += "<td width=\"100px\" bgcolor='#FCE9DA'>C�digo</td>\n";
                    html += "<td width=\"30px\" bgcolor='#FCE9DA'>Um</td>\n";
                    html += "<td width=\"80px\" bgcolor='#FCE9DA'>Entrega</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Bodega HP</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Bodega 11</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Requerido</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Por Entregar</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>A Entregar</td>\n";
                    html += "<td width=\"60px\" bgcolor='#FCE9DA'>Cambiar</td>\n";
                    html += "</tr>\n";
                    html += "</table>\n";
                    html += "</td>\n";
                    html += "</tr>\n";

                    for (int i = 0; i < Sesion.getDetallePedidosProcesarBodega().size(); i++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(i);
                        if (pedidoMaterial.getNumeroDoc() == equipo.getMaterial().getNumero()) {
                            html += "<tr>\n";
                            html += "<td colspan='2'>\n";
                            html += "<table cellpadding='2' cellspacing='2' border='1'>" + "\n";
                            if (equipo.getMaterial().getEstado().equals("0")) {
                                html += "<tr>\n";
                            } else if (equipo.getMaterial().getEstado().equals("1")) {
                                html += "<tr bgcolor='#FF0000'>";
                            } else if (equipo.getMaterial().getEstado().equals("2")) {
                                html += "<tr bgcolor='#00FF00'>";
                            }
                            html += "<td class='Estilo1' width=\"60px\">" + equipo.getMaterial().getLinea1() + "</td>";
                            html += "<td class='Estilo1' width=\"300px\" " + verOverLib("Producci�n", "<b> C�digo: </b><br/>" + equipo.getMaterial().getDescripcion()) + ">" + verHtmlNull(equipo.getMaterial().getDescripcion(), 40) + "</td>";
                            html += "<td class='Estilo1' width=\"100px\">" + equipo.getMaterial().getMateriaPrima() + "</td>";
                            html += "<td class='Estilo1' width=\"30px\">" + equipo.getMaterial().getUm() + "</td>";
                            html += "<td class='Estilo1' width=\"80px\">" + equipo.getMaterial().getFecha() + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + HelperHidropack.stock(Sesion.getSaldoMateriales(), equipo.getMaterial()) + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + HelperHidropack.stock(Sesion.getSaldoMaterialesSchaffner(), equipo.getMaterial()) + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + number.format(equipo.getMaterial().getCantRequerida()) + "</td>";
                            html += "<td class='Estilo1' width=\"60px\">" + number.format(equipo.getMaterial().getCantidad()) + "</td>";
                            html += "<td width=\"60px\"><input size='3' type='text' value='" + equipo.getMaterial().getEntregado() + "' name='traspasoBodega" + i + "' id='traspasoBodega" + i + "' /></td>\n";
                            html += "<td class='Estilo1' width=\"60px\"><input type='checkbox' name='cambiarCodigo" + i + "' id='cambiarCodigo" + i + "'></td>";
                            html += "</tr>\n";
                            html += "</table>\n";
                            html += "</td>\n";
                            html += "</tr>\n";
                        }
                    }
                    html += "<tr>" + "\n";
                    html += "<td>" + "\n";
                    html += "<table cellpadding='0' cellspacing='0'>" + "\n";
                    html += "<tr>" + "\n";

                    if (pedidoMaterial.getEstado() < 5) {
                        html += HelperHidropack.estadoPedidoMateriales(pedidoMaterial.getEstado(), y);
                    }
                    html += "</tr>" + "\n";
                    html += "</table>" + "\n";
                    html += "</td>" + "\n";
                    html += "<td align='right'>" + "\n";
                    html += "<table cellpadding='0' cellspacing='0'>" + "\n";
                    html += "<tr>" + "\n";
                    html += "<td><input class='Estilo1' type='submit' name='enviarCambiarCodigo' id='enviarCambiarCodigo' value='Cambiar C�digo'></td>" + "\n";
                    html += "</tr>" + "\n";
                    html += "</table>" + "\n";
                    html += "</td>" + "\n";

                    html += "</tr>" + "\n";

                    html += "<tr><td colspan='2'>&nbsp</td></tr>" + "\n";
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaThead(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            for (int i = 0; i < Sesion.getThead().size(); i++) {
                ModMaterial mat = (ModMaterial) Sesion.getThead().get(i);
                html += "<th " + verOverLib("Hidropack", "<b>" + mat.getDescripcionLarga() + "</b>") + ">";
                html += mat.getDescripcion();
                html += "</th>";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='" + Sesion.getThead().size() + "' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaTbody(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            for (int i = 0; i < Sesion.getThead().size(); i++) {
                ModMaterial mat = (ModMaterial) Sesion.getThead().get(i);
                html += "<th>";
                html += "&nbsp;";
                html += "</th>";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='" + Sesion.getThead().size() + "' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String popup(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = Sesion.getPopUp();
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='" + Sesion.getMateriales().size() + "' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaObraPendiente(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html += "<td style=\"padding-top: 20px\">";
            html += "<table border='1' cellpadding='3' cellspacing='3' width='400'>";
            html += "<tr>";
            html += "<td colspan='2' class='Estilo1' align='center'>Obras pendientes</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td class='Estilo1' align='center'>N� obra</td>";
            html += "<td class='Estilo1' align='center'>Entrega Materiales</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getDatos().size(); i++) {
                ModEquipo equi = (ModEquipo) Sesion.getDatos().get(i);
                html += "<tr>";
                html += "<td align='center'><a href='../FrmHidropack?buscarObraBodega=yes&numeroObra=" + equi.getNp() + "' class='Estilo1' style='text-decoration:underline;'>" + equi.getNp() + "</a></td>";
                html += "<td class='Estilo1' align='center'>" + equi.getMaterial().getFecha() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='" + Sesion.getThead().size() + "' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String comboTrabajo(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbTrabajo\" id=\"cmbTrabajo\">";
            html += "<option value='-1'>Elija un trabajo</option>";
            html += "<option value='0'>Sin trabajo</option>";
            for (int i = 0; i < Sesion.getDatos().size(); i++) {
                ModTrabajo tra = (ModTrabajo) Sesion.getDatos().get(i);
                html += "<option value=\"" + tra.getCodigo() + "\">";
                html += tra.getDescripcion() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }
}
