<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw" scope="page" class="view.VwHidropack" />
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />


        <title>Mrp</title>
        <style type="text/css" title="currentStyle" >
            @import "../css/DataTables/demo_page.css";
            @import "../css/DataTables/demo_table.css";
            @import "../css/DataTables/demo_table_jui.css";
            @import "../css/DataTables/jquery-ui-1.8.4.custom.css";
            @import "../css/DataTables/tableTools.css";
        </style>
        <script type="text/javascript" language="javascript" src="../js/dataTable/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="../js/dataTable/ZeroClipboard.js"></script>
        <script type="text/javascript" language="javascript" src="../js/dataTable/dataTables.tableTools.js"></script>
        <script type="text/javascript" >
            $(document).ready(function() {

            var oTable = $('#example').dataTable({
            "sPaginationType": "full_numbers",                                      
                    
                    
                    "sScrollY": "100%",
                    "sScrollYInner": "150%",                   
                    "bPaginate": true,
                    "bScrollCollapse": true,
                    "iDisplayLength":-1,
                    "oLanguage": {
            "sLengthMenu": 'Mostrando <select>' +                    
                    '<option value="-1">TODOS</option>' +
                    '</select> registros',
                    "sZeroRecords": "Sin registros",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar"

            },"sDom": '<Tlfr>t<"F"ip>',
                    "oTableTools": {
                        "sSwfPath": "../js/dataTable/copy_csv_xls_pdf.swf",
                        "aButtons": [
                            "xls",
                            {
                                "sExtends": "pdf",
                                "sPdfOrientation": "landscape",
                                "sPdfMessage": "Your custom message would go here."
                            }
                        ]
                    },
                    "aoColumns": [
            <%for (int i = 0; i < Sesion.getThead().size(); i++) {%>
            <%if (i < (Sesion.getThead().size() - 1)) {%>
            {"sClass": "Estilo4", "aTargets": [<%=i%>]},
            <%} else {%>
            {"sClass": "Estilo4", "aTargets": [<%=i%>]}
            <%}
                   }%>


            ],
                    "bAutoWidth": true,
                    "bLengthChange": true,
                    "bAutoWidth": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "../FrmJason"
            });
                    var elemento = $('.container');
                    var posicion = elemento.position();
                    var inicio = posicion.top;
                    var tamanoVentana = 700;
                    var redimensionar = tamanoVentana - inicio;
                    oTable.fnSettings().oScroll.sY = redimensionar - 200;
                    $('.container').height(redimensionar - 200);
            });
        </script>

    </head>
    <body>        
        <div id="dt_example">
            <div id="container" class="container">
                <div id="demo">                
                    <table cellpadding="0" cellspacing="0" class="display" id="example">
                        <thead>
                            <tr>
                                <%=vw.grillaThead(request)%>
                            </tr>
                        </thead>
                        <tbody>
                        <td colspan="<%=Sesion.getThead().size()%>" class="dataTables_empty">Loading data from server</td>
                        <%//=vw.grillaActividades(request)%>

                        </tbody>
                        <tfoot>
                            <tr>
                                <%=vw.grillaTbody(request)%>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>        
        </div>        
    </body>
</html>

