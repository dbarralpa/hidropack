package model;

public class ModMenu {

    private int id;
    private String nombre;
    private String url;

    public ModMenu() {
        id = 0;
        nombre = "";
        url = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
