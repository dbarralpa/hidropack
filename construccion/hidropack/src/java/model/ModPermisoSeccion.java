package model;

public class ModPermisoSeccion {

    private int id;
    private String permiso;

    public ModPermisoSeccion() {
        id = 0;
        permiso = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPermiso() {
        return permiso;
    }

    public void setPermiso(String permiso) {
        this.permiso = permiso;
    }
}
