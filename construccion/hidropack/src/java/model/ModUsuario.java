package model;

import java.util.ArrayList;

public class ModUsuario implements Cloneable {

    private String usuario;
    private String tipoUsuario;
    private String estado;
    private String nombre;
    private String apellido;
    private String rut;
    private ArrayList menus;
    private ArrayList permisosSeccion;

    public ModUsuario() {
        usuario = "";
        tipoUsuario = "";
        estado = "";
        nombre = "";
        apellido = "";
        rut = "";
        menus = new ArrayList();
        permisosSeccion = new ArrayList();
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public ArrayList getMenus() {
        return menus;
    }

    public void setMenus(ArrayList menus) {
        this.menus = menus;
    }

    public ArrayList getPermisosSeccion() {
        return permisosSeccion;
    }

    public void setPermisosSeccion(ArrayList permisosSeccion) {
        this.permisosSeccion = permisosSeccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public ModUsuario copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModUsuario) obj;
    }
}
