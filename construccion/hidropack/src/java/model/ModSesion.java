package model;

import controllers.PrmPaginacion;
import java.util.ArrayList;
import model.comercial.ModCliente;
import model.comercial.ModMantencion;
import model.comercial.ModPresupuesto;
import model.hidropack.ModObra;

public class ModSesion implements Cloneable {

    private String Http_Body = "";
    //** CONTROL DE ERRORES VIA MESSAGE BEAN **//
    private String tip = "";
    private int tipoTip = 0;
    private boolean conTip = false;
    private int etapaHidropack = 0;
    private int etapaComercial = 0;
    private int aux1 = 0;
    private PrmPaginacion pset;
    private boolean enSesion;

    /*ANY*/
    private long numeroDoc;
    private String url;
    private String popUp;
    //** COLECCIONES
    private ArrayList saldoMateriales;
    private ArrayList saldoMaterialesSchaffner;
    private ArrayList datos;
    private ArrayList pedidosEspeciales;
    private ArrayList detallePedidosProcesarBodega;
    private ArrayList pedidosProcesarBodega;
    private ArrayList materiales;
    private ArrayList auxiliar;
    private ArrayList auxiliar2;
    private ArrayList thead;
    //** MODELOS
    private ModEquipo equipo;
    private ModMaterial material;
    private ModUsuario usuario;
    private ModObra obra;
    private ModCliente cliente;
    private ModPresupuesto presupuesto;
    private ModMantencion mantencion;
    //**AJAX
    private String chk_PER_CODIGO = "checked";
    private String chk_PER_NOMBRE = "";
    private String chk_PER_USUARIO = "";
    private String aux = "";

    public ModSesion() {
        usuario = new ModUsuario();
        equipo = new ModEquipo();
        material = new ModMaterial();
        obra = new ModObra();
        cliente = new ModCliente();
        mantencion = new ModMantencion();
        presupuesto = new ModPresupuesto();
        saldoMaterialesSchaffner = new ArrayList();
        saldoMateriales = new ArrayList();
        pedidosEspeciales = new ArrayList();
        datos = new ArrayList();
        pset = new PrmPaginacion();
        detallePedidosProcesarBodega = new ArrayList();
        pedidosProcesarBodega = new ArrayList();
        materiales = new ArrayList();
        thead = new ArrayList();
        enSesion = false;
        numeroDoc = 0l;
        url = "";
        auxiliar = new ArrayList();
        auxiliar2 = new ArrayList();

    }

    public ArrayList getAuxiliar2() {
        return auxiliar2;
    }

    public void setAuxiliar2(ArrayList auxiliar2) {
        this.auxiliar2 = auxiliar2;
    }

    public ModMantencion getMantencion() {
        return mantencion;
    }

    public void setMantencion(ModMantencion mantencion) {
        this.mantencion = mantencion;
    }

    public int getAux1() {
        return aux1;
    }

    public void setAux1(int aux1) {
        this.aux1 = aux1;
    }

    public ModPresupuesto getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(ModPresupuesto presupuesto) {
        this.presupuesto = presupuesto;
    }

    public ModCliente getCliente() {
        return cliente;
    }

    public void setCliente(ModCliente cliente) {
        this.cliente = cliente;
    }

    public String getAux() {
        return aux;
    }

    public void setAux(String aux) {
        this.aux = aux;
    }

    public int getEtapaComercial() {
        return etapaComercial;
    }

    public void setEtapaComercial(int etapaComercial) {
        this.etapaComercial = etapaComercial;
    }

    public ArrayList getSaldoMaterialesSchaffner() {
        return saldoMaterialesSchaffner;
    }

    public void setSaldoMaterialesSchaffner(ArrayList saldoMaterialesSchaffner) {
        this.saldoMaterialesSchaffner = saldoMaterialesSchaffner;
    }

    public String getPopUp() {
        return popUp;
    }

    public void setPopUp(String popUp) {
        this.popUp = popUp;
    }

    public ArrayList getThead() {
        return thead;
    }

    public void setThead(ArrayList thead) {
        this.thead = thead;
    }

    public ModObra getObra() {
        return obra;
    }

    public void setObra(ModObra obra) {
        this.obra = obra;
    }

    public ArrayList getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(ArrayList auxiliar) {
        this.auxiliar = auxiliar;
    }

    public String getChk_PER_USUARIO() {
        return chk_PER_USUARIO;
    }

    public void setChk_PER_USUARIO(String chk_PER_USUARIO) {
        this.chk_PER_USUARIO = chk_PER_USUARIO;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ModUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(ModUsuario usuario) {
        this.usuario = usuario;
    }

    public long getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(long numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public int getEtapaHidropack() {
        return etapaHidropack;
    }

    public void setEtapaHidropack(int etapaHidropack) {
        this.etapaHidropack = etapaHidropack;
    }

    public boolean isEnSesion() {
        return enSesion;
    }

    public void setEnSesion(boolean enSesion) {
        this.enSesion = enSesion;
    }

    public ArrayList getMateriales() {
        return materiales;
    }

    public void setMateriales(ArrayList materiales) {
        this.materiales = materiales;
    }

    public ArrayList getDetallePedidosProcesarBodega() {
        return detallePedidosProcesarBodega;
    }

    public void setDetallePedidosProcesarBodega(ArrayList detallePedidosProcesarBodega) {
        this.detallePedidosProcesarBodega = detallePedidosProcesarBodega;
    }

    public ArrayList getPedidosProcesarBodega() {
        return pedidosProcesarBodega;
    }

    public void setPedidosProcesarBodega(ArrayList pedidosProcesarBodega) {
        this.pedidosProcesarBodega = pedidosProcesarBodega;
    }

    public ArrayList getPedidosEspeciales() {
        return pedidosEspeciales;
    }

    public void setPedidosEspeciales(ArrayList pedidosEspeciales) {
        this.pedidosEspeciales = pedidosEspeciales;
    }

    public ModMaterial getMaterial() {
        return material;
    }

    public void setMaterial(ModMaterial material) {
        this.material = material;
    }

    public ArrayList getDatos() {
        return datos;
    }

    public void setDatos(ArrayList datos) {
        this.datos = datos;
    }

    public String getChk_PER_CODIGO() {
        return chk_PER_CODIGO;
    }

    public void setChk_PER_CODIGO(String chk_PER_CODIGO) {
        this.chk_PER_CODIGO = chk_PER_CODIGO;
    }

    public String getChk_PER_NOMBRE() {
        return chk_PER_NOMBRE;
    }

    public void setChk_PER_NOMBRE(String chk_PER_NOMBRE) {
        this.chk_PER_NOMBRE = chk_PER_NOMBRE;
    }

    public ModEquipo getEquipo() {
        return equipo;
    }

    public void setEquipo(ModEquipo equipo) {
        this.equipo = equipo;
    }

    public ArrayList getSaldoMateriales() {
        return saldoMateriales;
    }

    public void setSaldoMateriales(ArrayList saldoMateriales) {
        this.saldoMateriales = saldoMateriales;
    }

    public PrmPaginacion getPset() {
        return pset;
    }

    public void setPset(PrmPaginacion pset) {
        this.pset = pset;
    }

    public String getLinkDO() {
        return "Main.do";
    }

    public String getHttp_Body() {
        return Http_Body;
    }

    public void setHttp_Body(String Http_Body) {
        this.Http_Body = Http_Body;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip, int tipoTip) {
        this.setTipoTip(tipoTip);
        this.setConTip(true);
        this.tip = tip;
    }

    public void cleanTip() {
        this.setTipoTip(0);
        this.setConTip(false);
        this.tip = "";
    }

    public boolean isConTip() {
        return conTip;
    }

    public void setConTip(boolean conTip) {
        this.conTip = conTip;
    }

    public int getTipoTip() {
        return tipoTip;
    }

    public void setTipoTip(int tipoTip) {
        this.tipoTip = tipoTip;
    }

    public ModSesion copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModSesion) obj;
    }
}
