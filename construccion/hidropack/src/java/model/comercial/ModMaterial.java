package model.comercial;

public class ModMaterial {

    private String codigo;
    private String descripcion;
    private String um;

    public ModMaterial() {
        codigo = "";
        descripcion = "";
        um = "";
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }
}
