package forms;

import controllers.PrmApp;
import controllers.PrmHidropack;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModSesion;

public class LoginReq extends HttpServlet {

    //public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        /**
         * INICIO DE SESION *
         */
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            if (req.getParameter("Submit.x") != null) {
                //LimpiarSesiones(req, true);
                Sesion = new ModSesion();
                String user = req.getParameter("txtUsr");
                String pass = req.getParameter("txtPwd");

                if (!user.equals("") && !pass.equals("")) {
                    if (PrmHidropack.cargarUsuario(user, pass, Sesion)) {
                        Sesion.setEnSesion(true);
                        //Sesion.setHttp_Body("home");
                        Sesion.setUrl("../page/body.jsp");                        
                        
                    } else {
                        Sesion.setTip("Datos incorrectos", 0);
                    }
                } else {
                    Sesion.setTip("Los campos Usuario y Clave no pueden estar vacios", 0);
                }
            }
            if (req.getParameter("LogOut") != null) {
                //LimpiarSesiones(req, true);
                Sesion = new ModSesion();
            }
            req.getSession().setAttribute("Sesion", Sesion);
            res.sendRedirect("template/appLayout.jsp");
        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }
        //super.SendtoHome(res, req);
        //return null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
