/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.comercial;

/**
 *
 * @author dbarra
 */
public class ModTrabajo {

    private int codigo;
    private String descripcion;

    public ModTrabajo() {
        codigo = 0;
        descripcion = "";
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
