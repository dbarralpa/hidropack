package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.ModSesion;
import model.comercial.ModCliente;
import model.comercial.ModMantencion;
import model.comercial.ModPlantilla;
import model.comercial.ModPresupuesto;
import model.comercial.ModVentaDirecta;
import model.hidropack.ModObra;
import sql.ConexionSQL;

public class PrmGrabarComercial {

    private static SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat hora = new SimpleDateFormat("hhmmss");

    public static boolean actualizarCambiarMaterial(String nombre, ArrayList auxiliar) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        boolean est1 = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                est = cn.DeleteReg("TBL_COM_PLANTILLA", "vc_nombre = '" + nombre + "'");
                if (est) {
                    est1 = ingresarPlantilla(cn, auxiliar, nombre);
                }
                cn.commitSQL(est && est1);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est && est1;
    }

    private static boolean ingresarPlantilla(ConexionSQL cn, ArrayList auxiliar, String nombre) throws SQLException, Exception {
        boolean est = false;
        String ccc = "vc_nombre,vc_codigo_material,nb_cantidad";
        String vvv = "";
        for (int i = 0; i < auxiliar.size(); i++) {
            ModPlantilla pla = (ModPlantilla) auxiliar.get(i);
            vvv = nombre + "�";
            vvv += pla.getCodigo() + "�";
            vvv += pla.getCantidad();
            est = cn.InsertReg("TBL_COM_PLANTILLA", ccc, vvv);
            if (!est) {
                break;
            }
        }
        return est;
    }

    public static boolean guardarNuevaPlantilla(String nombre, ArrayList auxiliar) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs = null;
        boolean est = false;
        int val = 0;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = "select count(*) from TBL_COM_PLANTILLA where vc_nombre = '" + nombre + "'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    val = rs.getInt(1);
                }
                if (val == 0) {
                    est = ingresarPlantilla(cn, auxiliar, nombre);
                    cn.commitSQL(est);
                }
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean guardarPresupuesto(int numPresupuesto, int idTrabajo, ArrayList auxiliar, String usuario, long rut, int productoOServicio, String plantilla) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs = null;
        boolean est = false;
        int val = 0;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = "select count(*) from TBL_COM_NAV_ENC_DET where nb_numero_presupuesto = " + numPresupuesto + " and nb_id_trabajo = " + idTrabajo;
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    val = rs.getInt(1);
                }
                if (val == 0) {
                    long num = PrmComercial.ultimoIdNav(cn);
                    est = guardarNav(cn, numPresupuesto, idTrabajo, num);
                    if (est) {
                        est = guardarPresupuestoEnc(cn, num, usuario, rut, productoOServicio, plantilla);
                    }
                    if (est) {
                        est = guardarPresupuestoDet(cn, num, auxiliar);
                    }
                }
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    private static boolean guardarNav(ConexionSQL cn, int numPresupuesto, int idTrabajo, long num) throws SQLException, Exception {
        String ccc = "nb_id,nb_numero_presupuesto,nb_id_trabajo";
        String vvv = "";
        vvv = num + "�";
        vvv += numPresupuesto + "�";
        vvv += idTrabajo;
        return cn.InsertReg("TBL_COM_NAV_ENC_DET", ccc, vvv);
    }

    private static boolean guardarPresupuestoEnc(ConexionSQL cn, long num, String usuario, long rut, int productoOServicio, String plantilla) throws SQLException, Exception {
        String ccc = "dt_fecha,vc_usuario,nb_rut_cliente,nb_id_nub_enc_det,nb_id_producto_o_servicio,vc_plantilla";
        String vvv = "";
        vvv += fecha.format(new Date()) + "�";
        vvv += usuario + "�";
        vvv += rut + "�";
        vvv += num + "�";
        vvv += productoOServicio + "�";
        vvv += plantilla;
        return cn.InsertReg("TBL_COM_PRESUPUESTO_ENC", ccc, vvv);
    }

    private static boolean guardarPresupuestoDet(ConexionSQL cn, long num, ArrayList auxiliar) throws SQLException, Exception {
        boolean est = false;
        String ccc = "vc_codigo, vc_descripcion, vc_um, nb_cantidad, nb_precio_unitario, nb_id_nub_enc_det";
        String vvv = "";
        for (int i = 0; i < auxiliar.size(); i++) {
            ModPlantilla pla = (ModPlantilla) auxiliar.get(i);
            vvv = pla.getCodigo() + "�";
            vvv += pla.getDescripcion().trim() + "�";
            vvv += pla.getUm() + "�";
            vvv += pla.getCantidad() + "�";
            vvv += pla.getPrecioUnitario() + "�";
            vvv += num;
            est = cn.InsertReg("TBL_COM_PRESUPUESTO_DET", ccc, vvv);
            if (!est) {
                break;
            }
        }
        return est;
    }

    public static boolean insertarObra(ModSesion Sesion) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                long num = PrmComercial.ultimaObraInsertada(cn);
                long fila = PrmComercial.idUltimaFilaSysTabla(cn);
                String ccc = "tab_desc, tab_valor1, tab_texto1, tab_codigo, tab_texto3, tab_fecha1, tab_texto4,tab_estado,";
                ccc += "tab_codtabla, tab_desctabla, tab_tipo, id_fila, UsCreacion,FecCreacion,HoraCreacion";
                String vvv = "";
                String[] fech = Sesion.getObra().getCliente().getFecha().split("/");
                vvv = Sesion.getObra().getNombreObra() + "�";
                vvv += Sesion.getObra().getVentaEstimada() + "�";
                vvv += Sesion.getObra().getCostoServicio() + "�";
                vvv += num + "�";
                vvv += Sesion.getObra().getCliente().getRut() + "�";
                vvv += fech[2] + fech[1] + fech[0] + "�";
                vvv += Sesion.getObra().getNumPresupuesto() + "�";
                vvv += 1 + "�";
                vvv += 500 + "�";
                vvv += "Numero_de_Obras" + "�";
                vvv += "U" + "�";
                vvv += fila + "�";
                vvv += Sesion.getUsuario().getUsuario() + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += hora.format(new Date());
                est = cn.InsertReg("SYS_TABLAS", ccc, vvv);
                Sesion.setNumeroDoc(num);
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean actualizarObra(ModObra obra, String usuario, int numPresupuesto) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        String ccc = "tab_desc, tab_valor1, tab_texto1, tab_texto3, tab_fecha1, tab_texto4,";
        ccc += "tab_codtabla, tab_desctabla, tab_tipo, UsModif,FecModif,HoraModif";
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String vvv = obra.getNombreObra() + "�";
                vvv += obra.getVentaEstimada() + "�";
                vvv += obra.getCostoServicio() + "�";
                vvv += obra.getCliente().getRut() + "�";
                String[] fecha1 = obra.getCliente().getFecha().split("/");
                vvv += fecha1[2] + fecha1[1] + fecha1[0] + "�";
                vvv += numPresupuesto + "�";
                vvv += 500 + "�";
                vvv += "Numero_de_Obras" + "�";
                vvv += "U" + "�";
                vvv += usuario + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += hora.format(new Date());
                est = cn.UpdateReg("SYS_TABLAS", ccc, vvv, "tab_codigo = " + obra.getIdObra() + " and TAB_Tipo = 'U' and TAB_CodTabla = 500");
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean actualizarPresupuestoObra(ArrayList materiales, ModCliente cli, int idTrabajo, int id) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        boolean est1 = false;
        boolean est2 = false;
        boolean est3 = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                est = actualizarPresupuestoObraNav(cn, idTrabajo, id);
                if (est) {
                    est1 = actualizarPresupuestoObraEnc(cn, cli.getRut(), id);
                }
                if (est1) {
                    est2 = eliminarPresupuestoDetalleObra(cn, id);
                }

                if (est2) {
                    est3 = ingresarPresupuestoObraDet(cn, materiales, id);
                }
                cn.commitSQL(est && est1 && est2 && est3);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est && est1 && est2 && est3;
    }

    private static boolean actualizarPresupuestoObraEnc(ConexionSQL cn, long rutCliente, int id) throws SQLException, Exception {
        String ccc = "nb_rut_cliente";
        String vvv = "";
        vvv += rutCliente;
        return cn.UpdateReg("TBL_COM_PRESUPUESTO_ENC", ccc, vvv, "nb_id_nub_enc_det = " + id);
    }

    private static boolean actualizarPresupuestoObraNav(ConexionSQL cn, int idTrabajo, int id) throws SQLException, Exception {
        String ccc = "nb_id_trabajo";
        String vvv = "";
        vvv += idTrabajo;
        return cn.UpdateReg("TBL_COM_NAV_ENC_DET", ccc, vvv, "nb_id = " + id);
    }

    private static boolean ingresarPresupuestoObraDet(ConexionSQL cn, ArrayList materiales, int navEncDet) throws SQLException, Exception {
        boolean est = false;
        String ccc = "vc_codigo,nb_cantidad,nb_precio_unitario,nb_id_nub_enc_det,vc_descripcion,vc_um";
        String vvv = "";
        for (int i = 0; i < materiales.size(); i++) {
            ModPresupuesto pla = (ModPresupuesto) materiales.get(i);
            vvv = pla.getCodigo() + "�";
            vvv += pla.getCantidad() + "�";
            vvv += pla.getPrecioUnitario() + "�";
            vvv += navEncDet + "�";
            vvv += pla.getDescripcion() + "�";
            vvv += pla.getUm();
            est = cn.InsertReg("TBL_COM_PRESUPUESTO_DET", ccc, vvv);
            if (!est) {
                break;
            }
        }
        return est;
    }

    private static boolean eliminarPresupuestoDetalleObra(ConexionSQL cn, int id) throws SQLException, Exception {
        return cn.DeleteReg("TBL_COM_PRESUPUESTO_DET", "nb_id_nub_enc_det = " + id);
    }

    public static boolean guardarPresupuestoMantencionCorrectiva(ModSesion Sesion, long rut, int productoOServicio) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs = null;
        boolean est = false;
        int val = 0;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                int numPresupuesto = PrmComercial.ultimoPresupuesto(cn);

                long num = PrmComercial.ultimoIdNav(cn);
                est = guardarNav(cn, numPresupuesto, 0, num);
                if (est) {
                    est = guardarPresupuestoEnc(cn, num, Sesion.getUsuario().getUsuario(), rut, productoOServicio, "");
                }
                if (est) {
                    est = guardarPresupuestoMantencionCorrectivaDet(cn, num, Sesion.getMateriales(), Sesion.getAux1());
                    Sesion.setNumeroDoc(numPresupuesto);
                }

                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    private static boolean guardarPresupuestoMantencionCorrectivaDet(ConexionSQL cn, long num, ArrayList materiales, int uf) throws SQLException, Exception {
        boolean est = false;
        String ccc = "vc_codigo, vc_descripcion, vc_um, nb_cantidad, nb_precio_unitario, nb_id_nub_enc_det, nb_instalacion, nb_uf";
        String vvv = "";
        for (int i = 0; i < materiales.size(); i++) {
            ModPresupuesto pla = (ModPresupuesto) materiales.get(i);
            vvv = pla.getCodigo() + "�";
            vvv += pla.getDescripcion().trim() + "�";
            vvv += pla.getUm() + "�";
            vvv += pla.getCantidad() + "�";
            vvv += pla.getPrecioUnitario() + "�";
            vvv += num + "�";
            vvv += pla.getInstalacion() + "�";
            vvv += uf;
            est = cn.InsertReg("TBL_COM_PRESUPUESTO_DET", ccc, vvv);
            if (!est) {
                break;
            }
        }
        return est;
    }

    public static boolean insertarManCorrectiva(ModSesion Sesion, String nombre, double venta, long rut, int presupuesto, String fecha1, String descripcion) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                long num = PrmComercial.ultimaObraInsertada(cn);
                long fila = PrmComercial.idUltimaFilaSysTabla(cn);
                String ccc = "tab_desc, tab_valor1, tab_codigo, tab_texto3, tab_fecha1, tab_texto4, tab_texto5,tab_estado,";
                ccc += "tab_codtabla, tab_desctabla, tab_tipo, id_fila, UsCreacion,FecCreacion,HoraCreacion";
                String vvv = "";
                String[] fech = fecha1.split("/");
                vvv = nombre + "�";
                vvv += venta + "�";
                vvv += num + "�";
                vvv += rut + "�";
                vvv += fech[2] + fech[1] + fech[0] + "�";
                vvv += presupuesto + "�";
                vvv += descripcion + "�";
                vvv += 2 + "�";
                vvv += 500 + "�";
                vvv += "Numero_de_Obras" + "�";
                vvv += "U" + "�";
                vvv += fila + "�";
                vvv += Sesion.getUsuario().getUsuario() + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += hora.format(new Date());
                est = cn.InsertReg("SYS_TABLAS", ccc, vvv);
                Sesion.setNumeroDoc(num);
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean actualizarObraManCorrectiva(ModObra obra, String usuario, int numPresupuesto) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        String ccc = "tab_desc, tab_valor1, tab_texto3, tab_fecha1, tab_texto4, tab_texto5,";
        ccc += "UsModif,FecModif,HoraModif";
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String vvv = obra.getNombreObra() + "�";
                vvv += obra.getVentaEstimada() + "�";
                vvv += obra.getCliente().getRut() + "�";
                String[] fecha1 = obra.getCliente().getFecha().split("/");
                vvv += fecha1[2] + fecha1[1] + fecha1[0] + "�";
                vvv += numPresupuesto + "�";
                vvv += obra.getDescripcion() + "�";
                vvv += usuario + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += hora.format(new Date());
                est = cn.UpdateReg("SYS_TABLAS", ccc, vvv, "tab_codigo = " + obra.getIdObra() + " and TAB_Tipo = 'U' and TAB_CodTabla = 500");
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean insertarManPreventiva(ModSesion Sesion, ModMantencion man) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                long num = PrmComercial.ultimaObraInsertada(cn);
                long fila = PrmComercial.idUltimaFilaSysTabla(cn);
                String ccc = "tab_desc, tab_valor1, tab_codigo, tab_texto3, tab_texto4, tab_texto5,tab_estado,tab_texto1,";
                ccc += "tab_codtabla, tab_desctabla, tab_tipo, id_fila, UsCreacion,FecCreacion,HoraCreacion";
                String vvv = "";
                vvv = man.getNombre() + "�";
                vvv += man.getUf() + "�";
                vvv += num + "�";
                vvv += man.getCliente().getRut() + "�";
                vvv += man.getValorUf() + "�";
                vvv += man.getObservacion() + "�";
                vvv += 3 + "�";
                vvv += man.getFrecuencia() + "�";
                vvv += 500 + "�";
                vvv += "Numero_de_Obras" + "�";
                vvv += "U" + "�";
                vvv += fila + "�";
                vvv += Sesion.getUsuario().getUsuario() + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += hora.format(new Date());
                est = cn.InsertReg("SYS_TABLAS", ccc, vvv);
                Sesion.setNumeroDoc(num);
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean actualizarManPreventiva(ModSesion Sesion, ModMantencion man) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String ccc = "tab_desc, tab_valor1, tab_texto3, tab_texto4, tab_texto5,tab_texto1,";
                ccc += "UsModif,FecModif,HoraModif";
                String vvv = "";
                vvv = man.getNombre() + "�";
                vvv += man.getUf() + "�";
                vvv += man.getCliente().getRut() + "�";
                vvv += man.getValorUf() + "�";
                vvv += man.getObservacion() + "�";
                vvv += man.getFrecuencia() + "�";
                vvv += Sesion.getUsuario().getUsuario() + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += hora.format(new Date());
                est = cn.UpdateReg("SYS_TABLAS", ccc, vvv, "tab_codigo = " + man.getNumero() + " and TAB_Tipo = 'U' and TAB_CodTabla = 500");
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean insertarVentaDirecta(ModSesion Sesion, long rutCliente, int productoOservicio) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                long num = PrmComercial.ultimaObraInsertada(cn);
                long fila = PrmComercial.idUltimaFilaSysTabla(cn);
                String ccc = "tab_desc, tab_codigo, tab_texto3, tab_estado,";
                ccc += "tab_codtabla, tab_desctabla, tab_tipo, id_fila, UsCreacion,FecCreacion,HoraCreacion";
                String vvv = "";
                vvv = "Venta directa" + "�";
                vvv += num + "�";
                vvv += rutCliente + "�";
                vvv += productoOservicio + "�";
                vvv += 500 + "�";
                vvv += "Numero_de_Obras" + "�";
                vvv += "U" + "�";
                vvv += fila + "�";
                vvv += Sesion.getUsuario().getUsuario() + "�";
                vvv += fecha.format(new Date()) + "�";
                vvv += hora.format(new Date());
                est = cn.InsertReg("SYS_TABLAS", ccc, vvv);
                if (est) {
                    est = insertarVentaDirectaDet(cn, num, Sesion.getAuxiliar());
                }
                Sesion.setNumeroDoc(num);
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    private static boolean insertarVentaDirectaDet(ConexionSQL cn, long num, ArrayList auxiliar) throws SQLException, Exception {
        boolean est = false;
        String ccc = "vc_codigo, nb_precio, nb_cantidad, nb_numero_obra";
        String vvv = "";
        for (int i = 0; i < auxiliar.size(); i++) {
            ModVentaDirecta pla = (ModVentaDirecta) auxiliar.get(i);
            vvv = pla.getCodigo() + "�";
            vvv += pla.getPrecio() + "�";
            vvv += pla.getCantidad() + "�";
            vvv += num;
            est = cn.InsertRegSinPrefix("HIDROPACK.dbo.TBL_COM_VENTA_DIRECTA", ccc, vvv);
            if (!est) {
                break;
            }
        }
        return est;
    }

    public static boolean actualizarPresupuestoManCorr(ArrayList materiales, ModCliente cli, int id, int uf) {
        ConexionSQL cn = new ConexionSQL();
        boolean est1 = false;
        boolean est2 = false;
        boolean est3 = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                est1 = actualizarPresupuestoObraEnc(cn, cli.getRut(), id);

                if (est1) {
                    est2 = eliminarPresupuestoDetalleObra(cn, id);
                }

                if (est2) {
                    est3 = guardarPresupuestoMantencionCorrectivaDet(cn, id, materiales, uf);
                }
                cn.commitSQL(est1 && est2 && est3);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est1 && est2 && est3;
    }
}
