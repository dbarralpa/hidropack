package excel;

import controllers.PrmApp;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import model.ModAuxiliar;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

public class AsExcel {

    private HSSFWorkbook workbook;
    private HSSFSheet sheet_estilos;
    private FileInputStream inputfile;
    private float alturaCell;

    public AsExcel() {
        workbook = new HSSFWorkbook();
        sheet_estilos = null;
        inputfile = null;
        alturaCell = 0;
    }

    public void crearExcelSinEstilos(String nomArchivo) {
        try {
            inputfile = new FileInputStream(nomArchivo);
            workbook = new HSSFWorkbook(inputfile);
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    public ArrayList leerDatosHoja(int hoja) throws IOException {
        ArrayList datos = new ArrayList();
        HSSFWorkbook workbookTMP = this.getWorkBook();
        HSSFSheet sheet = workbookTMP.getSheetAt(hoja);

        int filas = sheet.getPhysicalNumberOfRows();
        for (int j = 0; j < filas; j++) {
            HSSFRow row = sheet.getRow(j);
            if (row != null) {
                int celdas = row.getPhysicalNumberOfCells();
                ModAuxiliar fila = new ModAuxiliar();
                fila.setID(datos.size());
                for (int k = 0; k < celdas; k++) {
                    HSSFCell cell = row.getCell(k);
                    if (cell != null) {
                        switch (cell.getCellType()) {
                            case HSSFCell.CELL_TYPE_BOOLEAN:
                                break;
                            case HSSFCell.CELL_TYPE_NUMERIC:
                                if (k == 1) {
                                    fila.setValor(cell.getNumericCellValue());
                                }
                                if (k == 0) {
                                    cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                                    fila.setNombre(cell.getStringCellValue());
                                }
                                break;
                            case HSSFCell.CELL_TYPE_STRING:
                                if (k == 0) {
                                    fila.setNombre(cell.getRichStringCellValue().getString());
                                }

                                break;
                            case HSSFCell.CELL_TYPE_ERROR:
                                break;
                            case HSSFCell.CELL_TYPE_BLANK:
                                break;
                            case HSSFCell.CELL_TYPE_FORMULA:
                                break;
                            default:
                                break;
                        }
                    }
                    cell = null;
                }
                if (fila.getNombre().length() > 0) {
                    datos.add(fila);
                }
                fila = null;
            }
            row = null;
        }
        return datos;
    }

    public void crearExcel(String nomArchivo) {
        try {
            inputfile = new FileInputStream(nomArchivo);
            workbook = new HSSFWorkbook(inputfile);
            sheet_estilos = workbook.getSheetAt(workbook.getSheetIndex("ESTILO"));
            alturaCell = Float.parseFloat(sheet_estilos.getRow(0).getCell(1).getRichStringCellValue().getString());
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    public void borrarHojaEstilo() {
        workbook.removeSheetAt(workbook.getSheetIndex("ESTILO"));
    }

    public void borrarHoja(String nomHoja) {
        workbook.removeSheetAt(workbook.getSheetIndex(nomHoja));
    }

    public void cerrarPlantilla() {
        try {
            workbook = null;
            inputfile.close();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    private HSSFCellStyle getStylePlantilla(String estilo) {
        HSSFCellStyle cellStyle = null;
        if (estilo.equals("CAB_TAB")) {
            cellStyle = sheet_estilos.getRow(1).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("CAB_STAB")) {
            cellStyle = sheet_estilos.getRow(2).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TTEXTO")) {
            cellStyle = sheet_estilos.getRow(3).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TNUMERO")) {
            cellStyle = sheet_estilos.getRow(4).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TEXTO1")) {
            cellStyle = sheet_estilos.getRow(5).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_THORA")) {
            cellStyle = sheet_estilos.getRow(6).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TNUMERICO")) {
            cellStyle = sheet_estilos.getRow(7).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TESTADO")) {
            cellStyle = sheet_estilos.getRow(8).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TFACTOR")) {
            cellStyle = sheet_estilos.getRow(9).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TMONEDA")) {
            cellStyle = sheet_estilos.getRow(10).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TPORCENTAJE")) {
            cellStyle = sheet_estilos.getRow(11).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TCABEZERA")) {
            cellStyle = sheet_estilos.getRow(12).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STTEXTO")) {
            cellStyle = sheet_estilos.getRow(13).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STNUMERO")) {
            cellStyle = sheet_estilos.getRow(14).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STFECHA")) {
            cellStyle = sheet_estilos.getRow(15).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STHORA")) {
            cellStyle = sheet_estilos.getRow(16).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STNUMERICO")) {
            cellStyle = sheet_estilos.getRow(17).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STESTADO")) {
            cellStyle = sheet_estilos.getRow(18).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STFACTOR")) {
            cellStyle = sheet_estilos.getRow(19).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STMONEDA")) {
            cellStyle = sheet_estilos.getRow(20).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TTEXTO")) {
            cellStyle = sheet_estilos.getRow(21).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TNUMERO")) {
            cellStyle = sheet_estilos.getRow(22).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TFECHA")) {
            cellStyle = sheet_estilos.getRow(23).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_THORA")) {
            cellStyle = sheet_estilos.getRow(24).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TNUMERICO")) {
            cellStyle = sheet_estilos.getRow(25).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TESTADO")) {
            cellStyle = sheet_estilos.getRow(26).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TFACTOR")) {
            cellStyle = sheet_estilos.getRow(27).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TMONEDA")) {
            cellStyle = sheet_estilos.getRow(28).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STTEXTO")) {
            cellStyle = sheet_estilos.getRow(29).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STNUMERO")) {
            cellStyle = sheet_estilos.getRow(30).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STFECHA")) {
            cellStyle = sheet_estilos.getRow(31).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STHORA")) {
            cellStyle = sheet_estilos.getRow(32).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STNUMERICO")) {
            cellStyle = sheet_estilos.getRow(33).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STESTADO")) {
            cellStyle = sheet_estilos.getRow(34).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STFACTOR")) {
            cellStyle = sheet_estilos.getRow(35).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STMONEDA")) {
            cellStyle = sheet_estilos.getRow(36).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STPORCENTAJE")) {
            cellStyle = sheet_estilos.getRow(37).getCell(1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("CELL_NO_EXIST")) {
            cellStyle = sheet_estilos.getRow(38).getCell(1).getCellStyle();
            return cellStyle;
        } else {
            cellStyle = sheet_estilos.getRow(38).getCell(1).getCellStyle();
            return cellStyle;
        }
    }

    public void setValor(int hoja, int fila, int celda, String valorStr, String estilo, int fila1) {
        setValor(hoja, fila, celda, celda, 0, valorStr, estilo, false, fila1);
    }

    public void setValor(int hoja, int fila, int celda, double valorNum, String estilo, int fila1) {
        setValor(hoja, fila, celda, celda, valorNum, "", estilo, true, fila1);
    }

    public void setValor(int hoja, int fila, int fromCelda, int toCelda, double valorNum, String estilo, int fila1) {
        setValor(hoja, fila, fromCelda, toCelda, valorNum, "", estilo, true, fila1);
    }

    public void setValor(int hoja, int fila, int fromCelda, int toCelda, String valorStr, String estilo, int fila1) {
        setValor(hoja, fila, fromCelda, toCelda, 0, valorStr, estilo, false, fila1);
    }

    private void setValor(int hoja, int fila, int fromCelda, int toCelda, double valorNum, String valorStr, String estilo, boolean isNum, int fila1) {
        HSSFCell cell = crearCelda(hoja, fila, fromCelda, toCelda, estilo, fila1);
        try {
            if (cell != null) {
                if (isNum) {
                    cell.setCellValue(valorNum);
                } else {
                    cell.setCellValue(valorStr);
                }
            }
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    private HSSFCell crearCelda(int hoja, int fila, int fromCelda, int toCelda, String estilo, int fila1) {
        HSSFCell cell = null;
        try {
            HSSFRow row = null;
            if (workbook.getSheetAt(hoja).getRow(fila) != null) {
                row = workbook.getSheetAt(hoja).getRow(fila);
            } else {
                row = workbook.getSheetAt(hoja).createRow(fila);
            }

            row.setHeightInPoints(alturaCell);
            for (int i = fromCelda; i <= toCelda; i++) {
                row.createCell(i);
                row.getCell(i).setCellStyle(getStylePlantilla(estilo));
            }
            crearRegion(hoja, fila, fromCelda, toCelda, fila1);
            cell = workbook.getSheetAt(hoja).getRow(fila).getCell(fromCelda);

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return cell;
    }

    private void crearRegion(int hoja, int fila, int fromCelda, int toCelda, int fila1) {
        workbook.getSheetAt(hoja).addMergedRegion(new CellRangeAddress(fila, fila1, fromCelda, toCelda));
    }

    public HSSFWorkbook getWorkBook() {
        return workbook;
    }
}
