var cargando = "<tr><td align=\"center\">";
cargando += "<img alt=\"Cargando...\" src=\"../img/loading16.gif\" style=\"vertical-align:middle;\">&nbsp;<b>CARGANDO...</b>";
cargando += "<br/>Mientras carga favor no ejecute ninguna acci�n.";
cargando += "<br/>Gracias.";
cargando += "</td></tr>";

function loading() {
    $("#overLoading").fadeIn('fast');
    $("#mensajeLoading").fadeIn('fast');
}

$(document).ready(function (){
    var table_w = $("#mensajeLoading table").width() + 100;
    var table_h = $("#mensajeLoading table").height() + 100;

    $("#mensajeLoading table").html(cargando);    
    $("#mensajeLoading").hide();

    //Consigue valores de la ventana del navegador
    var w = $(this).width();
    var h = $(this).height();

    $("#overLoading").css('width', w);
    $("#overLoading").css('height', h);
    $("#overLoading").css('-moz-opacity', "0.8");
    $("#overLoading").css('opacity', "0.8");
    $("#overLoading").css('-KHTML-opacity', "0.8");
    
    
    w = (w/2) - (table_w/2);
    h = (h/2) - (table_h/2);
    $("#mensajeLoading").css("left",w + "px");
    $("#mensajeLoading").css("top",h + "px");
});