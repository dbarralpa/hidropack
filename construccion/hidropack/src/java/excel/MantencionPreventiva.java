/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package excel;

import controllers.PrmApp;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModSesion;
import model.comercial.ModMantencion;
import util.HelperComercial;

/**
 *
 * @author dbarra
 */
public class MantencionPreventiva extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModSesion Sesion = null;
        DecimalFormat number = new DecimalFormat("###,###.##");
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/mantencionPreventiva.xls");
            int indice = 5;

            ArrayList mantenciones = Sesion.getAuxiliar();
            for (int i = 0; i < mantenciones.size(); i++) {
                ModMantencion man = (ModMantencion) mantenciones.get(i);
                excel.setValor(0, indice, 0, man.getNumero(), "DET_TNUMERICO", indice);
                excel.setValor(0, indice, 1, man.getNombre(), "DET_TTEXTO", indice);
                excel.setValor(0, indice, 2, HelperComercial.comboFrecuenciaSelected(request, man.getFrecuencia()), "DET_TTEXTO", indice);
                excel.setValor(0, indice, 3, man.getValorUf(), "DET_TNUMERICO", indice);
                excel.setValor(0, indice, 4, man.getUf(), "DET_TNUMERO", indice);
                excel.setValor(0, indice, 5, man.getCliente().getRut(), "DET_TNUMERICO", indice);
                excel.setValor(0, indice, 6, man.getCliente().getNombre(), "DET_TNUMERO", indice);
                excel.setValor(0, indice, 7, man.getValorUf() * man.getUf(), "DET_TMONEDA", indice);
                indice++;
            }
            excel.borrarHojaEstilo();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"Mantencion preventiva\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
