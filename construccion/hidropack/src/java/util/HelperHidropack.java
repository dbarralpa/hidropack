/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import controllers.PrmApp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import model.ModEquipo;
import model.ModMaterial;
import model.ModSesion;
import model.hidropack.ModOrdenCompra;
import javax.servlet.http.HttpServletRequest;
import model.Imprimir;

/**
 *
 * @author dbarra
 */
public class HelperHidropack {

    public static void actualizarStockSaldoMaterialesAfterInsertPedidoEspecialOEstructura(ArrayList saldoMateriales, ModMaterial mate) {
        for (int i = 0; i < saldoMateriales.size(); i++) {
            ModMaterial mat = ((ModMaterial) saldoMateriales.get(i)).copia();
            if (mat.getMateriaPrima().equals(mate.getMateriaPrima())) {
                mat.setReservado(mat.getReservado() + mate.getCantRequerida());
                if ((mat.getStock() - mat.getReservado()) >= 0) {
                    mat.setSaldo(mat.getStock() - mat.getReservado());
                } else {
                    mat.setSaldo(0);
                }
                saldoMateriales.set(i, mat);
            }
        }
    }

    public static String estadoPedidoMateriales(byte estado, int i) {
        String html = "";
        if (estado == 2) {
            html = "<td><a href='../FrmHidropack?enProceso=SI&numeroDoc=" + i + "'/>En proceso</a></td>\n";
            html += "<td>&nbsp;&nbsp;Listo para retirar</td>\n";
            html += "<td>&nbsp;&nbsp;Entregado</td>\n";
        } else if (estado == 3) {
            html = "<td>En proceso</td>\n";
            html += "<td>&nbsp;&nbsp;<a href='../FrmHidropack?listoParaRetirar=SI&numeroDoc=" + i + "'/>Listo para retirar</a></td>\n";
            html += "<td>&nbsp;&nbsp;Entregado</td>\n";
        } else if (estado == 4) {
            html = "<td>En proceso</td>\n";
            html += "<td>&nbsp;&nbsp;Listo para retirar</td>\n";
            html += "<td>&nbsp;&nbsp;<a href='../FrmHidropack?entregado=SI&numeroDoc=" + i + "'/>Entregado</a></td>\n";
        }
        return html;
    }

    public static String estadoPedidoMateriales(byte estado) {
        String html = "";
        if (estado == 2) {
            html = "<td>&nbsp<font size='+1' color='#FF0000'>PENDIENTE</font></td>\n";
        } else if (estado == 3) {
            html = "<td>&nbsp;<font size='+1' color='#00FF00'>EN PROCESO</font></td>\n";
        } else if (estado == 4) {
            html += "<td>&nbsp;<font size='+1' color='#663399'>LISTO PARA RETIRAR</font></td>\n";
        }

        return html;
    }

    public static double piso(ArrayList saldoMateriales, ModMaterial mate) {
        double val = 0d;
        for (int i = 0; i < saldoMateriales.size(); i++) {
            ModMaterial mat = ((ModMaterial) saldoMateriales.get(i)).copia();
            if (mat.getMateriaPrima().equals(mate.getMateriaPrima())) {
                val = mat.getPiso();
            }
        }
        return val;
    }

    public static double reservado(ArrayList saldoMateriales, ModMaterial mate) {
        double val = 0d;
        for (int i = 0; i < saldoMateriales.size(); i++) {
            ModMaterial mat = ((ModMaterial) saldoMateriales.get(i)).copia();
            if (mat.getMateriaPrima().equals(mate.getMateriaPrima())) {
                val = mat.getReservado();
            }
        }
        return val;
    }

    public static double stock(ArrayList saldoMateriales, ModMaterial mate) {
        double val = 0d;
        for (int i = 0; i < saldoMateriales.size(); i++) {
            ModMaterial mat = ((ModMaterial) saldoMateriales.get(i)).copia();
            if (mat.getMateriaPrima().equals(mate.getMateriaPrima())) {
                val = mat.getStock();
            }
        }
        return val;
    }

    public static double saldo(ArrayList saldoMateriales, ModMaterial mate) {
        double val = 0d;
        for (int i = 0; i < saldoMateriales.size(); i++) {
            ModMaterial mat = ((ModMaterial) saldoMateriales.get(i)).copia();
            if (mat.getMateriaPrima().equals(mate.getMateriaPrima())) {
                val = mat.getSaldo();
            }
        }
        return val;
    }

    public static void actualizarSaldoBodega(ModEquipo equi, ArrayList saldoMateriales) {
        for (int i = 0; i < saldoMateriales.size(); i++) {
            ModMaterial mat = ((ModMaterial) saldoMateriales.get(i)).copia();
            if (mat.getMateriaPrima().equals(equi.getMaterial().getMateriaPrima()) && equi.getMaterial().getCantidad() > 0) {
                mat.setReservado(mat.getReservado() - equi.getMaterial().getCantRequerida());
                mat.setSaldo(mat.getStock() - mat.getReservado());
                saldoMateriales.set(i, mat);
            }
        }
    }

    //METODO QUE VALIDA QUE LA FECHA ENVIADA NO SEA MENOR A LA FECHA DE SISTEMA
    public static boolean validaFecha(String fec) throws ParseException {
        Date fechaActual = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        String fechaSis = formateador.format(fechaActual);
        Date fechaSistema = formateador.parse(fechaSis);
        Date fecha = formateador.parse(fec);
        if (fecha.before(fechaSistema)) {
            return false;
        } else {
            return true;
        }
    }

    public static Boolean buscaCoincidencia(ModMaterial mat, String buscar) {
        if (mat.getMateriaPrima().toLowerCase().indexOf(buscar.toLowerCase()) > -1
                || mat.getDescripcion().toLowerCase().indexOf(buscar.toLowerCase()) > -1
                || mat.getUm().toLowerCase().indexOf(buscar.toLowerCase()) > -1
                || String.valueOf(mat.getCantRequerida()).indexOf(buscar) > -1
                || String.valueOf(mat.getCantidadR()).indexOf(buscar) > -1
                || String.valueOf(mat.getStockMin()).indexOf(buscar) > -1
                || String.valueOf(mat.getStockMax()).indexOf(buscar) > -1
                || String.valueOf(mat.getSaldo()).indexOf(buscar) > -1
                || String.valueOf(mat.getLeadTime()).indexOf(buscar) > -1
                || String.valueOf(mat.getOc()).indexOf(buscar) > -1) {
            return true;
        } else {
            return false;
        }
    }

    public static ArrayList ordenarArreglo(ArrayList arreglo, int col, String dir) {
        if (dir.equals("asc")) {
            if (col == 0) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return mat1.getMateriaPrima().compareTo(mat2.getMateriaPrima());
                    }
                });
            }
            if (col == 1) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return mat1.getDescripcion().compareTo(mat2.getDescripcion());
                    }
                });
            }
            if (col == 2) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return mat1.getUm().compareTo(mat2.getUm());
                    }
                });
            }
            if (col == 3) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat1.getCantRequerida()).compareTo(new Double(mat2.getCantRequerida()));
                    }
                });
            }
            if (col == 4) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat1.getCantidadR()).compareTo(new Double(mat2.getCantidadR()));
                    }
                });
            }
            if (col == 5) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat1.getStockMin()).compareTo(new Double(mat2.getStockMin()));
                    }
                });
            }
            if (col == 6) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat1.getStockMax()).compareTo(new Double(mat2.getStockMax()));
                    }
                });
            }
            if (col == 7) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat1.getSaldo()).compareTo(new Double(mat2.getSaldo()));
                    }
                });
            }
            if (col == 8) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Integer(mat1.getLeadTime()).compareTo(new Integer(mat2.getLeadTime()));
                    }
                });
            }
            if (col == 9) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat1.getOc()).compareTo(new Double(mat2.getOc()));
                    }
                });
            }
        } else {
            if (col == 0) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return mat2.getMateriaPrima().compareTo(mat1.getMateriaPrima());
                    }
                });
            }
            if (col == 1) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return mat2.getDescripcion().compareTo(mat1.getDescripcion());
                    }
                });
            }
            if (col == 2) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return mat2.getUm().compareTo(mat1.getUm());
                    }
                });
            }
            if (col == 3) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat2.getCantRequerida()).compareTo(new Double(mat1.getCantRequerida()));
                    }
                });
            }
            if (col == 4) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat2.getCantidadR()).compareTo(new Double(mat1.getCantidadR()));
                    }
                });
            }
            if (col == 5) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat2.getStockMin()).compareTo(new Double(mat1.getStockMin()));
                    }
                });
            }
            if (col == 6) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat2.getStockMax()).compareTo(new Double(mat1.getStockMax()));
                    }
                });
            }
            if (col == 7) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat2.getSaldo()).compareTo(new Double(mat1.getSaldo()));
                    }
                });
            }
            if (col == 8) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Integer(mat2.getLeadTime()).compareTo(new Integer(mat1.getLeadTime()));
                    }
                });
            }
            if (col == 9) {
                Collections.sort(arreglo, new Comparator<ModMaterial>() {
                    @Override
                    public int compare(ModMaterial mat1, ModMaterial mat2) {
                        return new Double(mat2.getOc()).compareTo(new Double(mat1.getOc()));
                    }
                });
            }
        }
        return arreglo;
    }

    public static String htmlOrdenesCompra(ModSesion Sesion) {
        String html = "";
        html += "<div class='content-popup-ordcomp' style='padding-top: 6px'>";
        html += "<table width='100%'>";
        html += "<tr class='titulo-tabla-ordcomp'>";
        html += "<td width='40'>Orden</td>";
        html += "<td width='10' " + PrmApp.verOverLib("Operaciones", "Item de la orden") + ">It</td>";
        html += "<td width='70'>C�digo</td>";
        html += "<td width='120'>Descripci�n</td>";
        html += "<td width='20'>Um</td>";
        html += "<td width='40'>Cant</td>";
        html += "<td width='40'>Saldo</td>";
        html += "<td width='60'>Precio</td>";
        html += "<td width='60'>Emisi�n</td>";
        html += "<td width='60'>Entrega</td>";
        html += "<td width='60' " + PrmApp.verOverLib("Operaciones", "Fecha confirmada por proveedor") + ">Confirm</td>";
        html += "<td width='120'>Proveedor</td>";
        html += "<td width='40' " + PrmApp.verOverLib("Operaciones", "Saldar oc") + ">Sal</td>";
        html += "</tr>";
        html += "</table>";
        html += "</div>";
        html += "<div class='scrollTabla-ordcomp'>";
        html += "<table width='100%'>";
        if (Sesion.getMateriales().isEmpty()) {
            html += "<tr>";
            html += "<td  colspan='12' class='sinDatos'>SIN DATOS</td>";
            html += "</tr>";
        } else {
            for (int a = 0; a < Sesion.getMateriales().size(); a++) {
                ModOrdenCompra act = (ModOrdenCompra) Sesion.getMateriales().get(a);
                if ((a % 2) == 0) {
                    html += "<tr class='trPar'>" + "\n";
                } else {
                    html += "<tr class='trImPar'>" + "\n";
                }
                html += "<td width='40' class='Estilo10'>" + act.getNumOrdenCompra() + "</td>";
                html += "<td width='10' class='Estilo10'>" + act.getItem() + "</td>";
                html += "<td width='70' class='Estilo10'>" + act.getMateriaPrima() + "</td>";
                html += "<td width='120' class='Estilo10' " + PrmApp.verOverLib("MRP", act.getDescripcion()) + ">" + PrmApp.verHtmlNull(act.getDescripcion(), 15) + "</td>";
                html += "<td width='20' class='Estilo10'>" + act.getUm() + "</td>";
                html += "<td width='40' class='Estilo10'>" + act.getCantidad() + "</td>";
                html += "<td width='40' class='Estilo10'>" + act.getCantPenOc() + "</td>";
                html += "<td width='60' class='Estilo10'>" + act.getPrecio() + "</td>";
                html += "<td width='65' class='Estilo10'>" + act.getFechaEmision() + "</td>";
                html += "<td width='60' class='Estilo10'>" + act.getFechaEntrega() + "</td>";
                html += "<td width='120' class='Estilo10' " + PrmApp.verOverLib("MRP", act.getProveedor()) + ">" + PrmApp.verHtmlNull(act.getProveedor(), 15) + "</td>";
                html += "<td width='40' class='Estilo10'> <input type='checkbox' name='salOc" + a + "' id='salOc" + a + "' value='checkbox' /></td>";
                html += "</tr>" + "\n";
            }
        }
        html += "</table>";
        html += "</div>";
        html += "<p align='right'>";
        html += "<input name='saldarOc' type='submit' value='Saldar O/c' />";
        return html;
    }

    public static String htmlNecesidadMaterial(ModSesion Sesion) {
        String html = "";
        html += "<div class='content-popup-necmat' style='padding-top: 6px'>";
        html += "<table width='100%'>";
        html += "<tr>";
        html += "<td align='center'><b>Necesidad de material</b></td>";
        html += "</tr>";
        html += "</table>";
        html += "<table width='100%'>";
        html += "<tr ><td style='font-family:Verdana;font-size:10px' colspan='4' align='center'>C�digo: " + Sesion.getMaterial().getMateriaPrima() + " </td></tr>";
        html += "<tr class='titulo-tabla-tr-necmat'>";
        html += "<td class='titulo-tabla-necmat' width='150px'>Expeditacion</td>";
        html += "<td class='titulo-tabla-necmat' width='150px'>C�digo de Obra</td>";
        html += "<td class='titulo-tabla-necmat' width='350px'>Nombre Cliente</td>";
        html += "<td class='titulo-tabla-necmat'>Cantidad</td>";
        html += "</tr>";
        html += "</table>";
        html += "</div>";
        html += "<div class='scrollTabla-necmat'>";
        html += "<table width='100%'>";
        if (Sesion.getMateriales().isEmpty()) {
            html += "<tr>";
            html += "<td  colspan='10' class='sinDatos'>SIN DATOS</td>";
            html += "</tr>";
        } else {
            for (int a = 0; a < Sesion.getMateriales().size(); a++) {
                ModEquipo act = (ModEquipo) Sesion.getMateriales().get(a);
                if ((a % 2) == 0) {
                    html += "<tr class='trPar'>" + "\n";
                } else {
                    html += "<tr class='trImPar'>" + "\n";
                }
                html += "<td width='155px' class='Estilo10'>" + act.getExpeditacion() + "</td>";
                html += "<td width='150px' class='Estilo10'>" + act.getNp() + "</td>";
                html += "<td width='350px' class='Estilo10'>" + act.getNombreCliente() + "</td>";
                html += "<td class='Estilo10'>" + act.getMaterial().getCantidad() + "</td>";
                html += "</tr>" + "\n";
            }
        }
        html += "</table>";
        return html;
    }

    public static double entregado(ArrayList pedidos, String mat, HttpServletRequest req) {
        double val = 0d;
        for (int t = 0; t < pedidos.size(); t++) {
            ModEquipo equipo = (ModEquipo) pedidos.get(t);
            if (equipo.getMaterial().getMateriaPrima().equals(mat)) {
                val += Double.parseDouble(req.getParameter("traspasoBodega" + t).trim());
            }
        }
        return val;
    }

    public static Collection dataReportConsumo(ArrayList materiales, long numeroDoc, ArrayList saldoMateriales) {
        Collection lista = new ArrayList();
        java.text.NumberFormat number = new java.text.DecimalFormat("#######.#");
        number.setMinimumFractionDigits(0);
        number.setMaximumFractionDigits(2);
        for (int i = 0; i < materiales.size(); i++) {
            ModEquipo equi = (ModEquipo) materiales.get(i);
            if (equi.getMaterial().getNumero() == numeroDoc && equi.getMaterial().getCantidad() > 0) {
                lista.add(new Imprimir(equi.getMaterial().getMateriaPrima(), equi.getMaterial().getDescripcion(), equi.getMaterial().getUm(), number.format(HelperHidropack.stock(saldoMateriales, equi.getMaterial())), equi.getMaterial().getUbicacion(), number.format(equi.getMaterial().getCantidad()), String.valueOf(equi.getMaterial().getLinea())));
            }
        }
        return lista;
    }

    public static Collection dataReportConsumoUsuario(ArrayList materiales, long numeroDoc) {
        Collection lista = new ArrayList();
        java.text.NumberFormat number = new java.text.DecimalFormat("#######.#");
        number.setMinimumFractionDigits(0);
        number.setMaximumFractionDigits(2);
        for (int i = 0; i < materiales.size(); i++) {
            ModEquipo equi = (ModEquipo) materiales.get(i);
            if (equi.getMaterial().getNumero() == numeroDoc && equi.getMaterial().getCantidad() > 0) {
                lista.add(new Imprimir(equi.getMaterial().getMateriaPrima(), equi.getMaterial().getDescripcion(), equi.getMaterial().getUm(), equi.getMaterial().getUbicacion(), number.format(equi.getMaterial().getCantidad()), String.valueOf(equi.getMaterial().getLinea())));
            }
        }
        return lista;
    }

    public static String devolverDescripcion(String observacion) {
        String devolver = "";
        if (observacion == null || observacion.trim().length() == 0) {
            observacion = "Sin descripci�n";
        }
        devolver = observacion.replace("\n", " ");
        devolver = devolver.replace("\r", " ");
        devolver = devolver.replace("\t", " ");
        devolver = devolver.replace("/", " ");
        devolver = devolver.replace('"', ' ');
        devolver = devolver.replace('?', ' ');
        devolver = devolver.replace("'", " ");
        devolver = devolver.replace("(", " ");
        devolver = devolver.replace(")", " ");
        //devolver = devolver.replace("$"," ");
        return devolver;
    }
}
