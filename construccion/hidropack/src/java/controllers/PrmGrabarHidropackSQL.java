package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.ModEquipo;
import model.ModMaterial;
import model.ModPedidoMaterial;
import model.ModSesion;
import model.hidropack.ModObra;
import sql.ConexionSQL;

public class PrmGrabarHidropackSQL {

    public static boolean grabarSolicitudPrevioGuiaDespacho(ArrayList materiales, String usuario, ModObra obra, int trabajo) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estadoDet1 = false;
        String ccc = null;
        String vvv = null;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat periodoLibro = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat hora = new SimpleDateFormat("hhmmss");
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                Date date = new Date();
                long numeroDoc = PrmHidropack.ultimoFolioFisicoPrevioDespacho(cn);
                ccc = " MVE_CodSistema,MVE_CodClase,MVE_TipoDoc,MVE_NumeroDoc,MVE_FechaDoc,MVE_PeriodoLibro,MVE_TipoPersona,MVE_CodPersona,MVE_CodSucursal";
                ccc += " ,MVE_FolioFisico,MVE_Zona,MVE_Moneda,MVE_Paridad,MVE_TipoCambioBimoneda,MVE_SubTotal,MVE_Afecto,MVE_Exento,MVE_Iva,MVE_Impuesto1,MVE_Impuesto2";
                ccc += " ,MVE_Impuesto3,MVE_Impuesto4,MVE_Impuesto5,MVE_Impuesto6,MVE_Impuesto7,MVE_Impuesto8,MVE_Impuesto9,MVE_Impuesto10,MVE_Total,MVE_SubTotalMA";
                ccc += " ,MVE_AfectoMA,MVE_ExentoMA,MVE_IvaMA,MVE_Impuesto1MA,MVE_Impuesto2MA,MVE_Impuesto3MA,MVE_Impuesto4MA,MVE_Impuesto5MA,MVE_Impuesto6MA,MVE_Impuesto7MA";
                ccc += " ,MVE_Impuesto8MA,MVE_Impuesto9MA,MVE_Impuesto10MA,MVE_TotalMA,MVE_Estado,MVE_EstadoImpresion,MVE_FechaDelEstado,UsCreacion,FecCreacion,HoraCreacion,mve_analisisadic15,mve_analisisadic1,mve_analisisadic14";

                vvv = 6 + "�";
                vvv += 1 + "�";
                vvv += 10 + "�";
                vvv += numeroDoc + "�";
                vvv += fecha.format(date) + "�";
                vvv += periodoLibro.format(date) + "�";
                vvv += 1 + "�";
                vvv += obra.getCliente().getRut() + "�";
                vvv += 0 + "�";
                vvv += numeroDoc + "�";
                vvv += "N" + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 0 + "�";
                vvv += 1 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 0 + "�";
                vvv += 6 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 1 + "�";
                vvv += "V" + "�";
                vvv += "S" + "�";
                vvv += fecha.format(date) + "�";
                vvv += usuario + "�";
                vvv += fecha.format(date) + "�";
                vvv += hora.format(date) + "�";
                vvv += 2 + "�";
                vvv += obra.getIdObra() + "�";
                vvv += trabajo;


                if (cn.InsertReg("STO_MOVENC", ccc, vvv)) {
                    estado = true;
                } else {
                    estado = false;
                }
                int increment = 1;
                //boolean est = true;
                for (int a = 0; a < materiales.size(); a++) {
                    ModEquipo equi = (ModEquipo) materiales.get(a);
                    if (grabarDetalleSolicitudPrevioGuiaDespacho(cn, equi.getMaterial(), numeroDoc, usuario, increment, date, obra.getFecha())) {
                        //equi.getMaterial().setNumeroDoc(numeroDoc);
                        equi.getMaterial().setLinea(increment);
                        equi.getMaterial().setSolicitado(equi.getMaterial().getCantRequerida());
                        materiales.set(a, equi);
                        estadoDet1 = true;
                        increment++;
                    } else {
                        estadoDet1 = false;
                        break;
                    }
                }
                cn.commitSQL(estadoDet1 && estado);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return estado && estadoDet1;
    }

    public static boolean grabarDetalleSolicitudPrevioGuiaDespacho(ConexionSQL cn, ModMaterial material, long numeroDoc, String usuario, int increment, Date date, String fechaCliente) throws SQLException, Exception {
        boolean estadoDET = false;
        String ccc = null;
        String vvv = null;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat hora = new SimpleDateFormat("hhmmss");

        ccc = " MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_CodProd,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,MVD_FactorUMAlternativa";
        ccc += " ,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_Precio,MVD_PorcDRLinea1,MVD_PrecioAjustado,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_PrecioCosteo,MVD_PrecioCosteoBimoneda";
        ccc += " ,MVD_SubTotal,MVD_PrecioMA,MVD_PrecioAjustadoMA,MVD_PrecioCosteoMA,MVD_SubTotalMA,MVD_Fecha,MVD_Zona,MVD_NumeroSerie,MVD_NumeroLote,MVD_Estado";
        ccc += " ,MVD_Costeo,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,MVD_LineaOrigen,UsCreacion,FecCreacion,HoraCreacion,MVD_FechaEntrega";
        vvv = 6 + "�";
        vvv += 1 + "�";
        vvv += 10 + "�";
        vvv += numeroDoc + "�";
        vvv += increment + "�";
        vvv += material.getMateriaPrima() + "�";
        vvv += material.getTipoProd() + "�";
        vvv += String.format("%f", material.getCantRequerida()) + "�";
        vvv += 0 + "�";
        vvv += material.getUm() + "�";
        vvv += 1 + "�";
        vvv += String.format("%f", material.getCantRequerida()) + "�";
        vvv += 0 + "�";
        vvv += 1 + "�";
        vvv += 0 + "�";
        vvv += 1 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 1 + "�";
        vvv += 1 + "�";
        vvv += String.format("%f", material.getCantRequerida()) + "�";
        vvv += 1 + "�";
        vvv += 1 + "�";
        vvv += 1 + "�";
        vvv += String.format("%f", material.getCantRequerida()) + "�";
        vvv += fecha.format(date) + "�";
        vvv += "N" + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += "V" + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += material.getNumero() + "�";
        vvv += material.getLinea() + "�";
        vvv += usuario + "�";
        vvv += fecha.format(date) + "�";
        vvv += hora.format(date) + "�";
        vvv += fechaCliente;

        estadoDET = cn.InsertReg("STO_MOVDET", ccc, vvv);
        increment++;
        return estadoDET;
    }

    public static boolean grabarSolicitudAPlantaEspeciolOEstructura(ArrayList materiales, String usuario, String usuDestino, ModSesion Sesion) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estado1 = false;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat hora = new SimpleDateFormat("hhmmss");
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                Date date = new Date();
                long numero = PrmHidropack.ultimoFolioSolicitudAPlanta(cn);
                String ccc = "nb_numero,vc_usuario,nb_estado,d_fecha,t_hora,vc_usuario_destino,ch_empresa";
                String vvv = numero + "�";
                vvv += usuario + "�";
                vvv += 2 + "�";
                vvv += fecha.format(date) + "�";
                vvv += hora.format(date) + "�";
                vvv += usuDestino + "�";
                vvv += "H";
                if (cn.InsertReg("TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC", ccc, vvv)) {
                    estado = true;
                }
                if (estado) {
                    for (int i = 0; i < materiales.size(); i++) {
                        ModEquipo equi = (ModEquipo) materiales.get(i);
                        if (equi.getMaterial().getSolicitado() > 0) {
                            if (grabarDetallePedidoAPlanta(cn, equi.getMaterial(), usuario, date, numero)) {
                                estado1 = true;
                            } else {
                                estado1 = false;
                                break;
                            }
                        }
                    }
                }
                Sesion.setNumeroDoc(numero);
                cn.commitSQL(estado && estado1);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }

        return estado && estado1;
    }

    public static boolean grabarDetallePedidoAPlanta(ConexionSQL cn, ModMaterial material, String usuario, Date date, long numero) throws SQLException, Exception {
        String ccc = "nb_numero,vc_codigo,vc_proceso,nb_numero_doc_original,nb_solicitado,nb_numero_linea_original";
        String vvv = numero + "�";
        vvv += material.getMateriaPrima() + "�";
        vvv += material.getProceso() + "�";
        vvv += material.getNumeroDoc() + "�";
        vvv += String.format("%f", material.getSolicitado()) + "�";
        vvv += material.getLinea();
        return cn.InsertReg("TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET", ccc, vvv);
    }

    public static boolean actualizarCantEstadoDetallePedidoDeFabricacion(long numeroSolicitud) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                estado = cn.UpdateReg("TBL_HID_SOLICITUD_DE_FABRICACION_ENC", "nb_estado", "1", "nb_numero_solicitud = " + numeroSolicitud);
                cn.commitSQL(estado);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return estado;
    }

    /*public static boolean actualizarPedidoDeFabricacion(ConexionSQL cn, ModPedidoMaterial sol) throws SQLException, Exception {
        boolean est = false;
        try {
            est = cn.UpdateRegSinPrefix("MAC2BETA.dbo.TBL_HID_SOLICITUD_DE_FABRICACION_ENC", "nb_estado", "3", "nb_numero_solicitud = " + sol.getNumeroDoc());
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean actualizarPedidoDeFabricacion(ModPedidoMaterial sol) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String ccc = "nb_estado,dt_fecha_fin";
                String vvv = "2" + "�";
                vvv += fecha.format(new Date());
                est = cn.UpdateReg("TBL_HID_SOLICITUD_DE_FABRICACION_ENC", ccc, vvv, "nb_numero_solicitud = " + sol.getNumeroDoc());
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }*/

    public static boolean grabarSolicitudCompra(ModSesion Sesion) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estado1 = false;
        SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                Date date = new Date();
                long numero = PrmHidropack.ultimoFolioCompra(cn);
                String ccc = "nb_numero, nb_obra, dt_fecha_emision, dt_fecha_entrega, vc_user";
                String vvv = numero + "�";
                vvv += Sesion.getObra().getIdObra() + "�";
                vvv += fecha.format(date) + "�";
                vvv += Sesion.getObra().getFechaEntregaMaterial() + "�";
                vvv += Sesion.getUsuario().getUsuario();
                //long num = PrmHidropack.existeCompra(cn, Sesion.getObra().getIdObra());
                ModEquipo equi1 = (ModEquipo) Sesion.getPedidosEspeciales().get(0);
                if (equi1.getMaterial().getNumero() > 0) {
                    if (cn.UpdateReg("TBL_LOG_COMPRA_MATERIAL_ENC", "dt_fecha_entrega", Sesion.getObra().getFechaEntregaMaterial(), "nb_numero = " + equi1.getMaterial().getNumero())) {
                        if (cn.DeleteReg("TBL_LOG_COMPRA_MATERIAL_DET", "nb_numero = " + equi1.getMaterial().getNumero())) {
                            estado = true;
                            numero = equi1.getMaterial().getNumero();
                        }
                    }
                } else {
                    if (cn.InsertReg("TBL_LOG_COMPRA_MATERIAL_ENC", ccc, vvv)) {
                        estado = true;
                    }
                }
                if (estado) {
                    int increment = 1;
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        ModEquipo equi = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                        if (equi.getMaterial().getCantRequerida() > 0) {
                            if (grabarDetalleCompra(cn, equi.getMaterial(), numero, increment)) {
                                estado1 = true;
                                increment++;
                            } else {
                                estado1 = false;
                                break;
                            }
                        }
                    }
                }
                //Sesion.setNumeroDoc(numero);
                cn.commitSQL(estado && estado1);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return estado && estado1;
    }

    public static boolean grabarDetalleCompra(ConexionSQL cn, ModMaterial material, long numero, int increment) throws SQLException, Exception {
        String ccc = "nb_numero, vc_codigo, nb_cantidad,nb_linea";
        String vvv = numero + "�";
        vvv += material.getMateriaPrima() + "�";
        vvv += String.format("%f", material.getCantRequerida()) + "�";
        vvv += increment;
        return cn.InsertReg("TBL_LOG_COMPRA_MATERIAL_DET", ccc, vvv);
    }

    public static boolean actualizarEstadoPedidoSinEntregar(long numeroDoc, byte estado) {
        boolean est = false;
        ConexionSQL cn = new ConexionSQL();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                est = cn.UpdateReg("STO_MOVENC", "mve_analisisadic15", String.valueOf(estado), "MVE_FolioFisico = " + numeroDoc + " and MVE_CodSistema = 6 and MVE_CodClase = 1 and MVE_TipoDoc = 10");
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean grabarTraspasoDeBodega(ArrayList materiales, String usuario, long numDoc, boolean est, ModPedidoMaterial pedidoMaterial, ModSesion Sesion) {
        ConexionSQL cn = new ConexionSQL();
        boolean estado = false;
        boolean estadoDet1 = false;
        boolean estadoDet2 = false;
        boolean estadoDet3 = false;
        boolean estadoDet4 = false;
        boolean estadoDet5 = false;
        String ccc = null;
        String vvv = null;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat hora = new SimpleDateFormat("hhmmss");
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                Date date = new Date();
                long numeroDoc = PrmHidropack.ultimoFolioFisicoTraspasoDeBodega(cn);
                ccc = " MVE_CodSistema,MVE_CodClase,MVE_TipoDoc,MVE_NumeroDoc,MVE_FechaDoc,MVE_FolioFisico,MVE_Zona,MVE_Moneda,MVE_Paridad,MVE_TipoCambioBimoneda";
                ccc += " ,MVE_Subtotal,MVE_Afecto,MVE_Exento,MVE_Iva,MVE_Total,MVE_SubTotalMA,MVE_AfectoMA,MVE_ExentoMA,MVE_IvaMA,MVE_TotalMA,MVE_Estado,MVE_FechaDelEstado";
                ccc += " ,MVE_Bodega1,MVE_Bodega2,UsCreacion,FecCreacion,HoraCreacion,MVE_AjusteIva";

                vvv = 8 + "�";
                vvv += 4 + "�";
                vvv += 4 + "�";
                vvv += numeroDoc + "�";
                vvv += fecha.format(date) + "�";
                vvv += numeroDoc + "�";
                vvv += "N" + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 1 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += 0 + "�";
                vvv += "V" + "�";
                vvv += fecha.format(date) + "�";
                vvv += 1 + "�";
                vvv += 2 + "�";
                vvv += usuario + "�";
                vvv += fecha.format(date) + "�";
                vvv += hora.format(date) + "�";
                vvv += 0;


                if (cn.InsertReg("STO_MOVENC", ccc, vvv)) {
                    estado = true;
                } else {
                    estado = false;
                }
                int increment = 1;
                for (int a = 0; a < materiales.size(); a++) {
                    ModEquipo equi = (ModEquipo) materiales.get(a);
                    if (equi.getMaterial().getNumero() == numDoc && equi.getMaterial().getEntregado() > 0) {
                        if (grabarDetalleTraspasoDeBodegaPositivo(cn, equi.getMaterial(), numeroDoc, usuario, increment, date)) {
                            equi.getMaterial().setLinea(increment);
                            materiales.set(a, equi);
                            estadoDet1 = true;
                            increment++;
                        } else {
                            estadoDet1 = false;
                            break;
                        }
                    }
                }
                increment = -1;
                for (int a = 0; a < materiales.size(); a++) {
                    ModEquipo equi = (ModEquipo) materiales.get(a);
                    if (equi.getMaterial().getNumero() == numDoc && equi.getMaterial().getEntregado() > 0) {
                        if (grabarDetalleTraspasoDeBodegaNegativo(cn, equi.getMaterial(), numeroDoc, usuario, increment, date)) {
                            equi.getMaterial().setLinea(increment);
                            materiales.set(a, equi);
                            estadoDet2 = true;
                            increment--;
                        } else {
                            estadoDet2 = false;
                            break;
                        }
                    }
                }

                for (int a = 0; a < materiales.size(); a++) {
                    ModEquipo equi = (ModEquipo) materiales.get(a);
                    if (equi.getMaterial().getNumero() == numDoc && equi.getMaterial().getEntregado() > 0) {
                        if (traspasoABodega(cn, equi.getMaterial(), usuario, date)) {
                            estadoDet3 = true;
                        } else {
                            estadoDet3 = false;
                            break;
                        }
                    }
                }

                for (int a = 0; a < materiales.size(); a++) {
                    ModEquipo equi = (ModEquipo) materiales.get(a);
                    if (equi.getMaterial().getNumero() == numDoc && equi.getMaterial().getEntregado() > 0) {
                        if (ingresoABodega(cn, equi.getMaterial(), usuario, date)) {
                            estadoDet4 = true;
                        } else {
                            estadoDet4 = false;
                            break;
                        }
                    }
                }

                if (!est) {
                    estadoDet5 = actualizarEstadoPedidoSinEntregar(cn, numDoc, (byte) 5);
                    if (estadoDet5) {
                        pedidoMaterial.setEstado((byte) 5);
                    }
                }
                if (est) {
                    if (estadoDet2 && estadoDet1 && estado && estadoDet3 && estadoDet4) {
                        Sesion.setNumeroDoc(numeroDoc);
                    }
                    cn.commitSQL(estadoDet2 && estadoDet1 && estado && estadoDet3 && estadoDet4);
                } else {
                    if (estadoDet2 && estadoDet1 && estado && estadoDet3 && estadoDet4 && estadoDet5) {
                        Sesion.setNumeroDoc(numeroDoc);
                    }
                    cn.commitSQL(estadoDet2 && estadoDet1 && estado && estadoDet3 && estadoDet4 && estadoDet5);
                }

                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        if (est) {
            return estado && estadoDet1 && estadoDet2 && estadoDet3 && estadoDet4;
        } else {
            return estado && estadoDet1 && estadoDet2 && estadoDet3 && estadoDet4 && estadoDet5;
        }
    }

    public static boolean grabarDetalleTraspasoDeBodegaPositivo(ConexionSQL cn, ModMaterial material, long numeroDoc, String usuario, int increment, Date date) throws SQLException, Exception {
        boolean estadoDET = false;
        String ccc = null;
        String vvv = null;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat hora = new SimpleDateFormat("hhmmss");

        ccc = " MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_Codprod,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,MVD_FactorUMAlternativa";
        ccc += " ,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_PorcDRLinea1,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_Fecha,MVD_NumeroSerie,MVD_NumeroLote,MVD_Estado";
        ccc += " ,MVD_Costeo,MVD_Bodega1,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,MVD_LineaOrigen,UsCreacion";
        ccc += " ,FecCreacion,HoraCreacion";
        vvv = 8 + "�";
        vvv += 4 + "�";
        vvv += 4 + "�";
        vvv += numeroDoc + "�";
        vvv += increment + "�";
        vvv += material.getMateriaPrima() + "�";
        vvv += material.getTipoProd() + "�";
        vvv += String.format("%f", material.getEntregado()) + "�";
        vvv += 0 + "�";
        vvv += material.getUm() + "�";
        vvv += 1 + "�";
        vvv += String.format("%f", material.getEntregado()) + "�";
        vvv += -1 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += fecha.format(date) + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += "V" + "�";
        vvv += 0 + "�";
        vvv += 1 + "�";
        vvv += 0 + "�";
        vvv += 1 + "�";
        vvv += 10 + "�";
        vvv += material.getNumero() + "�";
        vvv += material.getLinea() + "�";
        vvv += usuario + "�";
        vvv += fecha.format(date) + "�";
        vvv += hora.format(date);

        estadoDET = cn.InsertReg("STO_MOVDET", ccc, vvv);
        increment++;
        return estadoDET;
    }

    public static boolean grabarDetalleTraspasoDeBodegaNegativo(ConexionSQL cn, ModMaterial material, long numeroDoc, String usuario, int increment, Date date) throws SQLException, Exception {
        boolean estadoDET = false;
        String ccc = null;
        String vvv = null;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat hora = new SimpleDateFormat("hhmmss");

        ccc = " MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_Codprod,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,MVD_FactorUMAlternativa";
        ccc += " ,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_PorcDRLinea1,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_Fecha,MVD_NumeroSerie,MVD_NumeroLote,MVD_Estado";
        ccc += " ,MVD_Costeo,MVD_Bodega1,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,MVD_LineaOrigen,UsCreacion";
        ccc += " ,FecCreacion,HoraCreacion";
        vvv = 8 + "�";
        vvv += 4 + "�";
        vvv += 4 + "�";
        vvv += numeroDoc + "�";
        vvv += increment + "�";
        vvv += material.getMateriaPrima() + "�";
        vvv += material.getTipoProd() + "�";
        vvv += String.format("%f", material.getEntregado()) + "�";
        vvv += 0 + "�";
        vvv += material.getUm() + "�";
        vvv += 1 + "�";
        vvv += String.format("%f", material.getEntregado()) + "�";
        vvv += 1 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += fecha.format(date) + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += "V" + "�";
        vvv += 0 + "�";
        vvv += 2 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += 0 + "�";
        vvv += usuario + "�";
        vvv += fecha.format(date) + "�";
        vvv += hora.format(date);

        estadoDET = cn.InsertReg("STO_MOVDET", ccc, vvv);
        increment++;
        return estadoDET;
    }

    private static boolean traspasoABodega(ConexionSQL cn, ModMaterial material, String usuario, Date date) throws SQLException, Exception {
        ResultSet rs;
        String ccc = null;
        String vvv = null;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat hora = new SimpleDateFormat("HHmmss");
        double saldo = 0d;

        rs = cn.SelectReg("STO_PRODSAL", "PSL_Saldo", "PSL_CodProd = '" + material.getMateriaPrima() + "' and PSL_CodBodega = 1");
        while (rs.next()) {
            saldo = rs.getDouble("PSL_Saldo");
        }
        rs.close();

        if (saldo > 0 && saldo >= material.getEntregado()) {
            ccc = "PSL_Saldo,UsModif,FecModif,HoraModif";
            vvv = String.format("%f", (saldo - material.getEntregado())) + "�";
            vvv += usuario + "�";
            vvv += fecha.format(date) + "�";
            vvv += hora.format(date);
            return cn.UpdateReg("STO_PRODSAL", ccc, vvv, "PSL_CodProd = '" + material.getMateriaPrima() + "' and PSL_CodBodega = 1");
        } else {
            return false;
        }
    }

    private static boolean ingresoABodega(ConexionSQL cn, ModMaterial sol, String usuario, Date date) throws SQLException, Exception {
        ResultSet rs;
        String ccc = null;
        String vvv = null;
        SimpleDateFormat fecha = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat hora = new SimpleDateFormat("HHmmss");
        double saldo = 0d;
        boolean est = false;
        boolean est1 = false;

        rs = cn.SelectReg("STO_PRODSAL", "PSL_Saldo", "PSL_CodProd = '" + sol.getMateriaPrima() + "' and PSL_CodBodega = 2", "");
        while (rs.next()) {
            saldo = rs.getDouble("PSL_Saldo");
            est = true;
        }
        rs.close();

        if (est) {
            ccc = "PSL_Saldo,UsModif,FecModif,HoraModif";
            vvv = String.format("%f", (saldo + sol.getEntregado())) + "�";
            vvv += usuario + "�";
            vvv += fecha.format(date) + "�";
            vvv += hora.format(date);
            est1 = cn.UpdateReg("STO_PRODSAL", ccc, vvv, "PSL_CodProd = '" + sol.getMateriaPrima() + "' and PSL_CodBodega = 2");
        } else {
            String sql = "select max(id_fila) from STO_PRODSAL";
            int fila = 0;
            rs = cn.ExecuteQuery(sql);
            while (rs.next()) {
                fila = rs.getInt(1) + 1;
            }
            rs.close();
            ccc = "PSL_CodProd, PSL_CodBodega, PSL_Lote, PSL_Serie, PSL_Saldo, UsCreacion, FecCreacion, HoraCreacion, UsModif, FecModif, HoraModif, id_fila";
            vvv = sol.getMateriaPrima() + "�";
            vvv += 2 + "�";
            vvv += 0 + "�";
            vvv += 0 + "�";
            vvv += String.format("%f", sol.getEntregado()) + "�";
            vvv += usuario + "�";
            vvv += fecha.format(date) + "�";
            vvv += hora.format(date) + "�";
            vvv += usuario + "�";
            vvv += fecha.format(date) + "�";
            vvv += hora.format(date) + "�";
            vvv += fila;
            est1 = cn.InsertReg("STO_PRODSAL", ccc, vvv);
        }
        return est1;
    }

    private static boolean actualizarEstadoPedidoSinEntregar(ConexionSQL cn, long numeroDoc, byte estado) throws SQLException, Exception {
        return cn.UpdateReg("STO_MOVENC", "mve_analisisadic15", String.valueOf(estado), "MVE_FolioFisico = " + numeroDoc + " and MVE_CodSistema = 6 and MVE_CodClase = 1 and MVE_TipoDoc = 10");
    }

    public static boolean actualizarCambiarMaterial(int estado, long numero, int linea) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String vvv = "nb_estado";
                String ccc = String.valueOf(estado);
                est = cn.UpdateReg("TBL_LOG_COMPRA_MATERIAL_DET", vvv, ccc, "nb_numero = " + numero + " and nb_linea = " + linea);
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static boolean actualizarCambiarCodigo(long numeroDoc, int linea, long numeroDocOrigen, int lineaOrigen, String codigo) {
        boolean est = false;
        boolean est1 = false;
        ConexionSQL cn = new ConexionSQL();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                est = cn.UpdateReg("STO_MOVDET", "MVD_CodProd", codigo, " MVD_CodSistema = 6 and MVD_CodClase = 1 and MVD_TipoDoc = 10 and MVD_NumeroDoc = " + numeroDoc + " and MVD_Linea = " + linea);
                est1 = cn.UpdateRegSinPrefix("HIDROPACK.dbo.TBL_LOG_COMPRA_MATERIAL_DET", "vc_codigo,nb_estado", codigo + "�" + 2, "nb_numero = " + numeroDocOrigen + " and nb_linea = " + lineaOrigen);
                cn.commitSQL(est && est1);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est && est1;
    }

    public static boolean actualizarOc(long oc, int item, String usuario, double cant) {
        ConexionSQL cn = new ConexionSQL();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formateador = new SimpleDateFormat("HHmmss");
        boolean estado = false;
        String ccc = null;
        String vvv = null;
        String condicion = null;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                ccc = "MVD_CantAsignada,Pmp,UsModif,FecModif,HoraModif";
                vvv = cant + "�";
                vvv += "C" + "�";
                vvv += usuario + "�";
                vvv += formatDate.format(new java.util.Date()) + "�";
                vvv += formateador.format(new java.util.Date());
                condicion = "MVD_CodSistema=7 and MVD_CodClase =1 AND MVD_TipoDoc = 1 and "
                        + "  MVD_NumeroDoc = (select MVE_NumeroDoc from STO_MOVENC where "
                        + "  MVE_CodSistema=7 and "
                        + "  MVE_CodClase =1 AND "
                        + "  MVE_TipoDoc = 1 AND     "
                        + "  MVE_FolioFisico = '" + oc + "' "
                        + "  ) AND "
                        + "  MVD_Linea = '" + item + "' ";
                estado = cn.UpdateReg("STO_MOVDET", ccc, vvv, condicion);
                cn.commitSQL(estado);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return estado;
    }

    public static boolean eliminarMaterialRequerido(long numero, int linea, long numeroOrigen, int lineaOrigen) {
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                est = cn.DeleteReg("TBL_LOG_COMPRA_MATERIAL_DET", "nb_numero = " + numeroOrigen + " and nb_linea = " + lineaOrigen);
                if (est) {
                    est = eliminarMaterialRequeridoSia(cn, numero, linea);
                }
                cn.commitSQL(est);
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    private static boolean eliminarMaterialRequeridoSia(ConexionSQL cn, long numero, int linea) throws Exception {
        return cn.DeleteRegSinPrefix("SiaHidropack.dbo.STO_MOVDET", "MVD_CodSistema=6 and MVD_CodClase =1 AND MVD_TipoDoc = 10 and MVD_NumeroDoc = " + numero + " and MVD_Linea = " + linea);
    }
}
