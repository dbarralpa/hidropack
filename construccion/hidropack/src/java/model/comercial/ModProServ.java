/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.comercial;

/**
 *
 * @author dbarra
 */
public class ModProServ {

    private int codigo;
    private String descripcion;
    private String tipo;

    public ModProServ() {
        codigo = 0;
        descripcion = "";
        tipo = "";
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
