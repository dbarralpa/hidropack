<%@page import="controllers.PrmApp"%>
<jsp:useBean id="LOAD_ONSUBMIT" scope="request" class="java.lang.String" />
<jsp:useBean id="LOAD_ONCLICK" scope="request" class="java.lang.String" />
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"            scope="page"    class="view.VwHidropack" />
<%response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0,max-age=0");%>
<link rel="stylesheet" href="../css/reveal.css">	
<style type="text/css">
    .big-link { display:block; margin-top: 100px; text-align: center; font-size: 70px; color: #06f; }
</style>
<script type="text/javascript" src="../js/popcalendar.js"></script>
<script type="text/javascript" src="../js/initcalendar.js"></script>  
<script type="text/javascript" src="../js/jquery.reveal.js"></script>
<script>
    $(document).ready(function() {
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) {
                $("form").submit();
            }
        });
    }); 
    
    function asignarCodigoOrden(val){
        $.get("../jsp/lnk_datos_monitorear.jsp", {
            codigo: val,
            pag: 1
            
        }, function(data){
            $("#myModal").html(data);
        });			
    
    }

    function asignarCodigoNecMat(val){
        $.get("../jsp/lnk_datos_monitorear.jsp", {
            codigo: val,
            pag: 2
            
        }, function(data){
            $("#myModal").html(data);
        });			
    
    }
    
    function confirmar(val){
        if (confirm('�Seguro de eliminar?')){
            document.Frmidropack.submit();
        }else{
            val.checked = false;
            return false;
        }
    } 
    function validarFechaEntrega(){
        if (document.Frmidropack.fEntrega.value  == ""){
            alert('Debe ingresar fecha de entrega');
            return false;
        }
    }
    
    function validarObra(){
        if (document.Frmidropack.estadoPorObra.value  == ""){
            alert('Debe ingresar n� de obra');
            return false;
        }
    }
    
</script>

<form action="../FrmHidropack" method="post" <%=LOAD_ONSUBMIT%> name="Frmidropack">

    <input name="frm" type="hidden" value="hidropack"/>
    <table cellpadding='0' cellspacing='0'>
        <%if (Sesion.getEtapaHidropack() == 1) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK</td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 10px"><font color="#336600">Requerimientos</td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">
                <%if (PrmApp.permisosSeccion("solicitarCompraH", Sesion)) {%>
                <a href="../FrmHidropack?etapaHidropack12=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">Solicitar compra</font></a>
                    <%} else {%>
                <font color="#336600" class="Estilo1">Solicitar compra</font>
                <%}%>
            </td> 
        </tr>
        <tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">
                <%if (PrmApp.permisosSeccion("pedidoDeMaterialesH", Sesion)) {%>
                <a href="../FrmHidropack?etapaHidropack2=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">Pedido materiales a bodega</font></a>
                    <%} else {%>
                <font color="#336600" class="Estilo1">Pedido materiales a bodega</font>
                <%}%>
            </td> 
        </tr>
        <tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">
                <%if (PrmApp.permisosSeccion("bodegaH", Sesion)) {%>
                <a href="../FrmHidropack?bodegaH=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">BMP</font></a>
                    <%} else {%>
                <font color="#336600" class="Estilo1">BMP</font>
                <%}%>
            </td> 
        </tr>
        <tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">
                <%if (PrmApp.permisosSeccion("MrpH", Sesion)) {%>
                <a href="../FrmHidropack?MrpH=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">Mrp</font></a>
                    <%} else {%>
                <font color="#336600" class="Estilo1">Mrp</font>
                <%}%>
            </td> 
        </tr>


        <%} else if (Sesion.getEtapaHidropack() == 2) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  PEDIDO MATERIALES A BODEGA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
                <font color="#336600">&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?solicitarMateriales=SI" class="Estilo1" onclick="javascript:loading();">Solicitar materiales</a>&nbsp;&nbsp;&nbsp;|</font>

            </td>
        </tr>

        <%} else if (Sesion.getEtapaHidropack() == 3) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  PEDIDO MATERIALES A BODEGA - BUSCAR OBRA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px">
                <table cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class="Estilo10">N�mero de obra:&nbsp; <input type="text" name="numeroObra" id="numeroObra">&nbsp;<input type="submit" name="buscarObra" id="buscarObra" value="Buscar" class="Estilo10"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <%} else if (Sesion.getEtapaHidropack() == 4) {%>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  PEDIDO MATERIALES A BODEGA - SOLICITAR MATERIALES</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
                            <font color="#336600">&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack2=SI" class="Estilo1" onclick="javascript:loading();">Buscar obra</a>&nbsp;&nbsp;&nbsp;|</font>
                            <font color="#336600">&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?materialesPedidosABodega=SI" class="Estilo1" onclick="javascript:loading();">Materiales pedidos</a>&nbsp;&nbsp;&nbsp;|</font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top"  style="padding-top: 20px">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="C�digo"/>
                                <jsp:param name="campo2" value="Descripci�n"/>
                                <jsp:param name="campo3" value="yes"/>
                            </jsp:include>
                        </td>
                        <td style="padding-left: 20px">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Estilo10">N� obra: &nbsp;<%=Sesion.getObra().getIdObra()%></td>
                                </tr>
                                <tr>
                                    <td class="Estilo10">Nombre obra: &nbsp;<%=Sesion.getObra().getNombreObra()%></td>
                                </tr>
                                <tr>
                                    <td class="Estilo10">Rut Cliente: &nbsp;<%=Sesion.getObra().getCliente().getRut()%></td>
                                </tr>
                                <tr>
                                    <td class="Estilo10">Nombre Cliente: &nbsp;<%=Sesion.getObra().getCliente().getNombre()%></td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px">
                                        <%=vw.comboTrabajo(request)%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
        <%=vw.grillaSolicitudMaterialesObra(request)%>
        <%} else if (Sesion.getEtapaHidropack() == 5) {%>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  BMP  -  BUSCAR OBRA</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px">
                <table cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class="Estilo10">N�mero de obra:&nbsp; <input type="text" name="numeroObra" id="numeroObra">&nbsp;<input type="submit" name="buscarObraBodega" id="buscarObraBodega" value="Buscar" class="Estilo10"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <%=vw.grillaObraPendiente(request)%>
        </tr>
        <%} else if (Sesion.getEtapaHidropack() == 7) {%>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  PEDIDO MATERIALES A BODEGA - SOLICITAR MATERIALES - MATERIALES PEDIDOS A BODEGA</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
                <font color="#336600">&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?solicitarMateriales2=SI" class="Estilo1" onclick="javascript:loading();">Solicitar materiales</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px">
                <%=Sesion.getPset().verPaginador("SIN", true, true, 10, 15)%> 
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px;">
                <a href="../PedidoBodega" target="_blank">
                    <img src="../img/btn_excel.gif" width="30" height="30" border="0" <%=vw.verOverTip("Haga click para exportar a Excel")%>/>
                </a>
            </td>
        </tr>
        <%if (Sesion.getHttp_Body().equals("hidropackCambiarCodigo")) {%>
        <tr>
            <td>
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda "/>
                    <jsp:param name="campo1" value="C�digo"/>
                    <jsp:param name="campo2" value="Descripci�n"/>
                    <jsp:param name="campo3" value="yes"/>
                </jsp:include>
            </td>
        </tr>
        <%}%>
        <%=vw.grillaPedidosABodega(request)%>
        <%} else if (Sesion.getEtapaHidropack() == 9) {%>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  BMP</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
                <font color="#336600">&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?bodegaH=SI" class="Estilo1" onclick="javascript:loading();">Buscar obra</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px">
                <%=Sesion.getPset().verPaginador("SIN", true, true, 10, 15)%> 
            </td>
        </tr>
        <%=vw.grillaBodegaProcesar(request)%>
        <%} else if (Sesion.getEtapaHidropack() == 12) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  OBRA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px">
                <table cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class="Estilo10">N�mero de obra:&nbsp; <input type="text" name="numeroObra" id="numeroObra">&nbsp;<input type="submit" name="buscarObraCompra" id="buscarObraCompra" value="Buscar" class="Estilo10"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <%} else if (Sesion.getEtapaHidropack() == 13) {%>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  SOLICITAR COMPRA MATERIAL</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top"  style="padding-top: 20px">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="C�digo"/>
                                <jsp:param name="campo2" value="Descripci�n"/>
                                <jsp:param name="campo3" value="yes"/>
                            </jsp:include>
                        </td>
                        <td style="padding-left: 20px">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Estilo10">N� obra: &nbsp;<%=Sesion.getObra().getIdObra()%></td>
                                </tr>
                                <tr>
                                    <td class="Estilo10">Nombre obra: &nbsp;<%=Sesion.getObra().getNombreObra()%></td>
                                </tr>
                                <tr>
                                    <td class="Estilo10">Rut Cliente: &nbsp;<%=Sesion.getObra().getCliente().getRut()%></td>
                                </tr>
                                <tr>
                                    <td class="Estilo10">Nombre Cliente: &nbsp;<%=Sesion.getObra().getCliente().getNombre()%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <td style="padding-top: 10px;padding-bottom: 10px">
            <table border="0" cellpadding="0" cellspacing="0" class="Contenido">
                <tr class="FilaResaltada">
                    <td height="20" class="Estilo10">Fecha materiales en bodega</td>
                    <td align="right">:</td>
                    <td align="right"><input id="fEntrega" name="fEntrega" type="text" size="15" readonly="yes" class="txtFecha" value="" /></td>
                    <td><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fEntrega'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                </tr>
            </table>
        </td>
        <%=vw.grillaSolicitudCompraMaterialesObra(request)%>
        <%} else if (Sesion.getEtapaHidropack() == 14) {%>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >HIDROPACK  -  MRP</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmHidropack?etapaHidropack1=SI" class="Estilo1" onclick="javascript:loading();">Requerimientos</a>&nbsp;&nbsp;&nbsp;|</font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px" >
                <jsp:include page="../jsp/tabla.jsp">
                    <jsp:param name="val" value="1"/>
                </jsp:include> 
            </td>
        </tr>
        <%}%>
    </table>


    <div id="myModal" class="reveal-modal">


    </div>


</form>

