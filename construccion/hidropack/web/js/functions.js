function getElemento(obj){
    return document.getElementById(obj);
}

function showOverLib(titulo, contenido, fg, bg){
    overlib(contenido, CAPTION, titulo,
        FGCOLOR, fg,
        BGCOLOR, bg,
        BORDER, 1,
        CAPTIONFONT, "Verdana",
        CAPTIONSIZE, 1,
        TEXTFONT,"Verdana",
        TEXTSIZE, 1);
}

function changeValue(obj, texto){
    var vItem = getElemento(obj);
    vItem.value=texto;
}

function setFechaHora(obj){
    date = new Date();
    ano = date.getYear();
    mes = date.getMonth()+1;
    dia = date.getDate();
    hora = date.getHours();
    minuto = date.getMinutes();
    segundo = date.getSeconds();    
    if(dia<10){
        dia = "0"+dia;
    }
    if(mes<10){
        mes = "0"+mes;
    }
    if(segundo<10){
        segundo = "0"+segundo;
    }
    if(minuto<10){
        minuto = "0"+minuto;
    }
    dato = dia + "/" + mes + "/" + ano + " " + hora + ":" + minuto + ":" + segundo;
    changeValue(obj, dato);
}

 function tableDiv(cant){
            var ancho1,ancho2,i;
            var columnas=cant; //CANTIDAD DE COLUMNAS//
            for(i=0;i<columnas;i++){
                
                ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
                ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
                if(ancho1>ancho2){
                    document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-2;
                }else{
                    document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-2;
                }
                
            }
        }
