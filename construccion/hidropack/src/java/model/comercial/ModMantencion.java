package model.comercial;

public class ModMantencion {

    private long numero;
    private String nombre;
    private String observacion;
    private String fecha;
    private String aux;
    private int frecuencia;
    private float uf;
    private int valorUf;
    private ModCliente cliente;

    public ModMantencion() {
        nombre = "";
        observacion = "";
        fecha = "";
        aux = "";
        cliente = new ModCliente();
    }

    public String getAux() {
        return aux;
    }

    public void setAux(String aux) {
        this.aux = aux;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getValorUf() {
        return valorUf;
    }

    public void setValorUf(int valorUf) {
        this.valorUf = valorUf;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(int frecuencia) {
        this.frecuencia = frecuencia;
    }

    public float getUf() {
        return uf;
    }

    public void setUf(float uf) {
        this.uf = uf;
    }

    public ModCliente getCliente() {
        return cliente;
    }

    public void setCliente(ModCliente cliente) {
        this.cliente = cliente;
    }
}
