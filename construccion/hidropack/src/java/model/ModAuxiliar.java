package model;

import java.util.ArrayList;

public class ModAuxiliar implements Cloneable {

    private int ID;
    private String nombre;
    private double valor;

    public ModAuxiliar() {
        ID = 0;
        nombre = "";
        valor = 0.0;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public static double redondear(double numero, int decimales) {

        return Math.round(numero * Math.pow(10, decimales)) / Math.pow(10, decimales);

    }

    public ModAuxiliar copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModAuxiliar) obj;
    }
}
