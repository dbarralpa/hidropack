package excel;

import controllers.PrmApp;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModSesion;
import model.hidropack.ModObra;

public class MantencionCorrectiva extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/mantencionCorrectiva.xls");
            int indice = 7;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModObra obra = (ModObra) Sesion.getAuxiliar().get(i);
                excel.setValor(0, indice, 0, obra.getIdObra(), "DET_TNUMERO", indice);
                excel.setValor(0, indice, 1, obra.getVentaEstimada(), "DET_TMONEDA", indice);
                excel.setValor(0, indice, 2, obra.getCliente().getNombre(), "DET_TTEXTO", indice);
                excel.setValor(0, indice, 3, obra.getCliente().getRut(), "DET_TNUMERICO", indice);
                excel.setValor(0, indice, 4, obra.getDescripcion(), "DET_TTEXTO", indice);

                indice++;
            }

            excel.borrarHojaEstilo();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"Mantención correctiva.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
