package model.comercial;

public class ModPresupuesto {

    private int navEncDet;
    private int numero;
    private int uf;
    private String usuario;
    private ModCliente cliente;
    private String fecha;
    private String codigo;
    private String descripcion;
    private String um;
    private String plantilla;
    private double total;
    private double precioUnitario;
    private double precioTotal;
    private double instalacion;
    private float cantidad;
    private byte est;
    private ModTrabajo trabajo;

    public ModPresupuesto() {
        usuario = "";
        codigo = "";
        descripcion = "";
        um = "";
        cliente = new ModCliente();
        fecha = "";
        plantilla = "";
        trabajo = new ModTrabajo();
        est = 0;
        uf = 0;
    }

    public int getUf() {
        return uf;
    }

    public void setUf(int uf) {
        this.uf = uf;
    }

    public double getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(double instalacion) {
        this.instalacion = instalacion;
    }

    public ModCliente getCliente() {
        return cliente;
    }

    public void setCliente(ModCliente cliente) {
        this.cliente = cliente;
    }

    public int getNavEncDet() {
        return navEncDet;
    }

    public void setNavEncDet(int navEncDet) {
        this.navEncDet = navEncDet;
    }

    public byte getEst() {
        return est;
    }

    public void setEst(byte est) {
        this.est = est;
    }

    public ModTrabajo getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(ModTrabajo trabajo) {
        this.trabajo = trabajo;
    }

    public String getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(String plantilla) {
        this.plantilla = plantilla;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
