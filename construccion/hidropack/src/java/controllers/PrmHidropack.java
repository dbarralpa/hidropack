package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import model.ModAuxiliar;
import model.ModEquipo;
import model.ModMaterial;
import model.ModMenu;
import model.ModPedidoMaterial;
import model.ModPermisoSeccion;
import model.ModSesion;
import model.ModUsuario;
import model.comercial.ModTrabajo;
import model.hidropack.ModObra;
import model.hidropack.ModOrdenCompra;
import sql.ConexionSQL;

public class PrmHidropack {

    static final SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");

    public PrmHidropack() {
    }

    public static boolean cargarUsuario(String user, String pass, ModSesion Sesion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        boolean existe = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                //rs = cn.SelectReg("TBL_AUT_USUARIOS", "VC_USER = '" + user + "' AND VC_PASS = '" + PrmApp.encriptarString(pass) + "' AND VC_ESTADO = 'VIGENT'");
                String sql = "select VC_NOMBRE,VC_APELLIDO from TBL_AUT_USUARIOS";
                sql += " left outer join SiaHidropack.dbo.SYS_USER";
                sql += " on(USE_Codigo = VC_USER COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " where VC_USER = '" + user + "' AND VC_PASS = '" + PrmApp.encriptarString(pass) + "' AND VC_ESTADO = 'VIGENT'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    ModUsuario usuario = new ModUsuario();
                    usuario.setUsuario(user);
                    usuario.setNombre((rs.getString("VC_NOMBRE")));
                    usuario.setApellido((rs.getString("VC_APELLIDO")));
                    usuario.setMenus(PrmApp.menu(menuUsuario(usuario.getUsuario(), cn), menuGeneral(cn)));
                    usuario.setPermisosSeccion(cargarPermisosSeccion(usuario.getUsuario(), cn));
                    Sesion.setUsuario(usuario);
                    existe = true;
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return existe;
    }

    public static long ultimoFolioFisicoPrevioDespacho(ConexionSQL cn) throws Exception {
        ResultSet rs;
        long numero = 0l;
        rs = cn.SelectReg("STO_MOVENC", "MAX(MVE_FolioFisico)", " MVE_CodSistema = 6 AND MVE_CodClase = 1 and MVE_TipoDoc = 10");
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();

        return numero + 1;
    }

    public static long ultimoFolioFisicoTraspasoDeBodega(ConexionSQL cn) throws Exception {
        ResultSet rs;
        long numero = 0l;
        rs = cn.SelectReg("STO_MOVENC", "MAX(MVE_FolioFisico)", " MVE_CodSistema = 8 AND MVE_CodClase = 4 and MVE_TipoDoc = 4");
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();

        return numero + 1;
    }

    public static long ultimoFolioSolicitudAPlanta(ConexionSQL cn) throws Exception {
        ResultSet rs;
        long numero = 0l;
        rs = cn.ExecuteQuery("select MAX(nb_numero) from MAC2BETA.dbo.TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC");
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();
        return numero + 1;
    }

    public static long ultimoFolioCompra(ConexionSQL cn) throws Exception {
        ResultSet rs;
        long numero = 0l;
        rs = cn.ExecuteQuery("select MAX(nb_numero) from TBL_LOG_COMPRA_MATERIAL_ENC");
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();
        return numero + 1;
    }

    public static ArrayList consultaAjaxUsuario(String codigo, String campo, ModSesion Sesion) {
        ArrayList datos = new ArrayList();
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String condicion = null;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                condicion = campo + " LIKE '%" + codigo + "%' and PER_CLIENTE = 'S'";
                rs = cn.SelectReg("sys_persona", campo, condicion, campo);
                while (rs.next()) {
                    String tmp = rs.getString(campo).toUpperCase().trim();
                    //tmp = tmp.replaceAll(codigo.toUpperCase(), "<b>" + codigo.toUpperCase() + "</b>");
                    tmp = tmp.replaceAll(codigo.toUpperCase(), "" + codigo.toUpperCase() + "");
                    datos.add(tmp);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return datos;
    }

    public static ArrayList consultaAjaxArticulo(String codigo, String campo, String condicion1, ModSesion Sesion) {
        ArrayList datos = new ArrayList();
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String condicion = null;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                condicion = campo + " LIKE '%" + codigo + "%' " + condicion1;
                rs = cn.SelectReg("STO_PRODUCTO", campo, condicion, campo);
                while (rs.next()) {
                    String tmp = rs.getString(campo).toUpperCase().trim();
                    tmp = tmp.replaceAll(codigo.toUpperCase(), "<b>" + codigo.toUpperCase() + "</b>");
                    datos.add(tmp);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return datos;
    }

    private static ArrayList cargarPermisosSeccion(String usuario, ConexionSQL cn) throws SQLException, Exception {
        ResultSet rs = null;
        ArrayList permisosSeccion = new ArrayList();
        rs = cn.SelectReg("TBL_AUT_PERMISOS_SECCION", "vc_permiso", "TBL_AUT_USUARIOS_vc_user = '" + usuario + "' ");
        while (rs.next()) {
            ModPermisoSeccion permisoSeccion = new ModPermisoSeccion();
            permisoSeccion.setId(permisosSeccion.size());
            permisoSeccion.setPermiso(rs.getString("vc_permiso"));
            permisosSeccion.add(permisoSeccion);
            permisoSeccion = null;
        }
        rs.close();
        rs = null;
        return permisosSeccion;
    }

    private static ArrayList menuUsuario(String user, ConexionSQL cn) throws SQLException, Exception {
        ResultSet rs;
        ArrayList menus = new ArrayList();
        String sql = " select vc_nom_menu, vc_url from HIDROPACK.dbo.TBL_AUT_PERMISOS pe ";
        sql += " inner join TBL_AUT_RECURSOS re";
        sql += " on(pe.TBL_AUT_RECURSOS_nb_id_padre = re.nb_id_padre and pe.TBL_AUT_RECURSOS_nb_id_hijo = re.nb_id_hijo)";
        sql += " where TBL_AUT_USUARIOS_vc_user = '" + user + "' and vc_tipo_menu = 'HIDROPACK'";
        rs = cn.ExecuteQuery(sql);
        while (rs.next()) {
            ModMenu menu = new ModMenu();
            menu.setId(menus.size());
            menu.setNombre(rs.getString("vc_nom_menu"));
            menu.setUrl(rs.getString("vc_url"));
            menus.add(menu);
        }
        rs.close();
        rs = null;
        return menus;
    }

    private static ArrayList menuGeneral(ConexionSQL cn) throws SQLException, Exception {
        ResultSet rs;
        ArrayList menus = new ArrayList();
        String sql = " select vc_nom_menu";
        sql += " from TBL_AUT_RECURSOS";
        sql += " where vc_tipo_menu = 'HIDROPACK' and nb_id_padre <> nb_id_hijo";
        sql += " order by nb_id_padre, nb_id_hijo";
        rs = cn.ExecuteQuery(sql);
        while (rs.next()) {
            ModMenu menu = new ModMenu();
            menu.setId(menus.size());
            menu.setNombre(rs.getString("vc_nom_menu"));
            menus.add(menu);
        }
        rs.close();
        rs = null;
        return menus;
    }

    public static ModObra obra(String condicion) {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ModObra obra = new ModObra();;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT  TAB_Codigo,TAB_Desc AS descripcion, TAB_Texto3 AS Rut, TAB_Fecha1 AS FechaEntrega,PER_NOMBRE";
                sql += " FROM SYS_TABLAS ";
                sql += " INNER JOIN sys_persona ";
                sql += " ON (TAB_Texto3 = PER_CODIGO AND PER_CLIENTE = 'S') ";
                sql += condicion;
                //sql += " WHERE TAB_CodTabla = 500 AND TAB_Codigo = " + codigo;
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    obra.setIdObra(rs.getInt("TAB_Codigo"));
                    obra.setNombreObra(rs.getString("descripcion"));
                    obra.setFecha(rs.getString("FechaEntrega"));
                    obra.getCliente().setRut(Integer.parseInt(rs.getString("Rut")));
                    obra.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return obra;
    }

    public static ModEquipo cargarMaterial(String codigo, String campo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModEquipo pedidoMaterial = new ModEquipo();

        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "select PRO_DESC,PRO_CODPROD,PRO_INVMIN,PRO_INVMAX,PRO_UMPRINCIPAL,PRO_CODTIPO,PRO_ANALISIS1DOC,ID_FILA";
                sql += " from STO_PRODUCTO";
                sql += " where " + campo + " = '" + codigo + "'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    pedidoMaterial.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    pedidoMaterial.getMaterial().setMateriaPrima(rs.getString("PRO_CODPROD"));
                    pedidoMaterial.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    pedidoMaterial.getMaterial().setTipoProd(rs.getByte("PRO_CODTIPO"));
                    pedidoMaterial.getMaterial().setIdFila(rs.getInt("ID_FILA"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pedidoMaterial;
    }

    public static String codigoUsuario(String usuario) {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        String codigo = "";
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "select TAB_Codigo from sys_tablas where TAB_CodTabla = 600 and TAB_Desc = '" + usuario + "' ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    codigo = rs.getString("TAB_Codigo");
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return codigo;
    }

    public static ArrayList saldoMateriales() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList materiales = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select sm.Saldo,PSL_CodProd from VW_saldo_materiales sm";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModMaterial material = new ModMaterial();
                    material.setId(materiales.size());
                    material.setMateriaPrima(rs.getString("PSL_CodProd"));
                    material.setStock(rs.getDouble("Saldo"));
                    materiales.add(material);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return materiales;
    }

    public static ArrayList saldoMaterialesSchaffner() {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        ArrayList materiales = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIASCH)) {
                String sql = " select sm.Saldo,PSL_CodProd from VW_stock_materiales sm";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModMaterial material = new ModMaterial();
                    material.setId(materiales.size());
                    material.setMateriaPrima(rs.getString("PSL_CodProd"));
                    material.setStock(rs.getDouble("Saldo"));
                    materiales.add(material);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return materiales;
    }

    public static ArrayList cargarMaterialExcel(ArrayList materiales, long numero, int linea, int idObra) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList pedidos = null;
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                pedidos = new ArrayList();
                for (int i = 0; i < materiales.size(); i++) {
                    ModAuxiliar aux = (ModAuxiliar) materiales.get(i);
                    String sql = "select PRO_DESC,PRO_CODPROD,PRO_UMPRINCIPAL,PRO_CODTIPO";
                    sql += " from STO_PRODUCTO where PRO_CODPROD = '" + aux.getNombre() + "'";
                    rs = cn.ExecuteQuery(sql);
                    while (rs.next()) {
                        ModEquipo pedidoMaterial = new ModEquipo();
                        pedidoMaterial.setNp(idObra);
                        pedidoMaterial.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                        pedidoMaterial.getMaterial().setMateriaPrima(rs.getString("PRO_CODPROD"));
                        pedidoMaterial.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                        pedidoMaterial.getMaterial().setTipoProd(rs.getByte("PRO_CODTIPO"));
                        pedidoMaterial.getMaterial().setCantRequerida(aux.getValor());
                        pedidoMaterial.getMaterial().setNumero(numero);
                        pedidoMaterial.getMaterial().setLinea(linea);
                        pedidos.add(pedidoMaterial);
                    }
                    rs.close();
                }
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pedidos;
    }

    public static ArrayList pedidosProcesarBodega(byte est, String campo) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs = null;
        ArrayList pedidosProcesarBodega = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                if (est == 0) {
                    rs = cn.ExecuteQuery("select nb_numero,d_fecha,t_hora,vc_usuario,nb_estado from TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC where nb_estado < 5");
                } else if (est == 1) {
                    rs = cn.ExecuteQuery("select nb_numero,d_fecha,t_hora,vc_usuario,nb_estado from TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC where nb_estado < 5 and vc_usuario = '" + campo + "'");
                } else if (est == 2) {
                    rs = cn.ExecuteQuery("select nb_numero,d_fecha,t_hora,vc_usuario,nb_estado from TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC where nb_estado < 5 and nb_numero = " + Integer.parseInt(campo));
                } else if (est == 3) {
                    String query = " select distinct(enc.nb_numero) as nb_numero,enc.d_fecha,enc.t_hora,enc.vc_usuario,enc.nb_estado from VW_pedido_de_materiales ";
                    query += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det ";
                    query += " on(MVE_FolioFisico = nb_numero_doc_original)";
                    query += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
                    query += " on(enc.nb_numero = det.nb_numero)";
                    if (!campo.split(";")[1].trim().equals("0")) {
                        query += " where nb_np = " + campo.split(";")[0] + " and nb_linea = " + campo.split(";")[1];
                    } else {
                        query += " where nb_np = " + campo.split(";")[0];
                    }
                    query += " and enc.nb_estado <> 5";
                    rs = cn.ExecuteQuery(query);
                } else if (est == 4) {
                    String query = " select distinct(enc.nb_numero) as nb_numero,enc.d_fecha,enc.t_hora,enc.vc_usuario,enc.nb_estado ";
                    query += " from TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det ";
                    query += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
                    query += " on(enc.nb_numero = det.nb_numero)";
                    query += " where det.vc_codigo = '" + campo + "'";
                    query += " and enc.nb_estado <> 5";
                    rs = cn.ExecuteQuery(query);
                } else if (est == 5) {
                    String query = " select distinct(enc.nb_numero) as nb_numero,enc.d_fecha,enc.t_hora,enc.vc_usuario,enc.nb_estado from VW_pedido_de_materiales ";
                    query += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det ";
                    query += " on(MVE_FolioFisico = nb_numero_doc_original)";
                    query += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
                    query += " on(enc.nb_numero = det.nb_numero)";
                    query += " inner join SiaSchaffner.dbo.STO_MOVDET consumo";
                    query += " on(det.nb_numero = consumo.MVD_NumeroDocOrigen and det.nb_id = consumo.MVD_LineaOrigen and consumo.MVD_CodSistema=8 and consumo.MVD_CodClase =2 AND consumo.MVD_TipoDoc = 17 )";
                    query += " where consumo.MVD_NumeroDoc = " + Integer.parseInt(campo);
                    rs = cn.ExecuteQuery(query);
                } else if (est == 6) {
                    String query = " select MVE_FolioFisico AS nb_numero,FecCreacion AS d_fecha,HoraModif AS t_hora,UsCreacion AS vc_usuario,2 AS nb_estado";
                    //query += " from SiaPruebas_HP.dbo.STO_MOVENC where  ";
                    query += " from SiaHidropack.dbo.STO_MOVENC where  ";
                    query += " MVE_CodSistema=6 and  ";
                    query += " MVE_CodClase =1 and";
                    query += " MVE_TipoDoc = 10";
                    query += " and mve_analisisadic1 = " + campo.split(";")[0];
                    rs = cn.ExecuteQuery(query);
                }
                while (rs.next()) {
                    ModPedidoMaterial pedido = new ModPedidoMaterial();
                    pedido.setId(pedidosProcesarBodega.size());
                    pedido.setNumeroDoc(rs.getLong("nb_numero"));
                    pedido.setFechaCreacion(rs.getString("d_fecha"));
                    pedido.setUsuario(rs.getString("vc_usuario"));
                    pedido.setEstado(rs.getByte("nb_estado"));
                    pedidosProcesarBodega.add(pedido);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pedidosProcesarBodega;
    }

    public static ArrayList pedidosObraBodega(String campo, String condicion) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs = null;
        ArrayList pedidosObraBodega = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {

                String query = " select MVE_FolioFisico AS nb_numero,FecCreacion AS d_fecha,UsCreacion AS vc_usuario,mve_analisisadic15 AS nb_estado";
                query += " from STO_MOVENC where  ";
                query += " MVE_CodSistema=6 and  ";
                query += " MVE_CodClase =1 and";
                query += " MVE_TipoDoc = 10";
                query += " " + condicion + " and mve_analisisadic1 = " + campo;

                rs = cn.ExecuteQuery(query);

                while (rs.next()) {
                    ModPedidoMaterial pedido = new ModPedidoMaterial();
                    pedido.setId(pedidosObraBodega.size());
                    pedido.setNumeroDoc(rs.getLong("nb_numero"));
                    pedido.setFechaCreacion(rs.getString("d_fecha"));
                    pedido.setUsuario(rs.getString("vc_usuario"));
                    pedido.setEstado(rs.getByte("nb_estado"));
                    pedidosObraBodega.add(pedido);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pedidosObraBodega;
    }

    public static ArrayList detallePedidosProcesarBodega(byte est, String campo) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs;
        ArrayList detallePedidosProcesarBodega = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {
                String sql = "select pmdet.nb_id,pmdet.nb_numero,pmdet.vc_codigo,pmdet.vc_proceso,pmdet.nb_numero_doc_original,pmdet.nb_numero_linea_original,pmdet.nb_solicitado,nb_np,nb_linea,PRO_DESC,PRO_UMPRINCIPAL,TAB_Texto1,PRO_CODTIPO,pmo.MVD_CantAsignada,AAP_Texto,cm.MVD_Cant_consumo,razon";
                sql += " from TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET pmdet";
                sql += " left outer join VW_pedido_de_materiales pmo";
                sql += " on(pmo.MVD_NumeroDoc = nb_numero_doc_original and pmo.MVD_Linea = pmdet.nb_numero_linea_original and pmo.MVD_Linea = pmdet.nb_numero_linea_original and pmo.vc_proceso = pmdet.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS and pmo.MVD_CodProd = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " left outer join SiaSchaffner.dbo.STO_PRODUCTO ";
                sql += " on(pmo.MVD_CodProd = PRO_CODPROD)";
                sql += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC pmenc";
                sql += " on(pmdet.nb_numero = pmenc.nb_numero)";
                sql += " left outer join SiaSchaffner.dbo.sys_tablas";
                sql += " on(tab_codtabla = 614 and TAB_Codigo = pmdet.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " left outer join SiaSchaffner.dbo.sto_prodadic";
                sql += " on(AAP_CODANALISIS=1 and AAP_codprod = pmo.MVD_CodProd)";
                sql += " left outer join VW_consumo_materiales cm ";
                sql += " on(cm.MVD_NumeroDocOrigen = pmdet.nb_numero and cm.MVD_LineaOrigen = pmdet.nb_id)";
                if (est == 0) {
                    //sql += " where pmenc.nb_estado < 5 AND pmdet.nb_solicitado > pmo.MVD_CantAsignada";
                    sql += " where pmenc.nb_estado < 5";
                } else if (est == 1) {
                    sql += " where pmenc.nb_estado < 5 and pmenc.vc_usuario = '" + campo + "'";
                } else if (est == 2) {
                    sql += " where pmenc.nb_estado < 5 and pmenc.nb_numero = " + Long.parseLong(campo) + "";
                } else if (est == 3) {
                    sql += " where pmdet.nb_numero in(select distinct(enc.nb_numero)  from VW_pedido_de_materiales ";
                    sql += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det";
                    sql += " on(MVE_FolioFisico = nb_numero_doc_original)";
                    sql += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
                    sql += " on(enc.nb_numero = det.nb_numero)";
                    if (!campo.split(";")[1].trim().equals("0")) {
                        sql += " where nb_np = " + campo.split(";")[0] + " and nb_linea = " + campo.split(";")[1];
                    } else {
                        sql += " where nb_np = " + campo.split(";")[0];
                    }
                    sql += " and enc.nb_estado <> 5)";
                } else if (est == 4) {
                    sql += " where pmdet.nb_numero in(select distinct(enc.nb_numero) ";
                    sql += " from TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det ";
                    sql += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
                    sql += " on(enc.nb_numero = det.nb_numero)";
                    sql += " where det.vc_codigo = '" + campo + "'";
                    sql += " and enc.nb_estado <> 5)";
                } else if (est == 5) {
                    sql += " inner join SiaSchaffner.dbo.STO_MOVDET consumo";
                    sql += " on(pmdet.nb_numero = consumo.MVD_NumeroDocOrigen and pmdet.nb_id = consumo.MVD_LineaOrigen and consumo.MVD_CodSistema=8 and consumo.MVD_CodClase =2 AND consumo.MVD_TipoDoc = 17 )";
                    sql += " where consumo.MVD_NumeroDoc = " + Integer.parseInt(campo);
                } else if (est == 6) {
                    sql += " where pmdet.nb_numero in(select distinct(enc.nb_numero)  from VW_pedido_de_materiales ";
                    sql += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det";
                    sql += " on(MVE_FolioFisico = nb_numero_doc_original)";
                    sql += " inner join TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
                    sql += " on(enc.nb_numero = det.nb_numero)";
                    sql += " where nb_np = " + campo.split(";")[0] + ")";
                }

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEquipo equipo = new ModEquipo();
                    equipo.setNp(rs.getInt("nb_np"));
                    equipo.setItem(rs.getInt("nb_linea"));
                    equipo.getMaterial().setId(rs.getInt("nb_id"));
                    equipo.getMaterial().setNumero(rs.getLong("nb_numero"));
                    equipo.getMaterial().setMateriaPrima(rs.getString("vc_codigo"));
                    equipo.getMaterial().setNumeroDoc(rs.getLong("nb_numero_doc_original"));
                    equipo.getMaterial().setLinea(rs.getInt("nb_numero_linea_original"));
                    if ((rs.getDouble("nb_solicitado") - rs.getDouble("MVD_Cant_consumo")) <= 0) {
                        equipo.getMaterial().setSolicitado(0d);
                        equipo.getMaterial().setSolicitado1(0d);
                    } else {
                        equipo.getMaterial().setSolicitado(rs.getDouble("nb_solicitado") - rs.getDouble("MVD_Cant_consumo"));
                        equipo.getMaterial().setSolicitado1(rs.getDouble("nb_solicitado") - rs.getDouble("MVD_Cant_consumo"));
                    }
                    //equipo.getMaterial().setSolicitado(rs.getDouble("nb_solicitado"));
                    //equipo.getMaterial().setSolicitado1(rs.getDouble("nb_solicitado"));
                    equipo.getMaterial().setSolicitado2(rs.getDouble("nb_solicitado"));
                    equipo.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    equipo.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    equipo.getMaterial().setProceso(rs.getString("vc_proceso"));
                    equipo.getMaterial().setCantidadAsignada(rs.getDouble("MVD_CantAsignada"));
                    if (rs.getString("TAB_Texto1") == null) {
                        equipo.getMaterial().setCentroCosto("3005");
                    } else {
                        equipo.getMaterial().setCentroCosto(rs.getString("TAB_Texto1").trim());
                    }
                    equipo.getMaterial().setUbicacion(rs.getString("AAP_Texto"));
                    equipo.getMaterial().setTipoProd(rs.getByte("PRO_CODTIPO"));
                    equipo.getMaterial().setRazon(rs.getInt("razon"));
                    detallePedidosProcesarBodega.add(equipo);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return detallePedidosProcesarBodega;
    }

    public static ArrayList detallePedidosObraBodega(String campo, String condicion) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs;
        ArrayList detallePedidosProcesarBodega = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select pd.MVE_FolioFisico, pd.MVD_CodProd,pd.MVD_Cant,pd.MVD_Linea,PRO_DESC,PRO_UMPRINCIPAL,PRO_CODTIPO,isnull(tb.MVD_Cant,0) as CantidadAsignada, AAP_TEXTO as Ubicacion,convert(datetime,isnull(dt_fecha_entrega,'1900-01-01')) as MVD_FechaEntrega,isnull(det.nb_estado,0) as nb_estado,pd.MVD_NumeroDocOrigen,pd.MVD_LineaOrigen";
                sql += " from VW_previo_traspaso pd";
                sql += " left outer join TBL_LOG_COMPRA_MATERIAL_DET det";
                sql += " on(det.nb_numero = pd.MVD_NumeroDocOrigen and det.nb_linea = pd.MVD_LineaOrigen)";
                sql += " left outer join TBL_LOG_COMPRA_MATERIAL_ENC enc";
                sql += " on(enc.nb_numero = det.nb_numero)";
                sql += " left outer join  SiaHidropack.dbo.STO_PRODUCTO";
                sql += " on(PRO_CODPROD = MVD_CodProd) ";
                sql += " left outer join VW_traspaso_a_bodega tb";
                sql += " on(pd.MVE_FolioFisico = tb.MVD_NumeroDocOrigen and pd.MVD_Linea = tb.MVD_LineaOrigen)";
                sql += " left outer join SiaSchaffner.dbo.sto_prodadic ";
                sql += " on(AAP_CODANALISIS=1 and AAP_codprod = MVD_CodProd)";
                sql += " where mve_analisisadic1 = '" + campo + "' " + condicion + " order by pd.MVD_Linea";


                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEquipo equipo = new ModEquipo();
                    equipo.setId(detallePedidosProcesarBodega.size());
                    equipo.setNp(Integer.parseInt(campo));
                    equipo.getMaterial().setNumero(rs.getLong("MVE_FolioFisico"));
                    equipo.getMaterial().setMateriaPrima(rs.getString("MVD_CodProd"));
                    equipo.getMaterial().setLinea(rs.getInt("MVD_Linea"));
                    equipo.getMaterial().setLinea1(rs.getInt("MVD_Linea"));
                    equipo.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    equipo.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    equipo.getMaterial().setCantRequerida(rs.getDouble("MVD_Cant"));
                    equipo.getMaterial().setCantidad(rs.getDouble("MVD_Cant") - rs.getDouble("CantidadAsignada"));
                    equipo.getMaterial().setCantidadR(rs.getDouble("MVD_Cant") - rs.getDouble("CantidadAsignada"));
                    equipo.getMaterial().setCantidadAsignada(rs.getDouble("CantidadAsignada"));
                    equipo.getMaterial().setTipoProd(rs.getByte("PRO_CODTIPO"));
                    equipo.getMaterial().setUbicacion(rs.getString("Ubicacion"));
                    equipo.getMaterial().setFecha(fecha.format(rs.getTimestamp("MVD_FechaEntrega")));
                    equipo.getMaterial().setEstado(rs.getString("nb_estado"));
                    equipo.getMaterial().setNumeroDocOrigen(rs.getLong("MVD_NumeroDocOrigen"));
                    equipo.getMaterial().setLineaOrigen(rs.getInt("MVD_LineaOrigen"));
                    detallePedidosProcesarBodega.add(equipo);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return detallePedidosProcesarBodega;
    }

    public static ArrayList detallePedidosObraBodegaUsuario(String campo) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs;
        ArrayList detallePedidosProcesarBodega = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select pd.MVE_FolioFisico, pd.MVD_CodProd,pd.MVD_Cant,pd.MVD_Linea,PRO_DESC,PRO_UMPRINCIPAL,PRO_CODTIPO,isnull(tb.MVD_Cant,0) as CantidadAsignada, AAP_TEXTO as Ubicacion,convert(datetime,isnull(dt_fecha_entrega,'1900-01-01')) as MVD_FechaEntrega,isnull(det.nb_estado,0) as nb_estado,pd.MVD_NumeroDocOrigen,pd.MVD_LineaOrigen";
                sql += " ,gd.MVD_Cant as CantGuia,gd.MVE_FolioFisico as NumGuia,isnull(gd.MVE_FechaDoc,'&nbsp;') as FechaGuia, PSL_Saldo as Bodega1,oc.MVE_FolioFisico as NumOc,isnull(oc.MVD_FechaEntrega,'&nbsp;') as fechaOc,(oc.MVD_Cant - oc.MVD_CantAsignada) as CantOc";
                sql += " from VW_previo_traspaso pd";
                sql += " left outer join TBL_LOG_COMPRA_MATERIAL_DET det";
                sql += " on(det.nb_numero = pd.MVD_NumeroDocOrigen and det.nb_linea = pd.MVD_LineaOrigen)";
                sql += " left outer join TBL_LOG_COMPRA_MATERIAL_ENC enc";
                sql += " on(enc.nb_numero = det.nb_numero)";
                sql += " left outer join  SiaHidropack.dbo.STO_PRODUCTO";
                sql += " on(PRO_CODPROD = MVD_CodProd) ";
                sql += " left outer join VW_traspaso_a_bodega tb";
                sql += " on(pd.MVE_FolioFisico = tb.MVD_NumeroDocOrigen and pd.MVD_Linea = tb.MVD_LineaOrigen)";
                sql += " left outer join SiaSchaffner.dbo.sto_prodadic ";
                sql += " on(AAP_CODANALISIS=1 and AAP_codprod = MVD_CodProd)";
                sql += " left outer join VW_guia_despacho gd";
                sql += " on(pd.MVE_FolioFisico = gd.MVD_NumeroDocOrigen and pd.MVD_Linea = gd.MVD_LineaOrigen)";
                sql += " left outer join VW_bodega1";
                sql += " on(PSL_CodProd = pd.MVD_CodProd)";
                sql += " left outer join SiaHidropack.dbo.WV_ordenes_de_compra oc";
                sql += " on(oc.MVD_CodProd = pd.MVD_CodProd and (oc.MVD_Cant - oc.MVD_CantAsignada) > 0)";
                sql += " where pd.mve_analisisadic1 = '" + campo + "' order by pd.MVD_Linea";


                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEquipo equipo = new ModEquipo();
                    equipo.setId(detallePedidosProcesarBodega.size());
                    equipo.setNp(Integer.parseInt(campo));
                    equipo.getMaterial().setNumero(rs.getLong("MVE_FolioFisico"));
                    equipo.getMaterial().setMateriaPrima(rs.getString("MVD_CodProd"));
                    equipo.getMaterial().setLinea(rs.getInt("MVD_Linea"));
                    equipo.getMaterial().setLinea1(rs.getInt("MVD_Linea"));
                    equipo.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    equipo.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    equipo.getMaterial().setCantRequerida(rs.getDouble("MVD_Cant"));
                    equipo.getMaterial().setCantidad(rs.getDouble("MVD_Cant") - rs.getDouble("CantidadAsignada"));
                    equipo.getMaterial().setCantidadR(rs.getDouble("MVD_Cant") - rs.getDouble("CantidadAsignada"));
                    equipo.getMaterial().setCantidadAsignada(rs.getDouble("CantidadAsignada"));
                    equipo.getMaterial().setTipoProd(rs.getByte("PRO_CODTIPO"));
                    equipo.getMaterial().setUbicacion(rs.getString("Ubicacion"));
                    equipo.getMaterial().setFecha(fecha.format(rs.getTimestamp("MVD_FechaEntrega")));
                    equipo.getMaterial().setEstado(rs.getString("nb_estado"));
                    equipo.getMaterial().setNumeroDocOrigen(rs.getLong("MVD_NumeroDocOrigen"));
                    equipo.getMaterial().setLineaOrigen(rs.getInt("MVD_LineaOrigen"));
                    equipo.getMaterial().setNumGuia(rs.getInt("NumGuia"));
                    equipo.getMaterial().setCantGuia(rs.getDouble("CantGuia"));
                    equipo.getMaterial().setFechaGuia(rs.getString("FechaGuia"));
                    equipo.getMaterial().setStock(rs.getDouble("Bodega1"));
                    equipo.getMaterial().setNumOc(rs.getInt("NumOc"));
                    equipo.getMaterial().setFechaOc(rs.getString("fechaOc"));
                    equipo.getMaterial().setCantOc(rs.getDouble("CantOc"));
                    detallePedidosProcesarBodega.add(equipo);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return detallePedidosProcesarBodega;
    }

    public static ArrayList pedidosProductosProduccion(String signo, int val) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs = null;
        ArrayList pedidosProcesarBodega = new ArrayList();

        try {
            if (cn.openSQL(ConexionSQL.MS_MAC2BETA)) {

                String query = " select nb_numero_solicitud,dt_fecha_solicitud,nb_id_obra,he.vc_nombre,enc.nb_estado";
                query += " from MAC2BETA.dbo.TBL_HID_SOLICITUD_DE_FABRICACION_ENC enc ";
                query += " inner join MAC2BETA.dbo.TBL_HID_ESTADO he";
                query += " on(he.nb_id = enc.nb_estado)";
                query += " where nb_estado " + signo + " " + val;
                query += " order by nb_numero_solicitud";
                rs = cn.ExecuteQuery(query);

                while (rs.next()) {
                    ModPedidoMaterial pedido = new ModPedidoMaterial();
                    pedido.setId(pedidosProcesarBodega.size());
                    pedido.setNumeroDoc(rs.getLong("nb_numero_solicitud"));
                    pedido.setFechaCreacion(fecha.format(rs.getDate("dt_fecha_solicitud")));
                    pedido.setNumero(rs.getLong("nb_id_obra"));
                    pedido.setEstadoSolicitud(rs.getString("vc_nombre"));
                    pedido.setEstado(rs.getByte("nb_estado"));
                    pedidosProcesarBodega.add(pedido);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pedidosProcesarBodega;
    }

    public static ArrayList cargarMaterialCompra(ModSesion Sesion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList pedidosEspeciales = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select PRO_DESC,PRO_CODPROD,PRO_UMPRINCIPAL,PRO_CODTIPO,nb_cantidad,det.nb_linea,enc.nb_numero";
                sql += " from SiaHidropack.dbo.STO_PRODUCTO ";
                sql += " left outer join TBL_LOG_COMPRA_MATERIAL_DET det";
                sql += " on(PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS = vc_codigo)";
                sql += " inner join TBL_LOG_COMPRA_MATERIAL_ENC enc";
                sql += " on(det.nb_numero = enc.nb_numero)";
                sql += " left outer join HIDROPACK.dbo.VW_previo_traspaso";
                sql += " on(det.nb_linea = MVD_LineaOrigen and enc.nb_numero = MVD_NumeroDocOrigen and mve_analisisadic1 = '" + Sesion.getObra().getIdObra() + "')";
                sql += " where enc.nb_obra = " + Sesion.getObra().getIdObra() + " and MVD_NumeroDocOrigen is null";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEquipo pedidoMaterial = new ModEquipo();
                    pedidoMaterial.setNp(Sesion.getObra().getIdObra());
                    pedidoMaterial.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    pedidoMaterial.getMaterial().setMateriaPrima(rs.getString("PRO_CODPROD"));
                    pedidoMaterial.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    pedidoMaterial.getMaterial().setTipoProd(rs.getByte("PRO_CODTIPO"));
                    pedidoMaterial.getMaterial().setCantRequerida(rs.getDouble("nb_cantidad"));
                    pedidoMaterial.getMaterial().setNumero(rs.getLong("nb_numero"));
                    pedidoMaterial.getMaterial().setLinea(rs.getInt("nb_linea"));
                    pedidosEspeciales.add(pedidoMaterial);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pedidosEspeciales;
    }

    public static long existeCompra(ConexionSQL cn, int idObra) throws Exception {
        ResultSet rs;
        long numero = 0l;
        rs = cn.ExecuteQuery("select nb_numero from TBL_LOG_COMPRA_MATERIAL_ENC where nb_obra = " + idObra);
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();
        return numero;
    }

    public static ArrayList thead(int idTabla) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList theads = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select nb_nombre_columna,vc_descripcion_columna from TBL_ALL_THEAD order by nb_id";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModMaterial mat = new ModMaterial();
                    mat.setId(theads.size());
                    mat.setDescripcion(rs.getString("nb_nombre_columna"));
                    mat.setDescripcionLarga(rs.getString("vc_descripcion_columna"));
                    theads.add(mat);
                    mat = null;
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return theads;
    }

    public static ArrayList cargarMrp() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList cargarMrps = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = "select sum(nb_cantidad) as Cantidad,vc_codigo,PRO_DESC,PRO_UMALTERNATIVA,(isnull(cp.Cantidad,0)-isnull(dp.Cantidad,0)) as ConProm,PRO_INVMIN,PRO_INVMAX,Saldo,LeadTime as leadTime,isnull(op.Cantidad,0) as Oc";
                sql += " from VW_previos_sin_traspaso";
                sql += " left outer join SiaHidropack.dbo.STO_PRODUCTO";
                sql += " on(PRO_CODPROD = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " left outer join VW_saldo_materiales";
                sql += " on(PRO_CODPROD = PSL_CodProd)";
                sql += " left outer join MAC.dbo.ProductoAnexo lt";
                sql += " on(PRO_CODPROD = lt.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " left outer join VW_ordenes_pendientes op";
                sql += " on(PRO_CODPROD = op.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " left outer join VW_consumo_promedio cp";
                sql += " on(PRO_CODPROD = cp.MVD_CodProd)";
                sql += " left outer join VW_devolucion_promedio dp";
                sql += " on(PRO_CODPROD = dp.MVD_CodProd)";
                sql += " where mve_analisisadic15 <> '5'";
                sql += " group by vc_codigo,PRO_DESC,PRO_UMALTERNATIVA,PRO_INVMIN,PRO_INVMAX,Saldo,LeadTime,isnull(op.Cantidad,0),cp.Cantidad,dp.Cantidad";
                
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModMaterial mat = new ModMaterial();
                    mat.setId(cargarMrps.size());
                    mat.setCantRequerida(rs.getDouble("Cantidad"));
                    mat.setMateriaPrima(rs.getString("vc_codigo"));
                    mat.setDescripcion(rs.getString("PRO_DESC"));
                    mat.setUm(rs.getString("PRO_UMALTERNATIVA"));
                    mat.setCantidadR(rs.getDouble("ConProm") / 12);
                    mat.setStockMin(rs.getDouble("PRO_INVMIN"));
                    mat.setStockMax(rs.getDouble("PRO_INVMAX"));
                    mat.setSaldo(rs.getDouble("Saldo"));
                    mat.setOc(rs.getDouble("Oc"));
                    mat.setLeadTime(rs.getInt("leadTime"));
                    cargarMrps.add(mat);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return cargarMrps;
    }

    public static ArrayList ordenesCompra(String materiaPrima) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList ordenesCompras = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = " select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha) as fechaEmision,tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, ";
                sql += " convert(datetime,tc.MVD_FechaEntrega) AS fechaEntrega,pro.PRO_DESC,per.PER_NOMBRE,tc.MVD_PrecioAjustado, ";
                sql += " (select TAB_Desc from SYS_TABLAS where TAB_CodTabla = 9 and TAB_Codigo = enc.MVE_Moneda) as moneda,PRO_UMALTERNATIVA,convert(datetime,MVD_Analisis5) as MVD_Analisis5 ";
                sql += " from STO_MOVDET tc ,STO_MOVENC enc, STO_PRODUCTO pro,SYS_PERSONA per ";
                sql += " where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1  ";
                sql += " and enc.MVE_CodSistema=7  ";
                sql += " and enc.MVE_CodClase =1  ";
                sql += " and enc.MVE_TipoDoc = 1 ";
                sql += " and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc ";
                sql += " and tc.MVD_CodProd = pro.PRO_CODPROD ";
                sql += " and enc.MVE_CodPersona = per.PER_CODIGO ";
                //sql += " and tc.MVD_CodProd = '" + materiaPrima + "' and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0";
                sql += " and tc.MVD_CodProd = '" + materiaPrima + "'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModOrdenCompra oc = new ModOrdenCompra();
                    oc.setNumOrdenCompra(rs.getLong("MVE_FolioFisico"));
                    oc.setItem(rs.getInt("MVD_Linea"));
                    oc.setRut(rs.getInt("MVE_CodPersona"));
                    oc.setFechaEmision(rs.getDate("fechaEmision"));
                    oc.setMateriaPrima(rs.getString("MVD_CodProd"));
                    oc.setCantidad(rs.getDouble("MVD_cant"));
                    oc.setCantPenOc(rs.getDouble("CAN_PEN"));
                    oc.setFechaEntrega(rs.getDate("fechaEntrega"));
                    oc.setDescripcion(rs.getString("PRO_DESC"));
                    oc.setProveedor(rs.getString("PER_NOMBRE"));
                    oc.setPrecio(rs.getDouble("MVD_PrecioAjustado"));
                    oc.setMoneda(rs.getString("moneda"));
                    oc.setUm(rs.getString("PRO_UMALTERNATIVA"));
                    oc.setFechaConfirmada(rs.getDate("MVD_Analisis5"));
                    ordenesCompras.add(oc);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return ordenesCompras;
    }

    public static ArrayList necesidadMaterial(String materiaPrima) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList materiales = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select dt_fecha_entrega,";
                sql += "(SELECT  PER_NOMBRE";
                sql += " FROM SiaHidropack.dbo.SYS_TABLAS";
                sql += " INNER JOIN SiaHidropack.dbo.sys_persona";
                sql += " ON (PER_CODIGO = TAB_Texto3  AND PER_CLIENTE = 'S') WHERE TAB_CodTabla = '500' AND TAB_Codigo = nb_obra) as nombre_cliente,nb_obra,(nb_cantidad-isnull(MVD_CantAsignada,0)) as cantidad";
                sql += " from TBL_LOG_COMPRA_MATERIAL_ENC enc";
                sql += " inner join TBL_LOG_COMPRA_MATERIAL_DET det";
                sql += " on(enc.nb_numero=det.nb_numero)";
                sql += " left outer join VW_previo_traspaso";
                sql += " on(MVD_NumeroDocOrigen = det.nb_numero AND MVD_LineaOrigen = det.nb_linea)";
                sql += " where vc_codigo = '" + materiaPrima + "' and (nb_cantidad-isnull(MVD_CantAsignada,0)) > 0 and mve_analisisadic15 <> '5' ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEquipo equi = new ModEquipo();
                    equi.setExpeditacion(rs.getDate("dt_fecha_entrega"));
                    equi.setNombreCliente(rs.getString("nombre_cliente"));
                    equi.setNp(rs.getInt("nb_obra"));
                    equi.getMaterial().setCantidad(rs.getInt("cantidad"));
                    materiales.add(equi);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return materiales;
    }

    public static ArrayList obraPendiente() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList materiales = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select distinct(mve_analisisadic1) as obra,convert(datetime,dt_fecha_entrega) as fechaEntrega";
                sql += " from VW_previo_traspaso pd";
                sql += " inner join TBL_LOG_COMPRA_MATERIAL_DET det";
                sql += " on(det.nb_numero = pd.MVD_NumeroDocOrigen and det.nb_linea = pd.MVD_LineaOrigen)";
                sql += " inner join TBL_LOG_COMPRA_MATERIAL_ENC enc";
                sql += " on(enc.nb_numero = det.nb_numero)";
                sql += " left outer join VW_traspaso_a_bodega tb";
                sql += " on(pd.MVE_NumeroDoc = tb.MVD_NumeroDocOrigen and pd.MVD_Linea = tb.MVD_LineaOrigen)";
                sql += " where (pd.MVD_Cant-isnull(tb.MVD_Cant,0)) > 0 and mve_analisisadic15 <> '5'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEquipo equi = new ModEquipo();
                    equi.setNp(rs.getInt("obra"));
                    equi.getMaterial().setFecha(fecha.format(rs.getDate("fechaEntrega")));
                    materiales.add(equi);
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return materiales;
    }

    public static void cargarMaterialCambiado(String codigo, String campo, ModEquipo equi) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();

        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "select PRO_DESC,PRO_CODPROD,PRO_INVMIN,PRO_INVMAX,PRO_UMPRINCIPAL,PRO_CODTIPO,PRO_ANALISIS1DOC,ID_FILA";
                sql += " from STO_PRODUCTO";
                sql += " where " + campo + " = '" + codigo + "'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    equi.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    equi.getMaterial().setMateriaPrima(rs.getString("PRO_CODPROD"));
                    equi.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    equi.getMaterial().setTipoProd(rs.getByte("PRO_CODTIPO"));
                    equi.getMaterial().setIdFila(rs.getInt("ID_FILA"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
    }

    public static int materialConTraspaso(long numeroDocOrigen, int lineaOrigen) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        int val = 0;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = "select count(MVD_Cant) from VW_traspaso_a_bodega where MVD_NumeroDocOrigen = " + numeroDocOrigen + " and MVD_LineaOrigen = " + lineaOrigen;
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    val = rs.getInt(1);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return val;
    }

    public static ArrayList trabajo() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList trabajos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT  TAB_Codigo, TAB_Desc FROM SYS_TABLAS WHERE (TAB_CodTabla = 615)";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModTrabajo tra = new ModTrabajo();
                    tra.setCodigo(rs.getInt("TAB_Codigo"));
                    tra.setDescripcion(rs.getString("TAB_Desc"));
                    trabajos.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return trabajos;
    }
}
