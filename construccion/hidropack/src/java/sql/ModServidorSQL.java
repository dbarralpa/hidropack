package sql;

import com.microsoft.sqlserver.jdbc.SQLServerDriver;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ModServidorSQL {

    private boolean activa;
    private String Svr;
    private int port;
    private String Bdd;
    private String Usr;
    private String Pwd;
    private String prefix;

    public ModServidorSQL() {
        activa = false;
        Svr = "";
        port = 0;
        Bdd = "";
        Usr = "";
        Pwd = "";
        prefix = "";
    }

    public String getBdd() {
        return Bdd;
    }

    public void setBdd(String Bdd) {
        this.Bdd = Bdd;
    }

    public String getPwd() {
        return Pwd;
    }

    public void setPwd(String Pwd) {
        this.Pwd = Pwd;
    }

    public String getSvr() {
        return Svr;
    }

    public void setSvr(String Svr) {
        this.Svr = Svr;
    }

    public String getUsr() {
        return Usr;
    }

    public void setUsr(String Usr) {
        this.Usr = Usr;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public java.sql.Connection ConexionMSSQL() throws SQLException {
        java.sql.Connection tmp = null;
        DriverManager.registerDriver(new SQLServerDriver());
        tmp = java.sql.DriverManager.getConnection("jdbc:sqlserver://" + getSvr() + ";DatabaseName=" + getBdd(), getUsr(), getPwd());
        return tmp;
    }

    public java.sql.Connection ConexionMSSQLPOOL() throws SQLException, NamingException {
        InitialContext ic = new InitialContext();
        DataSource ds = (DataSource) ic.lookup("java:/MSSQL_PORTALZN");
        return ds.getConnection();
    }
}
