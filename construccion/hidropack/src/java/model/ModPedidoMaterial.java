/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author dbarra
 */
public class ModPedidoMaterial implements Cloneable {

    private int id;
    private long numeroDoc;
    private long numero;
    private int linea;
    private String fechaCreacion;
    private String usuRetira;
    private String usuario;
    private String checked;
    private byte estado;
    private String estadoSolicitud;

    public ModPedidoMaterial() {
        linea = 0;
        id = 0;
        numeroDoc = 0l;
        numero = 0l;
        fechaCreacion = "";
        usuRetira = "";
        usuario = "";
        checked = "";
        estadoSolicitud = "";
        estado = 0;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuRetira() {
        return usuRetira;
    }

    public void setUsuRetira(String usuRetira) {
        this.usuRetira = usuRetira;
    }

    public byte getEstado() {
        return estado;
    }

    public void setEstado(byte estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(long numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public ModPedidoMaterial copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModPedidoMaterial) obj;
    }
}
