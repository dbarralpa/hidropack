package model.hidropack;

import java.util.Date;

public class ModOrdenCompra {

    private long numOrdenCompra;
    private int item;
    private int rut;
    private Date fechaEmision;
    private Date fechaEntrega;
    private Date fechaConfirmada;
    private String materiaPrima;
    private String descripcion;
    private String proveedor;
    private String moneda;
    private String um;
    private double cantidad;
    private double cantPenOc;
    private double precio;

    public ModOrdenCompra() {
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public long getNumOrdenCompra() {
        return numOrdenCompra;
    }

    public void setNumOrdenCompra(long numOrdenCompra) {
        this.numOrdenCompra = numOrdenCompra;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getMateriaPrima() {
        return materiaPrima;
    }

    public void setMateriaPrima(String materiaPrima) {
        this.materiaPrima = materiaPrima;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getCantPenOc() {
        return cantPenOc;
    }

    public void setCantPenOc(double cantPenOc) {
        this.cantPenOc = cantPenOc;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Date getFechaConfirmada() {
        return fechaConfirmada;
    }

    public void setFechaConfirmada(Date fechaConfirmada) {
        this.fechaConfirmada = fechaConfirmada;
    }
    
}
