/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import controllers.PrmApp;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import model.ModSesion;
import model.comercial.ModInforme;
import model.comercial.ModProServ;
import model.comercial.ModPrueba1;

/**
 *
 * @author dbarra
 */
public class HelperComercial {

    public static String comboFrecuenciaSelected(HttpServletRequest req, int frecuencia) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                ModProServ tra = (ModProServ) Sesion.getMateriales().get(i);
                if (frecuencia == tra.getCodigo()) {
                    html += tra.getDescripcion();
                }
            }
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public static void codigosSinRepetir(ArrayList codigos, ArrayList informe) {
        for (int i = 0; i < informe.size(); i++) {
            boolean est = false;
            ModInforme inf = (ModInforme) informe.get(i);
            for (int g = 0; g < codigos.size(); g++) {
                ModInforme inf1 = (ModInforme) codigos.get(g);
                if (inf.getMaterial().getCodigo().equals(inf1.getMaterial().getCodigo())) {
                    est = true;
                }
            }
            if (!est) {
                codigos.add(inf);
            }
        }
    }

    public static double cantidad(String codigo, ArrayList auxiliar) {
        double val = 0;
        for (int a = 0; a < auxiliar.size(); a++) {
            ModInforme inf = (ModInforme) auxiliar.get(a);
            if (codigo.equals(inf.getMaterial().getCodigo())) {
                val = inf.getCantidad();
                break;
            }
        }
        return val;
    }

    public static ModPrueba1 prueba(ArrayList pruebas, String comprador) {
        ModPrueba1 pru = new ModPrueba1();
        for (int i = 0; i < pruebas.size(); i++) {
            ModPrueba1 prueba = (ModPrueba1) pruebas.get(i);
            if (prueba.getComprador().equals(comprador)) {
                pru = prueba;
            }
        }
        return pru;
    }
}
