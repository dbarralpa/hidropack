package forms;

import controllers.PrmApp;
import controllers.PrmComercial;
import controllers.PrmGrabarHidropackSQL;

import controllers.PrmHidropack;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import model.ModEquipo;
import model.ModSesion;
import model.comercial.ModCliente;
import model.comercial.ModPlantilla;
import model.comercial.ModPresupuesto;
import model.comercial.ModVentaDirecta;

public class FrmAjax extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        ModSesion Sesion = null;
        try {
            res.setContentType("text/html; charset=iso-8859-1");
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            PrintWriter out = res.getWriter();
            String html = "";
            if (req.getParameter("busqueda") != null && req.getParameter("txtCampo") != null) {
                if (req.getParameter("txtCampo").trim().equals("PRO_CODPROD")) {
                    Sesion.setChk_PER_CODIGO("checked");
                    Sesion.setChk_PER_NOMBRE("");
                } else {
                    Sesion.setChk_PER_CODIGO("");
                    Sesion.setChk_PER_NOMBRE("checked");
                }

                String campo;

                if (req.getParameter("txtCampo").trim().equals("PRO_CODPROD")) {
                    campo = "PRO_CODPROD";
                } else if (req.getParameter("txtCampo").trim().equals("PRO_DESC")) {
                    campo = "PRO_DESC";
                } else {
                    campo = "PRO_CODPROD";
                }
                if (Sesion.getHttp_Body().equals("hidropackMateriales")) {
                    Sesion.setDatos(PrmHidropack.consultaAjaxArticulo(req.getParameter("busqueda").trim(), campo, " and PRO_CODTIPO in(1, 3)", Sesion));
                }

                if (Sesion.getHttp_Body().equals("hidropacProductosTerminados")) {
                    Sesion.setDatos(PrmHidropack.consultaAjaxArticulo(req.getParameter("busqueda").trim(), campo, " and PRO_CODTIPO in(1, 3) ", Sesion));
                }

                if (Sesion.getHttp_Body().equals("hidropackCambiarCodigo")
                        || Sesion.getHttp_Body().equals("comercialPresupuestoObra")
                        || Sesion.getHttp_Body().equals("comercialModificarPlantilla")
                        || Sesion.getHttp_Body().equals("comercialVentaDirecta")
                        || Sesion.getHttp_Body().equals("comercialPresupuestoMantencionCorrectiva")) {
                    Sesion.setDatos(PrmHidropack.consultaAjaxArticulo(req.getParameter("busqueda").trim(), campo, "", Sesion));
                }


                if (Sesion.getHttp_Body().equals("comercialIngresarPresupuesto")
                        || Sesion.getHttp_Body().equals("comercialBuscarPresupuestoCliente")
                        || Sesion.getHttp_Body().equals("comercialBuscarPresupuestoClienteManCorrectiva")
                        || Sesion.getHttp_Body().equals("comercialIngresarObra")
                        || Sesion.getHttp_Body().equals("comercialBuscarObraCliente")
                        || Sesion.getHttp_Body().equals("comercialPresupuestoMantencionCorrectivaCliente")
                        || Sesion.getHttp_Body().equals("comercialManPrevCliente")
                        || Sesion.getHttp_Body().equals("comercialVentasDirectas")
                        || Sesion.getHttp_Body().equals("comercialVentaDirectaCliente")
                        || Sesion.getHttp_Body().equals("comercialInformes")
                        || Sesion.getHttp_Body().equals("comercialManPreventiva")) {

                    if (req.getParameter("txtCampo").trim().equals("PRO_CODPROD")) {
                        campo = "PER_CODIGO";
                    } else if (req.getParameter("txtCampo").trim().equals("PRO_DESC")) {
                        campo = "PER_NOMBRE";
                    } else {
                        campo = "PER_CODIGO";
                    }
                    Sesion.setDatos(PrmHidropack.consultaAjaxUsuario(req.getParameter("busqueda").trim(), campo, Sesion));
                }

                if (Sesion.getDatos().isEmpty()) {
                    html += "0&";
                    html += "<div onClick=\"clickLista(this);\" onMouseOver=\"mouseDentro(this);\">" + "vacio" + "</div>";
                } else {
                    html += "1&";
                    for (int i = 0; i < Sesion.getDatos().size(); i++) {
                        if (Sesion.getDatos().size() > i) {
                            html += "<div onClick=\"clickLista(this); \" onMouseOver=\"mouseDentro(this);\" >" + Sesion.getDatos().get(i) + "</div>";
                        }
                    }
                }
            }

            if (req.getParameter("indiceAjax") != null) {
                int indice = Integer.parseInt(req.getParameter("indiceAjax"));
                if (indice > -1 && !Sesion.getDatos().isEmpty()) {
                    String codigo = (String) Sesion.getDatos().get(indice);
                    codigo = codigo.replaceAll("<b>", "");
                    codigo = codigo.replaceAll("</b>", "");
                    String campo = "";

                    if (Sesion.getHttp_Body().equals("hidropackMateriales")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "PRO_CODPROD";
                            Sesion.setEquipo(PrmHidropack.cargarMaterial(codigo, campo));
                            Sesion.getEquipo().setNp(Sesion.getObra().getIdObra());
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "PRO_DESC";
                            Sesion.setEquipo(PrmHidropack.cargarMaterial(codigo, campo));
                            Sesion.getEquipo().setNp(Sesion.getObra().getIdObra());
                        } else if (!Sesion.getChk_PER_USUARIO().equals("")) {
                            Sesion.setChk_PER_NOMBRE("");
                            Sesion.setChk_PER_CODIGO("checked");
                            Sesion.setChk_PER_USUARIO("");
                        }
                    }

                    if (Sesion.getHttp_Body().equals("hidropacProductosTerminados")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "PRO_CODPROD";
                            Sesion.setEquipo(PrmHidropack.cargarMaterial(codigo, campo));
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "PRO_DESC";
                            Sesion.setEquipo(PrmHidropack.cargarMaterial(codigo, campo));
                        }
                        Sesion.setDatos(PrmHidropack.trabajo());
                    }

                    if (Sesion.getHttp_Body().equals("hidropackCambiarCodigo")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "PRO_CODPROD";
                            PrmHidropack.cargarMaterialCambiado(codigo, campo, Sesion.getEquipo());
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "PRO_DESC";
                            PrmHidropack.cargarMaterialCambiado(codigo, campo, Sesion.getEquipo());
                        }
                        if (PrmGrabarHidropackSQL.actualizarCambiarCodigo(Sesion.getEquipo().getMaterial().getNumero(), Sesion.getEquipo().getMaterial().getLinea1(), Sesion.getEquipo().getMaterial().getNumeroDocOrigen(), Sesion.getEquipo().getMaterial().getLineaOrigen(), Sesion.getEquipo().getMaterial().getMateriaPrima())) {
                            for (int i = 0; i < Sesion.getDetallePedidosProcesarBodega().size(); i++) {
                                ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(i);
                                if (equipo.getId() == Sesion.getEquipo().getId()) {
                                    equipo.getMaterial().setEstado("2");
                                    Sesion.getDetallePedidosProcesarBodega().set(i, equipo);
                                }
                            }
                            Sesion.setHttp_Body("");
                            Sesion.setTip("Cambio realizado", 0);
                        }
                    }

                    if (Sesion.getHttp_Body().equals("comercialModificarPlantilla")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "PRO_CODPROD";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "PRO_DESC";
                        }
                        ModPlantilla plan = PrmComercial.cambiarMaterialPlantilla(codigo, campo);
                        boolean est = true;
                        for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                            ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                            if (pla.getCodigo().equals(plan.getCodigo())) {
                                est = false;
                                Sesion.setTip("El c�digo que est� intentando agregar ya est� en esta lista", 1);
                                break;
                            }
                            if (pla.getEst() == 1) {
                                est = false;
                                Sesion.getAuxiliar().set(i, plan);
                                break;
                            }
                        }
                        if (est) {
                            Sesion.getAuxiliar().add(plan);
                        }
                    }

                    if (Sesion.getHttp_Body().equals("comercialPresupuestoObra")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "PRO_CODPROD";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "PRO_DESC";
                        }
                        ModPresupuesto pren = PrmComercial.cambiarMaterialPresupuesto(codigo, campo);
                        boolean est = true;
                        for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                            ModPresupuesto pre = (ModPresupuesto) Sesion.getMateriales().get(i);
                            if (pre.getCodigo().equals(pren.getCodigo())) {
                                est = false;
                                Sesion.setTip("El c�digo que est� intentando agregar ya est� en esta lista", 1);
                                break;
                            }
                            if (pre.getEst() == 1) {
                                est = false;
                                float cant = pre.getCantidad();
                                pren.setCantidad(cant);
                                Sesion.getMateriales().set(i, pren);
                                Sesion.setAux("c");
                                break;
                            }
                        }
                        if (est) {
                            Sesion.getMateriales().add(pren);
                        }
                        Sesion.setDatos(PrmComercial.trabajo());
                    }

                    if (Sesion.getHttp_Body().equals("comercialIngresarPresupuesto")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        Sesion.setCliente(PrmComercial.cargarCLiente(codigo, campo));
                        Sesion.setDatos(PrmComercial.trabajo());
                        if (Sesion.getCliente().getRut() > 0) {
                            Sesion.setTip("Cliente cargado", 0);
                        } else {
                            Sesion.setTip("No se pudo cargar el cliente", 1);
                        }
                    }

                    if (Sesion.getHttp_Body().equals("comercialBuscarPresupuestoCliente")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        Sesion.setCliente(PrmComercial.cargarCLiente(codigo, campo));
                        Sesion.setAuxiliar(PrmComercial.listadoPresupuestosObra("nb_rut_cliente = " + Sesion.getCliente().getRut(), 1));
                        Sesion.setAux("");
                    }

                    if (Sesion.getHttp_Body().equals("comercialBuscarObraCliente")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        Sesion.setCliente(PrmComercial.cargarCLiente(codigo, campo));
                        Sesion.setAuxiliar(PrmComercial.listadoObra("tab_texto3 = '" + Sesion.getCliente().getRut() + "'"));
                        Sesion.setAux("");
                    }

                    if (Sesion.getHttp_Body().equals("comercialPresupuestoMantencionCorrectiva")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "PRO_CODPROD";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "PRO_DESC";
                        }
                        ModPresupuesto pren = PrmComercial.cambiarMaterialPresupuesto(codigo, campo);
                        Sesion.getMateriales().add(pren);
                    }

                    if (Sesion.getHttp_Body().equals("comercialBuscarPresupuestoClienteManCorrectiva")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        Sesion.setCliente(PrmComercial.cargarCLiente(codigo, campo));
                        Sesion.setAuxiliar(PrmComercial.listadoPresupuestosManCorrectiva("nb_rut_cliente = " + Sesion.getCliente().getRut(), 2));
                        Sesion.setAux("");
                    }

                    if (Sesion.getHttp_Body().equals("comercialManPrevCliente")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        Sesion.setCliente(PrmComercial.cargarCLiente(codigo, campo));
                        Sesion.setDatos(PrmComercial.frecuencia());
                    }
                    if (Sesion.getHttp_Body().equals("comercialManPreventiva")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        Sesion.setCliente(PrmComercial.cargarCLiente(codigo, campo));
                    }

                    if (Sesion.getHttp_Body().equals("comercialVentaDirecta")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "PRO_CODPROD";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "PRO_DESC";
                        }
                        ModVentaDirecta pren = PrmComercial.agregarMaterialVentaDirecta(codigo, campo);
                        Sesion.getAuxiliar().add(pren);
                    }
                    
                    if (Sesion.getHttp_Body().equals("comercialInformes")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        ModCliente cli = PrmComercial.cargarCLiente(codigo, campo);
                        Sesion.setAuxiliar(PrmComercial.informesResumido("PER_CODIGO = " + cli.getRut()));
                    }
                    
                    if (Sesion.getHttp_Body().equals("comercialVentasDirectas")) {
                        if (!Sesion.getChk_PER_CODIGO().equals("")) {
                            campo = "ltrim(PER_CODIGO)";
                        } else if (!Sesion.getChk_PER_NOMBRE().equals("")) {
                            campo = "ltrim(PER_NOMBRE)";
                        }
                        ModCliente cli = PrmComercial.cargarCLiente(codigo, campo);
                        Sesion.setAuxiliar(PrmComercial.ventasDirectasResumido("PER_CODIGO = " + cli.getRut()));
                    }

                }
            }
            out.println(html);
            out.close();
            //req.getSession().setAttribute("Sesion", Sesion);
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        req.getSession().setAttribute("Sesion", Sesion);
        //return null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
