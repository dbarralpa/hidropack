package sql;

import controllers.PrmApp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class ConexionSQL {

    private Connection con;
    private ModServidorSQL MSMAC;
    private ModServidorSQL MSMAC2BETA;
    private ModServidorSQL MSENS;
    private ModServidorSQL MSSIASCH;
    private ModServidorSQL MSSIAPRUEBA;
    private ModServidorSQL MSBETAPRUEBA;
    private ModServidorSQL MSSIAHIDROPACK;
    private ModServidorSQL MSHIDROPACK;
    public final static int MS_MAC = 1;
    public final static int MS_ENS = 2;
    public final static int MS_SIASCH = 3;
    public final static int MS_MAC2BETA = 4;
    public final static int MS_SIAPRUEBA = 5;
    public final static int MS_SIAHIDROPACK = 6;
    public final static int MS_BETAPRUEBA = 7;
    public final static int MS_HIDROPACK = 8;

    public ConexionSQL() {
        MSMAC = new ModServidorSQL();
        MSMAC2BETA = new ModServidorSQL();
        MSENS = new ModServidorSQL();
        MSSIASCH = new ModServidorSQL();
        MSSIAPRUEBA = new ModServidorSQL();
        MSBETAPRUEBA = new ModServidorSQL();
        MSSIAHIDROPACK = new ModServidorSQL();
        MSHIDROPACK = new ModServidorSQL();
        Properties prop = null;
        try {
            prop = PrmApp.loadProperties("/Application.properties", this.getClass());

            MSMAC.setSvr(prop.getProperty("mssql_MAC.servidor"));
            MSMAC.setPort(Integer.parseInt(prop.getProperty("mssql_MAC.puerto")));
            MSMAC.setBdd(prop.getProperty("mssql_MAC.basedatos"));
            MSMAC.setUsr(prop.getProperty("mssql_MAC.usuario"));
            MSMAC.setPwd(prop.getProperty("mssql_MAC.password"));
            MSMAC.setPrefix(prop.getProperty("mssql_MAC.prefix"));

            MSMAC2BETA.setSvr(prop.getProperty("mssql_MAC2BETA.servidor"));
            MSMAC2BETA.setPort(Integer.parseInt(prop.getProperty("mssql_MAC2BETA.puerto")));
            MSMAC2BETA.setBdd(prop.getProperty("mssql_MAC2BETA.basedatos"));
            MSMAC2BETA.setUsr(prop.getProperty("mssql_MAC2BETA.usuario"));
            MSMAC2BETA.setPwd(prop.getProperty("mssql_MAC2BETA.password"));
            MSMAC2BETA.setPrefix(prop.getProperty("mssql_MAC2BETA.prefix"));

            MSENS.setSvr(prop.getProperty("mssql_ENS.servidor"));
            MSENS.setPort(Integer.parseInt(prop.getProperty("mssql_ENS.puerto")));
            MSENS.setBdd(prop.getProperty("mssql_ENS.basedatos"));
            MSENS.setUsr(prop.getProperty("mssql_ENS.usuario"));
            MSENS.setPwd(prop.getProperty("mssql_ENS.password"));
            MSENS.setPrefix(prop.getProperty("mssql_ENS.prefix"));

            MSSIASCH.setSvr(prop.getProperty("mssql_SIASCH.servidor"));
            MSSIASCH.setPort(Integer.parseInt(prop.getProperty("mssql_SIASCH.puerto")));
            MSSIASCH.setBdd(prop.getProperty("mssql_SIASCH.basedatos"));
            MSSIASCH.setUsr(prop.getProperty("mssql_SIASCH.usuario"));
            MSSIASCH.setPwd(prop.getProperty("mssql_SIASCH.password"));
            MSSIASCH.setPrefix(prop.getProperty("mssql_SIASCH.prefix"));


            MSSIAPRUEBA.setSvr(prop.getProperty("mssql_SIAPRUEBA.servidor"));
            MSSIAPRUEBA.setPort(Integer.parseInt(prop.getProperty("mssql_SIASCH.puerto")));
            MSSIAPRUEBA.setBdd(prop.getProperty("mssql_SIAPRUEBA.basedatos"));
            MSSIAPRUEBA.setUsr(prop.getProperty("mssql_SIAPRUEBA.usuario"));
            MSSIAPRUEBA.setPwd(prop.getProperty("mssql_SIAPRUEBA.password"));
            MSSIAPRUEBA.setPrefix(prop.getProperty("mssql_SIAPRUEBA.prefix"));

            MSBETAPRUEBA.setSvr(prop.getProperty("mssql_BETAPRUEBA.servidor"));
            MSBETAPRUEBA.setPort(Integer.parseInt(prop.getProperty("mssql_BETAPRUEBA.puerto")));
            MSBETAPRUEBA.setBdd(prop.getProperty("mssql_BETAPRUEBA.basedatos"));
            MSBETAPRUEBA.setUsr(prop.getProperty("mssql_BETAPRUEBA.usuario"));
            MSBETAPRUEBA.setPwd(prop.getProperty("mssql_BETAPRUEBA.password"));
            MSBETAPRUEBA.setPrefix(prop.getProperty("mssql_BETAPRUEBA.prefix"));

            MSSIAHIDROPACK.setSvr(prop.getProperty("mssql_SIAHIDROPACK.servidor"));
            MSSIAHIDROPACK.setPort(Integer.parseInt(prop.getProperty("mssql_SIASCH.puerto")));
            MSSIAHIDROPACK.setBdd(prop.getProperty("mssql_SIAHIDROPACK.basedatos"));
            MSSIAHIDROPACK.setUsr(prop.getProperty("mssql_SIAHIDROPACK.usuario"));
            MSSIAHIDROPACK.setPwd(prop.getProperty("mssql_SIAHIDROPACK.password"));
            MSSIAHIDROPACK.setPrefix(prop.getProperty("mssql_SIAHIDROPACK.prefix"));

            MSHIDROPACK.setSvr(prop.getProperty("mssql_HIDROPACK.servidor"));
            MSHIDROPACK.setPort(Integer.parseInt(prop.getProperty("mssql_SIASCH.puerto")));
            MSHIDROPACK.setBdd(prop.getProperty("mssql_HIDROPACK.basedatos"));
            MSHIDROPACK.setUsr(prop.getProperty("mssql_HIDROPACK.usuario"));
            MSHIDROPACK.setPwd(prop.getProperty("mssql_HIDROPACK.password"));
            MSHIDROPACK.setPrefix(prop.getProperty("mssql_HIDROPACK.prefix"));

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    public boolean openSQL(int model) {
        boolean estado = false;
        try {
            if (model == 1) {
                con = MSMAC.ConexionMSSQL();
                MSMAC.setActiva(true);
            } else if (model == 2) {
                con = MSENS.ConexionMSSQL();
                MSENS.setActiva(true);
            } else if (model == 3) {
                con = MSSIASCH.ConexionMSSQL();
                MSSIASCH.setActiva(true);
            } else if (model == 4) {
                con = MSMAC2BETA.ConexionMSSQL();
                MSMAC2BETA.setActiva(true);
            } else if (model == 5) {
                con = MSSIAPRUEBA.ConexionMSSQL();
                MSSIAPRUEBA.setActiva(true);
            } else if (model == 6) {
                con = MSSIAHIDROPACK.ConexionMSSQL();
                MSSIAHIDROPACK.setActiva(true);
            } else if (model == 7) {
                con = MSBETAPRUEBA.ConexionMSSQL();
                MSBETAPRUEBA.setActiva(true);
            } else if (model == 8) {
                con = MSHIDROPACK.ConexionMSSQL();
                MSHIDROPACK.setActiva(true);
            } else {
                con = null;
            }

            if (con != null) {
                con.setAutoCommit(false);
                estado = true;
            } else {
                estado = false;
            }
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return estado;
    }

    public void closeSQL() {
        try {
            con.close();
            con = null;
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
    }

    public void commitSQL(boolean estado) {
        try {
            if (con != null) {
                if (estado) {
                    con.commit();
                } else {
                    con.rollback();
                }
            }
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
    }

    private ModServidorSQL getConActiva() {
        if (MSMAC.isActiva()) {
            return MSMAC;
        } else if (MSENS.isActiva()) {
            return MSENS;
        } else if (MSMAC2BETA.isActiva()) {
            return MSMAC2BETA;
        } else if (MSSIAHIDROPACK.isActiva()) {
            return MSSIAHIDROPACK;
        } else if (MSBETAPRUEBA.isActiva()) {
            return MSBETAPRUEBA;
        } else if (MSHIDROPACK.isActiva()) {
            return MSHIDROPACK;
        } else {
            return new ModServidorSQL();
        }
    }

    public PreparedStatement getPrepareStatement(String query) throws SQLException {
        return con.prepareStatement(query);
    }

    public ResultSet SelectReg(String tabla) throws SQLException {
        return SelectReg(tabla, "*", null);
    }

    public ResultSet SelectReg(String tabla, String condicion) throws SQLException {
        return SelectReg(tabla, "*", condicion, "");
    }

    public ResultSet SelectReg(String tabla, String campos, String condicion) throws SQLException {
        return SelectReg(tabla, campos, condicion, "");
    }

    public ResultSet SelectReg(String tabla, String campos, String condicion, String orden) throws SQLException {
        String sSQL = "";
        ResultSet rs = null;
        String tTabla = "";
        for (int i = 0; i < tabla.split(",").length; i++) {
            tTabla += getConActiva().getPrefix().concat(tabla.split(",")[i].trim());
            if (tabla.split(",").length > 1 && (tabla.split(",").length - 1 > i)) {
                tTabla = tTabla + ", ";
            }
        }
        if (con != null) {
            sSQL = (new StringBuilder()).append("SELECT ").append(campos).append(" FROM ").append(tTabla).toString();
            if (condicion != null) {
                sSQL = (new StringBuilder()).append(sSQL).append(" WHERE ").append(condicion).toString();
            }
            if (!orden.equals("")) {
                sSQL = (new StringBuilder()).append(sSQL).append(" ORDER BY ").append(orden).toString();
            }
            rs = con.createStatement().executeQuery(sSQL);
        }
        return rs;
    }

    public ResultSet SelectRegSinPrefix(String tabla, String campos, String condicion, String orden) throws SQLException {
        String sSQL = "";
        ResultSet rs = null;
        String tTabla = "";
        for (int i = 0; i < tabla.split(",").length; i++) {
            tTabla += tabla.split(",")[i].trim();
            if (tabla.split(",").length > 1 && (tabla.split(",").length - 1 > i)) {
                tTabla = tTabla + ", ";
            }
        }
        if (con != null) {
            sSQL = (new StringBuilder()).append("SELECT ").append(campos).append(" FROM ").append(tTabla).toString();
            if (condicion != null) {
                sSQL = (new StringBuilder()).append(sSQL).append(" WHERE ").append(condicion).toString();
            }
            if (!orden.equals("")) {
                sSQL = (new StringBuilder()).append(sSQL).append(" ORDER BY ").append(orden).toString();
            }
            rs = con.createStatement().executeQuery(sSQL);
        }
        return rs;
    }

    public ResultSet ExecuteQuery(String sql) throws SQLException {
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery(sql);
        }
        return rs;
    }

    public String CallFunction(String funcion, String parametros) throws SQLException {
        return CallFunction(funcion, parametros, true);
    }

    public String CallFunction(String funcion, String parametros, boolean nulo) throws SQLException {
        String aux = "";
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery((new StringBuilder()).append("SELECT ").append(getConActiva().getPrefix()).append(funcion).append("(").append(parametros).append(")").toString());
            if (rs.next()) {
                if (!nulo) {
                    if (rs.getString(1) != null) {
                        aux = rs.getString(1);
                    } else {
                        aux = "";
                    }
                } else {
                    aux = rs.getString(1);
                }
            } else {
                aux = "";
            }
        }
        return aux;
    }

    public double CallFunction_Dbl(String funcion, String parametros) throws SQLException {
        double aux = 0.0;
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery((new StringBuilder()).append("SELECT ").append(getConActiva().getPrefix()).append(funcion).append("(").append(parametros).append(")").toString());
            if (rs.next()) {
                if (rs.getString(1) != null) {
                    aux = rs.getDouble(1);
                } else {
                    aux = 0.0;
                }
            } else {
                aux = 0.0;
            }
        }
        return aux;
    }

    public boolean DeleteReg(String tabla, String condicion) throws SQLException {
        boolean estado = false;
        if (con.createStatement().executeUpdate((new StringBuilder()).append("DELETE FROM ").append(getConActiva().getPrefix()).append(tabla).append(" WHERE ").append(condicion).toString()) > 0) {
            estado = true;
        }
        return estado;
    }
    
    public boolean DeleteRegSinPrefix(String tabla, String condicion) throws SQLException {
        boolean estado = false;
        if (con.createStatement().executeUpdate((new StringBuilder()).append("DELETE FROM ").append(tabla).append(" WHERE ").append(condicion).toString()) > 0) {
            estado = true;
        }
        return estado;
    }

    public boolean InsertReg(String tabla, String campos, String valores) throws SQLException {
        boolean estado = false;
        valores = valores.replace(',', '.');
        valores = valores.replaceAll("'", "\264");
        valores = valores.replaceAll("\n", "");
        valores = valores.replaceAll("\r", "");
        valores = (new StringBuilder()).append("'").append(valores.replaceAll("\267", "','")).append("'").toString();
        valores = valores.replaceAll("'SYSDATE'", "SYSDATE");
        valores = valores.replaceAll("'NULL'", "Null");
        valores = valores.replaceAll("'null'", "Null");
        if (con.createStatement().executeUpdate((new StringBuilder()).append("INSERT INTO ").append(getConActiva().getPrefix()).append(tabla).append("(").append(campos).append(") VALUES(").append(valores).append(")").toString()) > 0) {
            estado = true;
        }

        return estado;
    }

    public boolean InsertRegSinPrefix(String tabla, String campos, String valores) throws SQLException {
        boolean estado = false;
        valores = valores.replace(',', '.');
        valores = valores.replaceAll("'", "\264");
        valores = valores.replaceAll("\n", "");
        valores = valores.replaceAll("\r", "");
        valores = (new StringBuilder()).append("'").append(valores.replaceAll("\267", "','")).append("'").toString();
        valores = valores.replaceAll("'SYSDATE'", "SYSDATE");
        valores = valores.replaceAll("'NULL'", "Null");
        valores = valores.replaceAll("'null'", "Null");
        if (con.createStatement().executeUpdate((new StringBuilder()).append("INSERT INTO ").append(tabla).append("(").append(campos).append(") VALUES(").append(valores).append(")").toString()) > 0) {
            estado = true;
        }

        return estado;
    }

    public boolean UpdateReg(String tabla, String campos, String valores, String condicion) throws SQLException {
        boolean estado = false;
        String sSQL = "";
        String cc[] = null;
        String vv[] = null;
        valores = valores.replace(',', '.');
        valores = valores.replaceAll("'", "\264");
        valores = valores.replaceAll("\n", "");
        valores = valores.replaceAll("\r", "");
        cc = campos.split(",");
        vv = valores.split("\267");
        for (int i = 0; i < cc.length; i++) {
            if (!sSQL.equals("")) {
                sSQL = (new StringBuilder()).append(sSQL).append(", ").toString();
            }
            if (vv[i].toLowerCase().equals("null")) {
                sSQL = (new StringBuilder()).append(sSQL).append(cc[i]).append("=Null").toString();
            } else {
                sSQL = (new StringBuilder()).append(sSQL).append(cc[i]).append("='").append(vv[i]).append("'").toString();
            }
        }

        sSQL = (new StringBuilder()).append("UPDATE ").append(getConActiva().getPrefix()).append(tabla).append(" SET ").append(sSQL).toString();
        sSQL = (new StringBuilder()).append(sSQL).append(" WHERE ").append(condicion).toString();

        if (con.createStatement().executeUpdate(sSQL) > 0) {
            estado = true;
        }

        return estado;
    }

    public boolean UpdateRegSinPrefix(String tabla, String campos, String valores, String condicion) throws SQLException {
        boolean estado = false;
        String sSQL = "";
        String cc[] = null;
        String vv[] = null;
        valores = valores.replace(',', '.');
        valores = valores.replaceAll("'", "\264");
        valores = valores.replaceAll("\n", "");
        valores = valores.replaceAll("\r", "");
        cc = campos.split(",");
        vv = valores.split("\267");
        for (int i = 0; i < cc.length; i++) {
            if (!sSQL.equals("")) {
                sSQL = (new StringBuilder()).append(sSQL).append(", ").toString();
            }
            if (vv[i].toLowerCase().equals("null")) {
                sSQL = (new StringBuilder()).append(sSQL).append(cc[i]).append("=Null").toString();
            } else {
                sSQL = (new StringBuilder()).append(sSQL).append(cc[i]).append("='").append(vv[i]).append("'").toString();
            }
        }

        sSQL = (new StringBuilder()).append("UPDATE ").append(tabla).append(" SET ").append(sSQL).toString();
        sSQL = (new StringBuilder()).append(sSQL).append(" WHERE ").append(condicion).toString();

        if (con.createStatement().executeUpdate(sSQL) > 0) {
            estado = true;
        }

        return estado;
    }

    public double getDouble_Function(String func, String param) throws SQLException {
        String sSQL = "";
        ResultSet rx = null;
        double tmp = 0.0;
        if (con != null) {
            sSQL = "SELECT " + getConActiva().getPrefix() + "." + func + "(" + param + ")";
            rx = con.createStatement().executeQuery(sSQL);
            if (rx.next()) {
                tmp = rx.getDouble(1);
            }
            rx.close();
        }
        return tmp;
    }

    public int getInt_Function(String func, String param) throws SQLException {
        String sSQL = "";
        ResultSet rx = null;
        int tmp = 0;
        if (con != null) {
            sSQL = (new StringBuilder()).append("SELECT ").append(getConActiva().getPrefix()).append(func).append("(").append(param).append(")").toString();
            rx = con.createStatement().executeQuery(sSQL);
            if (rx.next()) {
                tmp = rx.getInt(1);
            }
            rx.close();
        }
        return tmp;
    }

    public ResultSet ExecuteProcedure(String proced, String args) throws SQLException {
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery((new StringBuilder()).append(proced).append(" ").append(args).toString());
        }
        return rs;
    }
}
