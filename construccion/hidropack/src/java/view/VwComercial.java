package view;

import controllers.PrmApp;
import controllers.PrmComercial;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import model.ModSesion;
import model.comercial.ModCliente;
import model.comercial.ModInforme;
import model.comercial.ModMantencion;
import model.comercial.ModPlantilla;
import model.comercial.ModPresupuesto;
import model.comercial.ModProServ;
import model.comercial.ModPrueba;
import model.comercial.ModPrueba1;
import model.comercial.ModTrabajo;
import model.comercial.ModVentaDirecta;
import model.hidropack.ModObra;
import util.HelperComercial;

public class VwComercial extends PrmApp {

    DecimalFormat number = new DecimalFormat("###,###.##");
    static final SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");

    public String comboProductoOServicio(HttpServletRequest req, boolean est) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbProdServ\" id=\"cmbProdServ\" onchange='submit();javascript:loading();'>";
            html += "<option value='-1'>Elija un producto o servicio</option>";
            for (int i = 0; i < Sesion.getDatos().size(); i++) {
                ModProServ tra = (ModProServ) Sesion.getDatos().get(i);
                if (est) {
                    if (!tra.getTipo().equals("B")) {
                        html += "<option value=\"" + tra.getCodigo() + "\">";
                        html += tra.getDescripcion() + "</option>";
                    }
                } else {
                    html += "<option value=\"" + tra.getCodigo() + "\">";
                    html += tra.getDescripcion() + "</option>";
                }
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboPlantilla(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbPlantilla\" id=\"cmbPlantilla\" onchange='submit();javascript:loading();'>";
            html += "<option value='-1'>Elija una plantilla</option>";
            for (int i = 0; i < Sesion.getDatos().size(); i++) {
                ModProServ tra = (ModProServ) Sesion.getDatos().get(i);
                html += "<option value=\"" + tra.getDescripcion() + "\">";
                html += tra.getDescripcion() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboFrecuencia(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbFrecuencia\" id=\"cmbFrecuencia\" style='width: 205px;'>";
            html += "<option value='-1'>Elija frecuencia</option>";
            for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                ModProServ tra = (ModProServ) Sesion.getMateriales().get(i);
                html += "<option value=\"" + tra.getCodigo() + "\">";
                html += tra.getDescripcion() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboFrecuenciaSelected(HttpServletRequest req, int frecuencia) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbFrecuencia\" id=\"cmbFrecuencia\" style='width: 205px;'>";
            html += "<option value='-1'>Elija frecuencia</option>";
            for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                ModProServ tra = (ModProServ) Sesion.getMateriales().get(i);
                if (frecuencia == tra.getCodigo()) {
                    html += "<option value=\"" + tra.getCodigo() + "\" selected>";
                } else {
                    html += "<option value=\"" + tra.getCodigo() + "\">";
                }
                html += tra.getDescripcion() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String comboTrabajo(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbTrabajo\" id=\"cmbTrabajo\">";
            html += "<option value='-1'>Elija un trabajo</option>";
            for (int i = 0; i < Sesion.getDatos().size(); i++) {
                ModTrabajo tra = (ModTrabajo) Sesion.getDatos().get(i);
                html += "<option value=\"" + tra.getCodigo() + "\">";
                html += tra.getDescripcion() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String grillaPlantillaObra(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");


            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>Id producto</td>";
            html += "<td class='Estilo1' width='300'>Descripci�n</td>";
            html += "<td class='Estilo1' width='40'>Unidad</td>";
            html += "<td class='Estilo1' width='80'>Cantidad</td>";
            html += "<td class='Estilo1' width='100'>Precio unitario</td>";
            html += "<td class='Estilo1' width='100'>Costo total</td>";
            html += "<td class='Estilo1' width='30' " + verOverLib("Comercial", "Eliminar de la plantilla") + ">Eli</td>";
            html += "</tr>";
            double unitario = 0d;
            double total = 0d;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                if (pla.getEst() == 1) {
                    html += "<tr bgcolor='#FF0000'>";
                } else {
                    html += "<tr>";
                }
                html += "<td class='Estilo1' width='100'><a href='../FrmComercial?cambioCodigoPlantillaObra=" + i + "' onclick='javascript:loading();'>" + pla.getCodigo() + "</a></td>";
                html += "<td class='Estilo1' width='300'>" + pla.getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='100'>" + pla.getUm() + "</td>";
                html += "<td class='Estilo1' width='100'><input type='text' size='7' onkeypress=\"return justNumbers(event);\" name='modificarCantidad" + i + "' id='modificarCantidad" + i + "' value='" + pla.getCantidad() + "'></td>";
                html += "<td class='Estilo1' width='80'><input type='text' size='7' onkeypress=\"return justNumbers(event);\" name='modificarPrecioUnitario" + i + "' id='modificarPrecioUnitario" + i + "' value='" + pla.getPrecioUnitario() + "'></td>";
                html += "<td class='Estilo1' width='40'>" + number.format(pla.getPrecioCosto()) + "</td>";
                html += "<td class='Estilo1' width='40'><input type='checkbox' name='eliMaterialPlantilla" + i + "' id='eliMaterialPlantilla" + i + "' onclick='eliminarMaterial(this)'></td>";
                html += "</tr>";
                unitario += pla.getPrecioUnitario();
                total += pla.getPrecioCosto();
            }
            html += "<tr>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td class='Estilo1'>Total</td>";
            html += "<td class='Estilo1'>" + number.format(unitario) + "</td>";
            html += "<td class='Estilo1'>" + number.format(total) + "</td>";
            html += "<td>&nbsp;</td>";
            html += "</tr>";
            html += "</table>";


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String agregarMaterialesAPlantilla() {
        String html = "";
        try {

            html += "<tr>";
            html += "<td style='padding-top: 20px'>";
            html += "<table border='0' cellpadding='1' cellspacing='1'>";
            html += "<tr bgcolor='#FAE8D8'>";
            //html += "<td class='Estilo1'>Id producto:&nbsp;&nbsp;<input type='text' size='5' name='codigoMaterial'  id='codigoMaterial' value='0'></td>";
            html += "<td class='Estilo1'>Descripci�n:&nbsp;&nbsp;<input type='text' size='10' name='descMaterial'  id='descMaterial' value=''></td>";
            html += "<td class='Estilo1'>Unidad:&nbsp;&nbsp;<input type='text' size='5' name='uniMaterial'  id='uniMaterial' value=''></td>";
            html += "<td class='Estilo1'>Cantidad:&nbsp;&nbsp;<input type='text' size='5' name='cantMaterial'  id='cantMaterial' value='0'></td>";
            html += "<td class='Estilo1'>Precio unitario:&nbsp;&nbsp;<input type='text' size='5' name='precioUniMaterial'  id='precioUniMaterial' value='0'></td>";
            html += "<td>&nbsp;<input type='submit' class='Estilo1' name='agregarMaterialPlantilla'  id='agregarMaterialPlantilla' value='Agregar material'></td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    private String comboTrabajoSelected(HttpServletRequest req, int codigo) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select class='Estilo1' name=\"cmbTrabajo\" id=\"cmbTrabajo\">";
            html += "<option value='-1'>Elija un trabajo</option>";
            for (int i = 0; i < Sesion.getDatos().size(); i++) {
                ModTrabajo tra = (ModTrabajo) Sesion.getDatos().get(i);
                if (codigo == tra.getCodigo()) {
                    html += "<option value=\"" + tra.getCodigo() + "\" selected>";
                } else {
                    html += "<option value=\"" + tra.getCodigo() + "\">";
                }
                html += tra.getDescripcion() + "</option>";
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String grillaPresupuestosObraResumido(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" class=\"Estilo1\" style=\"padding-top: 20px;\">PRESUPUESTOS</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='80'>N� presupuesto</td>";
            html += "<td class='Estilo1' width='300'>Cliente</td>";
            html += "<td class='Estilo1' width='80'>Fecha</td>";
            html += "<td class='Estilo1' width='80'>Usuario</td>";
            html += "<td class='Estilo1' width='60'>Costo total</td>";
            html += "<td class='Estilo1' width='100'>Plantilla</td>";
            html += "<td class='Estilo1' width='100'>Trabajo</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModPresupuesto pre = (ModPresupuesto) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='80'><a href='../FrmComercial?numeroPresupuestoObra=" + i + "' onclick='javascript:loading();'>" + pre.getNumero() + "</td>";
                html += "<td class='Estilo1' width='300' " + verOverLib("Comercial", pre.getCliente().getNombre()) + ">" + verHtmlNull(pre.getCliente().getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + pre.getFecha() + "</td>";
                html += "<td class='Estilo1' width='80'>" + pre.getUsuario() + "</td>";
                html += "<td class='Estilo1' width='60'>" + pre.getTotal() + "</td>";
                html += "<td class='Estilo1' width='100'>" + pre.getPlantilla() + "</td>";
                html += "<td class='Estilo1' width='100'>" + comboTrabajoSelected(req, pre.getTrabajo().getCodigo()) + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaPresupuestosObra(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        double precioUnitario = 0d;
        double precioTotal = 0d;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0' width='720'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>Id producto</td>";
            html += "<td class='Estilo1' width='300'>Descripci�n</td>";
            html += "<td class='Estilo1' width='40'>Unidad</td>";
            html += "<td class='Estilo1' width='80'>Cantidad</td>";
            html += "<td class='Estilo1' width='100'>Precio unitario</td>";
            html += "<td class='Estilo1' width='100'>Costo total</td>";
            html += "<td class='Estilo1' width='30' " + verHtmlNull("Eliminar del presupuesto") + ">Eli</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                precioUnitario += pla.getPrecioUnitario();
                precioTotal += pla.getPrecioTotal();
                if (pla.getEst() == 1) {
                    html += "<tr bgcolor='#FF0000'>";
                } else {
                    html += "<tr>";
                }
                html += "<td class='Estilo1' width='100'><a href='../FrmComercial?cambioCodigoPresupuestoObra=" + i + "' onclick='javascript:loading();'>" + pla.getCodigo() + "</td>";
                html += "<td class='Estilo1' width='300'>" + pla.getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='40'>" + pla.getUm() + "</td>";
                html += "<td class='Estilo1' width='80'><input type='text' size='7' onkeypress=\"return justNumbers(event);\" name='modificarCantidad" + i + "' id='modificarCantidad" + i + "' value='" + pla.getCantidad() + "'></td>";
                html += "<td class='Estilo1' width='100'><input type='text' size='7' onkeypress=\"return justNumbers(event);\" name='modificarPrecioUnutario" + i + "' id='modificarPrecioUnutario" + i + "' value='" + pla.getPrecioUnitario() + "'></td>";
                html += "<td class='Estilo1' width='100'>" + number.format(pla.getPrecioTotal()) + "</td>";
                html += "<td class='Estilo1' width='30'><input type='checkbox' name='eliminarDePresupuestoObra" + i + "' onclick='eliminarMaterial(this)' id='eliminarDePresupuestoObra" + i + "'></td>";
                html += "</tr>";
            }
            html += "<tr>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td class='Estilo1'>Total</td>";
            html += "<td class='Estilo1'>" + number.format(precioUnitario) + "</td>";
            html += "<td class='Estilo1'>" + number.format(precioTotal) + "</td>";
            html += "<td>&nbsp;</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaObraResumido(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" class=\"Estilo1\" style=\"padding-top: 20px;\">OBRAS</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='60'>N� obra</td>";
            html += "<td class='Estilo1' width='200'>Nombre</td>";
            html += "<td class='Estilo1' width='80'>Fecha ingreso</td>";
            html += "<td class='Estilo1' width='80'>Venta estimada</td>";
            html += "<td class='Estilo1' width='80'>Costo servicio</td>";
            html += "<td class='Estilo1' width='200'>Nombre Cliente</td>";
            html += "<td class='Estilo1' width='80'>Fecha cliente</td>";
            html += "<td class='Estilo1' width='80'>N� presupuesto</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModObra obra = (ModObra) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='60'><a href='../FrmComercial?numeroObra=" + i + "' onclick='javascript:loading();'>" + obra.getIdObra() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", obra.getNombreObra()) + ">" + verHtmlNull(obra.getNombreObra(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getFecha() + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getVentaEstimada() + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getCostoServicio() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", obra.getCliente().getNombre()) + ">" + verHtmlNull(obra.getCliente().getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getCliente().getFecha() + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getNumPresupuesto() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String agregarMaterialesAMantencionCorrectiva() {
        String html = "";
        try {

            html += "<tr>";
            html += "<td style='padding-top: 20px'>";
            html += "<table border='0' cellpadding='1' cellspacing='1'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1'>Descripci�n:&nbsp;&nbsp;<input autocomplete=\"off\" type='text' size='30' name='descMaterial'  id='descMaterial' value=''></td>";
            html += "<td class='Estilo1'>Cantidad:&nbsp;&nbsp;<input onkeypress=\"return justNumbers(event);\" autocomplete=\"off\" type='text' size='10' name='cantidadMaterial'  id='cantidadMaterial' value=''></td>";
            html += "<td class='Estilo1'>Costo:&nbsp;&nbsp;<input onkeypress=\"return justNumbers(event);\" autocomplete=\"off\" type='text' size='10' name='costoMaterial'  id='costoMaterial' value=''></td>";
            html += "<td class='Estilo1'>Instalaci�n:&nbsp;&nbsp;<input autocomplete=\"off\" type='text' size='10' name='instalacionMaterial'  id='instalacionMaterial' value=''></td>";
            html += "<td>&nbsp;<input type='submit' class='Estilo1' name='agregarMaterialMantencionCorrectiva'  id='agregarMaterialMantencionCorrectiva' value='Agregar material'></td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaPresupuestosMantencionCorrectiva(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        float cantidad = 0f;
        double precioUnitario = 0d;
        double precioTotal = 0d;
        double instalacion = 0d;
        double precio = 0d;
        double margen = 0d;
        double ee = 0d;
        double uf = 0d;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td class='Estilo1' style='padding-top: 20px;'>Valor UF:&nbsp;<input autocomplete=\"off\" type='text' onkeypress=\"return justNumbers(event);\" size='20' name='unidadFomento' id='unidadFomento' value='" + Sesion.getAux1() + "'></td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td>";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>Id producto</td>";
            html += "<td class='Estilo1' width='200'>Descripci�n</td>";
            html += "<td class='Estilo1' width='30'>Um</td>";
            html += "<td class='Estilo1' width='100'>Cantidad</td>";
            html += "<td class='Estilo1' width='100'>Costo unitario</td>";
            html += "<td class='Estilo1' width='100'>Costo total</td>";
            html += "<td class='Estilo1' width='100'>Instalaci�n</td>";
            html += "<td class='Estilo1' width='80'>Precio</td>";
            html += "<td class='Estilo1' width='80'>Margen</td>";
            html += "<td class='Estilo1' width='80'>&nbsp;</td>";
            html += "<td class='Estilo1' width='80'>UF</td>";
            html += "<td class='Estilo1' width='30' " + verOverLib("Comercial", "Eliminar del presupuesto") + ">Eli</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                cantidad += pla.getCantidad();
                precioUnitario += pla.getPrecioUnitario();
                precioTotal += pla.getPrecioTotal();
                instalacion += pla.getInstalacion();
                precio += ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7);
                margen += (((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion()));
                ee += ((((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion())) / ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7));
                uf += ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) / Sesion.getAux1();
                html += "<tr>";
                if (!pla.getCodigo().trim().equals("")) {
                    html += "<td class='Estilo1' width='100'>" + pla.getCodigo() + "</td>";
                } else {
                    html += "<td class='Estilo1' width='100'>&nbsp;</td>";
                }
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", pla.getDescripcion()) + ">" + verHtmlNull(pla.getDescripcion(), 30) + "</td>";
                html += "<td class='Estilo1' width='30'>" + pla.getUm() + "</td>";
                html += "<td class='Estilo1' width='100'><input type='text' size='7' autocomplete=\"off\" onkeypress=\"return justNumbers(event);\" name='modificarCantidad" + i + "' id='modificarCantidad" + i + "' value='" + pla.getCantidad() + "'></td>";
                html += "<td class='Estilo1' width='100'><input type='text' size='7' autocomplete=\"off\" onkeypress=\"return justNumbers(event);\" name='modificarCosto" + i + "' id='modificarCosto" + i + "' value='" + pla.getPrecioUnitario() + "'></td>";
                html += "<td class='Estilo1' width='100'>" + pla.getPrecioUnitario() + "</td>";
                html += "<td class='Estilo1' width='100'><input type='text' size='7' autocomplete=\"off\" onkeypress=\"return justNumbers(event);\" name='modificarInstalacion" + i + "' id='modificarInstalacion" + i + "' value='" + pla.getInstalacion() + "'></td>";
                html += "<td class='Estilo1' width='80'>" + number.format(((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7)) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format((((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion()))) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(((((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion())) / ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7))) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) / Sesion.getAux1()) + "</td>";
                html += "<td class='Estilo1' width='30'><input type='checkbox' name='eliminarDePresupuestoMantencionCorrectiva" + i + "' id='eliminarDePresupuestoMantencionCorrectiva" + i + "' onclick='eliminarMaterial(this)'></td>";
                html += "</tr>";
            }
            html += "<tr>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td class='Estilo1'>Total</td>";
            html += "<td class='Estilo1'>" + number.format(cantidad) + "</td>";
            html += "<td class='Estilo1'>" + number.format(precioUnitario) + "</td>";
            html += "<td class='Estilo1'>" + number.format(precioTotal) + "</td>";
            html += "<td class='Estilo1'>" + number.format(instalacion) + "</td>";
            html += "<td class='Estilo1'>" + number.format(precio) + "</td>";
            html += "<td class='Estilo1'>" + number.format(margen) + "</td>";
            html += "<td class='Estilo1'>" + number.format(ee) + "</td>";
            html += "<td class='Estilo1'>" + number.format(uf) + "</td>";
            html += "<td>&nbsp;</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaPresupuestosManCorrectivaResumido(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" class=\"Estilo1\" style=\"padding-top: 20px;\">PRESUPUESTOS</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='80'>N� presupuesto</td>";
            html += "<td class='Estilo1' width='300'>Cliente</td>";
            html += "<td class='Estilo1' width='80'>Fecha</td>";
            html += "<td class='Estilo1' width='80'>Usuario</td>";
            html += "<td class='Estilo1' width='60'>Costo mantenci�n</td>";
            html += "<td class='Estilo1' width='60'>Costo instalacion</td>";
            html += "<td class='Estilo1' width='60'>UF</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModPresupuesto pre = (ModPresupuesto) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='80'><a href='../FrmComercial?numeroPresupuestoManCorrectiva=" + i + "' onclick='javascript:loading();'>" + pre.getNumero() + "</td>";
                html += "<td class='Estilo1' width='300' " + verOverLib("Comercial", pre.getCliente().getNombre()) + ">" + verHtmlNull(pre.getCliente().getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + pre.getFecha() + "</td>";
                html += "<td class='Estilo1' width='80'>" + pre.getUsuario() + "</td>";
                html += "<td class='Estilo1' width='60'>" + pre.getPrecioTotal() + "</td>";
                html += "<td class='Estilo1' width='60'>" + pre.getInstalacion() + "</td>";
                html += "<td class='Estilo1' width='60'>" + pre.getUf() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaPresupuestosMantencionCorrectivaNumero(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        float cantidad = 0f;
        double precioUnitario = 0d;
        double precioTotal = 0d;
        double instalacion = 0d;
        double precio = 0d;
        double margen = 0d;
        double ee = 0d;
        double uf = 0d;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td style='padding-top: 20px;'>";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>Id producto</td>";
            html += "<td class='Estilo1' width='200'>Descripci�n</td>";
            html += "<td class='Estilo1' width='30'>Um</td>";
            html += "<td class='Estilo1' width='100'>Cantidad</td>";
            html += "<td class='Estilo1' width='100'>Costo unitario</td>";
            html += "<td class='Estilo1' width='100'>Costo total</td>";
            html += "<td class='Estilo1' width='100'>Instalaci�n</td>";
            html += "<td class='Estilo1' width='80'>Precio</td>";
            html += "<td class='Estilo1' width='80'>Margen</td>";
            html += "<td class='Estilo1' width='80'>&nbsp;</td>";
            html += "<td class='Estilo1' width='80'>UF</td>";
            html += "<td class='Estilo1' width='30' " + verOverLib("Comercial", "Eliminar del presupuesto") + ">Eli</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                cantidad += pla.getCantidad();
                precioUnitario += pla.getPrecioUnitario();
                precioTotal += pla.getPrecioTotal();
                instalacion += pla.getInstalacion();
                precio += ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7);
                margen += (((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion()));
                ee += ((((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion())) / ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7));
                uf += ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) / pla.getUf();
                html += "<tr>";
                if (!pla.getCodigo().trim().equals("")) {
                    html += "<td class='Estilo1' width='100'>" + pla.getCodigo() + "</td>";
                } else {
                    html += "<td class='Estilo1' width='100'>&nbsp;</td>";
                }
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", pla.getDescripcion()) + ">" + verHtmlNull(pla.getDescripcion(), 30) + "</td>";
                if (!pla.getUm().trim().equals("")) {
                    html += "<td class='Estilo1' width='30'>" + pla.getUm() + "</td>";
                } else {
                    html += "<td class='Estilo1' width='30'>&nbsp;</td>";
                }
                html += "<td class='Estilo1' width='100'><input type='text' size='7' autocomplete=\"off\" onkeypress=\"return justNumbers(event);\" name='modificarCantidad" + i + "' id='modificarCantidad" + i + "' value='" + pla.getCantidad() + "'></td>";
                html += "<td class='Estilo1' width='100'><input type='text' size='7' autocomplete=\"off\" onkeypress=\"return justNumbers(event);\" name='modificarCosto" + i + "' id='modificarCosto" + i + "' value='" + pla.getPrecioUnitario() + "'></td>";
                html += "<td class='Estilo1' width='100'>" + pla.getPrecioTotal() + "</td>";
                html += "<td class='Estilo1' width='100'><input type='text' size='7' autocomplete=\"off\" onkeypress=\"return justNumbers(event);\" name='modificarInstalacion" + i + "' id='modificarInstalacion" + i + "' value='" + pla.getInstalacion() + "'></td>";
                html += "<td class='Estilo1' width='80'>" + number.format(((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7)) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format((((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion()))) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(((((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) - (pla.getPrecioTotal() + pla.getInstalacion())) / ((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7))) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(((pla.getPrecioTotal() + pla.getInstalacion()) * 1.7) / pla.getUf()) + "</td>";
                html += "<td class='Estilo1' width='30'><input type='checkbox' name='eliminarDePresupuestoMantencionCorrectiva" + i + "' id='eliminarDePresupuestoMantencionCorrectiva" + i + "' onclick='eliminarMaterial(this)'></td>";
                html += "</tr>";
            }
            html += "<tr>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td class='Estilo1'>Total</td>";
            html += "<td class='Estilo1'>" + number.format(cantidad) + "</td>";
            html += "<td class='Estilo1'>" + number.format(precioUnitario) + "</td>";
            html += "<td class='Estilo1'>" + number.format(precioTotal) + "</td>";
            html += "<td class='Estilo1'>" + number.format(instalacion) + "</td>";
            html += "<td class='Estilo1'>" + number.format(precio) + "</td>";
            html += "<td class='Estilo1'>" + number.format(margen) + "</td>";
            html += "<td class='Estilo1'>" + number.format(ee) + "</td>";
            html += "<td class='Estilo1'>" + number.format(uf) + "</td>";
            html += "<td>&nbsp;</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaManCorrectivaResumido(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='60'>N� mantenci�n</td>";
            html += "<td class='Estilo1' width='200'>Nombre</td>";
            html += "<td class='Estilo1' width='80'>Fecha ingreso</td>";
            html += "<td class='Estilo1' width='80'>Venta estimada</td>";
            html += "<td class='Estilo1' width='200'>Nombre Cliente</td>";
            html += "<td class='Estilo1' width='80'>Fecha cliente</td>";
            html += "<td class='Estilo1' width='80'>N� presupuesto</td>";
            html += "<td class='Estilo1' width='300'>Descripci�n</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModObra obra = (ModObra) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='60'><a href='../FrmComercial?numeroObraManCorre=" + i + "' onclick='javascript:loading();'>" + obra.getIdObra() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", obra.getNombreObra()) + ">" + verHtmlNull(obra.getNombreObra(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getFecha() + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getVentaEstimada() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", obra.getCliente().getNombre()) + ">" + verHtmlNull(obra.getCliente().getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getCliente().getFecha() + "</td>";
                html += "<td class='Estilo1' width='80'>" + obra.getNumPresupuesto() + "</td>";
                html += "<td class='Estilo1' width='300' " + verOverLib("Comercial", obra.getDescripcion()) + ">" + verHtmlNull(obra.getDescripcion(), 80) + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaManPreventivaResumido(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='60'>N� mantenci�n</td>";
            html += "<td class='Estilo1' width='200'>Nombre</td>";
            html += "<td class='Estilo1' width='80'>Fecha ingreso</td>";
            html += "<td class='Estilo1' width='200'>Cliente</td>";
            html += "<td class='Estilo1' width='80'>UF</td>";
            html += "<td class='Estilo1' width='80'>Valor en (UF)</td>";
            html += "<td class='Estilo1' width='80'>Monto</td>";
            html += "<td class='Estilo1' width='80'>Frecuencia</td>";
            html += "<td class='Estilo1' width='200'>Observaci�n</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModMantencion man = (ModMantencion) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='60'><a href='../FrmComercial?numeroObraManPrev=" + i + "' onclick='javascript:loading();'>" + man.getNumero() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", man.getNombre()) + ">" + verHtmlNull(man.getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + man.getFecha() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", man.getCliente().getNombre()) + ">" + verHtmlNull(man.getCliente().getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + man.getUf() + "</td>";
                html += "<td class='Estilo1' width='80'>" + man.getValorUf() + "</td>";
                html += "<td class='Estilo1' width='80'>$&nbsp;" + number.format(man.getValorUf() * man.getUf()) + "</td>";
                html += "<td class='Estilo1' width='80'>" + HelperComercial.comboFrecuenciaSelected(req, man.getFrecuencia()) + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", man.getObservacion()) + ">" + verHtmlNull(man.getObservacion(), 40) + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaVentaDirecta(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");


            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>Id producto</td>";
            html += "<td class='Estilo1' width='300'>Descripci�n</td>";
            html += "<td class='Estilo1' width='40'>Unidad</td>";
            html += "<td class='Estilo1' width='80'>Cantidad</td>";
            html += "<td class='Estilo1' width='80'>Precio unitario</td>";
            html += "<td class='Estilo1' width='80'>Total</td>";
            html += "<td class='Estilo1' width='30' " + verOverLib("Comercial", "Eliminar de la venta directa") + ">Eli</td>";
            html += "</tr>";
            double precio = 0d;
            double total = 0d;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModVentaDirecta pla = (ModVentaDirecta) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='100'>" + pla.getCodigo() + "</td>";
                html += "<td class='Estilo1' width='300'>" + pla.getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='40'>" + pla.getUm() + "</td>";
                html += "<td class='Estilo1' width='80'><input type='text' size='7' onkeypress=\"return justNumbers(event);\" name='modificarCantidad" + i + "' id='modificarCantidad" + i + "' value='" + pla.getCantidad() + "'></td>";
                html += "<td class='Estilo1' width='80'>" + number.format(pla.getPrecio()) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(pla.getPrecio() * pla.getCantidad()) + "</td>";
                html += "<td class='Estilo1' width='30'><input type='checkbox' name='eliMaterialVentaDirecta" + i + "' id='eliMaterialVentaDirecta" + i + "' onclick='eliminarMaterial(this)'></td>";
                html += "</tr>";
                precio += pla.getPrecio();
                total += pla.getPrecio() * pla.getCantidad();
            }
            html += "<tr>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td class='Estilo1'>Total</td>";
            html += "<td class='Estilo1'>" + number.format(precio) + "</td>";
            html += "<td class='Estilo1'>" + number.format(total) + "</td>";
            html += "<td>&nbsp;</td>";
            html += "</tr>";
            html += "</table>";


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaInformesResumido(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='60'>N�</td>";
            html += "<td class='Estilo1' width='200'>Nombre</td>";
            html += "<td class='Estilo1' width='80'>Fecha ingreso</td>";
            html += "<td class='Estilo1' width='200'>Cliente</td>";
            html += "<td class='Estilo1' width='80'>Trabajo</td>";
            html += "<td class='Estilo1' width='80'>Costo ppto</td>";
            html += "<td class='Estilo1' width='80'>Costo guias</td>";
            html += "<td class='Estilo1' width='80'>Dev. mat</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModInforme man = (ModInforme) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='60'><a href='../Informes?idNumeroInforme=" + i + "'>" + man.getNumero() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", man.getNombre()) + ">" + verHtmlNull(man.getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + man.getFecha() + "</td>";
                html += "<td class='Estilo1' width='200' " + verOverLib("Comercial", man.getCliente().getNombre()) + ">" + verHtmlNull(man.getCliente().getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + man.getNombreTrabajo() + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(man.getCostoPresupuesto()) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(man.getCostoGuia()) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(man.getCostoDevolucion()) + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaVentasDirectasResumido(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='80'>N� Venta</td>";
            html += "<td class='Estilo1' width='300'>Cliente</td>";
            html += "<td class='Estilo1' width='80'>Fecha</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModObra pre = (ModObra) Sesion.getAuxiliar().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='80'><a href='../FrmComercial?numeroVentaDirecta=" + i + "' onclick='javascript:loading();'>" + pre.getIdObra() + "</td>";
                html += "<td class='Estilo1' width='300' " + verOverLib("Comercial", pre.getCliente().getNombre()) + ">" + verHtmlNull(pre.getCliente().getNombre(), 40) + "</td>";
                html += "<td class='Estilo1' width='80'>" + pre.getFecha() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaVentaDirectaDetalle(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='100'>Id producto</td>";
            html += "<td class='Estilo1' width='300'>Descripci�n</td>";
            html += "<td class='Estilo1' width='40'>Unidad</td>";
            html += "<td class='Estilo1' width='80'>Cantidad</td>";
            html += "<td class='Estilo1' width='80'>Precio unitario</td>";
            html += "<td class='Estilo1' width='80'>Total</td>";
            html += "</tr>";
            double precio = 0d;
            double total = 0d;
            for (int i = 0; i < Sesion.getAuxiliar2().size(); i++) {
                ModVentaDirecta pla = (ModVentaDirecta) Sesion.getAuxiliar2().get(i);
                html += "<tr>";
                html += "<td class='Estilo1' width='100'>" + pla.getCodigo() + "</td>";
                html += "<td class='Estilo1' width='300'>" + pla.getDescripcion() + "</td>";
                html += "<td class='Estilo1' width='40'>" + pla.getUm() + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(pla.getCantidad()) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(pla.getPrecio()) + "</td>";
                html += "<td class='Estilo1' width='80'>" + number.format(pla.getPrecio() * pla.getCantidad()) + "</td>";
                html += "</tr>";
                precio += pla.getPrecio();
                total += pla.getPrecio() * pla.getCantidad();
            }
            html += "<tr>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td>&nbsp;</td>";
            html += "<td class='Estilo1'>Total</td>";
            html += "<td class='Estilo1'>" + number.format(precio) + "</td>";
            html += "<td class='Estilo1'>" + number.format(total) + "</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String prueba(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        ArrayList compradores = new ArrayList();
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html += "<tr>";
            html += "<td align=\"center\" style=\"padding-top: 20px;\">";
            html += "<table border='1' cellpadding='0' cellspacing='0'>";
            html += "<tr bgcolor='#FAE8D8'>";
            html += "<td class='Estilo1' width='250'>Evento</td>";
            html += "<td class='Estilo1' width='100'>Comprador</td>";
            html += "<td class='Estilo1' width='100'>" + fecha.format(new Date()) + "</td>";
            html += "</tr>";
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModCliente man = (ModCliente) Sesion.getAuxiliar().get(i);
                ArrayList auxiliar2 = PrmComercial.codigos(man.getNombre());
                int ocVencidaNece = 0;
                int ocFaltantesNece = 0;
                int ocVencidaStockM = 0;
                int ocFaltantesStockM = 0;
                int sobrepasado = 0;
                for (int r = 0; r < auxiliar2.size(); r++) {
                    ModPrueba pru = (ModPrueba) auxiliar2.get(r);
                    if (pru.getCodigo1().length() > 1) {
                        //Necesidad de material con OC vencida
                        if (pru.getOcAtrasada().length() > 1) {
                            ocVencidaNece++;
                        }
                        //Necesidad de material sin OC
                        double cant = 0;
                        double ocFaltantes = 0;
                        if (pru.getLeadTime() > 59) {
                            cant = pru.getNece1() + pru.getNece2() + pru.getNece3();
                        } else if (pru.getLeadTime() > 29) {
                            cant = pru.getNece1() + pru.getNece2();
                        } else {
                            cant = pru.getNece1();
                        }
                        ocFaltantes = (pru.getStock() - (pru.getInvMin() + pru.getOcPendiente())) - cant;
                        if (ocFaltantes < 0) {
                            ocFaltantesNece++;
                        }
                    }
                    if (pru.getCodigo1().length() == 1) {
                        //Necesidad de material con OC vencida
                        if (pru.getOcAtrasada().length() > 1) {
                            ocVencidaStockM++;
                        }
                        //Necesidad de material sin OC
                        double cant = 0;
                        double ocFaltantes = 0;
                        if (pru.getLeadTime() > 59) {
                            cant = pru.getNece1() + pru.getNece2() + pru.getNece3();
                        } else if (pru.getLeadTime() > 29) {
                            cant = pru.getNece1() + pru.getNece2();
                        } else {
                            cant = pru.getNece1();
                        }
                        ocFaltantes = (pru.getStock() - (pru.getInvMin() + pru.getOcPendiente())) - cant;
                        if (ocFaltantes < 0) {
                            ocFaltantesStockM++;
                        }
                    }

                    if (pru.getInvMax() > 0) {
                        if (pru.getInvMax() < pru.getStock()) {
                            sobrepasado++;
                        }
                    }
                }
                ModPrueba1 prue1 = new ModPrueba1();
                prue1.setComprador(man.getNombre());
                prue1.setOcVencidaNecesidad(ocVencidaNece);
                prue1.setOcFaltanteNecesidad(ocFaltantesNece);
                prue1.setOcVencidaStockM(ocVencidaStockM);
                prue1.setOcFaltanteStockM(ocFaltantesStockM);
                prue1.setSobrepasado(sobrepasado);
                compradores.add(prue1);
            }
            int total = 0;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModCliente man = (ModCliente) Sesion.getAuxiliar().get(i);
                ModPrueba1 prue = HelperComercial.prueba(compradores, man.getNombre());
                html += "<tr>";
                if (i == 0) {
                    html += "<td class='Estilo1' width='250'>Necesidad de material con OC vencida</td>";
                } else {
                    html += "<td class='Estilo1' width='250'>&nbsp;</td>";
                }

                html += "<td class='Estilo1' width='100'>" + man.getNombre() + "</td>";
                html += "<td class='Estilo1' width='100'>" + prue.getOcVencidaNecesidad() + "</td>";
                html += "</tr>";
                total += prue.getOcVencidaNecesidad();
            }
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>Total</td>";
            html += "<td class='Estilo1' width='100'>" + total + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "</tr>";
            
            total = 0;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModCliente man = (ModCliente) Sesion.getAuxiliar().get(i);
                ModPrueba1 prue = HelperComercial.prueba(compradores, man.getNombre());
                html += "<tr>";
                if (i == 0) {
                    html += "<td class='Estilo1' width='250'>Necesidad de material sin OC</td>";
                } else {
                    html += "<td class='Estilo1' width='250'>&nbsp;</td>";
                }
                html += "<td class='Estilo1' width='100'>" + man.getNombre() + "</td>";
                html += "<td class='Estilo1' width='100'>" + prue.getOcFaltanteNecesidad() + "</td>";
                html += "</tr>";
                total += prue.getOcFaltanteNecesidad();
            }
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>Total</td>";
            html += "<td class='Estilo1' width='100'>" + total + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "</tr>";
            total = 0;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModCliente man = (ModCliente) Sesion.getAuxiliar().get(i);
                ModPrueba1 prue = HelperComercial.prueba(compradores, man.getNombre());
                html += "<tr>";
                if (i == 0) {
                    html += "<td class='Estilo1' width='250'>Stock m�nimo con OC vencida</td>";
                } else {
                    html += "<td class='Estilo1' width='250'>&nbsp;</td>";
                }
                html += "<td class='Estilo1' width='100'>" + man.getNombre() + "</td>";
                html += "<td class='Estilo1' width='100'>" + prue.getOcVencidaStockM() + "</td>";
                html += "</tr>";
                total += prue.getOcVencidaStockM();
            }
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>Total</td>";
            html += "<td class='Estilo1' width='100'>" + total + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "</tr>";
            total = 0;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModCliente man = (ModCliente) Sesion.getAuxiliar().get(i);
                ModPrueba1 prue = HelperComercial.prueba(compradores, man.getNombre());
                html += "<tr>";
                if (i == 0) {
                    html += "<td class='Estilo1' width='250'>Stock m�nimo sin OC</td>";
                } else {
                    html += "<td class='Estilo1' width='250'>&nbsp;</td>";
                }
                html += "<td class='Estilo1' width='100'>" + man.getNombre() + "</td>";
                html += "<td class='Estilo1' width='100'>" + prue.getOcFaltanteStockM() + "</td>";
                html += "</tr>";
                total += prue.getOcFaltanteStockM();
            }
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>Total</td>";
            html += "<td class='Estilo1' width='100'>" + total + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "</tr>";
            total = 0;
            for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                ModCliente man = (ModCliente) Sesion.getAuxiliar().get(i);
                ModPrueba1 prue = HelperComercial.prueba(compradores, man.getNombre());
                html += "<tr>";
                if (i == 0) {
                    html += "<td class='Estilo1' width='250'>Stock M�ximo sobrepasado</td>";
                } else {
                    html += "<td class='Estilo1' width='250'>&nbsp;</td>";
                }
                html += "<td class='Estilo1' width='100'>" + man.getNombre() + "</td>";
                html += "<td class='Estilo1' width='100'>" + prue.getSobrepasado() + "</td>";
                html += "</tr>";
                total += prue.getSobrepasado();
            }
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>Total</td>";
            html += "<td class='Estilo1' width='100'>" + total + "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td class='Estilo1' width='250'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "<td class='Estilo1' width='100'>&nbsp;</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
}
