package forms;

import controllers.PrmApp;

import controllers.PrmGrabarHidropackSQL;
import controllers.PrmHidropack;
import excel.AsExcel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModEquipo;
import model.ModMaterial;
import model.ModPedidoMaterial;
import model.ModSesion;
import model.hidropack.ModOrdenCompra;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import util.HelperHidropack;

public class FrmHidropack extends HttpServlet {

    //public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        ModSesion Sesion = new ModSesion();
        AsExcel excel = null;
        try {
            if (req.getSession().getAttribute("Sesion") != null) {
                Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
                if (req.getParameter("etapaHidropack1") != null) {
                    Sesion.setSaldoMateriales(PrmHidropack.saldoMateriales());
                    Sesion.setSaldoMaterialesSchaffner(PrmHidropack.saldoMaterialesSchaffner());
                    Sesion.setEtapaHidropack(1);
                }

                if (req.getParameter("bodegaH") != null) {
                    Sesion.setDatos(PrmHidropack.obraPendiente());
                    Sesion.setEtapaHidropack(5);
                }

                if (req.getParameter("buscarObraBodega") != null) {
                    if (!req.getParameter("numeroObra").trim().equals("")) {
                        String condicion = " WHERE TAB_CodTabla = 500 AND TAB_Codigo = " + Integer.parseInt(req.getParameter("numeroObra"));
                        Sesion.setObra(PrmHidropack.obra(condicion));
                        if (Sesion.getObra().getIdObra() != Integer.parseInt(req.getParameter("numeroObra"))) {
                            Sesion.setTip("Obra no encontrada", 2);
                        } else {
                            String np = Sesion.getObra().getIdObra() + "";
                            Sesion.setPedidosProcesarBodega(PrmHidropack.pedidosObraBodega(np, "and  mve_analisisadic15 <> '5' and MVE_FolioFisico > 590"));
                            Sesion.getPset().indiceSinEntregar(Sesion.getPedidosProcesarBodega());
                            Sesion.setDetallePedidosProcesarBodega(PrmHidropack.detallePedidosObraBodega(np, "and (pd.MVD_Cant - isnull(tb.MVD_Cant,0)) > 0 and  mve_analisisadic15 <> '5'"));
                            Sesion.setEtapaHidropack(9);
                        }
                    } else {
                        Sesion.setTip("Debe ingresar un n�mero de obra", 1);
                    }
                }

                if (req.getParameter("enProceso") != null) {
                    ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(Integer.parseInt(req.getParameter("numeroDoc")));
                    if (PrmGrabarHidropackSQL.actualizarEstadoPedidoSinEntregar(pedidoMaterial.getNumeroDoc(), (byte) 3)) {
                        pedidoMaterial.setEstado((byte) 3);
                        Sesion.getPedidosProcesarBodega().set(Integer.parseInt(req.getParameter("numeroDoc")), pedidoMaterial);
                    }
                }

                if (req.getParameter("listoParaRetirar") != null) {
                    ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(Integer.parseInt(req.getParameter("numeroDoc")));
                    if (PrmGrabarHidropackSQL.actualizarEstadoPedidoSinEntregar(pedidoMaterial.getNumeroDoc(), (byte) 4)) {
                        pedidoMaterial.setEstado((byte) 4);
                        Sesion.getPedidosProcesarBodega().set(Integer.parseInt(req.getParameter("numeroDoc")), pedidoMaterial);
                    }
                }

                if (req.getParameter("descartar") != null) {
                    ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(Integer.parseInt(req.getParameter("descartar")));
                    if (PrmGrabarHidropackSQL.actualizarEstadoPedidoSinEntregar(pedidoMaterial.getNumeroDoc(), (byte) 5)) {
                        pedidoMaterial.setEstado((byte) 5);
                        Sesion.getPedidosProcesarBodega().set(Integer.parseInt(req.getParameter("descartar")), pedidoMaterial);
                        Sesion.setTip("Pedido descartado", 2);
                    }
                }

                if (req.getParameter("etapaHidropack2") != null) {
                    Sesion.getPedidosEspeciales().clear();
                    Sesion.getEquipo().setMaterial(new ModMaterial());
                    Sesion.setEtapaHidropack(3);
                }

                //si
                if (req.getParameter("etapaHidropack12") != null) {
                    Sesion.getPedidosEspeciales().clear();
                    Sesion.getEquipo().setMaterial(new ModMaterial());
                    Sesion.setEtapaHidropack(12);
                }


                if (req.getParameter("buscarObra") != null) {
                    if (!req.getParameter("numeroObra").trim().equals("")) {
                        String condicion = " WHERE TAB_CodTabla = 500 AND TAB_Codigo = " + Integer.parseInt(req.getParameter("numeroObra"));
                        Sesion.setObra(PrmHidropack.obra(condicion));
                        if (Sesion.getObra().getIdObra() != Integer.parseInt(req.getParameter("numeroObra"))) {
                            Sesion.setTip("Obra no encontrada", 2);
                        } else {
                            Sesion.setEtapaHidropack(4);
                            Sesion.setPedidosEspeciales(PrmHidropack.cargarMaterialCompra(Sesion));
                            Sesion.setHttp_Body("hidropacProductosTerminados");
                        }
                    } else {
                        Sesion.setTip("Debe ingresar un n�mero de obra", 1);
                    }
                    Sesion.setDatos(PrmHidropack.trabajo());
                }

                if (req.getParameter("agregarApedidoMaterial") != null) {
                    long numero = 0;
                    int linea = 0;
                    boolean est = false;
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                        if (equipo.getMaterial().getMateriaPrima().equals(Sesion.getEquipo().getMaterial().getMateriaPrima()) || Sesion.getEquipo().getMaterial().getMateriaPrima().trim().equals("")) {
                            est = true;
                        }
                    }
                    if (!est) {
                        ModEquipo equi = Sesion.getEquipo().copia();
                        if (Sesion.getPedidosEspeciales().size() > 0) {
                            ModEquipo equi1 = (ModEquipo) Sesion.getPedidosEspeciales().get(Sesion.getPedidosEspeciales().size() - 1);
                            numero = equi1.getMaterial().getNumero();
                            linea = equi1.getMaterial().getLinea();
                            equi.getMaterial().setNumero(numero);
                            equi.getMaterial().setLinea(linea + 1);
                        }
                        equi.setMaterial(equi.getMaterial().copia());
                        if (!equi.getMaterial().getMateriaPrima().equals("")) {
                            Sesion.getPedidosEspeciales().add(equi);
                        }
                    }
                }

                if (req.getParameter("agregarApedidoMaterial1") != null) {
                    boolean est = false;
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                        if (equipo.getMaterial().getMateriaPrima().equals(Sesion.getEquipo().getMaterial().getMateriaPrima()) || Sesion.getEquipo().getMaterial().getMateriaPrima().trim().equals("")) {
                            est = true;
                        }
                    }
                    if (!est) {
                        ModEquipo equi = Sesion.getEquipo().copia();
                        equi.setMaterial(equi.getMaterial().copia());
                        if (!equi.getMaterial().getMateriaPrima().equals("")) {
                            Sesion.getPedidosEspeciales().add(equi);
                        }
                    }
                }

                if (req.getParameter("limpiarMaterialAAgregar") != null) {
                    Sesion.setMaterial(new ModMaterial());
                    Sesion.setEquipo(new ModEquipo());
                }

                if (req.getParameter("cargarArchivo") != null) {
                    long numero = 0;
                    int linea = 0;
                    if (req.getParameter("subir") != null && !req.getParameter("subir").trim().equals("")) {
                        excel = new AsExcel();
                        excel.crearExcelSinEstilos(req.getParameter("subir"));
                        ArrayList datos = excel.leerDatosHoja(0);
                        if (Sesion.getPedidosEspeciales().size() > 0) {
                            ModEquipo equi = (ModEquipo) Sesion.getPedidosEspeciales().get(0);
                            numero = equi.getMaterial().getNumero();
                            linea = equi.getMaterial().getLinea();
                        }
                        Sesion.setPedidosEspeciales(PrmHidropack.cargarMaterialExcel(datos, numero, linea, Sesion.getObra().getIdObra()));
                    } else {
                        Sesion.setTip("Debe agregar una ruta para cargar un archivo", 0);
                    }
                }

                if (req.getParameter("limpiarPedidoMaterial") != null) {
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        ModEquipo equi = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                        HelperHidropack.actualizarSaldoBodega(equi, Sesion.getSaldoMateriales());
                    }
                    Sesion.getPedidosEspeciales().clear();
                    Sesion.setEquipo(new ModEquipo());
                }

                if (Sesion.getEtapaHidropack() == 4) {
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        if (req.getParameter("eliPedidoHidropack" + i) != null) {
                            ModEquipo equi = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                            Sesion.getPedidosEspeciales().remove(equi);
                            Sesion.getPedidosEspeciales().trimToSize();
                        }
                    }
                    for (int t = 0; t < Sesion.getPedidosEspeciales().size(); t++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(t);
                        if (req.getParameter("cantRequerida" + t) != null && !req.getParameter("cantRequerida" + t).trim().equals("0.0")) {
                            equipo.getMaterial().setCantRequerida(Double.parseDouble(req.getParameter("cantRequerida" + t)));
                            Sesion.getPedidosEspeciales().set(t, equipo);
                        }
                    }
                }

                if (req.getParameter("generarPedidoMaterial") != null) {
                    boolean est = false;
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                        if (equipo.getMaterial().getCantRequerida() == 0) {
                            est = true;
                        }
                    }
                    if (!est) {
                        if (!req.getParameter("cmbTrabajo").trim().equals("-1")) {
                            if (!Sesion.getPedidosEspeciales().isEmpty()) {
                                if (PrmGrabarHidropackSQL.grabarSolicitudPrevioGuiaDespacho(Sesion.getPedidosEspeciales(), Sesion.getUsuario().getUsuario(), Sesion.getObra(), Integer.parseInt(req.getParameter("cmbTrabajo")))) {
                                    Sesion.setPedidosEspeciales(new ArrayList());
                                    Sesion.setEquipo(new ModEquipo());
                                    Sesion.setTip("Pedido generado", 0);
                                    Sesion.setNumeroDoc(0);
                                }
                            } else {
                                Sesion.setTip("Debe agregar al menos un material", 2);
                            }
                        } else {
                            Sesion.setTip("Debe ingresar un trabajo", 2);
                        }
                    } else {
                        Sesion.setTip("La cantidad ha solicitar no debe ser cero", 2);
                    }
                }



                if (req.getParameter("agregarApedidoProducto") != null) {
                    boolean est = false;
                    if (req.getParameter("finicio") != null) {
                        if (!req.getParameter("finicio").trim().equals("")) {
                            //if (HelperHidropack.validaFecha(req.getParameter("finicio"))) {
                            for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                                ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                                if (equipo.getMaterial().getMateriaPrima().equals(Sesion.getEquipo().getMaterial().getMateriaPrima())) {
                                    est = true;
                                }
                            }
                            if (!est) {
                                ModEquipo equi = Sesion.getEquipo().copia();
                                equi.getMaterial().setFecha(req.getParameter("finicio").trim());
                                equi.setMaterial(equi.getMaterial().copia());
                                Sesion.getPedidosEspeciales().add(equi);

                            }
                        } else {
                            Sesion.setTip("Debe ingresar una fecha de entrega", 1);
                        }

                    }
                }
                if (req.getParameter("materialesPedidosABodega") != null) {
                    String np = Sesion.getObra().getIdObra() + "";
                    Sesion.setPedidosProcesarBodega(PrmHidropack.pedidosObraBodega(np, " and MVE_FolioFisico > 590"));
                    Sesion.getPset().indiceSinEntregar(Sesion.getPedidosProcesarBodega());
                    Sesion.setDetallePedidosProcesarBodega(PrmHidropack.detallePedidosObraBodegaUsuario(np));
                    Sesion.setEtapaHidropack(7);
                }

                if (req.getParameter("solicitarMateriales2") != null) {
                    Sesion.setEtapaHidropack(4);
                }



                if (req.getParameter("limpiarPedidoProducto") != null) {
                    Sesion.getPedidosEspeciales().clear();
                    Sesion.setEquipo(new ModEquipo());
                }

                if (req.getParameter("buscarObraCompra") != null) {
                    if (!req.getParameter("numeroObra").trim().equals("")) {
                        String condicion = " WHERE TAB_CodTabla = 500 AND TAB_Codigo = " + Integer.parseInt(req.getParameter("numeroObra"));
                        Sesion.setObra(PrmHidropack.obra(condicion));
                        if (Sesion.getObra().getIdObra() != Integer.parseInt(req.getParameter("numeroObra"))) {
                            Sesion.setTip("Obra no encontrada", 2);
                        } else {
                            Sesion.setEtapaHidropack(13);
                            Sesion.setPedidosEspeciales(PrmHidropack.cargarMaterialCompra(Sesion));
                            Sesion.setHttp_Body("hidropackMateriales");
                        }
                    } else {
                        Sesion.setTip("Debe ingresar un n�mero de obra", 1);
                    }
                }

                if (Sesion.getEtapaHidropack() == 13) {
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        if (req.getParameter("eliPedidoHidropack" + i) != null) {
                            ModEquipo equi = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                            HelperHidropack.actualizarSaldoBodega(equi, Sesion.getSaldoMateriales());
                            Sesion.getPedidosEspeciales().remove(i);
                            Sesion.getPedidosEspeciales().trimToSize();
                        }
                    }
                }

                if (Sesion.getEtapaHidropack() == 13) {
                    for (int t = 0; t < Sesion.getPedidosEspeciales().size(); t++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(t);
                        if (req.getParameter("cantRequerida" + t) != null && !req.getParameter("cantRequerida" + t).trim().equals("0.0")) {
                            equipo.getMaterial().setCantRequerida(Double.parseDouble(req.getParameter("cantRequerida" + t)));
                            Sesion.getPedidosEspeciales().set(t, equipo);
                        }
                    }
                }

                if (req.getParameter("generarSolicitudCompraMaterial") != null) {
                    boolean est = false;
                    for (int i = 0; i < Sesion.getPedidosEspeciales().size(); i++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getPedidosEspeciales().get(i);
                        if (equipo.getMaterial().getCantRequerida() == 0) {
                            est = true;
                        }
                    }
                    if (!req.getParameter("fEntrega").trim().equals("")) {
                        String[] fechaEntrega = req.getParameter("fEntrega").split("/");
                        Sesion.getObra().setFechaEntregaMaterial(fechaEntrega[2] + fechaEntrega[1] + fechaEntrega[0]);
                        if (!est) {
                            if (!Sesion.getPedidosEspeciales().isEmpty()) {
                                if (PrmGrabarHidropackSQL.grabarSolicitudCompra(Sesion)) {
                                    //Sesion.setPedidosEspeciales(new ArrayList());
                                    //Sesion.setEquipo(new ModEquipo());
                                    Sesion.setTip("Pedido generado", 0);
                                    Sesion.setNumeroDoc(0);
                                } else {
                                    Sesion.setTip("No se pudo generar la solicitud", 2);
                                }
                            } else {
                                Sesion.setTip("Debe agregar al menos un material", 2);
                            }
                        } else {
                            Sesion.setTip("La cantidad ha solicitar no debe ser cero", 2);
                        }
                    } else {
                        Sesion.setTip("Debe ingresar una fecha de entrega de material en bodega", 2);
                    }
                }
                if (Sesion.getEtapaHidropack() == 9) {
                    for (int t = 0; t < Sesion.getDetallePedidosProcesarBodega().size(); t++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(t);
                        if (req.getParameter("traspasoBodega" + t) != null) {
                            if (Double.parseDouble(req.getParameter("traspasoBodega" + t).trim()) <= equipo.getMaterial().getCantidadR()) {
                                if (HelperHidropack.stock(Sesion.getSaldoMateriales(), equipo.getMaterial()) >= HelperHidropack.entregado(Sesion.getDetallePedidosProcesarBodega(), equipo.getMaterial().getMateriaPrima(), req)) {
                                    equipo.getMaterial().setEntregado(Double.parseDouble(req.getParameter("traspasoBodega" + t)));
                                    equipo.getMaterial().setCantidad(equipo.getMaterial().getCantidadR() - Double.parseDouble(req.getParameter("traspasoBodega" + t)));
                                    Sesion.getDetallePedidosProcesarBodega().set(t, equipo);
                                } else {
                                    Sesion.setTip("La cantidad de entrega no puede ser mayor al stock de bodega", 2);
                                }
                            } else {
                                Sesion.setTip("La cantidad de entrega no puede ser mayor a lo que falta por retirar", 2);
                            }
                        }
                    }
                }

                if (req.getParameter("enviarCambiarCodigo") != null) {
                    for (int t = 0; t < Sesion.getDetallePedidosProcesarBodega().size(); t++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(t);
                        if (req.getParameter("cambiarCodigo" + t) != null) {
                            if (PrmHidropack.materialConTraspaso(equipo.getMaterial().getNumero(), equipo.getMaterial().getLinea1()) == 0) {
                                if (PrmGrabarHidropackSQL.actualizarCambiarMaterial(1, equipo.getMaterial().getNumeroDocOrigen(), equipo.getMaterial().getLineaOrigen())) {
                                    equipo.getMaterial().setEstado("1");
                                    Sesion.getDetallePedidosProcesarBodega().set(t, equipo);
                                }
                            } else {
                                Sesion.setTip("El item tiene traspaso, no se puede modificar el c�digo", 0);
                            }
                        }
                    }
                }

                if (req.getParameter("changeCodigo") != null) {
                    ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(Integer.parseInt(req.getParameter("changeCodigo")));
                    Sesion.setEquipo(equipo);
                    Sesion.setHttp_Body("hidropackCambiarCodigo");
                }

                if (req.getParameter("MrpH") != null) {
                    Sesion.setThead(PrmHidropack.thead(1));
                    Sesion.setAuxiliar(PrmHidropack.cargarMrp());
                    Sesion.setEtapaHidropack(14);
                }

                if (req.getParameter("entregado") != null) {
                    ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(Integer.parseInt(req.getParameter("numeroDoc")));
                    boolean est = false;
                    boolean est1 = false;
                    for (int y = 0; y < Sesion.getDetallePedidosProcesarBodega().size(); y++) {
                        ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(y);
                        if (pedidoMaterial.getNumeroDoc() == equipo.getMaterial().getNumero()) {
                            if (equipo.getMaterial().getEntregado() > 0) {
                                est = true;
                            }
                            if (equipo.getMaterial().getCantidad() > 0) {
                                est1 = true;
                            }
                        }
                    }

                    if (est) {
                        if (PrmGrabarHidropackSQL.grabarTraspasoDeBodega(Sesion.getDetallePedidosProcesarBodega(), Sesion.getUsuario().getUsuario(), pedidoMaterial.getNumeroDoc(), est1, pedidoMaterial, Sesion)) {
                            Sesion.setSaldoMateriales(PrmHidropack.saldoMateriales());
                            Sesion.setSaldoMaterialesSchaffner(PrmHidropack.saldoMaterialesSchaffner());
                            for (int y = 0; y < Sesion.getDetallePedidosProcesarBodega().size(); y++) {
                                ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(y);
                                if (pedidoMaterial.getNumeroDoc() == equipo.getMaterial().getNumero()) {
                                    equipo.getMaterial().setCantidadR(equipo.getMaterial().getCantidad());
                                    equipo.getMaterial().setEntregado(0);
                                    Sesion.getDetallePedidosProcesarBodega().set(y, equipo);
                                }
                            }
                            Sesion.getPset().indicePedidos(Sesion.getPedidosProcesarBodega());
                            Sesion.setTip("Traspaso realizado N�: " + Sesion.getNumeroDoc(), 0);

                        } else {
                            Sesion.setTip("No se pudo procesar la entrega", 1);
                        }
                    } else {
                        Sesion.setTip("Debe entregar algo", 2);
                    }
                }

                if (req.getParameter("imprimirBodega") != null) {
                    try {
                        ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(Integer.parseInt(req.getParameter("imprimirBodega")));
                        Map pars = new HashMap();
                        pars.put("N_PEDIDO", String.valueOf(pedidoMaterial.getNumeroDoc()));
                        pars.put("USUARIO", pedidoMaterial.getUsuario());
                        pars.put("FECHA", pedidoMaterial.getFechaCreacion());
                        pars.put("OBRA", String.valueOf(Sesion.getObra().getIdObra()));
                        pars.put("CLIENTE", String.valueOf(Sesion.getObra().getCliente().getNombre()));

                        JRBeanCollectionDataSource dataSource;
                        /*Collections.sort(Sesion.getDetallePedidosProcesarBodega(), new Comparator<ModEquipo>() {
                         public int compare(ModEquipo mat1, ModEquipo mat2) {
                         return new Integer(mat1.getMaterial().getLinea()).compareTo(new Integer(mat2.getMaterial().getLinea()));
                         }
                         });*/
                        Collection lista = HelperHidropack.dataReportConsumo(Sesion.getDetallePedidosProcesarBodega(), pedidoMaterial.getNumeroDoc(), Sesion.getSaldoMateriales());

                        dataSource = new JRBeanCollectionDataSource(lista);
                        JasperReport reporte = JasperCompileManager.compileReport("E:\\reportes\\entregaMaterialesHidropack.jrxml");
                        JasperPrint p = JasperFillManager.fillReport(reporte, pars, dataSource);
                        byte[] bite = net.sf.jasperreports.engine.JasperExportManager.exportReportToPdf(p);
                        res.setContentType("application/pdf");
                        res.setContentLength(bite.length);
                        ServletOutputStream ouputStream = res.getOutputStream();
                        ouputStream.write(bite, 0, bite.length);
                        ouputStream.flush();
                        ouputStream.close();
                    } catch (Exception e) {
                        PrmApp.addError(e, true);
                    }
                }

                if (req.getParameter("imprimirUsuario") != null) {
                    try {
                        ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(Integer.parseInt(req.getParameter("imprimirUsuario")));
                        Map pars = new HashMap();
                        pars.put("N_PEDIDO", String.valueOf(pedidoMaterial.getNumeroDoc()));
                        pars.put("USUARIO", pedidoMaterial.getUsuario());
                        pars.put("FECHA", pedidoMaterial.getFechaCreacion());
                        pars.put("OBRA", String.valueOf(Sesion.getObra().getIdObra()));

                        JRBeanCollectionDataSource dataSource;
                        Collection lista = HelperHidropack.dataReportConsumoUsuario(Sesion.getDetallePedidosProcesarBodega(), pedidoMaterial.getNumeroDoc());
                        dataSource = new JRBeanCollectionDataSource(lista);
                        JasperReport reporte = JasperCompileManager.compileReport("E:\\reportes\\entregaMaterialesHidropackUsuario.jrxml");
                        JasperPrint p = JasperFillManager.fillReport(reporte, pars, dataSource);
                        byte[] bite = net.sf.jasperreports.engine.JasperExportManager.exportReportToPdf(p);
                        res.setContentType("application/pdf");
                        res.setContentLength(bite.length);
                        ServletOutputStream ouputStream = res.getOutputStream();
                        ouputStream.write(bite, 0, bite.length);
                        ouputStream.flush();
                        ouputStream.close();
                    } catch (Exception e) {
                        PrmApp.addError(e, true);
                    }
                }

                if (req.getParameter("pagSIN") != null) {
                    Sesion.getPset().getDetalle("SIN").pag = Integer.parseInt(req.getParameter("pagSIN"));
                }

                if (req.getParameter("saldarOc") != null) {
                    for (int a = 0; a < Sesion.getMateriales().size(); a++) {
                        ModOrdenCompra borrar = (ModOrdenCompra) Sesion.getMateriales().get(a);
                        if (req.getParameter("salOc" + a) != null) {
                            //System.out.println("Num orden:" + borrar.getNumOrdenCompra() + "C�digo:" + borrar.getMateriaPrima());
                            if (PrmGrabarHidropackSQL.actualizarOc(borrar.getNumOrdenCompra(), borrar.getItem(), Sesion.getUsuario().getUsuario(), borrar.getCantidad())) {
                                Sesion.setTip("Oc actualizada", 0);
                            } else {
                                Sesion.setTip("No se pudo actualizar", 2);
                            }
                        }
                    }
                }


                if (req.getParameter("eliminarMaterialRequerimiento") != null) {
                    ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(Integer.parseInt(req.getParameter("eliminarMaterialRequerimiento")));
                    if (PrmGrabarHidropackSQL.eliminarMaterialRequerido(equipo.getMaterial().getNumero(), equipo.getMaterial().getLinea(), equipo.getMaterial().getNumeroDocOrigen(), equipo.getMaterial().getLineaOrigen())) {
                        String np = Sesion.getObra().getIdObra() + "";
                        Sesion.setPedidosProcesarBodega(PrmHidropack.pedidosObraBodega(np, ""));
                        Sesion.getPset().indiceSinEntregar(Sesion.getPedidosProcesarBodega());
                        Sesion.setDetallePedidosProcesarBodega(PrmHidropack.detallePedidosObraBodegaUsuario(np));
                        Sesion.setTip("Dato eliminado", 0);
                    } else {
                        Sesion.setTip("No se pudo eliminar", 2);
                    }

                }

                Sesion.setUrl("../page/hidropack.jsp");
            }

        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }

        req.getSession()
                .setAttribute("Sesion", Sesion);
        res.sendRedirect(
                "template/appLayout.jsp");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
