<jsp:useBean id="LOAD_ONSUBMIT" scope="request" class="java.lang.String" />
<jsp:useBean id="LOAD_ONCLICK" scope="request" class="java.lang.String" />   
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Cargar datos con el scroll</title>

        <style type="text/css">
            .mensaje{font-size:40px;}

        </style>

        <script type="text/javascript" src="http://lineadecodigo.com/js/jquery/jquery.js"></script>
        <script type="text/javascript">

            var pagina=1;

            $(document).ready(function()
            {
                // Carga inicial        
                cargardatos();
        
            });

            function cargardatos(){
                // Petici�n AJAX
                
                $("#loader").html("<img src='img/loading16.gif'/>");
                $.get("jsp/datos.jsp?pagina="+pagina,
                function(data){
                    if (data != "") {
                        $(".mensaje:last").before(data); 
                    }
                    $('#loader').empty();
                }
            );                              
            }

            $(window).scroll(function(){
                if ($(window).scrollTop() == $(document).height() - $(window).height()){
                    pagina++;
                    cargardatos()
                }                                       
            });
        </script>
    </head>
    <body>
        <form action="" method="post" <%=LOAD_ONSUBMIT%> name="FrmOperaciones">
            <input name="frm" type="hidden" value="operaciones" />
            <div id="mensajes">
                <div class="mensaje"></div>     
            </div>
            <div id="loader"></div>
        </form>
    </body>
</html>
