/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package excel;

import controllers.PrmApp;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModEquipo;
import model.ModPedidoMaterial;
import model.ModSesion;

/**
 *
 * @author dbarra
 */
public class PedidoBodega extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ModSesion Sesion = null;
        DecimalFormat number = new DecimalFormat("###,###.##");
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/pedidoObra.xls");
            int indice = 0;

            for (int y = 0; y < Sesion.getPedidosProcesarBodega().size(); y++) {
                ModPedidoMaterial pedidoMaterial = (ModPedidoMaterial) Sesion.getPedidosProcesarBodega().get(y);
                indice++;
                excel.setValor(0, indice, 0, "Requerimiento", "DET_TTEXTO", indice);
                excel.setValor(0, indice, 1, pedidoMaterial.getNumeroDoc(), "DET_TNUMERO", indice);
                excel.setValor(0, indice, 2, "Fecha ingreso", "DET_TTEXTO", indice);
                excel.setValor(0, indice, 3, pedidoMaterial.getFechaCreacion(), "DET_TTEXTO", indice);
                excel.setValor(0, indice, 4, "Usuario", "DET_TTEXTO", indice);
                excel.setValor(0, indice, 5, pedidoMaterial.getUsuario(), "DET_TTEXTO", indice);
                indice++;
                excel.setValor(0, indice, 0, "Item", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 1, "Descripci�n", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 2, "C�digo", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 3, "UM", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 4, "Obra", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 5, "Solicitado", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 6, "Traspasado", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 7, "N� Gu�a", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 8, "Cant Gu�a", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 9, "Fecha Gu�a", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 10, "Num OC", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 11, "Fecha OC", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 12, "Cant OC", "DET_STTEXTO", indice);
                excel.setValor(0, indice, 13, "Stok Bod 1", "DET_STTEXTO", indice);
                indice++;
                for (int i = 0; i < Sesion.getDetallePedidosProcesarBodega().size(); i++) {
                    ModEquipo equipo = (ModEquipo) Sesion.getDetallePedidosProcesarBodega().get(i);
                    if (pedidoMaterial.getNumeroDoc() == equipo.getMaterial().getNumero()) {
                        excel.setValor(0, indice, 0, equipo.getMaterial().getLinea(), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 1, equipo.getMaterial().getDescripcion(), "DET_TTEXTO", indice);
                        excel.setValor(0, indice, 2, equipo.getMaterial().getMateriaPrima(), "DET_TTEXTO", indice);
                        excel.setValor(0, indice, 3, equipo.getMaterial().getUm(), "DET_TTEXTO", indice);
                        excel.setValor(0, indice, 4, equipo.getNp(), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 5, number.format(equipo.getMaterial().getCantRequerida()), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 6, number.format(equipo.getMaterial().getCantidadAsignada()), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 7, equipo.getMaterial().getNumGuia(), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 8, number.format(equipo.getMaterial().getCantGuia()), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 9, equipo.getMaterial().getFechaGuia(), "DET_TTEXTO", indice);
                        excel.setValor(0, indice, 10, equipo.getMaterial().getNumOc(), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 11, equipo.getMaterial().getFechaOc(), "DET_TTEXTO", indice);
                        excel.setValor(0, indice, 12, equipo.getMaterial().getCantOc(), "DET_TNUMERO", indice);
                        excel.setValor(0, indice, 13, equipo.getMaterial().getStock(), "DET_TNUMERO", indice);
                        indice++;
                    }
                }
            }
            excel.borrarHojaEstilo();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"Pedido a bodega.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
