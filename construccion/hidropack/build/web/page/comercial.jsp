<%@page import="controllers.PrmApp"%>
<jsp:useBean id="LOAD_ONSUBMIT" scope="request" class="java.lang.String" />
<jsp:useBean id="LOAD_ONCLICK" scope="request" class="java.lang.String" />
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"            scope="page"    class="view.VwComercial" />
<%response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0,max-age=0");%>
<link rel="stylesheet" href="../css/reveal.css">	
<script type="text/javascript" src="../js/popcalendar.js"></script>
<script type="text/javascript" src="../js/initcalendar.js"></script>
<script>
    $(document).ready(function() {
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) {
                $("form").submit();
            }
        });
    }); 
    
    function eliminarMaterial(val){
        if(confirm("�Realmente quieres eliminar este item")) {
            document.FrmComercial.submit();
        }else{
            val.checked = false;
            return false;
            
        }
    }
    function confirmacion(){
        if(confirm("�Realmente quiere realizar esta accion")) {
            loading();
            document.FrmComercial.submit();
        }else{
            return false;
        }
    }
    
    function noVacio(){
        if(document.FrmComercial.nuevaPlantilla.value == ''){
            alert('Debe ingresar un nombre para la nueva plantilla');
            return false;
        }else{
            document.FrmComercial.submit();
        }
    }
    function guardarPresupuesto(){
        if(document.FrmComercial.numeroPresupuesto.value == '' && document.FrmComercial.cmbTrabajo.value == '-1'){
            alert('Debe agregar numero de presupuesto y un trabajo');
            return false;
        }else{            
            document.FrmComercial.submit();
        }
    }
    function guardarObra(){
        if(document.FrmComercial.input_2.value == '' || 
            document.FrmComercial.ventaEstimada.value == '' ||
            document.FrmComercial.montoServicios.value == '' ||
            document.FrmComercial.fcliente.value == '' ||
            document.FrmComercial.npresupuesto.value == '' ||
            document.FrmComercial.nombreObra.value == ''){
            alert('Debe agregar todos los campos requeridos');
            return false;
        }else{            
            if(confirm("�Realmente quiere realizar esta accion")) {
                document.FrmComercial.submit();
            }else{
                return false;
            }
        }
    }
    
    function actualizarObra(){
        if(document.FrmComercial.ventaEstimada.value == '' ||
            document.FrmComercial.montoServicios.value == '' ||
            document.FrmComercial.fcliente.value == '' ||
            document.FrmComercial.npresupuesto.value == '' ||
            document.FrmComercial.nombreObra.value == ''){
            alert('Debe agregar todos los campos requeridos');
            return false;
        }else{            
            if(confirm("�Realmente quiere realizar esta accion")) {
                document.FrmComercial.submit();
            }else{
                return false;
            }
        }
    }
    
    function actualizarManCorrectiva(){
        if(document.FrmComercial.ventaEstimada.value == '' ||
            document.FrmComercial.montoServicios.value == '' ||
            document.FrmComercial.fcliente.value == '' ||
            document.FrmComercial.npresupuesto.value == '' ||
            document.FrmComercial.nombreObra.value == '' ||
            document.FrmComercial.descripcion.value == '' ){
            alert('Debe agregar todos los campos requeridos');
            return false;
        }else{            
            if(confirm("�Realmente quiere realizar esta accion")) {
                document.FrmComercial.submit();
            }else{
                return false;
            }
        }
    }
    function ingresarManPre(){
        if(document.FrmComercial.nombreObra.value == '' ||
            document.FrmComercial.frecuencia.value == '-1' ||
            document.FrmComercial.valorUf.value == '' ||
            document.FrmComercial.uf.value == ''){
            alert('Debe agregar todos los campos requeridos');
            return false;
        }else{            
            if(confirm("�Realmente quiere realizar esta accion")) {
                document.FrmComercial.submit();
            }else{
                return false;
            }
        }
    }
    function actualizarManPre(){
        if(document.FrmComercial.nombreObra.value == '' ||
            document.FrmComercial.frecuencia.value == '-1' ||
            document.FrmComercial.valorUf.value == '' ||
            document.FrmComercial.uf.value == ''){
            alert('Debe agregar todos los campos requeridos');
            return false;
        }else{            
            if(confirm("�Realmente quiere realizar esta accion")) {
                document.FrmComercial.submit();
            }else{
                return false;
            }
        }
    }
    
    function buscarObra(){
        if(document.FrmComercial.obraPorNumero.value == ''){
            alert('Debe ingresar un numero para buscar');
            return false;
        }else{            
            document.FrmComercial.submit();
        }
    }
    
    function buscarManPre(){
        if(document.FrmComercial.manPrePorNumero.value == ''){
            alert('Debe ingresar un numero para buscar');
            return false;
        }else{            
            document.FrmComercial.submit();
        }
    }
    
    function justNumbers(e)
    {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
            return true;
     
        return /\d/.test(String.fromCharCode(keynum));
    }
    
    function guardarMantencionCorrectiva(){
        if(document.FrmComercial.input_2.value == '' ||
            document.FrmComercial.nombre.value == '' ||
            document.FrmComercial.descripcion.value == '' ||
            document.FrmComercial.monto.value == '' ||
            document.FrmComercial.fcliente.value == '' ||
            document.FrmComercial.npresupuesto.value == ''){
            alert('Debe agregar todos los campos requeridos');
            return false;
        }else{            
            if(confirm("�Realmente quiere realizar esta accion")) {
                document.FrmComercial.submit();
            }else{
                return false;
            }
        }
    }


</script>

<form action="../FrmComercial" method="post" <%=LOAD_ONSUBMIT%> name="FrmComercial">

    <table cellpadding='0' cellspacing='0'>
        <%if (Sesion.getEtapaComercial() == 1) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL</td>
        </tr>

        <tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">
                <%if (PrmApp.permisosSeccion("ingresoPresupuesto", Sesion)) {%>
                <a href="../FrmComercial?etapaComercial2=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">Ingreso de presupuesto</font></a>
                    <%} else {%>
                <font color="#336600" class="Estilo1">Ingreso de presupuesto</font>
                <%}%>
            </td> 
        </tr>
        <tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">
                <%if (PrmApp.permisosSeccion("productoServicios", Sesion)) {%>
                <a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">Ingreso de productos o servicios</font></a>
                    <%} else {%>
                <font color="#336600" class="Estilo1">Ingreso de productos o servicios</font>
                <%}%>
            </td> 
        </tr>
        <tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">
                <%if (PrmApp.permisosSeccion("informesGerencia", Sesion)) {%>
                <a href="../FrmComercial?etapaComercial6=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">Informes</font></a>
                    <%} else {%>
                <font color="#336600" class="Estilo1">Informes</font>
                <%}%>
            </td> 
        </tr>

        <!--tr>
            <td class="Estilo1" style="padding-left: 10px;padding-top: 5px">

                <a href="../FrmComercial?Prueba=SI" class="Estilo1" onclick="javascript:loading();"><font color="#336600">Prueba</font></a>
            </td> 
        </tr-->

        <%} else if (Sesion.getEtapaComercial() == 2) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRESUPUESTO</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 100px;padding-top: 20px" class="Estilo1">PRESUPUESTO</td>
        </tr>
        <tr>
            <td style="padding-left: 60px;padding-top: 20px">
                <%=vw.comboProductoOServicio(request, true)%>
            </td>
        </tr>

        <%} else if (Sesion.getEtapaComercial() == 3) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRESUPUESTO  -  PRESUPUESTO OBRA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial2=SI" class="Estilo1" onclick="javascript:loading();">Presupuesto</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?buscarPresupuestoObra=SI" class="Estilo1" onclick="javascript:loading();">Buscar presupuesto obra</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 100px;padding-top: 20px" class="Estilo1">INGRESO DE PRESUPUESTO</td>
        </tr>
        <tr>
            <td style="padding-left: 120px;padding-top: 20px">
                <%=vw.comboPlantilla(request)%>
            </td>
        </tr>
        <%} else if (Sesion.getEtapaComercial() == 4) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRESUPUESTO  -  PRESUPUESTO OBRA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial2=SI" class="Estilo1" onclick="javascript:loading();">Presupuesto</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?buscarPresupuestoObra=SI" class="Estilo1" onclick="javascript:loading();">Buscar presupuesto obra</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px" class="Estilo1">INGRESO DE PRESUPUESTO</td>
        </tr>
        <%if (!Sesion.getAux().equals("b")) {%>
        <tr>
            <td align="center" style="padding-top: 20px">
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda "/>
                    <jsp:param name="campo1" value="C�digo"/>
                    <jsp:param name="campo2" value="Descripci�n"/>
                    <jsp:param name="campo3" value="yes"/>
                </jsp:include>
            </td>
        </tr>
        <%}%>
        <tr>
            <td style="padding-top: 20px" class="Estilo10">Plantilla:&nbsp;<%=Sesion.getPopUp()%></td>
        </tr>
        <%=vw.agregarMaterialesAPlantilla()%>
        <tr>
            <td style="padding-top: 20px">
                <a href="../FrmComercial?actualizarPlantilla=YES" class="Estilo1" onclick="return confirmacion();">Actualizar plantilla</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="../FrmComercial?guardarComoNuevaPlantilla=YES" class="Estilo1" onclick="javascript:loading();">Guardar como nueva plantilla</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="../FrmComercial?ingresarPresupuesto=YES" class="Estilo1" onclick="javascript:loading();">Ingresar presupuesto</a>
            </td>
        </tr>
        <%if (Sesion.getAux().equals("a")) {%>
        <tr>
            <td style="padding-top: 20px" class="Estilo1">Nombre plantilla:&nbsp;&nbsp;
                <input type="text" size="40" name="nuevaPlantilla" id="nuevaPlantilla">&nbsp;&nbsp;
                <input type="submit" name="guardarNuevaPlantilla" id="guardarNuevaPlantilla" onclick="return confirmacion();noVacio()" class="Estilo1" value="Guardar nueva plantilla">
            </td>
        </tr>
        <%}%>
        <%if (Sesion.getAux().equals("b")) {%>
        <tr>
            <td>
                <table cellpadding='0' cellspacing='0'>
                    <tr>
                        <td align="center" style="padding-top: 20px;">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo10" style="padding-left: 30px">
                            N� de presupuesto:&nbsp;&nbsp;<input type="text" name="numeroPresupuesto" id="numeroPresupuesto">
                        </td>
                        <td style="padding-left: 30px">
                            <%=vw.comboTrabajo(request)%>
                        </td>
                        <td  style="padding-left: 30px">
                            <input type="submit" value="Guardar presupuesto" onclick="return guardarPresupuesto()" name="guardarPresupuesto" id="guardarPresupuesto" class="Estilo10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%}%>
        <tr>
            <td style="padding-top: 20px">
                <%=vw.grillaPlantillaObra(request)%>
            </td>
        </tr>
        <%} else if (Sesion.getEtapaComercial() == 5) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRESUPUESTO  -  PRESUPUESTO OBRA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial2=SI" class="Estilo1" onclick="javascript:loading();">Presupuesto</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 20px">
                <table width="900" cellpadding="0" cellspacing="0" class="Contenido">
                    <tr class="CabeceraListado">
                        <td colspan="4" align="center" class="Estilo1">BUSCAR PRESUPUESTO OBRA</td>
                    </tr>
                    <%if (!Sesion.getAux().equals("e")) {%>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="Contenido">
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.Inicio</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="finicio" name="finicio" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('finicio'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                                </tr>
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.T�rmino</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="fterm" name="fterm" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fterm'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de t�rmino")%> /></td>
                                </tr> 
                            </table>
                        </td>
                        <td valign="middle">
                            <input type="submit" class="Estilo1" name="enviarFechaPresupuestoObra" id="enviarFechaPresupuestoObra" value="Buscar">
                        </td>
                        <td align="center" style="padding-top: 20px;padding-left: 20px">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo1" style="padding-left: 30px">
                            N� presupuesto:&nbsp;&nbsp;<input type="text" size="7" name="presupuestoObraPorNumero" id="presupuestoObraPorNumero">&nbsp;&nbsp;
                            <input type="submit" value="Buscar" name="buscarPresupuestoObraPorNumero" id="buscarPresupuestoObraPorNumero" class="Estilo10">
                        </td>
                    </tr>
                    <%}%>
                    <tr class="CabeceraListado"><td colspan="4">&nbsp;</td></tr>
                </table>
            </td>
        </tr>

        <%=vw.grillaPresupuestosObraResumido(request)%>

        <%if (Sesion.getAux().equals("e")) {%>
        <tr>
            <td align="center" style="padding-top: 20px;padding-left: 20px">
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda "/>
                    <jsp:param name="campo1" value="C�digo"/>
                    <jsp:param name="campo2" value="Descripci�n"/>
                    <jsp:param name="campo3" value="yes"/>
                </jsp:include>
            </td>
        </tr>
        <%}%>
        <%if (Sesion.getAux().equals("c") || Sesion.getAux().equals("e")) {%>
        <tr>
            <td align="center" style="padding-top: 20px;">
                <a href="../PresupuestoObra" target="_blank">
                    <img src="../img/btn_excel.gif" width="30" height="30" border="0" <%=vw.verOverTip("Haga click para exportar a Excel")%>/>
                </a>
            </td>
        </tr>
        <tr>
            <td align="center">
                <input type="submit" name="actualizarPresupuestoObra" onclick="return confirmacion();" id="actualizarPresupuestoObra" value="Actualizar presupuesto" class="Estilo1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="submit" name="agregarMaterialPresupuestoObra" id="agregarMaterialPresupuestoObra" value="Agregar material" class="Estilo1">
                <!--a href="../FrmComercial?actualizarPresupuestoObra=YES" class="Estilo1" onclick="confirmacion();javascript:loading();">Actualizar detalle presupuesto</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="../FrmComercial?agregarMaterialPresupuestoObra=YES" class="Estilo1" onclick="javascript:loading();">Agregar material</a-->
            </td>
        </tr>
        <%=vw.grillaPresupuestosObra(request)%>
        <%}%>

        <%} else if (Sesion.getEtapaComercial() == 6) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px">COMERCIAL  -  PRODUCTOS O SERVICIOS</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 100px;padding-top: 20px" class="Estilo1">INGRESO</td>
        </tr>
        <tr>
            <td style="padding-left: 60px;padding-top: 20px">
                <%=vw.comboProductoOServicio(request, false)%>
            </td>
        </tr>
        <%} else if (Sesion.getEtapaComercial() == 7) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  INGRESO OBRA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial4=SI" class="Estilo1" onclick="javascript:loading();">Buscar obra</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px" class="Estilo1" align="center">INGRESO OBRA</td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px;padding-left: 20px">
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda "/>
                    <jsp:param name="campo1" value="Rut cliente"/>
                    <jsp:param name="campo2" value="Nombre cliente"/>
                    <jsp:param name="campo4" value="YES"/>
                </jsp:include>
            </td>
        </tr>
        <tr>
            <td style='padding-top: 20px' align="center">
                <table border='1' cellpadding='1' cellspacing='1' align="center">
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Nombre</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' autocomplete="off" size='30' name='nombreObra'  id='nombreObra' value='<%=Sesion.getObra().getNombreObra()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Venta estimada</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='ventaEstimada'  id='ventaEstimada' value='<%=Sesion.getObra().getVentaEstimada()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1'>Costo servicios</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='montoServicios'  id='montoServicios' value='<%=Sesion.getObra().getCostoServicio()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Fecha Cliente</td>
                        <td align="right">:</td>
                        <td align="right"><input id="fcliente" name="fcliente" type="text" size="30" readonly class="txtFecha" value='<%=Sesion.getObra().getCliente().getFecha()%>' /></td>
                        <td><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fcliente'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha cliente")%> /></td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">N� presupuesto</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='npresupuesto'  id='npresupuesto' value='<%=Sesion.getObra().getNumPresupuesto()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='padding-top: 20px' align="center"><input type="submit" onclick="return guardarObra()" name="ingresarObra" id="ingresarObra" class="Estilo10" value="Ingresar obra"></td>
        </tr>
        <%} else if (Sesion.getEtapaComercial() == 8) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  BUSCAR OBRA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
            </td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 20px">
                <table width="900" cellpadding="0" cellspacing="0" class="Contenido">
                    <tr class="CabeceraListado">
                        <td colspan="4" align="center" class="Estilo1">BUSCAR OBRA</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="Contenido">
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.Inicio</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="finicio" name="finicio" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('finicio'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                                </tr>
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.T�rmino</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="fterm" name="fterm" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fterm'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de t�rmino")%> /></td>
                                </tr> 
                            </table>
                        </td>
                        <td valign="middle">
                            <input type="submit" class="Estilo1" name="enviarFechaObra" id="enviarFechaObra" value="Buscar">
                        </td>
                        <td align="center" style="padding-top: 20px;padding-left: 20px">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo1" style="padding-left: 30px">
                            N� obra:&nbsp;&nbsp;<input type="text" size="7" name="obraPorNumero" id="obraPorNumero">&nbsp;&nbsp;
                            <input type="submit" value="Buscar" onclick="return buscarObra()" name="enviarObraPorNumero" id="enviarObraPorNumero" class="Estilo10">
                        </td>
                    </tr>
                    <tr class="CabeceraListado"><td colspan="4">&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <%=vw.grillaObraResumido(request)%>
        <%if (Sesion.getAux().equals("d")) {%>
        <tr>
            <td align="center" class="Estilo1" style="padding-top: 20px;">OBRA N� &nbsp;<%=Sesion.getObra().getIdObra()%></td>
        </tr>
        <tr>
            <td style='padding-top: 20px' align="center">
                <table border='1' cellpadding='1' cellspacing='1' align="center">
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Nombre obra</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' autocomplete="off" size='30' name='nombreObra'  id='nombreObra' value='<%=Sesion.getObra().getNombreObra()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Venta estimada</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='ventaEstimada'  id='ventaEstimada' value='<%=Sesion.getObra().getVentaEstimada()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1'>Costo servicios</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='montoServicios'  id='montoServicios' value='<%=Sesion.getObra().getCostoServicio()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Fecha Cliente</td>
                        <td align="right">:</td>
                        <td align="right"><input id="fcliente" name="fcliente" type="text" size="30" readonly class="txtFecha" value='<%=Sesion.getObra().getCliente().getFecha()%>' /></td>
                        <td><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fcliente'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha cliente")%> /></td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Nombre cliente</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><%=Sesion.getObra().getCliente().getNombre()%></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">N� presupuesto</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='npresupuesto'  id='npresupuesto' value='<%=Sesion.getObra().getNumPresupuesto()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style='padding-top: 20px'> 
                <input type="submit" name="actualizarObra" onclick="return actualizarObra()" id="actualizarObra" value="Actualizar obra" class="Estilo10">
            </td>
        </tr>
        <%}%>
        <%} else if (Sesion.getEtapaComercial() == 9) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRESUPUESTO  -  PRESUPUESTO MANTENCION CORRECTIVA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial2=SI" class="Estilo1" onclick="javascript:loading();">Presupuesto</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?buscarPresupuestoMantencionCorrectiva=SI" class="Estilo1" onclick="javascript:loading();">Buscar presupuesto mantenci�n correctiva</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px" class="Estilo1">INGRESO DE PRESUPUESTO</td>
        </tr>
        <%if (Sesion.getAux().equals("")) {%>
        <tr>
            <td align="center" style="padding-top: 20px">
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda "/>
                    <jsp:param name="campo1" value="C�digo"/>
                    <jsp:param name="campo2" value="Descripci�n"/>
                    <jsp:param name="campo3" value="yes"/>
                </jsp:include>
            </td>
        </tr>
        <%}%>
        <%=vw.agregarMaterialesAMantencionCorrectiva()%>
        <tr>
            <td style="padding-top: 20px">
                <a href="../FrmComercial?ingresarPresupuestoMantencionCorrectiva=YES" class="Estilo1" onclick="javascript:loading();">Ingresar presupuesto</a>
            </td>
        </tr>
        <%if (Sesion.getAux().equals("b")) {%>
        <tr>
            <td>
                <table cellpadding='0' cellspacing='0'>
                    <tr>
                        <td align="center" style="padding-top: 20px;">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                                <jsp:param name="campo4" value="YES"/>
                            </jsp:include>
                        </td>
                        <td  style="padding-left: 30px">
                            <input type="submit" value="Guardar presupuesto" onclick="return confirmacion()" name="guardarPresupuestoMantencionCorrectiva" id="guardarPresupuestoMantencionCorrectiva" class="Estilo10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%}%>
        <%=vw.grillaPresupuestosMantencionCorrectiva(request)%>
        <%} else if (Sesion.getEtapaComercial() == 10) {%>

        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRESUPUESTO  -  BUSCAR PRESUPUESTO MANTENCION CORRECTIVA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial2=SI" class="Estilo1" onclick="javascript:loading();">Presupuesto</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 20px">
                <table width="900" cellpadding="0" cellspacing="0" class="Contenido">
                    <tr class="CabeceraListado">
                        <td colspan="4" align="center" class="Estilo1">BUSCAR PRESUPUESTO MANTENCION CORRECTIVA</td>
                    </tr>
                    <%if (!Sesion.getAux().equals("e")) {%>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="Contenido">
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.Inicio</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="finicio" name="finicio" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('finicio'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                                </tr>
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.T�rmino</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="fterm" name="fterm" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fterm'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de t�rmino")%> /></td>
                                </tr> 
                            </table>
                        </td>
                        <td valign="middle">
                            <input type="submit" class="Estilo1" name="enviarFechaPresupuestoManCorrectiva" id="enviarFechaPresupuestoManCorrectiva" value="Buscar">
                        </td>
                        <td align="center" style="padding-top: 20px;padding-left: 20px">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo1" style="padding-left: 30px">
                            N� presupuesto:&nbsp;&nbsp;<input type="text" size="7" name="presupuestoManCorrectivaPorNumero" id="presupuestoManCorrectivaPorNumero">&nbsp;&nbsp;
                            <input type="submit" value="Buscar" name="buscarPresupuestoManCorrectivaPorNumero" id="buscarPresupuestoManCorrectivaPorNumero" class="Estilo10">
                        </td>
                    </tr>
                    <%}%>
                    <tr class="CabeceraListado"><td colspan="4">&nbsp;</td></tr>
                </table>
            </td>
        </tr>

        <%=vw.grillaPresupuestosManCorrectivaResumido(request)%>
        <%if (Sesion.getAux().equals("b")) {%>
        <tr>
            <td style="padding-top: 20px">
                <input type="submit" value="Actualizar presupuesto" name="actualizarPresupuestoMantencionCorrectiva" id="actualizarPresupuestoMantencionCorrectiva" class="Estilo10" onclick="confirmacion();">
            </td>
        </tr>
        <%=vw.grillaPresupuestosMantencionCorrectivaNumero(request)%>
        <%}%>

        <%} else if (Sesion.getEtapaComercial() == 11) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  INGRESO MANTENCION CORRECTIVA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial12=SI" class="Estilo1" onclick="javascript:loading();">Buscar mantenci�n correctiva</a>&nbsp;&nbsp;&nbsp;|</font>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px" class="Estilo1" align="center">INGRESO MANTENCION CORECTIVA</td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px;padding-left: 20px">
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda "/>
                    <jsp:param name="campo1" value="Rut cliente"/>
                    <jsp:param name="campo2" value="Nombre cliente"/>
                    <jsp:param name="campo4" value="YES"/>
                </jsp:include>
            </td>
        </tr>
        <tr>
            <td style='padding-top: 20px' align="center">
                <table border='1' cellpadding='1' cellspacing='1' align="center">
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Nombre</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' autocomplete="off" size='30' name='nombre'  id='nombre' value=''></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Monto</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='monto'  id='monto' value=''></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Fecha Cliente</td>
                        <td align="right">:</td>
                        <td align="right"><input id="fcliente" name="fcliente" type="text" size="30" readonly class="txtFecha" value='' /></td>
                        <td><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fcliente'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha cliente")%> /></td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">N� presupuesto</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='npresupuesto'  id='npresupuesto' value=''></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1'>Descripci�n</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><textarea rows="2" cols="23" name="descripcion" id="descripcion"></textarea></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='padding-top: 20px' align="center"><input type="submit" onclick="return guardarMantencionCorrectiva()" name="ingresarManCorrectiva" id="ingresarManCorrectiva" class="Estilo10" value="Ingresar mantenci�n correctiva"></td>
        </tr>
        <%} else if (Sesion.getEtapaComercial() == 12) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  BUSCAR MANTENCION CORRECTIVA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
            </td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 20px">
                <table width="900" cellpadding="0" cellspacing="0" class="Contenido">
                    <tr class="CabeceraListado">
                        <td colspan="4" align="center" class="Estilo1">BUSCAR MANTENCION CORRECTIVA</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="Contenido">
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.Inicio</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="finicio" name="finicio" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('finicio'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                                </tr>
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.T�rmino</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="fterm" name="fterm" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fterm'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de t�rmino")%> /></td>
                                </tr> 
                            </table>
                        </td>
                        <td valign="middle">
                            <input type="submit" class="Estilo1" name="enviarFechaObra" id="enviarFechaObra" value="Buscar">
                        </td>
                        <td align="center" style="padding-top: 20px;padding-left: 20px">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo1" style="padding-left: 30px">
                            N� obra:&nbsp;&nbsp;<input type="text" size="7" name="obraPorNumero" id="obraPorNumero">&nbsp;&nbsp;
                            <input type="submit" value="Buscar" onclick="return buscarObra()" name="enviarObraManCorrPorNumero" id="enviarObraManCorrPorNumero" class="Estilo10">
                        </td>
                    </tr>
                    <tr class="CabeceraListado"><td colspan="4">&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" class="Estilo1" style="padding-top: 20px;">MANTENCION CORRECTIVA</td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px;">
                <a href="../MantencionCorrectiva" target="_blank">
                    <img src="../img/btn_excel.gif" width="30" height="30" border="0" <%=vw.verOverTip("Haga click para exportar a Excel")%>/>
                </a>
            </td>
        </tr>
        <%=vw.grillaManCorrectivaResumido(request)%>
        <%if (Sesion.getAux().equals("d")) {%>
        <tr>
            <td align="center" class="Estilo1" style="padding-top: 20px;">MANTENCION CORRECTIVA N� &nbsp;<%=Sesion.getObra().getIdObra()%></td>
        </tr>
        <tr>
            <td style='padding-top: 20px' align="center">
                <table border='1' cellpadding='1' cellspacing='1' align="center">
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Nombre obra</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' autocomplete="off" size='30' name='nombreObra'  id='nombreObra' value='<%=Sesion.getObra().getNombreObra()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Venta</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='ventaEstimada'  id='ventaEstimada' value='<%=Sesion.getObra().getVentaEstimada()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Fecha Cliente</td>
                        <td align="right">:</td>
                        <td align="right"><input id="fcliente" name="fcliente" type="text" size="30" readonly class="txtFecha" value='<%=Sesion.getObra().getCliente().getFecha()%>' /></td>
                        <td><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fcliente'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha cliente")%> /></td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Nombre cliente</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><%=Sesion.getObra().getCliente().getNombre()%></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">N� presupuesto</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='npresupuesto'  id='npresupuesto' value='<%=Sesion.getObra().getNumPresupuesto()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Descripcion</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><textarea rows="2" cols="23" name="descripcion" id="descripcion"><%=Sesion.getObra().getDescripcion()%></textarea></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style='padding-top: 20px'> 
                <input type="submit" name="actualizarObraManCorr" onclick="return actualizarManCorrectiva()" id="actualizarObraManCorr" value="Actualizar" class="Estilo10">
            </td>
        </tr>
        <%}%>
        <%} else if (Sesion.getEtapaComercial() == 13) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  MANTENCION PREVENTIVA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial14=SI" class="Estilo1" onclick="javascript:loading();">Buscar mantenci�n preventiva</a>&nbsp;&nbsp;&nbsp;|
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px">
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda cliente"/>
                    <jsp:param name="campo1" value="Rut cliente"/>
                    <jsp:param name="campo2" value="Nombre cliente"/>
                </jsp:include>
            </td>
        </tr>
        <tr>
            <td align="center" class="Estilo1" style="padding-top: 20px;">MANTENCION PREVENTIVA</td>
        </tr>
        <tr>
            <td style='padding-top: 20px' align="center">
                <table border='1' cellpadding='1' cellspacing='1' align="center">
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Nombre</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' autocomplete="off" size='30' name='nombreObra'  id='nombreObra' value=''></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Frecuencia</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><%=vw.comboFrecuencia(request)%></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Cuota mensual (UF)</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='uf'  id='uf' value=''></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">UF</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='valorUf'  id='valorUf' value=''></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Nombre cliente</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><%=Sesion.getCliente().getNombre()%></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Observaci�n</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><textarea rows="2" cols="23" name="observacion" id="observacion"></textarea></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style='padding-top: 20px'> 
                <input type="submit" name="ingresarManPre" id="ingresarManPre" onclick="return ingresarManPre()"  value="Actualizar" class="Estilo10">
            </td>
        </tr>
        <%} else if (Sesion.getEtapaComercial() == 14) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  BUSCAR MANTENCION PREVENTIVA</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
            </td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 20px">
                <table width="900" cellpadding="0" cellspacing="0" class="Contenido">
                    <tr class="CabeceraListado">
                        <td colspan="4" align="center" class="Estilo1">BUSCAR MANTENCION PREVENTIVA</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="Contenido">
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.Inicio</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="finicio" name="finicio" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('finicio'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                                </tr>
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.T�rmino</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="fterm" name="fterm" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fterm'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de t�rmino")%> /></td>
                                </tr> 
                            </table>
                        </td>
                        <td valign="middle">
                            <input type="submit" class="Estilo1" name="enviarFechaManPre" id="enviarFechaManPre" value="Buscar">
                        </td>
                        <td align="center" style="padding-top: 20px;padding-left: 20px">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo1" style="padding-left: 30px">
                            N� mantenci�n preventiva:&nbsp;&nbsp;<input type="text" size="7" onkeypress="return justNumbers(event);" autocomplete="off" name="manPrePorNumero" id="manPrePorNumero">&nbsp;&nbsp;
                            <input type="submit" value="Buscar" onclick="return buscarManPre()" name="enviarManPrePorNumero" id="enviarManPrePorNumero" class="Estilo10">
                        </td>
                    </tr>
                    <tr class="CabeceraListado"><td colspan="4">&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" class="Estilo1" style="padding-top: 20px;">MANTENCION PREVENTINA</td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px;">
                <a href="../MantencionPreventiva" target="_blank">
                    <img src="../img/btn_excel.gif" width="30" height="30" border="0" <%=vw.verOverTip("Haga click para exportar a Excel")%>/>
                </a>
            </td>
        </tr>
        <%=vw.grillaManPreventivaResumido(request)%>
        <%if (Sesion.getAux().equals("a")) {%>
        <tr>
            <td style='padding-top: 20px' align="center">
                <table border='1' cellpadding='1' cellspacing='1' align="center">
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Nombre</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' autocomplete="off" size='30' name='nombreObra'  id='nombreObra' value='<%=Sesion.getMantencion().getNombre()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Frecuencia</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><%=vw.comboFrecuenciaSelected(request, Sesion.getMantencion().getFrecuencia())%></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Cuota mensual (UF)</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='valorUf'  id='valorUf' value='<%=Sesion.getMantencion().getValorUf()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">UF</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><input type='text' onkeypress="return justNumbers(event);" autocomplete="off" size='30' name='uf'  id='uf' value='<%=Sesion.getMantencion().getUf()%>'></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class="Estilo1">Nombre cliente</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><%=Sesion.getMantencion().getCliente().getNombre()%></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                    <tr bgcolor='#FAE8D8'>
                        <td class='Estilo1' width="200">Observaci�n</td>
                        <td class='Estilo1'>:</td>
                        <td class='Estilo1'><textarea rows="2" cols="23" name="observacion" id="observacion"><%=Sesion.getMantencion().getObservacion()%></textarea></td>
                        <td class='Estilo1'>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style='padding-top: 20px'> 
                <input type="submit" name="actualizarManPre" id="actualizarManPre" onclick="return actualizarManPre()"  value="Actualizar" class="Estilo10">
            </td>
        </tr>
        <%}%>
        <%} else if (Sesion.getEtapaComercial() == 15) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  VENTAS DIRECTAS</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial17=SI" class="Estilo1" onclick="javascript:loading();">Buscar ventas directas</a>&nbsp;&nbsp;&nbsp;|
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 20px" class="Estilo1">INGRESO DE VENTAS DIRECTAS</td>
        </tr>
        <%if (!Sesion.getAux().equals("b")) {%>
        <tr>
            <td align="center" style="padding-top: 20px">
                <jsp:include page="../jsp/lnk_ajax.jsp">
                    <jsp:param name="info" value="B�squeda "/>
                    <jsp:param name="campo1" value="C�digo"/>
                    <jsp:param name="campo2" value="Descripci�n"/>
                    <jsp:param name="campo3" value="yes"/>
                </jsp:include>
            </td>
        </tr>
        <%}%>
        <%if (Sesion.getAux().equals("b")) {%>
        <tr>
            <td>
                <table cellpadding='0' cellspacing='0'>
                    <tr>
                        <td align="center" style="padding-top: 20px;">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                                <jsp:param name="campo4" value="YES"/>
                            </jsp:include>
                        </td>
                        <td  style="padding-left: 30px">
                            <input type="submit" value="Guardar venta directa" onclick="return confirmacion()" name="guardarVentaDirecta" id="guardarVentaDirecta" class="Estilo10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%}%>
        <tr>
            <td style="padding-top: 20px;">
                <a href="../FrmComercial?ingresarVentaDirecta=SI" class="Estilo1" onclick="javascript:loading();">Ingresar venta directa</a>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px">
                <%=vw.grillaVentaDirecta(request)%>
            </td>
        </tr>
        <%} else if (Sesion.getEtapaComercial() == 16) {%>
        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  INFORMES</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
            </td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 20px">
                <table width="900" cellpadding="0" cellspacing="0" class="Contenido">
                    <tr class="CabeceraListado">
                        <td colspan="4" align="center" class="Estilo1">BUSCAR INFORMES</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="Contenido">
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.Inicio</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="finicio" name="finicio" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('finicio'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                                </tr>
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.T�rmino</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="fterm" name="fterm" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fterm'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de t�rmino")%> /></td>
                                </tr> 
                            </table>
                        </td>
                        <td valign="middle">
                            <input type="submit" class="Estilo1" name="enviarFechaInforme" id="enviarFechaInforme" value="Buscar">
                        </td>
                        <td align="center" style="padding-top: 20px;padding-left: 20px">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo1" style="padding-left: 30px">
                            N� obra:&nbsp;&nbsp;<input type="text" size="7" onkeypress="return justNumbers(event);" autocomplete="off" name="numeroObraInforme" id="numeroObraInforme">&nbsp;&nbsp;
                            <input type="submit" value="Buscar" name="enviarNumeroObraInforme" id="enviarNumeroObraInforme" class="Estilo10">
                        </td>
                    </tr>
                    <tr class="CabeceraListado"><td colspan="4">&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <%=vw.grillaInformesResumido(request)%>
        <%} else if (Sesion.getEtapaComercial() == 17) {%>

        <tr>
            <td class="Estilo1" style="font-size: 18px; padding-top: 20px" >COMERCIAL  -  PRODUCTOS O SERVICIOS  -  BUSCAR VENTAS DIRECTAS</td>
        </tr>
        <tr>
            <td>
                <font color="#336600">|&nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial1=SI" class="Estilo1" onclick="javascript:loading();">Comercial</a>&nbsp;&nbsp;&nbsp;|
                &nbsp;&nbsp;&nbsp;<a href="../FrmComercial?etapaComercial3=SI" class="Estilo1" onclick="javascript:loading();">Productos o servicios</a>&nbsp;&nbsp;&nbsp;|
            </td>
        </tr>
        <tr>
            <td class="Estilo1" style="padding-top: 20px">
                <table width="900" cellpadding="0" cellspacing="0" class="Contenido">
                    <tr class="CabeceraListado">
                        <td colspan="4" align="center" class="Estilo1">BUSCAR VENTAS DIRECTAS</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="Contenido">
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.Inicio</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="finicio" name="finicio" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('finicio'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de inicio")%> /></td>
                                </tr>
                                <tr>
                                    <td width="50" height="20" class="Estilo1">F.T�rmino</td>
                                    <td width="20" align="right">:</td>
                                    <td width="100" align="right"><input id="fterm" name="fterm" type="text" size="10" readonly class="txtFecha" value="" /></td>
                                    <td width="25"><img alt=""  src="../img/E_examinar.gif" width="17" height="17" style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fterm'));" <%=vw.verOverTip("haga click aqui, para asignar la fecha de t�rmino")%> /></td>
                                </tr> 
                            </table>
                        </td>
                        <td valign="middle">
                            <input type="submit" class="Estilo1" name="enviarFechaVentas" id="enviarFechaVentas" value="Buscar">
                        </td>
                        <td align="center" style="padding-top: 20px;padding-left: 20px">
                            <jsp:include page="../jsp/lnk_ajax.jsp">
                                <jsp:param name="info" value="B�squeda "/>
                                <jsp:param name="campo1" value="Rut cliente"/>
                                <jsp:param name="campo2" value="Nombre cliente"/>
                            </jsp:include>
                        </td>
                        <td class="Estilo1" style="padding-left: 30px">
                            N� venta directa:&nbsp;&nbsp;<input type="text" size="7" onkeypress="return justNumbers(event);" autocomplete="off" name="ventasPorNumero" id="ventasPorNumero">&nbsp;&nbsp;
                            <input type="submit" value="Buscar" name="enviarVentasPorNumero" id="enviarVentasPorNumero" class="Estilo10">
                        </td>
                    </tr>
                    <tr class="CabeceraListado"><td colspan="4">&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" class="Estilo1" style="padding-top: 20px;">VENTAS DIRECTAS</td>
        </tr>

        <%=vw.grillaVentasDirectasResumido(request)%>
        <%if (Sesion.getAux().equals("a")) {%>
        <%=vw.grillaVentaDirectaDetalle(request)%>
        <%}%>





        <%} else if (Sesion.getEtapaComercial() == 20) {%>
        <%=vw.prueba(request)%>
        <%}%>
    </table>
</form>

