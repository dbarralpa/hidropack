/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

import controllers.PrmApp;
import controllers.PrmComercial;
import controllers.PrmGrabarComercial;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModSesion;
import model.comercial.ModCliente;
import model.comercial.ModMantencion;
import model.comercial.ModPlantilla;
import model.comercial.ModPresupuesto;
import model.comercial.ModVentaDirecta;
import model.hidropack.ModObra;

/**
 *
 * @author dbarra
 */
public class FrmComercial extends HttpServlet {

    protected void processRequest(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        ModSesion Sesion = new ModSesion();
        try {
            /**
             * ******************************************INGRESO-PRESUPUESTO-OBRA*******************************************
             */
            if (req.getSession().getAttribute("Sesion") != null) {
                Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
                if (req.getParameter("etapaComercial1") != null) {
                    Sesion.setHttp_Body("comercialModificarPlantilla");
                    Sesion.setAux("");
                    Sesion.setEtapaComercial(1);
                }

                if (req.getParameter("etapaComercial2") != null) {
                    Sesion.setHttp_Body("comercialModificarPlantilla");
                    Sesion.setAux("");
                    Sesion.setDatos(PrmComercial.productoOServicios());
                    Sesion.setEtapaComercial(2);
                }

                if (req.getParameter("cmbProdServ") != null && Sesion.getEtapaComercial() == 2) {
                    if (req.getParameter("cmbProdServ").equals("1")) {
                        Sesion.setDatos(PrmComercial.plantillas());
                        Sesion.setAuxiliar(new ArrayList());
                        Sesion.setMateriales(new ArrayList());
                        Sesion.setPopUp("");
                        Sesion.setAux("");
                        Sesion.setEtapaComercial(3);
                    }
                    if (req.getParameter("cmbProdServ").equals("2")) {
                        Sesion.setPopUp("");
                        Sesion.setAux("");
                        Sesion.setHttp_Body("comercialPresupuestoMantencionCorrectiva");
                        Sesion.setMateriales(new ArrayList());
                        Sesion.setAuxiliar(new ArrayList());
                        Sesion.setEtapaComercial(9);
                    }

                }

                if (req.getParameter("cmbPlantilla") != null) {
                    Sesion.setHttp_Body("comercialModificarPlantilla");
                    Sesion.setAuxiliar(PrmComercial.plantilla(req.getParameter("cmbPlantilla")));
                    Sesion.setPopUp(req.getParameter("cmbPlantilla"));
                    Sesion.setEtapaComercial(4);
                }

                if (req.getParameter("cambioCodigoPlantillaObra") != null) {
                    ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("cambioCodigoPlantillaObra")));
                    if (pla.getEst() == 1) {
                        pla.setEst((byte) 0);
                    } else if (pla.getEst() == 0) {
                        pla.setEst((byte) 1);
                    }
                    Sesion.getAuxiliar().set(Integer.parseInt(req.getParameter("cambioCodigoPlantillaObra")), pla);

                }

                if (Sesion.getEtapaComercial() == 4) {
                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        if (req.getParameter("modificarCantidad" + i) != null && !req.getParameter("modificarCantidad" + i).equals("") && Float.parseFloat(req.getParameter("modificarCantidad" + i)) > 0) {
                            ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                            pla.setCantidad(Float.parseFloat(req.getParameter("modificarCantidad" + i)));
                            pla.setPrecioCosto(pla.getCantidad() * pla.getPrecioUnitario());
                            Sesion.getAuxiliar().set(i, pla);
                        }
                    }

                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        if (req.getParameter("modificarPrecioUnitario" + i) != null && !req.getParameter("modificarPrecioUnitario" + i).equals("") && Double.parseDouble(req.getParameter("modificarPrecioUnitario" + i)) > 0) {
                            ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                            pla.setPrecioUnitario(Double.parseDouble(req.getParameter("modificarPrecioUnitario" + i)));
                            pla.setPrecioCosto(pla.getCantidad() * pla.getPrecioUnitario());
                            Sesion.getAuxiliar().set(i, pla);
                        }
                    }

                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        if (req.getParameter("eliMaterialPlantilla" + i) != null) {
                            ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                            Sesion.getAuxiliar().remove(pla);
                            Sesion.getAuxiliar().trimToSize();
                        }
                    }
                }


                if (req.getParameter("agregarMaterialPlantilla") != null) {
                    if (req.getParameter("descMaterial") != null && !req.getParameter("descMaterial").equals("")
                            && req.getParameter("cantMaterial") != null && Float.parseFloat(req.getParameter("cantMaterial")) > 0
                            && req.getParameter("precioUniMaterial") != null && Float.parseFloat(req.getParameter("precioUniMaterial")) > 0) {
                        ModPlantilla pla = new ModPlantilla();
                        pla.setCodigo("");
                        pla.setDescripcion(req.getParameter("descMaterial"));
                        pla.setUm(req.getParameter("uniMaterial"));
                        pla.setCantidad(Float.parseFloat(req.getParameter("cantMaterial")));
                        pla.setPrecioUnitario(Float.parseFloat(req.getParameter("precioUniMaterial")));
                        pla.setPrecioCosto(pla.getCantidad() * pla.getPrecioUnitario());
                        Sesion.getAuxiliar().add(pla);

                    } else {
                        Sesion.setTip("Debe al menos agregar descripci�n, cantidad y precio unitario", 2);
                    }
                }

                if (req.getParameter("actualizarPlantilla") != null) {
                    boolean est = true;
                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                        if (pla.getCodigo().trim().length() == 0 || pla.getCantidad() == 0) {
                            est = false;
                            Sesion.setTip("Para actualizar la plantilla todos los materiales deben tener c�digo, cantidad y precio unitario", 2);
                            break;
                        }
                    }
                    if (est) {
                        est = PrmGrabarComercial.actualizarCambiarMaterial(Sesion.getPopUp(), Sesion.getAuxiliar());
                        if (est) {
                            Sesion.setTip("Plantilla actualizada", 0);
                        }
                    }
                    Sesion.setHttp_Body("comercialModificarPlantilla");
                    Sesion.setAux("");
                }

                if (req.getParameter("guardarComoNuevaPlantilla") != null) {
                    Sesion.setHttp_Body("comercialModificarPlantilla");
                    if (Sesion.getAux().equals("a")) {
                        Sesion.setAux("");
                    } else {
                        Sesion.setAux("a");
                    }

                }

                if (req.getParameter("ingresarPresupuesto") != null) {
                    if (Sesion.getAux().equals("b")) {
                        Sesion.setAux("");
                        Sesion.setHttp_Body("comercialModificarPlantilla");
                        Sesion.setCliente(new ModCliente());
                    } else {
                        Sesion.setAux("b");
                        Sesion.setHttp_Body("comercialIngresarPresupuesto");
                        Sesion.setDatos(PrmComercial.trabajo());
                        Sesion.setCliente(new ModCliente());
                    }

                }


                if (req.getParameter("guardarNuevaPlantilla") != null) {
                    if (req.getParameter("nuevaPlantilla") != null && !req.getParameter("nuevaPlantilla").trim().equals("")) {
                        boolean est = true;
                        boolean est1 = true;
                        for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                            ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                            if (pla.getCodigo().trim().length() == 0 || pla.getCantidad() == 0) {
                                est = false;
                                Sesion.setTip("Para guardar la plantilla todos los productos deben llevar c�digo, cantidad y precio unitario", 2);
                                break;
                            }
                        }
                        if (est) {
                            est1 = PrmGrabarComercial.guardarNuevaPlantilla(req.getParameter("nuevaPlantilla"), Sesion.getAuxiliar());
                            if (est1) {
                                Sesion.setAux("");
                                Sesion.setTip("Plantilla guardada", 0);
                            } else {
                                Sesion.setTip("No se pudo guardar la plantilla", 1);
                            }
                        }

                    } else {
                        Sesion.setTip("Debe colocar un nombre para la plantilla", 2);
                    }
                    Sesion.setHttp_Body("comercialModificarPlantilla");
                    Sesion.setAux("");
                }

                if (req.getParameter("guardarPresupuesto") != null) {
                    boolean est = true;
                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        ModPlantilla pla = (ModPlantilla) Sesion.getAuxiliar().get(i);
                        if (pla.getCantidad() == 0 || pla.getPrecioUnitario() == 0) {
                            Sesion.setTip("La cantidad y el precio unitario deben ser mayor a cero", 0);
                            est = false;
                            break;
                        }
                    }
                    if (est) {
                        if (req.getParameter("numeroPresupuesto") != null && !req.getParameter("numeroPresupuesto").trim().equals("")
                                && req.getParameter("cmbTrabajo") != null && !req.getParameter("cmbTrabajo").equals("-1") && Sesion.getCliente().getRut() > 0) {
                            boolean est1 = PrmGrabarComercial.guardarPresupuesto(Integer.parseInt(req.getParameter("numeroPresupuesto")), Integer.parseInt(req.getParameter("cmbTrabajo")), Sesion.getAuxiliar(), Sesion.getUsuario().getUsuario(), Sesion.getCliente().getRut(), 1, Sesion.getPopUp());
                            if (est1) {
                                Sesion.setTip("Presupuesto ingresado", 0);
                                Sesion.setAux("");
                                Sesion.setCliente(new ModCliente());
                                Sesion.setAuxiliar(new ArrayList());
                                Sesion.setDatos(PrmComercial.productoOServicios());
                                Sesion.setEtapaComercial(2);
                            } else {
                                Sesion.setTip("No se pudo realizar la acci�n", 1);
                            }
                        } else {
                            Sesion.setTip("Debe colocar N� de presupuesto, cliente y trabajo", 2);
                        }
                    }
                }

                /**
                 * ************************************************************************************************************************
                 */
                /**
                 * ***************************************************BUSCAR-PRESUPUESTO-OBRA**********************************************
                 */
                if (req.getParameter("buscarPresupuestoObra") != null) {
                    Sesion.setAuxiliar(new ArrayList());
                    Sesion.setDatos(PrmComercial.trabajo());
                    Sesion.setHttp_Body("comercialBuscarPresupuestoCliente");
                    Sesion.setAux("");
                    Sesion.setEtapaComercial(5);
                }

                if (req.getParameter("numeroPresupuestoObra") != null) {
                    ModPresupuesto pre = (ModPresupuesto) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("numeroPresupuestoObra")));
                    Sesion.setMateriales(PrmComercial.presupuesto(pre.getNumero(), pre.getTrabajo().getCodigo()));
                    Sesion.setPresupuesto(pre);
                    Sesion.setAux("c");

                }
                if (req.getParameter("cambioCodigoPresupuestoObra") != null) {
                    ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(Integer.parseInt(req.getParameter("cambioCodigoPresupuestoObra")));
                    if (pla.getEst() == 1) {
                        pla.setEst((byte) 0);
                        Sesion.setAux("c");
                        Sesion.setHttp_Body("comercialBuscarPresupuestoCliente");
                    } else if (pla.getEst() == 0) {
                        pla.setEst((byte) 1);
                        Sesion.setAux("e");
                        Sesion.setHttp_Body("comercialPresupuestoObra");
                    }
                    Sesion.getMateriales().set(Integer.parseInt(req.getParameter("cambioCodigoPresupuestoObra")), pla);
                }


                if (req.getParameter("buscarPresupuestoObraPorNumero") != null) {
                    Sesion.setAuxiliar(PrmComercial.listadoPresupuestosObra("nb_numero_presupuesto = " + Integer.parseInt(req.getParameter("presupuestoObraPorNumero")), 1));
                    Sesion.setAux("");
                    //Sesion.setDatos(new ArrayList());
                }

                if (req.getParameter("enviarFechaPresupuestoObra") != null) {
                    if (req.getParameter("finicio") != null && req.getParameter("fterm") != null && !req.getParameter("finicio").equals("") && !req.getParameter("fterm").equals("")) {
                        Sesion.setAuxiliar(PrmComercial.listadoPresupuestosObra("dt_fecha between '" + req.getParameter("finicio") + "' and '" + req.getParameter("fterm") + "' ", 1));
                    } else {
                        Sesion.setTip("Debe colocar ambas fechas para buscar", 2);
                    }
                }

                if (req.getParameter("agregarMaterialPresupuestoObra") != null) {
                    if (Sesion.getAux().equals("e")) {
                        Sesion.setAux("c");
                        Sesion.setHttp_Body("comercialBuscarObraCliente");
                    } else {
                        Sesion.setAux("e");
                        Sesion.setHttp_Body("comercialPresupuestoObra");
                        for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                            ModPresupuesto pre = (ModPresupuesto) Sesion.getMateriales().get(i);
                            pre.setEst((byte) 0);
                            Sesion.getMateriales().set(i, pre);
                        }
                    }
                }

                if (Sesion.getEtapaComercial() == 5) {
                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        if (req.getParameter("modificarCantidad" + i) != null && !req.getParameter("modificarCantidad" + i).equals("") && Float.parseFloat(req.getParameter("modificarCantidad" + i)) > 0) {
                            ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                            pla.setCantidad(Float.parseFloat(req.getParameter("modificarCantidad" + i)));
                            pla.setPrecioTotal(pla.getCantidad() * pla.getPrecioUnitario());
                            Sesion.getMateriales().set(i, pla);
                        }
                    }

                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        if (req.getParameter("modificarPrecioUnutario" + i) != null && !req.getParameter("modificarPrecioUnutario" + i).equals("") && Double.parseDouble(req.getParameter("modificarPrecioUnutario" + i)) > 0) {
                            ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                            pla.setPrecioUnitario(Double.parseDouble(req.getParameter("modificarPrecioUnutario" + i)));
                            pla.setPrecioTotal(pla.getCantidad() * pla.getPrecioUnitario());
                            Sesion.getMateriales().set(i, pla);
                        }
                    }

                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        if (req.getParameter("eliminarDePresupuestoObra" + i) != null) {
                            ModPresupuesto pre = (ModPresupuesto) Sesion.getMateriales().get(i);
                            Sesion.getMateriales().remove(pre);
                            Sesion.getMateriales().trimToSize();
                        }
                    }
                }
                if (req.getParameter("actualizarPresupuestoObra") != null) {
                    boolean est1 = true;
                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        ModPresupuesto pre = (ModPresupuesto) Sesion.getMateriales().get(i);
                        if (pre.getCantidad() == 0 || pre.getPrecioUnitario() == 0) {
                            est1 = false;
                            Sesion.setTip("Las cantidades y precios unitarios deben ser mayor a cero", 2);
                            break;
                        }
                    }
                    if (est1) {
                        if (!req.getParameter("cmbTrabajo").trim().equals("-1")) {
                            ModCliente cliente = new ModCliente();
                            if (!req.getParameter("input_2").trim().equals("")) {
                                cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            } else {
                                cliente.setRut(Sesion.getPresupuesto().getCliente().getRut());
                                cliente.setNombre(Sesion.getPresupuesto().getCliente().getNombre());
                            }
                            boolean est = PrmGrabarComercial.actualizarPresupuestoObra(Sesion.getMateriales(), cliente, Integer.parseInt(req.getParameter("cmbTrabajo").trim()), Sesion.getPresupuesto().getNavEncDet());
                            if (est) {
                                Sesion.getPresupuesto().getTrabajo().setCodigo(Integer.parseInt(req.getParameter("cmbTrabajo").trim()));
                                Sesion.getPresupuesto().getCliente().setRut(cliente.getRut());
                                Sesion.getPresupuesto().getCliente().setNombre(cliente.getNombre());
                                Sesion.setTip("Presupuesto actualizado", 0);
                            } else {
                                Sesion.setTip("No se pudo actualizar", 2);
                            }
                        } else {
                            Sesion.setTip("No se puede dejar sin trabajo", 2);
                        }
                    }
                }


                /**
                 * ****************************************************************************************************************************
                 */
                /*
                 * *********************************************INGRESO-OBRA*****************************************************
                 */
                if (req.getParameter("etapaComercial3") != null) {
                    Sesion.setDatos(PrmComercial.productoOServicios());
                    Sesion.setHttp_Body("comercialIngresarObra");
                    Sesion.setEtapaComercial(6);
                }

                if (req.getParameter("cmbProdServ") != null && Sesion.getEtapaComercial() == 6) {
                    if (req.getParameter("cmbProdServ").equals("1")) {
                        Sesion.setObra(new ModObra());
                        Sesion.setEtapaComercial(7);
                    }

                    if (req.getParameter("cmbProdServ").equals("2")) {
                        Sesion.setObra(new ModObra());
                        Sesion.setEtapaComercial(11);
                    }

                    if (req.getParameter("cmbProdServ").equals("3")) {
                        Sesion.setObra(new ModObra());
                        Sesion.setPopUp("");
                        Sesion.setAux("");
                        Sesion.setMateriales(new ArrayList());
                        Sesion.setAuxiliar(new ArrayList());
                        Sesion.setHttp_Body("comercialManPrevCliente");
                        Sesion.setMateriales(PrmComercial.frecuencia());
                        Sesion.setCliente(new ModCliente());
                        Sesion.setEtapaComercial(13);
                    }

                    if (req.getParameter("cmbProdServ").equals("4") || req.getParameter("cmbProdServ").equals("5") || req.getParameter("cmbProdServ").equals("6")) {
                        Sesion.setPopUp(req.getParameter("cmbProdServ"));
                        Sesion.setAux("");
                        Sesion.setAuxiliar(new ArrayList());
                        Sesion.setHttp_Body("comercialVentaDirecta");
                        Sesion.setEtapaComercial(15);
                    }
                }
                if (req.getParameter("ingresarObra") != null) {
                    //intentar//
                    if (req.getParameter("input_2").trim().equals("")
                            || req.getParameter("ventaEstimada").trim().equals("")
                            || req.getParameter("montoServicios").trim().equals("")
                            || req.getParameter("fcliente").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("0")
                            || req.getParameter("nombreObra").trim().equals("")) {
                        Sesion.setTip("Debe colocar todos los datos requeridos", 2);
                    } else {
                        boolean est1 = PrmComercial.isExistePresupuestoObra(Integer.parseInt(req.getParameter("npresupuesto").trim()), 1);
                        if (est1) {
                            ModCliente cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            if (cliente.getRut() == 0) {
                                Sesion.setTip("No se encontr� el cliente, vuelva a intentarlo", 1);
                            } else {
                                ModObra obra = new ModObra();
                                obra.setVentaEstimada(Double.parseDouble(req.getParameter("ventaEstimada").trim()));
                                obra.setCostoServicio(Double.parseDouble(req.getParameter("montoServicios").trim()));
                                obra.getCliente().setFecha(req.getParameter("fcliente").trim());
                                obra.getCliente().setRut(cliente.getRut());
                                obra.setNumPresupuesto(Integer.parseInt(req.getParameter("npresupuesto").trim()));
                                obra.setNombreObra(req.getParameter("nombreObra").trim());
                                Sesion.setObra(obra);
                                boolean est = PrmGrabarComercial.insertarObra(Sesion);
                                if (est) {
                                    Sesion.setTip("Obra insertada N� " + Sesion.getNumeroDoc(), 0);
                                    Sesion.setNumeroDoc(0);
                                    Sesion.setObra(new ModObra());
                                }
                            }
                        } else {
                            Sesion.setTip("No existe o no es presupuesto de obra", 2);
                        }
                    }
                }



                /*
                 * **************************************************************************************************************
                 */

                /*
                 * *********BUSCAR-Y-ACTUALIZAR-OBRA*********
                 */
                if (req.getParameter("etapaComercial4") != null) {
                    Sesion.setAux("");
                    Sesion.setAuxiliar(new ArrayList());
                    Sesion.setHttp_Body("comercialBuscarObraCliente");
                    Sesion.setEtapaComercial(8);
                }
                if (req.getParameter("enviarObraPorNumero") != null) {
                    if (!req.getParameter("obraPorNumero").trim().equals("")) {
                        Sesion.setAuxiliar(PrmComercial.listadoObra("TAB_Codigo = " + req.getParameter("obraPorNumero")));
                    } else {
                        Sesion.setTip("Debe ingresar un N� para buscar", 2);
                    }
                    Sesion.setAux("");
                }
                if (req.getParameter("enviarFechaObra") != null) {
                    if (req.getParameter("finicio") != null && req.getParameter("fterm") != null && !req.getParameter("finicio").equals("") && !req.getParameter("fterm").equals("")) {
                        String[] fechaIni = req.getParameter("finicio").split("/");
                        String[] fechaFin = req.getParameter("fterm").split("/");
                        Sesion.setAuxiliar(PrmComercial.listadoObra("FecCreacion between '" + fechaIni[2] + fechaIni[1] + fechaIni[0] + "' and '" + fechaFin[2] + fechaFin[1] + fechaFin[0] + "'"));
                    } else {
                        Sesion.setTip("Debe colocar ambas fechas para buscar", 2);
                    }
                    Sesion.setAux("");
                }
                if (req.getParameter("numeroObra") != null) {
                    ModObra obra = (ModObra) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("numeroObra")));
                    Sesion.setObra(obra);
                    Sesion.setAux("d");
                }
                if (req.getParameter("actualizarObra") != null) {

                    if (req.getParameter("ventaEstimada").trim().equals("")
                            || req.getParameter("montoServicios").trim().equals("")
                            || req.getParameter("fcliente").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("0")
                            || req.getParameter("nombreObra").trim().equals("")) {
                        Sesion.setTip("Debe colocar todos los datos requeridos", 2);
                    } else {

                        if (!req.getParameter("input_2").trim().equals("")) {
                            ModCliente cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            Sesion.getObra().getCliente().setRut(cliente.getRut());
                            Sesion.getObra().getCliente().setNombre(cliente.getNombre());
                        }
                        Sesion.getObra().setVentaEstimada(Double.parseDouble(req.getParameter("ventaEstimada").trim()));
                        Sesion.getObra().setCostoServicio(Double.parseDouble(req.getParameter("montoServicios").trim()));
                        Sesion.getObra().getCliente().setFecha(req.getParameter("fcliente").trim());
                        Sesion.getObra().setNombreObra(req.getParameter("nombreObra").trim());
                        boolean est1 = PrmComercial.isExistePresupuestoObra(Integer.parseInt(req.getParameter("npresupuesto").trim()), 1);
                        if (est1) {
                            boolean est = PrmGrabarComercial.actualizarObra(Sesion.getObra(), Sesion.getUsuario().getUsuario(), Integer.parseInt(req.getParameter("npresupuesto").trim()));
                            if (est) {
                                Sesion.getObra().setNumPresupuesto(Integer.parseInt(req.getParameter("npresupuesto").trim()));
                                Sesion.setTip("Obra actualizada", 0);
                            }
                        } else {
                            Sesion.setTip("Debe existir el presupuesto", 2);
                        }
                    }
                }

                /*
                 * *****************************
                 */


                /*
                 * MANTENCION CORRECTIVA
                 */
                if (req.getParameter("agregarMaterialMantencionCorrectiva") != null) {
                    if (req.getParameter("descMaterial").trim().equals("") || req.getParameter("costoMaterial").trim().equals("") || req.getParameter("instalacionMaterial").trim().equals("") || req.getParameter("cantidadMaterial").trim().equals("")) {
                        Sesion.setTip("Debe ingresar todos los campos", 2);
                    } else {
                        ModPresupuesto pre = new ModPresupuesto();
                        pre.setDescripcion(req.getParameter("descMaterial"));
                        pre.setCantidad(Float.parseFloat(req.getParameter("cantidadMaterial")));
                        pre.setPrecioUnitario(Double.parseDouble(req.getParameter("costoMaterial")));
                        pre.setPrecioTotal(pre.getCantidad() * pre.getPrecioUnitario());
                        pre.setInstalacion(Double.parseDouble(req.getParameter("instalacionMaterial")));
                        Sesion.getMateriales().add(pre);
                    }
                }

                if (req.getParameter("unidadFomento") != null && !req.getParameter("unidadFomento").trim().equals("")) {
                    Sesion.setAux1(Integer.parseInt(req.getParameter("unidadFomento")));

                }

                if (Sesion.getEtapaComercial() == 9) {
                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                        if (req.getParameter("modificarCantidad" + i) != null && Float.parseFloat(req.getParameter("modificarCantidad" + i)) > 0) {
                            pla.setCantidad(Float.parseFloat(req.getParameter("modificarCantidad" + i)));
                            pla.setPrecioTotal(pla.getCantidad() * pla.getPrecioUnitario());
                        }
                        if (req.getParameter("modificarCosto" + i) != null && Double.parseDouble(req.getParameter("modificarCosto" + i)) > 0) {
                            pla.setPrecioUnitario(Double.parseDouble(req.getParameter("modificarCosto" + i)));
                            pla.setPrecioTotal(pla.getCantidad() * pla.getPrecioUnitario());
                        }
                        if (req.getParameter("modificarInstalacion" + i) != null && Double.parseDouble(req.getParameter("modificarInstalacion" + i)) > 0) {
                            pla.setInstalacion(Double.parseDouble(req.getParameter("modificarInstalacion" + i)));
                        }
                        Sesion.getMateriales().set(i, pla);
                        if (req.getParameter("eliminarDePresupuestoMantencionCorrectiva" + i) != null) {
                            Sesion.getMateriales().remove(pla);
                            Sesion.getMateriales().trimToSize();
                        }
                    }

                }

                if (req.getParameter("ingresarPresupuestoMantencionCorrectiva") != null) {
                    if (Sesion.getMateriales().isEmpty()) {
                        Sesion.setTip("Debe al menos ingresar un producto", 2);
                    } else {
                        if (Sesion.getAux().equals("b")) {
                            Sesion.setHttp_Body("comercialPresupuestoMantencionCorrectiva");
                            Sesion.setAux("");
                        } else {
                            Sesion.setAux("b");
                            Sesion.setHttp_Body("comercialPresupuestoMantencionCorrectivaCliente");
                        }

                    }

                }

                if (req.getParameter("guardarPresupuestoMantencionCorrectiva") != null) {
                    boolean est = true;
                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                        if (pla.getCantidad() == 0 || pla.getPrecioUnitario() == 0 || pla.getInstalacion() == 0 || Sesion.getAux1() == 0) {
                            Sesion.setTip("Ning�n valor debe estar en cero", 2);
                            est = false;
                            break;
                        }
                    }
                    if (est) {
                        if (!req.getParameter("input_2").trim().equals("")) {
                            ModCliente cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            if (cliente.getRut() == 0) {
                                Sesion.setTip("No se encontr� al cliente", 2);
                            } else {
                                boolean est1 = PrmGrabarComercial.guardarPresupuestoMantencionCorrectiva(Sesion, cliente.getRut(), 2);
                                if (est1) {
                                    Sesion.setMateriales(new ArrayList());
                                    Sesion.setAux("");
                                    Sesion.setAux1(0);
                                    Sesion.setTip("Presupuesto guardado N�: " + Sesion.getNumeroDoc(), 0);
                                    Sesion.setNumeroDoc(0l);
                                    cliente = null;
                                    Sesion.setHttp_Body("comercialPresupuestoMantencionCorrectiva");
                                }
                            }
                        } else {
                            Sesion.setTip("Debe agregar un cliente", 2);
                        }
                    }
                }
                if (req.getParameter("buscarPresupuestoMantencionCorrectiva") != null) {
                    Sesion.setAux("");
                    Sesion.setAux1(0);
                    Sesion.setHttp_Body("comercialBuscarPresupuestoClienteManCorrectiva");
                    Sesion.setMateriales(new ArrayList());
                    Sesion.setAuxiliar(new ArrayList());
                    Sesion.setEtapaComercial(10);
                }
                if (req.getParameter("buscarPresupuestoManCorrectivaPorNumero") != null) {
                    Sesion.setAuxiliar(PrmComercial.listadoPresupuestosManCorrectiva("nb_numero_presupuesto = " + Integer.parseInt(req.getParameter("presupuestoManCorrectivaPorNumero")), 2));
                    Sesion.setAux("");
                }

                if (req.getParameter("enviarFechaPresupuestoManCorrectiva") != null) {
                    if (req.getParameter("finicio") != null && req.getParameter("fterm") != null && !req.getParameter("finicio").equals("") && !req.getParameter("fterm").equals("")) {
                        Sesion.setAuxiliar(PrmComercial.listadoPresupuestosManCorrectiva("dt_fecha between '" + req.getParameter("finicio") + "' and '" + req.getParameter("fterm") + "' ", 2));
                    } else {
                        Sesion.setTip("Debe colocar ambas fechas para buscar", 2);
                    }
                }

                if (req.getParameter("numeroPresupuestoManCorrectiva") != null) {
                    Sesion.setAux("b");
                    ModPresupuesto pre = (ModPresupuesto) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("numeroPresupuestoManCorrectiva")));
                    Sesion.setPresupuesto(pre);
                    Sesion.setMateriales(PrmComercial.listadoPresupuestosManCorrectiva(pre.getNumero()));
                }

                if (req.getParameter("ingresarManCorrectiva") != null) {
                    //intentar//
                    if (req.getParameter("input_2").trim().equals("")
                            || req.getParameter("nombre").trim().equals("")
                            || req.getParameter("descripcion").trim().equals("")
                            || req.getParameter("monto").trim().equals("")
                            || req.getParameter("fcliente").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("0")) {
                        Sesion.setTip("Debe colocar todos los datos requeridos", 2);
                    } else {
                        boolean est1 = PrmComercial.isExistePresupuestoObra(Integer.parseInt(req.getParameter("npresupuesto").trim()), 2);
                        if (est1) {
                            ModCliente cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            if (cliente.getRut() == 0) {
                                Sesion.setTip("No se encontr� el cliente, vuelva a intentarlo", 1);
                            } else {

                                boolean est = PrmGrabarComercial.insertarManCorrectiva(Sesion, req.getParameter("nombre").trim(),
                                        Double.parseDouble(req.getParameter("monto").trim()),
                                        cliente.getRut(),
                                        Integer.parseInt(req.getParameter("npresupuesto").trim()),
                                        req.getParameter("fcliente").trim(),
                                        req.getParameter("descripcion").trim());
                                if (est) {
                                    Sesion.setTip("Obra insertada N� " + Sesion.getNumeroDoc(), 0);
                                    Sesion.setNumeroDoc(0);
                                }
                            }
                        } else {
                            Sesion.setTip("No existe el presupuesto ingresado", 2);
                        }
                    }
                }

                if (req.getParameter("etapaComercial12") != null) {
                    Sesion.setAux("");
                    Sesion.setAuxiliar(new ArrayList());
                    Sesion.setEtapaComercial(12);
                }

                if (req.getParameter("enviarObraManCorrPorNumero") != null) {
                    if (!req.getParameter("obraPorNumero").trim().equals("")) {
                        Sesion.setAuxiliar(PrmComercial.listadoObraManCorr("TAB_Codigo = " + req.getParameter("obraPorNumero")));
                    } else {
                        Sesion.setTip("Debe ingresar un N� para buscar", 2);
                    }
                    Sesion.setAux("");
                }

                if (req.getParameter("numeroObraManCorre") != null) {
                    ModObra obra = (ModObra) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("numeroObraManCorre")));
                    Sesion.setObra(obra);
                    Sesion.setAux("d");
                }

                if (req.getParameter("actualizarObraManCorr") != null) {
                    if (req.getParameter("ventaEstimada").trim().equals("")
                            || req.getParameter("fcliente").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("")
                            || req.getParameter("npresupuesto").trim().equals("0")
                            || req.getParameter("nombreObra").trim().equals("")
                            || req.getParameter("descripcion").trim().equals("")) {
                        Sesion.setTip("Debe colocar todos los datos requeridos", 2);
                    } else {
                        if (!req.getParameter("input_2").trim().equals("")) {
                            ModCliente cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            Sesion.getObra().getCliente().setRut(cliente.getRut());
                            Sesion.getObra().getCliente().setNombre(cliente.getNombre());
                        }
                        if (Sesion.getObra().getCliente().getRut() > 0) {
                            Sesion.getObra().setVentaEstimada(Double.parseDouble(req.getParameter("ventaEstimada").trim()));
                            Sesion.getObra().getCliente().setFecha(req.getParameter("fcliente").trim());
                            Sesion.getObra().setNombreObra(req.getParameter("nombreObra").trim());
                            Sesion.getObra().setDescripcion(req.getParameter("descripcion").trim());
                            boolean est1 = PrmComercial.isExistePresupuestoObra(Integer.parseInt(req.getParameter("npresupuesto").trim()), 2);
                            if (est1) {
                                boolean est = PrmGrabarComercial.actualizarObraManCorrectiva(Sesion.getObra(), Sesion.getUsuario().getUsuario(), Integer.parseInt(req.getParameter("npresupuesto").trim()));
                                if (est) {
                                    Sesion.getObra().setNumPresupuesto(Integer.parseInt(req.getParameter("npresupuesto").trim()));
                                    Sesion.setObra(new ModObra());
                                    Sesion.setTip("Mantenci�n correctiva actualizada", 0);
                                }
                            } else {
                                Sesion.setTip("Debe existir el presupuesto", 2);
                            }
                        } else {
                            Sesion.setTip("No se encuentra el cliente", 2);
                        }
                    }
                }

                if (Sesion.getEtapaComercial() == 10) {
                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        ModPresupuesto pla = (ModPresupuesto) Sesion.getMateriales().get(i);
                        if (req.getParameter("modificarCantidad" + i) != null && Float.parseFloat(req.getParameter("modificarCantidad" + i)) > 0) {
                            pla.setCantidad(Float.parseFloat(req.getParameter("modificarCantidad" + i)));
                            pla.setPrecioTotal(pla.getCantidad() * pla.getPrecioUnitario());
                        }
                        if (req.getParameter("modificarCosto" + i) != null && Double.parseDouble(req.getParameter("modificarCosto" + i)) > 0) {
                            pla.setPrecioUnitario(Double.parseDouble(req.getParameter("modificarCosto" + i)));
                            pla.setPrecioTotal(pla.getCantidad() * pla.getPrecioUnitario());
                        }
                        if (req.getParameter("modificarInstalacion" + i) != null && Double.parseDouble(req.getParameter("modificarInstalacion" + i)) > 0) {
                            pla.setInstalacion(Double.parseDouble(req.getParameter("modificarInstalacion" + i)));
                        }
                        Sesion.getMateriales().set(i, pla);
                        if (req.getParameter("eliminarDePresupuestoMantencionCorrectiva" + i) != null) {
                            Sesion.getMateriales().remove(pla);
                            Sesion.getMateriales().trimToSize();
                        }
                    }
                }

                if (req.getParameter("actualizarPresupuestoMantencionCorrectiva") != null) {
                    boolean est1 = true;
                    for (int i = 0; i < Sesion.getMateriales().size(); i++) {
                        ModPresupuesto pre = (ModPresupuesto) Sesion.getMateriales().get(i);
                        if (pre.getCantidad() == 0 || pre.getPrecioUnitario() == 0) {
                            est1 = false;
                            Sesion.setTip("Las cantidades y precios unitarios deben ser mayor a cero", 2);
                            break;
                        }
                    }
                    if (est1) {
                        ModCliente cliente = new ModCliente();
                        if (!req.getParameter("input_2").trim().equals("")) {
                            cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                        } else {
                            cliente.setRut(Sesion.getPresupuesto().getCliente().getRut());
                            cliente.setNombre(Sesion.getPresupuesto().getCliente().getNombre());
                        }
                        boolean est = PrmGrabarComercial.actualizarPresupuestoManCorr(Sesion.getMateriales(), cliente, Sesion.getPresupuesto().getNavEncDet(), Sesion.getPresupuesto().getUf());
                        if (est) {
                            Sesion.getPresupuesto().getCliente().setRut(cliente.getRut());
                            Sesion.getPresupuesto().getCliente().setNombre(cliente.getNombre());
                            Sesion.setTip("Presupuesto actualizado", 0);
                            Sesion.setPresupuesto(new ModPresupuesto());
                            Sesion.setAux("");
                            Sesion.setMateriales(new ArrayList());
                        } else {
                            Sesion.setTip("No se pudo actualizar", 2);
                        }

                    }
                }


                /*
                 **********
                 */


                /*
                 * MANTENCION PREVENTIVA
                 */

                if (req.getParameter("ingresarManPre") != null) {
                    if (req.getParameter("nombreObra").trim().equals("")
                            || req.getParameter("cmbFrecuencia").trim().equals("")
                            || req.getParameter("uf").trim().equals("")
                            || req.getParameter("valorUf").trim().equals("")
                            || Sesion.getCliente().getRut() == 0) {
                        Sesion.setTip("Debe colocar todos los datos requeridos", 2);
                    } else {
                        ModMantencion man = new ModMantencion();
                        man.setCliente(Sesion.getCliente());
                        man.setFrecuencia(Integer.parseInt(req.getParameter("cmbFrecuencia").trim()));
                        man.setNombre(req.getParameter("nombreObra").trim());
                        man.setUf(Float.parseFloat(req.getParameter("uf").trim()));
                        man.setValorUf(Integer.parseInt(req.getParameter("valorUf").trim()));
                        man.setObservacion(req.getParameter("observacion").trim());

                        boolean est = PrmGrabarComercial.insertarManPreventiva(Sesion, man);
                        if (est) {
                            Sesion.setTip("Mantenci�n preventiva insertada N�: " + Sesion.getNumeroDoc(), 0);
                            Sesion.setNumeroDoc(0l);
                            Sesion.setCliente(new ModCliente());
                        }
                    }
                }

                if (req.getParameter("etapaComercial14") != null) {
                    Sesion.setHttp_Body("comercialManPreventiva");
                    Sesion.setAuxiliar(new ArrayList());
                    Sesion.setAux("");
                    Sesion.setEtapaComercial(14);
                }
                if (req.getParameter("enviarManPrePorNumero") != null) {
                    if (!req.getParameter("manPrePorNumero").trim().equals("")) {
                        Sesion.setAuxiliar(PrmComercial.listadoObraManPrev("TAB_Codigo = " + req.getParameter("manPrePorNumero")));
                    } else {
                        Sesion.setTip("Debe colocar un N� a buscar", 2);
                    }
                }
                if (req.getParameter("numeroObraManPrev") != null) {
                    ModMantencion man = (ModMantencion) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("numeroObraManPrev")));
                    Sesion.setMantencion(man);
                    Sesion.setAux("a");
                }



                if (req.getParameter("actualizarManPre") != null) {
                    if (req.getParameter("nombreObra").trim().equals("")
                            || req.getParameter("cmbFrecuencia").trim().equals("")
                            || req.getParameter("uf").trim().equals("")
                            || req.getParameter("valorUf").trim().equals("")
                            || Sesion.getMantencion().getCliente().getRut() == 0) {
                        Sesion.setTip("Debe colocar todos los datos requeridos", 2);
                    } else {
                        if (!req.getParameter("input_2").trim().equals("")) {
                            ModCliente cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            Sesion.getMantencion().getCliente().setRut(cliente.getRut());
                            Sesion.getMantencion().getCliente().setNombre(cliente.getNombre());
                        }
                        if (Sesion.getMantencion().getCliente().getRut() > 0) {
                            Sesion.getMantencion().setFrecuencia(Integer.parseInt(req.getParameter("cmbFrecuencia").trim()));
                            Sesion.getMantencion().setNombre(req.getParameter("nombreObra").trim());
                            Sesion.getMantencion().setUf(Float.parseFloat(req.getParameter("uf").trim()));
                            Sesion.getMantencion().setValorUf(Integer.parseInt(req.getParameter("valorUf").trim()));
                            Sesion.getMantencion().setObservacion(req.getParameter("observacion").trim());

                            boolean est = PrmGrabarComercial.actualizarManPreventiva(Sesion, Sesion.getMantencion());
                            if (est) {
                                Sesion.setTip("Mantenci�n actualizada", 0);
                            }
                        } else {
                            Sesion.setTip("Cliente no encontrado", 2);
                        }
                    }
                }

                /*
                 ********** 
                 */


                /*
                 * VENTAS_DIRECTAS
                 */
                if (Sesion.getEtapaComercial() == 15) {
                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        ModVentaDirecta pla = (ModVentaDirecta) Sesion.getAuxiliar().get(i);
                        if (req.getParameter("eliMaterialVentaDirecta" + i) != null) {
                            Sesion.getAuxiliar().remove(pla);
                            Sesion.getAuxiliar().trimToSize();
                        }
                    }
                }

                if (req.getParameter("ingresarVentaDirecta") != null) {
                    if (Sesion.getAuxiliar().isEmpty()) {
                        Sesion.setTip("Debe agregar al menos un producto", 2);
                    } else {
                        if (Sesion.getAux().equals("b")) {
                            Sesion.setAux("");
                            Sesion.setHttp_Body("comercialVentaDirecta");
                        } else {
                            Sesion.setAux("b");
                            Sesion.setHttp_Body("comercialVentaDirectaCliente");
                        }

                    }

                }
                if (req.getParameter("guardarVentaDirecta") != null) {
                    boolean est = true;
                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        ModVentaDirecta pla = (ModVentaDirecta) Sesion.getAuxiliar().get(i);
                        if (pla.getCantidad() == 0 || pla.getPrecio() == 0) {
                            est = false;
                            Sesion.setTip("El precio y la cantidad deben ser mayor a cero", 2);
                        }
                    }
                    if (est) {
                        if (!req.getParameter("input_2").trim().equals("")) {
                            ModCliente cliente = PrmComercial.rutCLiente(req.getParameter("input_2").trim());
                            if (cliente.getRut() > 0) {
                                if (PrmGrabarComercial.insertarVentaDirecta(Sesion, cliente.getRut(), Integer.parseInt(Sesion.getPopUp()))) {
                                    Sesion.setTip("Venta insertada N� " + Sesion.getNumeroDoc(), 0);
                                    Sesion.setNumeroDoc(0l);
                                    Sesion.setPopUp("");
                                    Sesion.setAux("");
                                    Sesion.setAuxiliar(new ArrayList());
                                    Sesion.setEtapaComercial(1);
                                }
                            } else {
                                Sesion.setTip("Cliente no encontrado", 2);
                            }
                        } else {
                            Sesion.setTip("Debe agregar un cliente", 2);
                        }
                    }
                }

                if (req.getParameter("etapaComercial17") != null) {
                    Sesion.setAuxiliar(new ArrayList());
                    Sesion.setHttp_Body("comercialVentasDirectas");
                    Sesion.setEtapaComercial(17);
                }

                if (req.getParameter("enviarVentasPorNumero") != null) {
                    Sesion.setAuxiliar(PrmComercial.ventasDirectasResumido("TAB_Codigo = " + req.getParameter("ventasPorNumero")));
                }

                if (req.getParameter("enviarFechaVentas") != null) {
                    if (!req.getParameter("finicio").trim().equals("") && !req.getParameter("fterm").trim().equals("")) {
                        String[] fechaIni = req.getParameter("finicio").split("/");
                        String[] fechaFin = req.getParameter("fterm").split("/");
                        Sesion.setAuxiliar(PrmComercial.ventasDirectasResumido("FecCreacion between '" + (fechaIni[2] + fechaIni[1] + fechaIni[0]) + "' and '" + (fechaFin[2] + fechaFin[1] + fechaFin[0]) + "'"));
                    } else {
                        Sesion.setTip("Debe agregar ambas fechas", 2);
                    }
                }

                if (req.getParameter("numeroVentaDirecta") != null) {
                    ModObra obra = (ModObra) Sesion.getAuxiliar().get(Integer.parseInt(req.getParameter("numeroVentaDirecta")));
                    Sesion.setAuxiliar2(PrmComercial.ventasDirectasDetalle(obra.getIdObra()));
                    Sesion.setAux("a");
                }

                if (Sesion.getEtapaComercial() == 15) {
                    for (int i = 0; i < Sesion.getAuxiliar().size(); i++) {
                        ModVentaDirecta pla = (ModVentaDirecta) Sesion.getAuxiliar().get(i);
                        if (req.getParameter("modificarCantidad" + i) != null && Double.parseDouble(req.getParameter("modificarCantidad" + i).trim()) > 0) {
                            pla.setCantidad(Double.parseDouble(req.getParameter("modificarCantidad" + i).trim()));
                            Sesion.getAuxiliar().set(i, pla);
                        }
                    }
                }


                /*
                 **************** 
                 */

                /*
                 * INFORMES
                 */
                if (req.getParameter("etapaComercial6") != null) {
                    Sesion.setHttp_Body("comercialInformes");
                    Sesion.setAuxiliar(new ArrayList());
                    Sesion.setEtapaComercial(16);
                }

                if (req.getParameter("enviarNumeroObraInforme") != null) {
                    Sesion.setAuxiliar(PrmComercial.informesResumido("sys.TAB_Codigo = '" + req.getParameter("numeroObraInforme") + "'"));
                }

                if (req.getParameter("enviarFechaInforme") != null) {
                    if (!req.getParameter("finicio").trim().equals("") && !req.getParameter("fterm").trim().equals("")) {
                        String[] fechaIni = req.getParameter("finicio").split("/");
                        String[] fechaFin = req.getParameter("fterm").split("/");
                        Sesion.setAuxiliar(PrmComercial.informesResumido("sys.FecCreacion between '" + (fechaIni[2] + fechaIni[1] + fechaIni[0]) + "' and '" + (fechaFin[2] + fechaFin[1] + fechaFin[0]) + "'"));
                    } else {
                        Sesion.setTip("Debe agregar ambas fechas", 2);
                    }
                }
                /*
                 ********** 
                 */
                
                if (req.getParameter("Prueba") != null) {
                    Sesion.setAuxiliar(PrmComercial.compradores());
                    Sesion.setEtapaComercial(20);
                }
                
                Sesion.setUrl("../page/comercial.jsp");

            }

        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }

        req.getSession().setAttribute("Sesion", Sesion);
        res.sendRedirect("template/appLayout.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
