package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import model.comercial.ModCliente;
import model.comercial.ModInforme;
import model.comercial.ModMantencion;
import model.comercial.ModPlantilla;
import model.comercial.ModPresupuesto;
import model.comercial.ModProServ;
import model.comercial.ModPrueba;
import model.comercial.ModTrabajo;
import model.comercial.ModVentaDirecta;
import model.hidropack.ModObra;
import sql.ConexionSQL;

public class PrmComercial {

    static final SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");

    public PrmComercial() {
    }

    public static ArrayList productoOServicios() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList productoOServicios = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT  TAB_Codigo, TAB_Desc, TAB_Texto1 FROM SYS_TABLAS WHERE (TAB_CodTabla = 613)";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModProServ tra = new ModProServ();
                    tra.setCodigo(rs.getInt("TAB_Codigo"));
                    tra.setDescripcion(rs.getString("TAB_Desc"));
                    tra.setTipo(rs.getString("TAB_Texto1"));
                    productoOServicios.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return productoOServicios;
    }

    public static ArrayList plantillas() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList plantillas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = "SELECT DISTINCT(vc_nombre) FROM TBL_COM_PLANTILLA";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModProServ tra = new ModProServ();
                    tra.setDescripcion(rs.getString(1));
                    plantillas.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return plantillas;
    }

    public static ArrayList trabajo() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList trabajos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT  TAB_Codigo, TAB_Desc FROM SYS_TABLAS WHERE (TAB_CodTabla = 615)";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModTrabajo tra = new ModTrabajo();
                    tra.setCodigo(rs.getInt("TAB_Codigo"));
                    tra.setDescripcion(rs.getString("TAB_Desc"));
                    trabajos.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return trabajos;
    }

    public static ArrayList plantilla(String nombrePlantilla) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList plantillas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT vc_codigo_material, nb_cantidad ,PRO_DESC,PRO_UMPRINCIPAL,";
                sql += " isnull(Costo,0) as precioAjustado";
                sql += " FROM HIDROPACK.dbo.TBL_COM_PLANTILLA";
                sql += " inner join STO_PRODUCTO";
                sql += " on(PRO_CODPROD = vc_codigo_material COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " left outer join ENSchaffner.dbo.SCHCup";
                sql += " on(Codpro = vc_codigo_material COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " WHERE vc_nombre = '" + nombrePlantilla + "'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModPlantilla pla = new ModPlantilla();
                    pla.setCodigo(rs.getString("vc_codigo_material"));
                    pla.setDescripcion(rs.getString("PRO_DESC"));
                    pla.setUm(rs.getString("PRO_UMPRINCIPAL"));
                    pla.setCantidad(rs.getFloat("nb_cantidad"));
                    pla.setPrecioUnitario(rs.getDouble("precioAjustado"));
                    pla.setPrecioCosto(pla.getCantidad() * pla.getPrecioUnitario());
                    plantillas.add(pla);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return plantillas;
    }

    public static ArrayList presupuesto(int numeroPresupuesto, int idTrabajo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList presupuesto = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select vc_codigo,vc_descripcion,PRO_DESC,PRO_UMPRINCIPAL,nb_cantidad,nb_precio_unitario";
                sql += " from TBL_COM_PRESUPUESTO_ENC enc";
                sql += " inner join TBL_COM_NAV_ENC_DET  encdet";
                sql += " on(encdet.nb_id = enc.nb_id_nub_enc_det)";
                sql += " inner join TBL_COM_PRESUPUESTO_DET det";
                sql += " on(enc.nb_id_nub_enc_det = det.nb_id_nub_enc_det)";
                sql += " left outer join SiaHidropack.dbo.STO_PRODUCTO";
                sql += " on(vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS= PRO_CODPROD)";
                sql += " where  nb_numero_presupuesto = " + numeroPresupuesto + " and nb_id_trabajo = " + idTrabajo;
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModPresupuesto pla = new ModPresupuesto();
                    pla.setCodigo(rs.getString("vc_codigo"));
                    pla.setDescripcion(rs.getString("vc_descripcion"));
                    pla.setUm(rs.getString("PRO_UMPRINCIPAL"));
                    pla.setCantidad(rs.getFloat("nb_cantidad"));
                    pla.setPrecioUnitario(rs.getDouble("nb_precio_unitario"));
                    pla.setPrecioTotal(pla.getCantidad() * pla.getPrecioUnitario());
                    presupuesto.add(pla);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return presupuesto;
    }

    public static ModPlantilla cambiarMaterialPlantilla(String codigo, String campo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModPlantilla plantilla = null;

        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                plantilla = new ModPlantilla();
                String sql = "SELECT PRO_CODPROD,PRO_DESC,PRO_UMPRINCIPAL,";
                sql += " isnull(Costo,0) as precioAjustado";
                sql += " FROM STO_PRODUCTO";
                sql += " left outer join ENSchaffner.dbo.SCHCup";
                sql += " on(Codpro = PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " WHERE " + campo + " = '" + codigo + "'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    plantilla.setCodigo(rs.getString("PRO_CODPROD"));
                    plantilla.setDescripcion(rs.getString("PRO_DESC"));
                    plantilla.setUm(rs.getString("PRO_UMPRINCIPAL"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return plantilla;
    }

    public static ModPresupuesto cambiarMaterialPresupuesto(String codigo, String campo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModPresupuesto pre = null;

        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                pre = new ModPresupuesto();
                String sql = "SELECT PRO_CODPROD,PRO_DESC,PRO_UMPRINCIPAL,";
                sql += " isnull(Costo,0) as precioAjustado";
                sql += " FROM STO_PRODUCTO";
                sql += " left outer join ENSchaffner.dbo.SCHCup";
                sql += " on(Codpro = PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " WHERE " + campo + " = '" + codigo + "'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    pre.setCodigo(rs.getString("PRO_CODPROD"));
                    pre.setDescripcion(rs.getString("PRO_DESC"));
                    pre.setUm(rs.getString("PRO_UMPRINCIPAL"));
                    pre.setPrecioUnitario(rs.getDouble("precioAjustado"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pre;
    }

    public static ModCliente cargarCLiente(String codigo, String campo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModCliente cliente = null;

        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                cliente = new ModCliente();
                String sql = "SELECT PER_CODIGO,PER_NOMBRE ";
                sql += " FROM SYS_PERSONA";
                sql += " WHERE " + campo + " = '" + codigo + "'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    cliente.setRut(rs.getLong("PER_CODIGO"));
                    cliente.setNombre(rs.getString("PER_NOMBRE"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return cliente;
    }

    public static long ultimoIdNav(ConexionSQL cn) throws SQLException {
        ResultSet rs;
        long numero = 0l;
        String sql = "SELECT MAX(nb_id) FROM TBL_COM_NAV_ENC_DET";
        rs = cn.ExecuteQuery(sql);
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();
        return numero + 1;
    }

    public static ArrayList listadoPresupuestosObra(String condicion, int productoOServicio) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList presupuestos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = "select nb_numero_presupuesto,PER_CODIGO,PER_NOMBRE,dt_fecha,vc_usuario,sum(nb_precio_unitario) as precioUnitario,vc_plantilla,TAB_Desc,nb_id_trabajo,encdet.nb_id";
                sql += " from TBL_COM_PRESUPUESTO_ENC enc";
                sql += " inner join TBL_COM_NAV_ENC_DET  encdet";
                sql += " on(encdet.nb_id = enc.nb_id_nub_enc_det)";
                sql += " inner join TBL_COM_PRESUPUESTO_DET det";
                sql += " on(enc.nb_id_nub_enc_det = det.nb_id_nub_enc_det)";
                sql += " inner join SiaHidropack.dbo.SYS_PERSONA";
                sql += " on(PER_CODIGO = nb_rut_cliente)";
                sql += " inner join SiaHidropack.dbo.SYS_TABLAS";
                sql += " on(TAB_Codigo = nb_id_trabajo and TAB_CodTabla = 615)";
                sql += " where  " + condicion + " and nb_id_producto_o_servicio = " + productoOServicio;
                sql += " group by nb_numero_presupuesto,PER_CODIGO,PER_NOMBRE,dt_fecha,vc_usuario,vc_plantilla,TAB_Desc,nb_id_trabajo,encdet.nb_id";
                sql += " order by nb_numero_presupuesto,nb_id_trabajo";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModPresupuesto pres = new ModPresupuesto();
                    pres.setNumero(rs.getInt("nb_numero_presupuesto"));
                    pres.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                    pres.getCliente().setRut(rs.getLong("PER_CODIGO"));
                    pres.setFecha(fecha.format(rs.getDate("dt_fecha")));
                    pres.setUsuario(rs.getString("vc_usuario"));
                    pres.setTotal(rs.getDouble("precioUnitario"));
                    pres.setPlantilla(rs.getString("vc_plantilla"));
                    pres.getTrabajo().setCodigo(rs.getInt("nb_id_trabajo"));
                    pres.getTrabajo().setDescripcion(rs.getString("TAB_Desc"));
                    pres.setNavEncDet(rs.getInt("nb_id"));
                    presupuestos.add(pres);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return presupuestos;
    }

    public static ArrayList listadoPresupuestosManCorrectiva(String condicion, int productoOServicio) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList presupuestos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select nb_numero_presupuesto,PER_CODIGO,PER_NOMBRE,dt_fecha,vc_usuario,sum(nb_precio_unitario) as precioUnitario,encdet.nb_id";
                sql += " ,sum(nb_instalacion) as instalacion,nb_uf";
                sql += " from TBL_COM_PRESUPUESTO_ENC enc";
                sql += " inner join TBL_COM_NAV_ENC_DET  encdet";
                sql += " on(encdet.nb_id = enc.nb_id_nub_enc_det)";
                sql += " inner join TBL_COM_PRESUPUESTO_DET det";
                sql += " on(enc.nb_id_nub_enc_det = det.nb_id_nub_enc_det)";
                sql += " inner join SiaHidropack.dbo.SYS_PERSONA";
                sql += " on(PER_CODIGO = nb_rut_cliente)";
                sql += " where  " + condicion + " and nb_id_producto_o_servicio = " + productoOServicio;
                sql += " group by nb_numero_presupuesto,PER_CODIGO,PER_NOMBRE,dt_fecha,vc_usuario,encdet.nb_id,nb_uf";
                sql += " order by nb_numero_presupuesto";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModPresupuesto pres = new ModPresupuesto();
                    pres.setNumero(rs.getInt("nb_numero_presupuesto"));
                    pres.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                    pres.getCliente().setRut(rs.getLong("PER_CODIGO"));
                    pres.setFecha(fecha.format(rs.getDate("dt_fecha")));
                    pres.setUsuario(rs.getString("vc_usuario"));
                    pres.setPrecioTotal(rs.getDouble("precioUnitario"));
                    pres.setNavEncDet(rs.getInt("nb_id"));
                    pres.setInstalacion(rs.getDouble("instalacion"));
                    pres.setUf(rs.getInt("nb_uf"));
                    presupuestos.add(pres);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return presupuestos;
    }

    public static long ultimaObraInsertada(ConexionSQL cn) throws Exception {
        ResultSet rs;
        long numero = 0l;
        String sql = "SELECT MAX(TAB_Codigo) FROM SYS_TABLAS WHERE  (TAB_CodTabla = 500) AND (TAB_Tipo = 'U')";
        rs = cn.ExecuteQuery(sql);
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();
        return numero + 1;
    }

    public static long idUltimaFilaSysTabla(ConexionSQL cn) throws Exception {
        ResultSet rs;
        long numero = 0l;
        String sql = "SELECT MAX(Id_Fila) FROM SYS_TABLAS ";
        rs = cn.ExecuteQuery(sql);
        while (rs.next()) {
            numero = rs.getLong(1);
        }
        rs.close();
        return numero + 1;
    }

    public static ModCliente rutCLiente(String campo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModCliente cliente = new ModCliente();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT PER_CODIGO,PER_NOMBRE";
                sql += " FROM SYS_PERSONA";
                sql += " WHERE ltrim(PER_CODIGO) = '" + campo + "' or ltrim(PER_NOMBRE) = '" + campo + "'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    cliente.setRut(rs.getLong("PER_CODIGO"));
                    cliente.setNombre(rs.getString("PER_NOMBRE"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return cliente;
    }

    public static boolean isExistePresupuestoObra(int numPresupuesto, int proOser) {
        ResultSet rs;
        ConexionSQL cn = new ConexionSQL();
        boolean est = false;
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = "select count(*) ";
                sql += " from TBL_COM_NAV_ENC_DET encDet";
                sql += " inner join TBL_COM_PRESUPUESTO_ENC enc";
                sql += " on(encDet.nb_id = enc.nb_id_nub_enc_det)";
                sql += " where nb_numero_presupuesto = " + numPresupuesto + " and enc.nb_id_producto_o_servicio =  " + proOser;
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    if (rs.getInt(1) > 0) {
                        est = true;
                    }
                }
                rs.close();
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return est;
    }

    public static ArrayList listadoObra(String condicion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList obras = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = " SELECT tab_codigo as idObra,tab_desc as nombreObra,tab_texto1 as costoServicios,tab_valor1 as ventaEstimada,tab_texto3 rutCliente,convert(datetime,tab_fecha1) as fechaCliente,tab_texto4 as numPresupuesto, PER_NOMBRE, convert(datetime,FecCreacion) as fechaIngreso ";
                sql += " FROM SYS_TABLAS ";
                sql += " left join sys_persona";
                sql += " on(PER_CODIGO = tab_texto3)";
                sql += " where TAB_CodTabla = 500 and TAB_Tipo = 'U' and  " + condicion + " and TAB_Estado = '1'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModObra obra = new ModObra();
                    obra.setIdObra(rs.getInt("idObra"));
                    obra.setNombreObra(rs.getString("nombreObra"));
                    obra.setCostoServicio(rs.getDouble("costoServicios"));
                    obra.setVentaEstimada(rs.getDouble("ventaEstimada"));
                    obra.getCliente().setRut(rs.getLong("rutCliente"));
                    obra.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                    obra.getCliente().setFecha(fecha.format(rs.getDate("fechaCliente")));
                    obra.setFecha(fecha.format(rs.getDate("fechaIngreso")));
                    obra.setNumPresupuesto(rs.getInt("numPresupuesto"));
                    obras.add(obra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return obras;
    }

    public static int ultimoPresupuesto(ConexionSQL cn) throws SQLException {
        ResultSet rs;
        int numero = 0;
        String sql = "select max(nb_numero_presupuesto) from TBL_COM_NAV_ENC_DET ";
        rs = cn.ExecuteQuery(sql);
        while (rs.next()) {
            numero = rs.getInt(1);
        }
        rs.close();
        return numero + 1;
    }

    public static ArrayList listadoPresupuestosManCorrectiva(int num) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList presupuestos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_HIDROPACK)) {
                String sql = " select vc_codigo,vc_descripcion, vc_um,nb_precio_unitario,nb_instalacion,nb_cantidad,nb_uf";
                sql += " from TBL_COM_PRESUPUESTO_ENC enc";
                sql += " inner join TBL_COM_NAV_ENC_DET  encdet";
                sql += " on(encdet.nb_id = enc.nb_id_nub_enc_det)";
                sql += " inner join TBL_COM_PRESUPUESTO_DET det";
                sql += " on(enc.nb_id_nub_enc_det = det.nb_id_nub_enc_det)";
                sql += " inner join SiaHidropack.dbo.SYS_PERSONA";
                sql += " on(PER_CODIGO = nb_rut_cliente)";
                sql += " where  nb_numero_presupuesto = " + num + " and nb_id_producto_o_servicio = 2";
                sql += " order by nb_numero_presupuesto";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModPresupuesto pres = new ModPresupuesto();
                    pres.setNumero(num);
                    pres.setCodigo(rs.getString("vc_codigo"));
                    pres.setDescripcion(rs.getString("vc_descripcion"));
                    pres.setUm(rs.getString("vc_um"));
                    pres.setPrecioUnitario(rs.getDouble("nb_precio_unitario"));
                    pres.setInstalacion(rs.getDouble("nb_instalacion"));
                    pres.setCantidad(rs.getInt("nb_cantidad"));
                    pres.setPrecioTotal(pres.getCantidad() * pres.getPrecioUnitario());
                    pres.setUf(rs.getInt("nb_uf"));
                    presupuestos.add(pres);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return presupuestos;
    }

    public static ArrayList listadoObraManCorr(String condicion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList obras = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = " select tab_codigo as idObra,tab_desc as nombreObra,tab_valor1 as venta,tab_texto3 as rutCliente,convert(datetime,tab_fecha1) as fechaCliente,tab_texto4 as numPresupuesto, PER_NOMBRE, convert(datetime,FecCreacion) as fechaIngreso,tab_texto5 as descripcion ";
                sql += " FROM SYS_TABLAS ";
                sql += " inner join sys_persona";
                sql += " on(PER_CODIGO = tab_texto3)";
                sql += " inner join HIDROPACk.dbo.TBL_COM_NAV_ENC_DET nav";
                sql += " on(nb_numero_presupuesto = convert(int,tab_texto4))";
                sql += " inner join HIDROPACk.dbo.TBL_COM_PRESUPUESTO_ENC enc";
                sql += " on(nav.nb_id = enc.nb_id_nub_enc_det)";
                sql += " where TAB_CodTabla = 500 and TAB_Tipo = 'U' and " + condicion + " and nb_id_producto_o_servicio = 2";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModObra obra = new ModObra();
                    obra.setIdObra(rs.getInt("idObra"));
                    obra.setNombreObra(rs.getString("nombreObra"));
                    obra.setVentaEstimada(rs.getDouble("venta"));
                    obra.getCliente().setRut(rs.getLong("rutCliente"));
                    obra.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                    obra.getCliente().setFecha(fecha.format(rs.getDate("fechaCliente")));
                    obra.setFecha(fecha.format(rs.getDate("fechaIngreso")));
                    obra.setNumPresupuesto(rs.getInt("numPresupuesto"));
                    obra.setDescripcion(rs.getString("descripcion"));
                    obras.add(obra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return obras;
    }

    public static ArrayList frecuencia() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList productoOServicios = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT  TAB_Codigo, TAB_Desc FROM SYS_TABLAS WHERE (TAB_CodTabla = 616)";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModProServ tra = new ModProServ();
                    tra.setCodigo(rs.getInt("TAB_Codigo"));
                    tra.setDescripcion(rs.getString("TAB_Desc"));
                    productoOServicios.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return productoOServicios;
    }

    public static ArrayList listadoObraManPrev(String condicion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList obras = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = " select tab_codigo as idManPre,tab_desc as nombreManPre,tab_valor1 as UF,tab_texto3 as rutCliente,tab_texto4 as valorUF, PER_NOMBRE, convert(datetime,FecCreacion) as fechaIngreso,tab_texto5 as observacion, tab_texto1 as frecuencia ";
                sql += " FROM SYS_TABLAS ";
                sql += " inner join sys_persona";
                sql += " on(PER_CODIGO = tab_texto3)";
                sql += " where TAB_CodTabla = 500 and TAB_Tipo = 'U' and TAB_Estado = '3' AND " + condicion + " ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModMantencion man = new ModMantencion();
                    man.setNumero(rs.getLong("idManPre"));
                    man.setNombre(rs.getString("nombreManPre"));
                    man.setValorUf(rs.getInt("valorUF"));
                    man.getCliente().setRut(rs.getLong("rutCliente"));
                    man.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                    man.setUf(rs.getFloat("UF"));
                    man.setFecha(fecha.format(rs.getDate("fechaIngreso")));
                    man.setObservacion(rs.getString("observacion"));
                    man.setFrecuencia(rs.getInt("frecuencia"));
                    obras.add(man);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return obras;
    }

    public static ModVentaDirecta agregarMaterialVentaDirecta(String codigo, String campo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModVentaDirecta pre = null;

        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                pre = new ModVentaDirecta();
                String sql = "SELECT PRO_CODPROD,PRO_DESC,PRO_UMPRINCIPAL,";
                sql += " isnull(Costo,0) as precioAjustado";
                sql += " FROM STO_PRODUCTO";
                sql += " left outer join ENSchaffner.dbo.SCHCup";
                sql += " on(Codpro = PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " WHERE " + campo + " = '" + codigo + "'";
                rs = cn.ExecuteQuery(sql);
                if (rs.next()) {
                    pre.setCodigo(rs.getString("PRO_CODPROD"));
                    pre.setDescripcion(rs.getString("PRO_DESC"));
                    pre.setUm(rs.getString("PRO_UMPRINCIPAL"));
                    pre.setPrecio(rs.getDouble("precioAjustado"));
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return pre;
    }

    public static ArrayList informesResumido(String condicion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList informes = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = " SELECT  sys.TAB_Codigo,sys.TAB_Desc,sys.TAB_Texto4, detEnc.nb_id_trabajo,sum(det.nb_precio_unitario * det.nb_cantidad) as precio,PER_NOMBRE,PER_CODIGO,";
                sql += " (SELECT  TAB_Desc FROM SYS_TABLAS WHERE TAB_CodTabla = 615 and TAB_Codigo = detEnc.nb_id_trabajo) as Trabajo,convert(datetime,sys.FecCreacion) as fechaCreacion,";
                sql += " isnull(cg.PrecioTotal,0) as PrecioTotalGuiaDespacho,isnull(ds.DevolucionTotal,0) as PrecioTotalDevolucion";
                sql += " FROM   SYS_TABLAS sys";
                sql += " inner join HIDROPACK.dbo.TBL_COM_NAV_ENC_DET detEnc";
                sql += " on(nb_numero_presupuesto = convert(int,TAB_Texto4))";
                sql += " inner join HIDROPACK.dbo.TBL_COM_PRESUPUESTO_ENC enc";
                sql += " on(enc.nb_id_nub_enc_det = detEnc.nb_id)";
                sql += " inner join HIDROPACK.dbo.TBL_COM_PRESUPUESTO_DET det";
                sql += " on(det.nb_id_nub_enc_det = detEnc.nb_id)";
                sql += " inner join sys_persona";
                sql += " on(PER_CODIGO = convert(int,tab_texto3))";
                sql += " left outer join WV_costo_guias_sumadas cg";
                sql += " on(cg.numObra = convert(int,sys.TAB_Codigo)) and detEnc.nb_id_trabajo = cg.idTrabajo";
                sql += " left outer join WV_costo_devolucion_sumada ds";
                sql += " on(ds.numObra = convert(int,sys.TAB_Codigo))";
                sql += " WHERE   sys.TAB_CodTabla = 500 AND sys.TAB_Tipo = 'U' AND sys.TAB_Estado = '1' AND " + condicion;
                sql += " group by sys.TAB_Codigo,sys.TAB_Desc,sys.TAB_Texto4,detEnc.nb_id_trabajo,sys.FecCreacion,PER_NOMBRE,PER_CODIGO,cg.PrecioTotal,ds.DevolucionTotal";


                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModInforme inf = new ModInforme();
                    inf.setNumero(rs.getInt("TAB_Codigo"));
                    inf.setNombre(rs.getString("TAB_Desc"));
                    inf.setPresupuesto(rs.getInt("TAB_Texto4"));
                    inf.setIdTrabajo(rs.getInt("nb_id_trabajo"));
                    inf.setCostoPresupuesto(rs.getDouble("precio"));
                    inf.setNombreTrabajo(rs.getString("Trabajo"));
                    inf.setFecha(fecha.format(rs.getDate("fechaCreacion")));
                    inf.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                    inf.getCliente().setRut(rs.getLong("PER_CODIGO"));
                    inf.setCostoGuia(rs.getDouble("PrecioTotalGuiaDespacho"));
                    inf.setCostoDevolucion(rs.getDouble("PrecioTotalDevolucion"));
                    informes.add(inf);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return informes;
    }

    public static ArrayList informePresupuesto(int num, int idTrabajo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList trabajos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT  vc_codigo,PRO_DESC,PRO_UMPRINCIPAL,sum(nb_cantidad) as Cantidad";
                sql += " FROM SYS_TABLAS sys";
                sql += " inner join HIDROPACK.dbo.TBL_COM_NAV_ENC_DET detEnc";
                sql += " on(nb_numero_presupuesto = convert(int,TAB_Texto4))";
                sql += " inner join HIDROPACK.dbo.TBL_COM_PRESUPUESTO_ENC enc";
                sql += " on(enc.nb_id_nub_enc_det = detEnc.nb_id)";
                sql += " inner join HIDROPACK.dbo.TBL_COM_PRESUPUESTO_DET det";
                sql += " on(det.nb_id_nub_enc_det = detEnc.nb_id)";
                sql += " inner join STO_PRODUCTO pro";
                sql += " on(pro.PRO_CODPROD = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " WHERE   sys.TAB_CodTabla = 500 AND sys.TAB_Tipo = 'U' AND sys.TAB_Estado = '1' AND sys.TAB_Codigo = '" + num + "' and detEnc.nb_id_trabajo = " + idTrabajo;
                sql += " group by vc_codigo,PRO_DESC,PRO_UMPRINCIPAL";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModInforme tra = new ModInforme();
                    tra.getMaterial().setCodigo(rs.getString("vc_codigo"));
                    tra.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    tra.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    tra.setCantidad(rs.getDouble("Cantidad"));
                    trabajos.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return trabajos;
    }

    public static ArrayList informeGuia(int num, int idTrabajo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList trabajos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "select des.MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL,sum(des.MVD_Cant) as Cantidad";
                sql += " from HIDROPACK.dbo.VW_previo_traspaso pt";
                sql += " inner join WV_guia_despacho des";
                sql += " on(des.MVD_NumeroDocOrigen = pt.MVD_NumeroDoc and des.MVD_LineaOrigen = pt.MVD_Linea)";
                sql += " left join STO_PRODUCTO";
                sql += " on(des.MVD_CodProd COLLATE SQL_Latin1_General_CP850_CS_AS= PRO_CODPROD)";
                sql += " where pt.mve_analisisadic1 = " + num + " and pt.mve_analisisadic14 = '" + idTrabajo + "'";
                sql += " group by des.MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModInforme tra = new ModInforme();
                    tra.getMaterial().setCodigo(rs.getString("MVD_CodProd"));
                    tra.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    tra.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    tra.setCantidad(rs.getDouble("Cantidad"));
                    trabajos.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return trabajos;
    }

    public static ArrayList informeDevolucion(int num) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList trabajos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT  MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL,sum(MVD_Cant) as Cantidad";
                sql += " FROM SYS_TABLAS sys";
                sql += " left outer join WV_costo_devolucion cg";
                sql += " on(cg.numObra = convert(int,sys.TAB_Codigo))";
                sql += " inner join STO_PRODUCTO pro";
                sql += " on(pro.PRO_CODPROD = MVD_CodProd )";
                sql += " WHERE   sys.TAB_CodTabla = 500 AND sys.TAB_Tipo = 'U' AND sys.TAB_Estado = '1' AND sys.TAB_Codigo = '" + num + "'";
                sql += " group by MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModInforme tra = new ModInforme();
                    tra.getMaterial().setCodigo(rs.getString("MVD_CodProd"));
                    tra.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                    tra.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                    tra.setCantidad(rs.getDouble("Cantidad"));
                    trabajos.add(tra);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return trabajos;
    }

    public static ArrayList ventasDirectasResumido(String condicion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList trabajos = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "SELECT tab_codigo as idObra,tab_desc as nombreObra,tab_texto3 rutCliente, PER_NOMBRE, convert(datetime,FecCreacion) as fechaIngreso ";
                sql += " FROM SYS_TABLAS ";
                sql += " left join sys_persona";
                sql += " on(PER_CODIGO = tab_texto3)";
                sql += " where TAB_CodTabla = 500 and TAB_Tipo = 'U' and " + condicion + " and TAB_Estado in('4','5','6')";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModObra ven = new ModObra();
                    ven.setIdObra(rs.getInt("idObra"));
                    ven.setNombreObra(rs.getString("nombreObra"));
                    ven.getCliente().setRut(rs.getLong("rutCliente"));
                    ven.getCliente().setNombre(rs.getString("PER_NOMBRE"));
                    ven.setFecha(fecha.format(rs.getDate("fechaIngreso")));
                    trabajos.add(ven);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return trabajos;
    }

    public static ArrayList ventasDirectasDetalle(int obra) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList ventas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "select vc_codigo,nb_precio,nb_cantidad,PRO_DESC,PRO_UMPRINCIPAL";
                sql += " from HIDROPACK.dbo.TBL_COM_VENTA_DIRECTA ";
                sql += " inner join STO_PRODUCTO";
                sql += " on(PRO_CODPROD = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " where nb_numero_obra = " + obra;
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModVentaDirecta ven = new ModVentaDirecta();
                    ven.setCodigo(rs.getString("vc_codigo"));
                    ven.setPrecio(rs.getDouble("nb_precio"));
                    ven.setCantidad(rs.getDouble("nb_cantidad"));
                    ven.setDescripcion(rs.getString("PRO_DESC"));
                    ven.setUm(rs.getString("PRO_UMPRINCIPAL"));
                    ventas.add(ven);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return ventas;
    }

    //////////borrar///////////
    public static ArrayList compradores() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList ventas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                String sql = "select distinct(Comprador) as Comprador from MAC.dbo.ProductoAnexo where Comprador <> 'null'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModCliente ven = new ModCliente();
                    ven.setNombre(rs.getString("Comprador"));
                    ventas.add(ven);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return ventas;
    }

    public static ArrayList codigos(String comprador) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList ventas = new ArrayList();
        try {
            if (cn.openSQL(ConexionSQL.MS_SIAHIDROPACK)) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String[] fec = format.format(new java.util.Date()).split("-");
                Calendar c1 = Calendar.getInstance();
                c1.set(Integer.parseInt(fec[0]), Integer.parseInt(fec[1]) - 1, 1);
                int an = c1.get(c1.YEAR);
                String me = String.valueOf(c1.get(c1.MONTH) + 1);
                if (me.length() == 1) {
                    me = "0" + me;
                }
                String sql = "SELECT tc.PRO_CODPROD,isnull(es.CodigoMatPrima,0) as CodigoMatPrima,tc.PRO_INVMIN, ";
                sql += " tc.PRO_INVMAX,  ";
                sql += "  isnull(ml.Saldo,0) + isnull(ml9.Saldo,0) as Stock119, ";
                sql += "  isnull((SELECT sum(me.CantidadMP) ";
                sql += "  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
                sql += "  WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '" + an + me + "01'  and  a.readTime <= '" + an + me + c1.getActualMaximum(Calendar.DAY_OF_MONTH) + "' ";
                sql += "  AND a.proceso = CodigoSemiElaborado ";
                sql += "  AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) ";
                sql += "  + ";
                sql += "  isnull((SELECT SUM(me.CantidadMP) ";
                sql += "  FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma ";
                sql += "  WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
                sql += "  AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' ";
                sql += "  AND ma.readTime >= '" + an + me + "01'  and  ma.readTime <= '" + an + me + c1.getActualMaximum(Calendar.DAY_OF_MONTH) + "' ";
                sql += "  AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidad1mes, ";
                c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
                an = c1.get(c1.YEAR);
                me = String.valueOf(c1.get(c1.MONTH) + 1);
                if (me.length() == 1) {
                    me = "0" + me;
                }
                sql += "  isnull((SELECT sum(me.CantidadMP) ";
                sql += "  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
                sql += "  WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '" + an + me + "01'  and  a.readTime <= '" + an + me + c1.getActualMaximum(Calendar.DAY_OF_MONTH) + "' ";
                sql += "  AND a.proceso = CodigoSemiElaborado ";
                sql += "  AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) ";
                sql += "  + ";
                sql += "  isnull((SELECT SUM(me.CantidadMP) ";
                sql += "  FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma ";
                sql += "  WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
                sql += "  AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' ";
                sql += "  AND ma.readTime >= '" + an + me + "01'  and  ma.readTime <= '" + an + me + c1.getActualMaximum(Calendar.DAY_OF_MONTH) + "' ";
                sql += "  AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidad2mes,           ";
                c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
                an = c1.get(c1.YEAR);
                me = String.valueOf(c1.get(c1.MONTH) + 1);
                if (me.length() == 1) {
                    me = "0" + me;
                }
                sql += "  isnull((SELECT sum(me.CantidadMP) ";
                sql += "  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
                sql += "  WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '" + an + me + "01'  and  a.readTime <= '" + an + me + c1.getActualMaximum(Calendar.DAY_OF_MONTH) + "' ";
                sql += "  AND a.proceso = CodigoSemiElaborado ";
                sql += "  AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 )  ";
                sql += "  + ";
                sql += "  isnull((SELECT SUM(me.CantidadMP) ";
                sql += "  FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma ";
                sql += "  WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
                sql += "  AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' ";
                sql += "  AND ma.readTime >= '" + an + me + "01'  and  ma.readTime <= '" + an + me + c1.getActualMaximum(Calendar.DAY_OF_MONTH) + "' ";
                sql += "  AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidad3mes, ";
                sql += "  pa.LeadTime,isnull(oa.MVD_CodProd,'') oc_atrasada,isnull(tcc.porLlegar,0) as ocPendiente";
                sql += " FROM SiaSchaffner.dbo.STO_PRODUCTO tc                       ";
                sql += " left outer join SiaSchaffner.dbo.VW_stock_materiales ml ";
                sql += " on(ml.PSL_CodProd = tc.PRO_CODPROD ) ";
                sql += " left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 ";
                sql += " on(ml9.PSL_CodProd = tc.PRO_CODPROD) ";
                sql += " inner join MAC.dbo.ProductoAnexo pa ";
                sql += " on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) ";
                sql += " left outer join MAC.dbo.VW_oc_atrasadas  oa ";
                sql += " on(tc.PRO_CODPROD = oa.MVD_CodProd) ";
                sql += " left outer join MAC.dbo.VW_estructura es";
                sql += " on(es.CodigoMatPrima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)";
                sql += " left outer join SiaSchaffner.dbo.VW_oc_atrasadas_sumadas tcc ";
                sql += " on(tcc.MVD_CodProd = tc.PRO_CODPROD) ";
                sql += " WHERE pa.Estado <> 1 and tc.PRO_VIGENCIA = 'S'  and pa.Comprador = '" + comprador + "' ";
                sql += " ORDER BY tc.PRO_CODPROD ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModPrueba ven = new ModPrueba();
                    ven.setCodigo(rs.getString("PRO_CODPROD"));
                    ven.setCodigo1(rs.getString("CodigoMatPrima"));
                    ven.setInvMin(rs.getDouble("PRO_INVMIN"));
                    ven.setInvMax(rs.getDouble("PRO_INVMAX"));
                    ven.setStock(rs.getDouble("Stock119"));
                    ven.setNece1(rs.getDouble("necesidad1mes"));
                    ven.setNece2(rs.getDouble("necesidad2mes"));
                    ven.setNece3(rs.getDouble("necesidad3mes"));
                    ven.setLeadTime(rs.getInt("LeadTime"));
                    ven.setOcAtrasada(rs.getString("oc_atrasada"));
                    ven.setOcPendiente(rs.getDouble("ocPendiente"));
                    ventas.add(ven);
                }
                rs.close();
                rs = null;
                cn.closeSQL();
            }
        } catch (Exception ex) {
            cn.closeSQL();
            PrmApp.addError(ex, true);
        }
        return ventas;
    }
}
