<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"            scope="page"    class="view.VwLogistica" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Imprimir</title>
        <link rel="stylesheet" href="../css/body.css" type="text/css"/>
        <link rel="stylesheet" href="../css/table.css" type="text/css"/>
    </head>
    <body  onLoad="window.print(); window.close;">
        <table cellpadding='0' cellspacing='0'>
            <%=vw.grillaPedidosProcesarBodegaImprimir(request,Integer.parseInt(request.getParameter("y")))%>
        </table>
    </body>
</html>