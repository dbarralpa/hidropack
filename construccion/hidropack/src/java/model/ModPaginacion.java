package model;

public class ModPaginacion implements Cloneable {

    public int RPP = 10;
    public int TOPE_SET = 15;
    public int pag;        
    public String datos[][];

    public ModPaginacion(){
        pag = 1;        
        datos = new String[1][1];
    }

    public ModPaginacion(int x, int y){
        pag = 1;
        datos = new String[x][y];
    }

    public int getTotal() {
        return datos[0].length;
    }

    public int getIndice(int index) {
        return Integer.parseInt(datos[2][index]);
    }

    public String codigoDesde_Pag(int pg, int rpp) {
        int index = 0;
        index = (pg - 1) * rpp;
        if (index >= datos[0].length) {
            index = datos[0].length - 1;
        }
        return datos[1][index];
    }

    public String codigoHasta_Pag(int pg, int rpp) {
        int index = 0;
        index = (pg * rpp) - 1;
        if (index >= datos[0].length) {
            index = datos[0].length - 1;
        }
        return datos[1][index];
    }

    public void quicksort() {
        quicksort(0, datos[0].length - 1);
    }

    public void quicksort(int inicial, int finaliza) {
        int inferior = inicial;
        int superior = finaliza;
        int posicion = inicial;
        String codigoINF = null;
        String descripcionINF = null;
        String indiceINF = null;
        String codigoSUP = null;
        String descripcionSUP = null;
        String indiceSUP = null;
        String codigoPOS = null;
        String descripcionPOS = null;
        String indicePOS = null;

        boolean continuar = false;
        if (finaliza > 0) {
            codigoINF = (String) datos[0][(inferior)];
            descripcionINF = (String) datos[1][(inferior)];
            indiceINF = (String) datos[2][(inferior)];
            codigoSUP = (String) datos[0][(superior)];
            descripcionSUP = (String) datos[1][(superior)];
            indiceSUP = (String) datos[2][(superior)];
            codigoPOS = (String) datos[0][(posicion)];
            descripcionPOS = (String) datos[1][(posicion)];
            indicePOS = (String) datos[2][(posicion)];
            continuar = true;
        }
        while (continuar) {
            continuar = false;
            while ((codigoPOS.compareTo(codigoSUP) <= 0) && (posicion != superior)) {
                superior = superior - 1;
                codigoSUP = datos[0][(superior)];
                descripcionSUP = datos[1][(superior)];
                indiceSUP = datos[2][(superior)];
            }
            if (posicion != superior) {
                String codigoTEM = codigoPOS;
                String descripcionTEM = descripcionPOS;
                String indiceTEM = indicePOS;
                datos[0][posicion] = codigoSUP;
                datos[1][posicion] = descripcionSUP;
                datos[2][posicion] = indiceSUP;
                datos[0][superior] = codigoTEM;
                datos[1][superior] = descripcionTEM;
                datos[2][superior] = indiceTEM;
                posicion = superior;
                codigoSUP = datos[0][(superior)];
                descripcionSUP = datos[1][(superior)];
                indiceSUP = datos[2][(superior)];
                codigoPOS = datos[0][(posicion)];
                descripcionPOS = datos[1][(posicion)];
                indicePOS = datos[2][(posicion)];
                while ((codigoPOS.compareTo(codigoINF) >= 0) && (posicion != inferior)) {
                    inferior = inferior + 1;
                    codigoINF = datos[0][(inferior)];
                    descripcionINF = datos[1][(inferior)];
                    indiceINF = datos[2][(inferior)];
                }
                if (posicion != inferior) {
                    continuar = true;
                    codigoTEM = codigoPOS;
                    descripcionTEM = descripcionPOS;
                    indiceTEM = indicePOS;
                    datos[0][posicion] = codigoINF;
                    datos[1][posicion] = descripcionINF;
                    datos[2][posicion] = indiceINF;
                    datos[0][inferior] = codigoTEM;
                    datos[1][inferior] = descripcionTEM;
                    datos[2][inferior] = indiceTEM;
                    posicion = inferior;
                    codigoINF = datos[0][(inferior)];
                    descripcionINF = datos[1][(inferior)];
                    indiceINF = datos[2][(inferior)];
                    codigoPOS = datos[0][(posicion)];
                    descripcionPOS = datos[1][(posicion)];
                    indicePOS = datos[2][(posicion)];
                }
            }
        }
        codigoINF = null;
        codigoSUP = null;
        codigoPOS = null;
        descripcionINF = null;
        descripcionSUP = null;
        descripcionPOS = null;
        indiceINF = null;
        indiceSUP = null;
        indicePOS = null;
        if ((posicion - 1) > inicial) {
            quicksort(inicial, posicion - 1);
        }
        if (finaliza > (posicion + 1)) {
            quicksort(posicion + 1, finaliza);
        }
    }

    public ModPaginacion copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModPaginacion) obj;
    }
}
