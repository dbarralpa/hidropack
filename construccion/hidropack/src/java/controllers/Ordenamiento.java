/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.Comparator;
import model.ModEquipo;

/**
 *
 * @author dbarra
 */
public class Ordenamiento implements Comparator {

    public Ordenamiento() {
    }

    public int compare(Object o1, Object o2) {

        ModEquipo equipo1 = (ModEquipo) o1;
        ModEquipo equipo2 = (ModEquipo) o2;

        return equipo1.getExpeditacion().compareTo(equipo2.getExpeditacion());
    }
}
