package model.comercial;

public class ModPrueba {

    private String codigo;
    private String codigo1;
    private String ocAtrasada;
    private double invMin;
    private double invMax;
    private double stock;
    private double nece1;
    private double nece2;
    private double nece3;
    private double ocPendiente;
    private int leadTime;

    public ModPrueba() {
        codigo = "";
        codigo1 = "";
        ocAtrasada = "";
    }

    public double getOcPendiente() {
        return ocPendiente;
    }

    public void setOcPendiente(double ocPendiente) {
        this.ocPendiente = ocPendiente;
    }

    public int getLeadTime() {
        return leadTime;
    }

    public void setLeadTime(int leadTime) {
        this.leadTime = leadTime;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo1() {
        return codigo1;
    }

    public void setCodigo1(String codigo1) {
        this.codigo1 = codigo1;
    }

    public String getOcAtrasada() {
        return ocAtrasada;
    }

    public void setOcAtrasada(String ocAtrasada) {
        this.ocAtrasada = ocAtrasada;
    }

    public double getInvMin() {
        return invMin;
    }

    public void setInvMin(double invMin) {
        this.invMin = invMin;
    }

    public double getInvMax() {
        return invMax;
    }

    public void setInvMax(double invMax) {
        this.invMax = invMax;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public double getNece1() {
        return nece1;
    }

    public void setNece1(double nece1) {
        this.nece1 = nece1;
    }

    public double getNece2() {
        return nece2;
    }

    public void setNece2(double nece2) {
        this.nece2 = nece2;
    }

    public double getNece3() {
        return nece3;
    }

    public void setNece3(double nece3) {
        this.nece3 = nece3;
    }
}
