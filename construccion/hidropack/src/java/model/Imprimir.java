/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author dbarra
 */
public class Imprimir {

    private String codigo;
    private String descripcion;
    private String um;
    private String ubicacion;
    private String cantidad;
    private String stock;
    private String item;
    

    public Imprimir(String codigo, String descripcion,String um,String stock, String ubicacion, String cantidad, String item) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.um = um;
        this.ubicacion = ubicacion;
        this.cantidad = cantidad;
        this.stock = stock;
        this.item = item;
    }
    
    public Imprimir(String codigo, String descripcion,String um, String ubicacion, String cantidad, String item) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.um = um;
        this.ubicacion = ubicacion;
        this.cantidad = cantidad;
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
