package controllers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Properties;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import model.ModMaterial;
import model.ModMenu;
import model.ModPermisoSeccion;
import model.ModSesion;
import sun.misc.BASE64Encoder;

public class PrmApp {

//    public String FORMATO_YMD = "yyyy-MM-dd";
    public static String FORMATO_YDM = "yyyy-dd-MM";
    public static String FORMATO_DMY = "dd/MM/yyyy";
    public static String FORMATO_HMS = "HH:mm:ss";
    public static String FILE_ERROR = "E:/logErrorHidropack.txt";

    public static String verOverTip(String Contenido) {
        return verOverLib("Info. Tip", Contenido, "#E1E1FF", "#000066");
    }

    public static String verOverLib(String titulo, String Contenido) {
        return verOverLib(titulo, Contenido, "#FFFFCC", "#FF9900");
    }

    public static String verOverLib(String Titulo, String Contenido, String fg, String bg) {
        String tmp = "'";
        Contenido = Contenido.replace(tmp.charAt(0), '�');
        Contenido = Contenido.replace('"', '�');
        Contenido = Contenido.replaceAll("�", "<br>");
        Contenido = Contenido.replaceAll("\n", "<br>");
        Contenido = Contenido.replaceAll("\r", "");
        return "onMouseOver=" + '"' + "showOverLib('<center>" + Titulo + "</center>','" + Contenido + "', '" + fg + "', '" + bg + "');" + '"' + " OnMouseOut=" + '"' + "return nd();" + '"';
    }

    public static String getFechaActual() {
        return verFecha(new java.util.Date(), FORMATO_YDM);
    }

    public static String getHoraActual() {
        return verFecha(new java.util.Date(), FORMATO_HMS);
    }

    public static String verFecha(Date fecha, String formato) {
        SimpleDateFormat f = new SimpleDateFormat(formato);
        if (fecha != null) {
            return f.format(fecha);
        } else {
            return "";
        }
    }

    public static String verFecha(String fecha) {
        if (fecha == null) {
            return "";
        } else if (fecha.equals("")) {
            return fecha;
        } else {
            String ano = fecha.substring(0, 4);
            String mes = fecha.substring(5, 7);
            String dia = fecha.substring(8, 10);
            return dia + "-" + mes + "-" + ano;
        }
    }

    public static String verFecha(Timestamp fecha) {
        if (fecha == null) {
            return "";
        } else if (fecha.equals("")) {
            return "";
        } else {
            String fechaSeteada = fecha.toString();
            String ano = fechaSeteada.substring(0, 4);
            String mes = fechaSeteada.substring(4, 6);
            String dia = fechaSeteada.substring(6, 8);
            return dia + "/" + mes + "/" + ano;
        }
    }

    public static String verHtmlNull(String dato, int largo) {
        String tmp = verHtmlNull(dato);
        if (tmp.length() > largo) {
            tmp = tmp.substring(0, (largo - 3)) + "...";
        }
        return tmp;
    }

    public static String verHtmlNull(String dato) {
        if (dato != null) {
            if (dato.trim().equals("")) {
                return "&nbsp;";
            } else {
                return dato.trim();
            }
        } else {
            return "&nbsp;";
        }
    }

    public static String verValorHtml(double valor) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(4);
        nf.setMinimumFractionDigits(0);
        return nf.format(valor);
    }

    public static void addError(Exception ex) {
        addError(ex, false);
    }

    public static void addError(Exception ex, boolean conTraza) {
        String Err = "";
        if (conTraza) {
            StackTraceElement[] nn = ex.getStackTrace();
            for (int i = 0; i < nn.length; i++) {
                Err += nn[i].toString() + " ";
            }
        }
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(FILE_ERROR, true);
            pw = new PrintWriter(fichero);
            pw.println(getFechaActual() + " " + getHoraActual());
            pw.println(ex.toString());
            pw.println(Err);
            pw.println("");
        } catch (Exception e) {
        } finally {
            if (null != fichero) {
                try {
                    fichero.close();
                } catch (Exception e1) {
                }
            }
            try {
            } catch (Exception e2) {
            }
        }
    }

    public static Properties loadProperties(String resourceName, Class cl) {
        Properties properties = new Properties();
        ClassLoader loader = cl.getClassLoader();
        try {
            InputStream in = loader.getResourceAsStream(resourceName);
            if (in != null) {
                properties.load(in);
            }

        } catch (IOException ex) {
            addError(ex, true);
        }
        return properties;
    }

    public static String encriptarString(String Text) {
        Key key = retornarLlave3DES();
        String cipherText = encrypt3DES(Text, key);
        return cipherText;
    }

    public static Key retornarLlave3DES() {
        byte[] secret = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
        Key key = null;
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
            key = skf.generateSecret(new DESedeKeySpec(secret));
        } catch (NoSuchAlgorithmException ex) {
            addError(ex, true);
        } catch (InvalidKeyException ex) {
            addError(ex, true);
        } catch (InvalidKeySpecException ex) {
            addError(ex, true);
        }
        return key;
    }

    public static String encrypt3DES(String clearText, Key key) {
        String cipherTextB64 = "";
        try {
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte cipherText[] = cipher.doFinal(clearText.getBytes());
            BASE64Encoder base64encoder = new BASE64Encoder();
            cipherTextB64 = base64encoder.encode(cipherText);

        } catch (NoSuchAlgorithmException ex) {
            addError(ex, true);
        } catch (InvalidKeyException ex) {
            addError(ex, true);
        } catch (NoSuchPaddingException ex) {
            addError(ex, true);
        } catch (IllegalBlockSizeException ex) {
            addError(ex, true);
        } catch (BadPaddingException ex) {
            addError(ex, true);
        }
        return cipherTextB64;
    }

    public static ArrayList menu(ArrayList menuUsuario, ArrayList menuGeneral) {
        for (int i = 0; i < menuGeneral.size(); i++) {
            ModMenu menuGene = (ModMenu) menuGeneral.get(i);
            boolean est = false;
            for (int a = 0; a < menuUsuario.size(); a++) {
                ModMenu menuUser = (ModMenu) menuUsuario.get(a);
                if (menuGene.getNombre().equals(menuUser.getNombre())) {
                    menuGene.setUrl(menuUser.getUrl());
                    est = true;
                }
            }
            if (!est) {
                menuGene.setUrl("#");
            }
            menuGeneral.set(i, menuGene);
        }
        return menuGeneral;
    }

    public static boolean permisosSeccion(String permiso, ModSesion Sesion) {
        boolean estado = false;
        try {
            for (int i = 0; i < Sesion.getUsuario().getPermisosSeccion().size(); i++) {
                ModPermisoSeccion permisoSeccion = (ModPermisoSeccion) Sesion.getUsuario().getPermisosSeccion().get(i);
                if (permisoSeccion.getPermiso().equals(permiso)) {
                    estado = true;
                    break;
                }
                permisoSeccion = null;
            }
        } catch (Exception ex) {
            addError(ex, true);
        }
        return estado;
    }
    
    public static String subFamilia(ArrayList subFamilias){
        String val = "";
        for(int i=0;i<subFamilias.size();i++){
            ModMaterial mat = (ModMaterial)subFamilias.get(i);
            if(mat.getChecked().equals("selected")){
               val = mat.getMateriaPrima(); 
            }
        }
        return val;
        
    }
}
